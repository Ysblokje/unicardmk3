#include "interrupt.h"

#define K_EXTEND	1
#define	K_RELEASE	2
#define	K_RESET		4
#define	K_START		8
#define	K_START2	16
#define	K_BITS		32

/*static*/ char kb_state;

unsigned char znak=0;
static unsigned char parita=0;
static char bitnum=0;
static int time=0;

int keyb_timeout;
char mouse;

extern __IO uint32_t LocalTime;	//main.c

#define IRC(a)	((((a)&2)?(a)^1:(a))&0x03)
//const char IRC[4]={0,1,3,2};
char mouse_x_count;
char mouse_y_count;
char mouse_x_final;
char mouse_y_final;
static char escape_count;

void keyb_init() {
	kb_state=0;
	bitnum=0;
	keyb_timeout=0;
	mouse=0;
	mouse_x_count=0;
	mouse_y_count=0;
	mouse_x_final=0;
	mouse_y_final=0;
}

__INLINE void put_byte() {
/*	if ((znak>>4)>9) GDC_text[400]=((znak>>4)&0x0F)-9;
	else GDC_text[400]=((znak>>4)&0x0F)|0x20;
	if ((znak&15)>9) GDC_text[401]=((znak)&0x0F)-9;
	else GDC_text[401]=((znak)&0x0F)|0x20;
*/
	if (mouse) {
		if (znak==0xAA) escape_count++;
		else escape_count=0;
		if (escape_count>=3) {
			mouse=0;
			return;
		}
		switch (mouse) {
			case 1:
				if (znak&0x08) {
					ATARI_MOUSE=(ATARI_MOUSE&0xCF)|(((znak^3)&3)<<4);
					mouse=2;
				}
				break;
			case 2:
				mouse_x_final+=znak;
//				ATARI_MOUSE=(ATARI_MOUSE&0xFC)|IRC[(x_count>>2)&3];
				mouse=3;
				break;
			case 3:
				mouse_y_final+=znak;
//				ATARI_MOUSE=(ATARI_MOUSE&0xF3)|(IRC[(y_count>>2)&3]<<2);
				mouse=1;
				break;
			default:
				mouse=1;
				break;
		}
	} else switch (znak) {
//		case 0xF0:	kb_state|=K_RELEASE;break;
//		case 0xE0:	kb_state|=K_EXTEND;break;
		case 0xAA:	kb_state=K_RESET;/*FA_enabled=1;*/keyb_timeout=200000;/*MZ_BORDER++;*/mouse=0;break;
		case 0x00:	/*if (kb_state==K_RESET)*/ { mouse=3;escape_count=0;break; }
		case 0xFA:	break;	//ACK
		default:
//			switch (kb_state) {
//				case 0:	set_key();/*LPC_GPIO1->FIOCLR ^= (1<<10);*/break;
//				case K_RELEASE:	reset_key();/*LPC_GPIO1->FIOSET ^= (1<<10);*/break;
//				case K_EXTEND:	set_extend();/*LPC_GPIO1->FIOCLR ^= (1<<10);*/break;
//				case K_RELEASE|K_EXTEND:	reset_extend();/*LPC_GPIO1->FIOSET ^= (1<<10);*/break;
//			}
/*			int i;
			for(i=0;i<10;i++) {
				if (((keyb_tab[i]>>4)&0x0F)>9) GDC_text[440+i*3]=((keyb_tab[i]>>4)&0x0F)-9;
				else GDC_text[440+i*3]=((keyb_tab[i]>>4)&0x0F)|0x20;
				if (((keyb_tab[i])&0x0F)>9) GDC_text[441+i*3]=((keyb_tab[i])&0x0F)-9;
				else GDC_text[441+i*3]=((keyb_tab[i])&0x0F)|0x20;
			}
*/			kb_state=0;
	}
}

/*void put_bit(int keyboardbit) {	//vstup 0 nebo 0x80
	register unsigned char bit=keyboardbit;
	if (!bitnum) {
		if (!bit) {
			bitnum=1;
			parita=0x80;
			time=LocalTime;
		}
	} else {
		if (LocalTime-time>=100) {
			bitnum=0;
			if (!bit) {
				bitnum=1;
				parita=0x80;
				time=LocalTime+1;
			}
			return;
		}
		bitnum++;
		if (bitnum<=9) {
			znak=(znak>>1)|bit;
			parita=parita^bit;
		} else if (bitnum==10) {
			if (parita!=bit) {
				//TODO ajaj chyba parity
				bitnum=0;
			}
		} else if (bitnum==11) {
			if (!bit) {
				//TODO ajaj chyba stopbitu
			} else {
				put_byte();	//znak
			}
			bitnum=0;
		}
	}
}*/

void put_timeout_vga() {
	switch (kb_state) {
		case K_RESET:
			keyb_init();
			kb_state=K_RESET|K_START;
			keyb_timeout=500;	//500us
			GPIOD->BSRRH=(1<<0);	//PS/2 CLK low
			break;
		case K_RESET|K_START:
			keyb_timeout=50;	//50us
			kb_state|=K_START2;
			GPIOC->BSRRH=(1<<13);	//PS/2 DATA low
			break;
		case K_RESET|K_START|K_START2:
			keyb_timeout=15000;	//15ms
//			vga_key=0;
			bitnum=0;
			kb_state=K_RESET|K_BITS;
			GPIOD->BSRRL=(1<<0);	//PS/2 CLK hi
			break;
		case K_RESET|K_BITS:
			GPIOC->BSRRL=(1<<13);	//PS/2 DATA hi
			bitnum=0;
			kb_state=0;
			break;
		default:
			break;
	}
}

void put_bit(int keyboardbit) {	//vstup 0 nebo 0x80
	register unsigned char bit=keyboardbit;
	//zapis
	if (kb_state&K_RESET) {
		if (kb_state==(K_RESET|K_BITS)) {
			if ((/*(FA_enabled?0xFFED:0xFE02)*/0xFEF4>>bitnum)&1) {
				GPIOC->BSRRL=(1<<13);	//PS/2 DATA hi
			} else {
				GPIOC->BSRRH=(1<<13);	//PS/2 DATA low
			}
			bitnum++;
			if (bitnum>9) {
				bitnum=0;
				kb_state=0;
				keyb_timeout=0;
/*				if (FA_enabled) {
					state=K_RESET;FA_enabled=0;keyb_timeout=1000;
				}*/
			}
		}
		return;
	}
	//cteni
	if (!bitnum) {
		if (!bit) {
			bitnum=1;
			parita=0x80;
			time=LocalTime;
		}
	} else {
		if (LocalTime-time>=15/*100000*/) {
			bitnum=0;
			if (!bit) {
				bitnum=1;
				parita=0x80;
				time=LocalTime/*+100*/;
			}
			return;
		}
		bitnum++;
		if (bitnum<=9) {
			znak=(znak>>1)|bit;
			parita=parita^bit;
		} else if (bitnum==10) {
			if (parita!=bit) {
				//TODO ajaj chyba parity
				bitnum=0;
			}
		} else if (bitnum==11) {
			if (!bit) {
				//TODO ajaj chyba stopbitu
			} else {
//				unsigned int len;
//				f_write(&keyb_file,&znak,1,&len);
//				f_sync(&keyb_file);
				put_byte();	//znak
			}
			bitnum=0;
		}
	}
}

void EXTI0_IRQHandler()
{
	//put_bit(res);
	put_bit(((GPIOC->IDR)&(1<<13))?0x80:0x00);//res;
	EXTI->PR = 0x0001;
}

