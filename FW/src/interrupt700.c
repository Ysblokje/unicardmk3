/*
 *    Copyright (c) 2012 by Bohumil Novacek <http://dzi.n.cz/8bit/>
 *
 *    Thies program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    MZ-800 Unicard is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MZ-800 Unicard; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "interrupt.h"
#include "QD.h"
#include "pal.h"
#include "emu_MZFREPO.h"
#include "emu_RAMDISC.h"
#include "emu_SIO.h"
#include "event.h"
#include "mzint.h"
#include "emu_FDC.h"

//#include "monitor.h"

//#define EXTIPRFC00	EXTI->PR = 0xFC00;
#define EXTIPRFC00	{}

volatile const char vtor_new[512];

unsigned char VRAM[0x8000];		//umisteni kopie 32KB VRAM
//unsigned char ZX_VRAM[0x1B00];	//umisteni kopie 8KB VRAM ZX Spectra (4000h-5FFFh)
unsigned long zx48tab[0x800];	//4*0x800 bytes - tabulka pro urychleni vykreslovani ZX video pameti

#ifdef MZ700_ONLY
static unsigned char SYreg;
static unsigned short SYaddr;
unsigned char SYdata[0x0800];
#endif

unsigned short PSG_noise;
short PSG_volume[4];
unsigned short PSG_period[4];
unsigned short PSG_last[4];
static byte LAST_F2;

static int PSG_VOLUME[16]={
	5461,	4338,	3445,	2737,
	2174,	1727,	1372,	1090,
	865,	687,	546,	434,
	345,	274,	217,	0
};

#define psg_set_volume(a,b)		{if (PSG_volume[a]>0) PSG_volume[a]=PSG_VOLUME[b];else PSG_volume[a]=-PSG_VOLUME[b];}
#define psg_set_period(a,b)		PSG_period[a]=b

int8_t (*reload_FDD)( uint8_t drive_id );

#ifdef Z80_M1_SPY
volatile unsigned short AddrM1;
#endif

byte MZ_mode;
byte MZ_graphic_mode;
word SCROLL_SSA;
word SCROLL_SEA;
word SCROLL_SW;
word SCROLL_SOF;

volatile int	QD_event_addr;
volatile int	QD_event_data;

volatile int	FDD_event_addr;
volatile int	FDD_event_data;

volatile int	RAM_event_addr;
volatile int	RAM_event_data;

volatile int	SIO_event_addr;
volatile int	SIO_event_data;

static unsigned short ROMON;

static byte WRITE_CMD;
static byte WRITE_PLANE1;
static byte WRITE_PLANE2;
static byte WRITE_PLANE3;
static byte WRITE_PLANE4;
static byte WRITE_FRAME;

byte MZ_BORDER;
byte ZX_EMUL;

void GDG_init(void);

#include <string.h>

void interrupt_init()
{
/*	memcpy((void *)0x20020000,(const void *)0x08000000,512);
	SCB->VTOR=0x20020000;*/
	SYSCFG->EXTICR[2]=(3<<12)|3;								//pin PD11,PD8 = reset
	SYSCFG->EXTICR[3]=(3<<12)|(3<<8)|(3<<4)|3;	//pin PD15,PD14,PD13,PD12
	{
		NVIC_InitTypeDef NVIC_InitStructure;

		/* Configure the NVIC Preemption Priority Bits */
//		NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

		NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);

		NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
	}
	GDG_init();
#ifdef Z80_M1_SPY
	EXTI->FTSR=(1<<15)|(1<<14)|(1<<12);	//M1,WR,IORQ
	EXTI->RTSR=(1<<8)/*|(1<<13)*/;	//reset, RD nikoliv
	EXTI->IMR=(1<<8)|(1<<15)|(1<<14)/*|(1<<13)*/|(1<<12);
#else
#ifdef MZ700_ONLY
	EXTI->FTSR=(1<<14)|(1<<12)|(1<<13)|(1<<15);	//WR,IORQ,RD,M1
	EXTI->RTSR=(1<<8);					//reset
	EXTI->IMR=(1<<8)|(1<<14)|(1<<12)|(1<<13)|(1<<15);
#else
	EXTI->FTSR=(1<<14)|(1<<12);	//WR,IORQ
	EXTI->RTSR=(1<<8);					//reset
	EXTI->IMR=(1<<8)|(1<<14)|(1<<12);
#endif
#endif
}

void NoWr(register word Addr, register byte Value) {}

#define ram_write	NoWr

#define WrRAM_0xxx	ram_write
#define WrRAM_1xxx	ram_write
#define WrRAM_2xxx	ram_write
#define WrRAM_3xxx	ram_write
#define WrRAM_4xxx	ram_write
#define WrRAM_5xxx	ram_write
#define WrRAM_6xxx	ram_write
#define WrRAM_7xxx	ram_write
#define WrRAM_8xxx	ram_write
#define WrRAM_9xxx	ram_write
#define WrRAM_Axxx	ram_write
#define WrRAM_Bxxx	ram_write
#define WrRAM_Cxxx	ram_write
#define WrRAM_Dxxx	ram_write
#define WrRAM_Exxx	ram_write
#define WrRAM_Fxxx	ram_write

void WrZX_VRAM_45xx(register word Addr, register byte Value) {
//	Addr&=0x1FFF; if (Addr<0x1B00) ZX_VRAM[Addr]=Value;
}

void WrVRAM_ABxx(register word Addr, register byte Value) {
	register short offset=(Addr&1)<<13;
	Addr=((Addr>>1)&0x1FFF);
	if (SCROLL_SOF) {
		if ((Addr>=SCROLL_SSA)&&(Addr<SCROLL_SEA)) Addr=(((Addr+SCROLL_SOF-SCROLL_SSA)%(SCROLL_SW))+SCROLL_SSA)&0x1FFF;
	}
	Addr+=offset;
	switch (WRITE_CMD) {
	case 0: if (WRITE_PLANE1) VRAM[Addr]=Value;
			if (WRITE_PLANE3) VRAM[Addr+0x4000]=Value;
			break;
	case 1: if (WRITE_PLANE1) VRAM[Addr]^=Value;
			if (WRITE_PLANE3) VRAM[Addr+0x4000]^=Value;
			break;
	case 2: if (WRITE_PLANE1) VRAM[Addr]|=Value;
			if (WRITE_PLANE3) VRAM[Addr+0x4000]|=Value;
			break;
	case 3: if (WRITE_PLANE1) VRAM[Addr]&=~Value;
			if (WRITE_PLANE3) VRAM[Addr+0x4000]&=~Value;
			break;
	case 4:
	case 5:	if (WRITE_PLANE1) VRAM[Addr]=Value;else VRAM[Addr]=0;
			if (WRITE_PLANE3) VRAM[Addr+0x4000]=Value;else VRAM[Addr+0x4000]=0;
			break;
	case 6:
	case 7:	if (WRITE_PLANE1) VRAM[Addr]|=Value;else VRAM[Addr]&=~Value;
			if (WRITE_PLANE3) VRAM[Addr+0x4000]|=Value;else VRAM[Addr+0x4000]&=~Value;
			break;
	}
}
void WrVRAM_89xx(register word Addr, register byte Value) {
	if (MZ_graphic_mode&4) { WrVRAM_ABxx(Addr, Value);return; }
	Addr&=0x1FFF;
	if (SCROLL_SOF) {
		if ((Addr>=SCROLL_SSA)&&(Addr<SCROLL_SEA)) Addr=(((Addr+SCROLL_SOF-SCROLL_SSA)%SCROLL_SW)+SCROLL_SSA)&0x1FFF;
	}
	switch (WRITE_CMD) {
	case 0:
				if (WRITE_PLANE1) VRAM[Addr]=Value;
				if (WRITE_PLANE2) VRAM[Addr+0x2000]=Value;
				if (WRITE_PLANE3) VRAM[Addr+0x4000]=Value;
				if (WRITE_PLANE4) VRAM[Addr+0x6000]=Value;
			break;
	case 1:
				if (WRITE_PLANE1) VRAM[Addr]^=Value;
				if (WRITE_PLANE2) VRAM[Addr+0x2000]^=Value;
				if (WRITE_PLANE3) VRAM[Addr+0x4000]^=Value;
				if (WRITE_PLANE4) VRAM[Addr+0x6000]^=Value;
			break;
	case 2:
				if (WRITE_PLANE1) VRAM[Addr]|=Value;
				if (WRITE_PLANE2) VRAM[Addr+0x2000]|=Value;
				if (WRITE_PLANE3) VRAM[Addr+0x4000]|=Value;
				if (WRITE_PLANE4) VRAM[Addr+0x6000]|=Value;
			break;
	case 3:
				if (WRITE_PLANE1) VRAM[Addr]&=~Value;
				if (WRITE_PLANE2) VRAM[Addr+0x2000]&=~Value;
				if (WRITE_PLANE3) VRAM[Addr+0x4000]&=~Value;
				if (WRITE_PLANE4) VRAM[Addr+0x6000]&=~Value;
			break;
	case 4:
	case 5:	if (MZ_graphic_mode==2) {
				if (WRITE_PLANE1) VRAM[Addr]=Value;else VRAM[Addr]=0;
				if (WRITE_PLANE2) VRAM[Addr+0x2000]=Value;else VRAM[Addr+0x2000]=0;
				if (WRITE_PLANE3) VRAM[Addr+0x4000]=Value;else VRAM[Addr+0x4000]=0;
				if (WRITE_PLANE4) VRAM[Addr+0x6000]=Value;else VRAM[Addr+0x6000]=0;
			} else {
				if (WRITE_FRAME) {
					if (WRITE_PLANE3) VRAM[Addr+0x4000]=Value;else VRAM[Addr+0x4000]=0;
					if (WRITE_PLANE4) VRAM[Addr+0x6000]=Value;else VRAM[Addr+0x6000]=0;
				} else {
					if (WRITE_PLANE1) VRAM[Addr]=Value;else VRAM[Addr]=0;
					if (WRITE_PLANE2) VRAM[Addr+0x2000]=Value;else VRAM[Addr+0x2000]=0;
				}
			}
			break;
	case 6:
	case 7:	if (MZ_graphic_mode==2) {
				if (WRITE_PLANE1) VRAM[Addr]|=Value;else VRAM[Addr]&=~Value;
				if (WRITE_PLANE2) VRAM[Addr+0x2000]|=Value;else VRAM[Addr+0x2000]&=~Value;
				if (WRITE_PLANE3) VRAM[Addr+0x4000]|=Value;else VRAM[Addr+0x4000]&=~Value;
				if (WRITE_PLANE4) VRAM[Addr+0x6000]|=Value;else VRAM[Addr+0x6000]&=~Value;
			} else {
				if (WRITE_FRAME) {
					if (WRITE_PLANE3) VRAM[Addr+0x4000]|=Value;else VRAM[Addr+0x4000]&=~Value;
					if (WRITE_PLANE4) VRAM[Addr+0x6000]|=Value;else VRAM[Addr+0x6000]&=~Value;
				} else {
					if (WRITE_PLANE1) VRAM[Addr]|=Value;else VRAM[Addr]&=~Value;
					if (WRITE_PLANE2) VRAM[Addr+0x2000]|=Value;else VRAM[Addr+0x2000]&=~Value;
				}
			}
			break;
	}
}
void WrVRAM_Cxxx(register word Addr, register byte Value) { VRAM[(Addr|0x0000)&0x1FFF]=Value; }
#ifdef MZ700_ONLY
void WrVRAM_Dxxx(register word Addr, register byte Value) { VRAM[(Addr|0x0000)&0x1FFF]=Value; 
//	xprintf("%04X=%02X\r\n",Addr,Value);
	if (MZ_mode==MZ_80c) MZ_mode=MZ_700; }
#else
void WrVRAM_Dxxx(register word Addr, register byte Value) { VRAM[(Addr|0x0000)&0x1FFF]=Value; }
#endif

P_WRITE	WriteM[2][16];
P_WRITE	Write[16];

void set_mode(int m) {
	MZ_mode = m;
#ifdef MZ700_ONLY
	Write[13]=WriteM[MZ_700][13];
	Write[14]=WriteM[MZ_700][14];
	Write[15]=WriteM[MZ_700][15];
#else
	Write[8]=WriteM[MZ_mode][8];
	Write[9]=WriteM[MZ_mode][9];
	Write[10]=WriteM[MZ_mode][10];
	Write[11]=WriteM[MZ_mode][11];
	Write[12]=WriteM[MZ_mode][12];
	Write[13]=WriteM[MZ_mode][13];
#endif	
//	PALETE_UPDATE = 32;
}

#define WrROM_Exxx	NoWr

/*const */P_WRITE WrROM[2][16]={
/*700 mode*/	{NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,WrVRAM_Cxxx,WrVRAM_Dxxx,WrROM_Exxx,NoWr},
/*800 mode*/	{NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,WrVRAM_89xx,WrVRAM_89xx,WrVRAM_ABxx,WrVRAM_ABxx,NoWr,NoWr,NoWr,NoWr}
};

/*const */P_WRITE WrRAM[2][16]={
/*700 mode*/	{WrRAM_0xxx,WrRAM_1xxx,WrRAM_2xxx,WrRAM_3xxx,WrRAM_4xxx,WrRAM_5xxx,WrRAM_6xxx,WrRAM_7xxx,WrRAM_8xxx,WrRAM_9xxx,WrRAM_Axxx,WrRAM_Bxxx,WrRAM_Cxxx,WrRAM_Dxxx,WrRAM_Exxx,WrRAM_Fxxx},
/*800 mode*/	{WrRAM_0xxx,WrRAM_1xxx,WrRAM_2xxx,WrRAM_3xxx,WrRAM_4xxx,WrRAM_5xxx,WrRAM_6xxx,WrRAM_7xxx,WrRAM_8xxx,WrRAM_9xxx,WrRAM_Axxx,WrRAM_Bxxx,WrRAM_Cxxx,WrRAM_Dxxx,WrRAM_Exxx,WrRAM_Fxxx}
};

extern void tab800_init_palete(void);

void noport(register unsigned char Port) {
	EXWAIT_OFF();
  EXTIPRFC00
}

void iowrEMU(register unsigned char Port) {
	EMU_data=GET_MZDATA();
	EMU_addr=Port&3;
	if ((EMU_addr==0)&&(EMU_data==0x03)) {	//cmdSTSR lze rovnou, bez cekani na uvolneni main loopu
		emu_MZFREPO_write(EMU_addr,(unsigned int *)&EMU_data);
		EXWAIT_OFF();
		EXTIPRFC00
		return;
	}
  set_event(EVENT_EMU_WRITE);
	EXTIPRFC00
}

void iowrFDD(register unsigned char Port) {
  FDD_event_addr=Port&7;
  FDD_event_data=GET_MZDATA();
  set_event(EVENT_FDD_WRITE);
	EXTIPRFC00
}

void iowrSIO(register unsigned char Port) {
	SIO_event_data=GET_MZDATA();
	SIO_event_addr=Port&3;
  set_event(EVENT_SIO_WRITE);
	EXTIPRFC00
}

void iowrCC(register unsigned char Port) {
	switch (Port) {
		case 0xCC:	{
						register unsigned char Value=GET_MZDATA();
						WRITE_CMD=(Value>>5);
						WRITE_FRAME=(Value>>4)&1;
						WRITE_PLANE1=(Value&1);
						WRITE_PLANE2=(Value&2);
						WRITE_PLANE3=(Value&4);
						WRITE_PLANE4=(Value&8);
					}
					break;
		case 0xCE:	{
#ifdef MZ700_ONLY
//							xprintf("(CE)=%02X\r\n",GET_MZDATA());
							if (GET_MZDATA()&4) {
								MZ_mode=MZ_SUC;
							} else {
								MZ_mode=MZ_700;
							}
#else
						register unsigned char Value=GET_MZDATA();SCROLL_SOF = 0;// SCROLL_SW = 0; SCROLL_SSA = 0; SCROLL_SEA = 125<<6; SCROLL_SW = 125<<6;
						set_mode((Value&0x08)?MZ_700:MZ_800);MZ_graphic_mode=Value;
						tab800_init_palete();
#endif						
					}
					break;
		case 0xCF:
				{
					register unsigned char Value=GET_MZDATA();
						register unsigned char Addr=GET_MZADDR()>>8;
/*						if ((Value&0xFF)>=0xA0) VRAM[0x1026]=((Value&0xFF)>>4)-9; else VRAM[0x1026]=((Value&0xFF)>>4)+0x20;
						if ((Value&0xF)>=0xA) VRAM[0x1027]=(Value&0xF)-9; else VRAM[0x1027]=(Value&0xF)+0x20;
						if ((Addr&0xFF)>=0xA0) VRAM[0x1023]=((Addr&0xFF)>>4)-9; else VRAM[0x1023]=((Addr&0xFF)>>4)+0x20;
						if ((Addr&0xF)>=0xA) VRAM[0x1024]=(Addr&0xF)-9; else VRAM[0x1024]=(Addr&0xF)+0x20;*/
					switch (GET_MZADDR()>>8) {
						case 1:	SCROLL_SOF = (SCROLL_SOF&0x1800)|(((unsigned short)Value)<<3);break;
						case 2:	SCROLL_SOF = (SCROLL_SOF&0x07F8)|(((unsigned short)(Value&0x03))<<11);break;
						case 3:	SCROLL_SW = (((unsigned short)(Value&0x7F))<<6);break;
						case 4:	SCROLL_SSA = (((unsigned short)(Value&0x7F))<<6);break;
						case 5:	SCROLL_SEA = (((unsigned short)(Value&0x7F))<<6);break;
						case 6:	MZ_BORDER = Value&0x0F; break;	//border MZ800
					}
				}
				break;
	}
	EXWAIT_OFF();
	EXTIPRFC00
}

void iowrE0(register unsigned char Port) {
	switch (Port) {
		case 0xE1:
				ROMON=0xFFFF;
				WriteM[MZ_700][13]=WrRAM[MZ_700][13];
				set_mode(MZ_mode);
				break;
		case 0xE3:
				ROMON=0xE00F;
				WriteM[MZ_700][13]=WrROM[MZ_700][13];
				set_mode(MZ_mode);
				break;
	}
	EXWAIT_OFF();
	EXTIPRFC00
}

void iowrE4(register unsigned char Port) {
	switch (Port) {
		case 0xE4:
				WriteM[MZ_800][8]=WrROM[MZ_800][8];
				WriteM[MZ_800][9]=WrROM[MZ_800][9];
				WriteM[MZ_800][10]=WrRAM[MZ_800][10];
				WriteM[MZ_800][11]=WrRAM[MZ_800][11];
				WriteM[MZ_800][12]=WrRAM[MZ_800][12];
				WriteM[MZ_800][13]=WrRAM[MZ_800][13];
				WriteM[MZ_700][8]=WrRAM[MZ_700][8];
				WriteM[MZ_700][9]=WrRAM[MZ_700][9];
				WriteM[MZ_700][10]=WrRAM[MZ_700][10];
				WriteM[MZ_700][11]=WrRAM[MZ_700][11];
				WriteM[MZ_700][12]=WrRAM[MZ_700][12];
				WriteM[MZ_700][13]=WrROM[MZ_700][13];
#ifdef MZ700_ONLY
				ROMON=0xE00F;
				WriteM[MZ_700][14]=WrRAM[MZ_700][14];
				WriteM[MZ_700][15]=WrRAM[MZ_700][15];
				set_mode(MZ_mode);
				break;
		case 0xE5:	//TODO prohibited
				WriteM[MZ_700][14]=WrROM[MZ_700][14];
				WriteM[MZ_700][15]=WrROM[MZ_700][15];
//				Write[15]=WrROM[MZ_700][13];
				break;
		case 0xE6:
				WriteM[MZ_700][14]=WrRAM[MZ_700][14];
				WriteM[MZ_700][15]=WrRAM[MZ_700][15];
//				Write[15]=WrRAM[MZ_700][13];
#endif				
				set_mode(MZ_mode);
				break;
	}
	EXWAIT_OFF();
	EXTIPRFC00
}

void iowrF0(register unsigned char Port) {
	switch (Port) {
		case 0xF0:  {
						register unsigned char Value=GET_MZDATA();
						tab800_set_palete(Value>>4,Value&15);
					//puts_shell("F0=");puthex(Value,4);puts_shell("\r\n");
					}
					break;

		case 0xF2:	{	//PSG
						register unsigned char Value=GET_MZDATA();
						if (Value&0x80) {
							if (Value&0x10) psg_set_volume((Value>>5)&0x3,Value&0xF);
							LAST_F2 = Value;
							if ((Value&0x70)==0x60) psg_set_period(3,(Value&0x0F));
						} else {
							if (LAST_F2&0x10) {
								psg_set_volume((LAST_F2>>5)&0x3,Value&0xF);
							}
							else psg_set_period((LAST_F2>>5)&0x3,((Value<<4)|(LAST_F2&0xF))&0x3FF);
						}
					}
					break;
	}
	EXWAIT_OFF();
	EXTIPRFC00
}

void iowrE8(register unsigned char Port) {
	switch (Port) {
		case	0xE9:
		case	0xEA:
		case	0xEB:
		  RAM_event_addr=GET_MZADDR();	//B.N. 20.3.2012 - xxEB port
		  RAM_event_data=GET_MZDATA();
		  set_event(EVENT_RAM_WRITE);
			break;
		default:
			EXWAIT_OFF();
			break;
	}
	EXTIPRFC00
}

void iowrF8(register unsigned char Port) {
	switch (Port) {
		case	0xF8:
		case	0xFA:
		  RAM_event_addr=Port;
		  RAM_event_data=GET_MZDATA();
		  set_event(EVENT_RAM_WRITE);
			break;
		default:
			EXWAIT_OFF();
			break;
	}
	EXTIPRFC00
}

void iowrZX(register unsigned char Port) {
	if ((GET_MZADDR()==(((int)'Z')<<8))&&(GET_MZDATA()=='X')) {
		ZX_EMUL=1;
	} else {
		ZX_EMUL=0;
	}
	EXWAIT_OFF();
	EXTIPRFC00
}

void iowrQD(register unsigned char Port) {
  QD_event_addr=Port&3;
  QD_event_data=GET_MZDATA();
  set_event(EVENT_QD_WRITE);
	EXTIPRFC00
}

void iowrSY(register unsigned char Port) {
	switch (Port) {
		case	0x71:
		  SYdata[SYaddr&0x07FF]=GET_MZDATA();
			SYaddr++;
			EXWAIT_OFF();
			break;
		case	0x72:
		  SYreg=GET_MZDATA();
			MZ_mode=MZ_80c;
			EXWAIT_OFF();
			break;
		case	0x73:
		  KP80[SYreg&0x1F]=GET_MZDATA();
			//if ((SYreg&0x1F)==31) SYaddr=(KP80[18]<<8)|KP80[19];
			if (((SYreg&0x1F)==18)||((SYreg&0x1F)==19)) SYaddr=(KP80[18]<<8)|KP80[19];
			EXWAIT_OFF();
			break;
		default:
			EXWAIT_OFF();
			break;
	}
	EXTIPRFC00
}

void iordE0(register unsigned char Port) {
	switch (Port) {
		case 0xE0:
				//VRAM 8000
				WriteM[MZ_800][8]=WrROM[MZ_800][8];
				WriteM[MZ_800][9]=WrROM[MZ_800][9];
				if (MZ_graphic_mode&4) {	//640x200
					WriteM[MZ_800][10]=WrROM[MZ_800][10];
					WriteM[MZ_800][11]=WrROM[MZ_800][11];
				}
				//CG-RAM
				WriteM[MZ_700][12]=WrROM[MZ_700][12];
				set_mode(MZ_mode);
				break;

		case 0xE1:
				//VRAM 8000
				WriteM[MZ_800][8]=WrRAM[MZ_800][8];
				WriteM[MZ_800][9]=WrRAM[MZ_800][9];
				WriteM[MZ_800][10]=WrRAM[MZ_800][10];
				WriteM[MZ_800][11]=WrRAM[MZ_800][11];
				//CG-RAM
				WriteM[MZ_700][12]=WrRAM[MZ_700][12];
				set_mode(MZ_mode);
				break;
	}
	EXWAIT_OFF();
  EXTIPRFC00
}

void iordQD(register unsigned char Port) {
	//prepnout na vystup, nastavit hodnotu
  QD_event_addr=Port&3;
	set_event(EVENT_QD_READ);
  EXTIPRFC00
}

void iordFDD(register unsigned char Port) {
	switch (Port) {
		case 0x58:
		case 0x59:
		case 0x5A:
		case 0x5B:
		case 0xD8:
		case 0xD9:
		case 0xDA:
		case 0xDB:
			//prepnout na vystup, nastavit hodnotu
		  FDD_event_addr=Port&7;
			set_event(EVENT_FDD_READ);
			break;
		default:
			EXWAIT_OFF();
			break;
	}
  EXTIPRFC00
}

void iordE8(register unsigned char Port) {
	unsigned char io_data;
	switch (Port) {
		case 0xEA:
/*			if (MZDATA_BUSY) {
				RAM_event_addr=GET_MZADDR();
				set_event(EVENT_RAM_READ);
			} else*/ if (emu_RAMDISC_read_interrupt(&io_data)) {
				MZDATA_INT(io_data);
			} else {
				RAM_event_addr=GET_MZADDR();
				set_event(EVENT_RAM_READ);
			}
			break;
		case 0xEC:
			RAM_event_addr=GET_MZADDR();
			set_event(EVENT_RAM_READ);
			break;
		default:
			EXWAIT_OFF();
			break;
	}
  EXTIPRFC00
}

void iordF8(register unsigned char Port) {
	unsigned char io_data;
	switch (Port) {
		case 0xF8:
			RAM_event_addr=GET_MZADDR();
			set_event(EVENT_RAM_READ);
			break;
		case 0xF9:
/*			if (MZDATA_BUSY) {
				RAM_event_addr=GET_MZADDR();
				set_event(EVENT_RAM_READ);
			} else*/ if (emu_RAMDISC_read_interrupt(&io_data)) {
				MZDATA_INT(io_data);
			} else {
				RAM_event_addr=GET_MZADDR();
				set_event(EVENT_RAM_READ);
			}
			break;
		default:
			EXWAIT_OFF();
			break;
	}
  EXTIPRFC00
}

void iordEMU(register unsigned char Port) {
	EMU_addr=Port&3;
	set_event(EVENT_EMU_READ);
  EXTIPRFC00
}

void iordSIO(register unsigned char Port) {
	SIO_event_addr=Port&3;
	set_event(EVENT_SIO_READ);
  EXTIPRFC00
}

void iordSY(register unsigned char Port) {
	//unsigned char io_data;
	switch (Port) {
		case 0x70:
			PUT_MZDATA(SYdata[SYaddr&0x07FF]);
			//SYaddr++;
			EXWAIT_OFF();
			EXTIPRFC00
			while ((GET_MZBUS()&(1<<(13-0)))==0);
			MZDATA_OFF();
			return;
		case 0x72:
			PUT_MZDATA(0x80);
			EXWAIT_OFF();
			EXTIPRFC00
			while ((GET_MZBUS()&(1<<(13-0)))==0);
			MZDATA_OFF();
			return;
		case 0x73:
			PUT_MZDATA(KP80[SYreg&0x1F]);
			EXWAIT_OFF();
			EXTIPRFC00
			while ((GET_MZBUS()&(1<<(13-0)))==0);
			MZDATA_OFF();
			return;
		default:
			EXWAIT_OFF();
			break;
	}
  EXTIPRFC00
}

static void * IOWREnabledLONG[(256/4)+2+(256/4)]=
{
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)(&GET_MZADDR()),
	(void *)(&(GPIOC->BSRRL)),

	iowrZX,	/* 00-03 */
	noport,	/* 04-07 */
	noport,	/* 08-0B */
	noport,	/* 0C-0F */

	noport,	/* 10-13 */
	noport,	/* 14-17 */
	noport,	/* 18-1B */
	noport,	/* 1C-1F */

	noport,	/* 20-23 */
	noport,	/* 24-27 */
	noport,	/* 28-2B */
	noport,	/* 2C-2F */

	noport,	/* 30-33 */
	noport,	/* 34-37 */
	noport,	/* 38-3B */
	noport,	/* 3C-3F */

	noport,	/* 40-43 */
	noport,	/* 44-47 */
	noport,	/* 48-4B */
	noport,	/* 4C-4F */

	iowrEMU,	/* 50-53 */
	noport,	/* 54-57 */
	iowrFDD,	/* 58-5B */
	iowrFDD,	/* 5C-5F */

	noport,	/* 60-63 */
	noport,	/* 64-67 */
	noport,	/* 68-6B */
	noport,	/* 6C-6F */

	iowrSY,	/* 70-73 */
	noport,	/* 74-77 */
	noport,	/* 78-7B */
	noport,	/* 7C-7F */

	noport,	/* 80-83 */
	noport,	/* 84-87 */
	noport,	/* 88-8B */
	noport,	/* 8C-8F */

	noport,	/* 90-93 */
	noport,	/* 94-97 */
	noport,	/* 98-9B */
	noport,	/* 9C-9F */

	noport,	/* A0-A3 */
	noport,	/* A4-A7 */
	noport,	/* A8-AB */
	noport,	/* AC-AF */

	iowrSIO,	/* B0-B3 */
	noport,	/* B4-B7 */
	noport,	/* B8-BB */
	noport,	/* BC-BF */

	noport,	/* C0-C3 */
	noport,	/* C4-C7 */
	noport,	/* C8-CB */
	iowrCC,	/* CC-CF */

	noport,	/* D0-D3 */
	noport,	/* D4-D7 */
	iowrFDD,	/* D8-DB */
	iowrFDD,	/* DC-DF */

	iowrE0,	/* E0-E3 */
	iowrE4,	/* E4-E7 */
	iowrE8,	/* E8-EB */
	noport,	/* EC-EF */

	iowrF0,	/* F0-F3 */
	iowrQD,	/* F4-F7 */
	iowrF8,	/* F8-FB */
	noport	/* FC-FF */

};
static void * IORDEnabledLONG[(256/4)+2+(256/4)]=
{
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)(&GET_MZADDR()),
	(void *)(&(GPIOC->BSRRL)),

	noport,	/* 00-03 */
	noport,	/* 04-07 */
	noport,	/* 08-0B */
	noport,	/* 0C-0F */

	noport,	/* 10-13 */
	noport,	/* 14-17 */
	noport,	/* 18-1B */
	noport,	/* 1C-1F */

	noport,	/* 20-23 */
	noport,	/* 24-27 */
	noport,	/* 28-2B */
	noport,	/* 2C-2F */

	noport,	/* 30-33 */
	noport,	/* 34-37 */
	noport,	/* 38-3B */
	noport,	/* 3C-3F */

	noport,	/* 40-43 */
	noport,	/* 44-47 */
	noport,	/* 48-4B */
	noport,	/* 4C-4F */

	iordEMU,	/* 50-53 */
	noport,	/* 54-57 */
	iordFDD,	/* 58-5B */
	iordFDD,	/* 5C-5F */

	noport,	/* 60-63 */
	noport,	/* 64-67 */
	noport,	/* 68-6B */
	noport,	/* 6C-6F */

	iordSY,	/* 70-73 */
	noport,	/* 74-77 */
	noport,	/* 78-7B */
	noport,	/* 7C-7F */

	noport,	/* 80-83 */
	noport,	/* 84-87 */
	noport,	/* 88-8B */
	noport,	/* 8C-8F */

	noport,	/* 90-93 */
	noport,	/* 94-97 */
	noport,	/* 98-9B */
	noport,	/* 9C-9F */

	noport,	/* A0-A3 */
	noport,	/* A4-A7 */
	noport,	/* A8-AB */
	noport,	/* AC-AF */

	iordSIO,	/* B0-B3 */
	noport,	/* B4-B7 */
	noport,	/* B8-BB */
	noport,	/* BC-BF */

	noport,	/* C0-C3 */
	noport,	/* C4-C7 */
	noport,	/* C8-CB */
	noport,	/* CC-CF */

	noport,	/* D0-D3 */
	noport,	/* D4-D7 */
	iordFDD,	/* D8-DB */
	iordFDD,	/* DC-DF */

	iordE0,	/* E0-E3 */
	noport,	/* E4-E7 */
	iordE8,	/* E8-EB */
	noport,	/* EC-EF */

	noport,	/* F0-F3 */
	iordQD,	/* F4-F7 */
	iordF8,	/* F8-FB */
	noport	/* FC-FF */

};

#define IOWREnabled 	((unsigned char *)IOWREnabledLONG)
#define IORDEnabled 	((unsigned char *)IORDEnabledLONG)

void IO_tabs_init(void) {
	int i;
	for (i=0;i<256;i++)	{
		IOWREnabled[i]=1;
		IORDEnabled[i]=1;
	}
}

#undef EXTIPRFC00
#define EXTIPRFC00	EXTI->PR = 0xFC00;

typedef void (*fooport)(register unsigned char Port);

/*__forceinline */void noact(void) {
//	EXWAIT_OFF();
  EXTIPRFC00
	return;
}

static int xxxl;
static int xxxpos;

void iowr(void) {
	register unsigned char Port;
//	register unsigned char Value;
	EXWAIT_ON();
	Port=*((unsigned char *)IOWREnabledLONG[64]);		//register unsigned char Port=GET_MZADDR();
	EXTIPRFC00
/*
	Value=GET_MZDATA();
	xxxl++;if ((xxxl<0)||(xxxl>24)) xxxl=0;
	xxxpos=xxxl*40+0x1022;
	if ((Port&0xFF)>=0xA0) VRAM[xxxpos]=((Port&0xFF)>>4)-9; else VRAM[xxxpos]=((Port&0xFF)>>4)+0x20;
	xxxpos++;
	if ((Port&0xF)>=0xA) VRAM[xxxpos]=(Port&0xF)-9; else VRAM[xxxpos]=(Port&0xF)+0x20;
	xxxpos+=2;
	if ((Value&0xFF)>=0xA0) VRAM[xxxpos]=((Value&0xFF)>>4)-9; else VRAM[xxxpos]=((Value&0xFF)>>4)+0x20;
	xxxpos++;
	if ((Value&0xF)>=0xA) VRAM[xxxpos]=(Value&0xF)-9; else VRAM[xxxpos]=(Value&0xF)+0x20;*/

	if ((*((unsigned short *)IOWREnabledLONG[65])=IOWREnabled[Port])!=0) {	//if ((GPIOC->BSRRL=IOWREnabled[Port])!=0) {	//zaroven provede EXWAIT_OFF
		//EXTIPRFC00
		return;
	}
	((fooport)IOWREnabledLONG[66+(Port>>2)])(Port);
}

void iord(void) {
	register unsigned char Port;
	EXWAIT_ON();
	EXTIPRFC00
	Port=*((unsigned char *)IORDEnabledLONG[64]);		//register unsigned char Port=GET_MZADDR();
	if ((*((unsigned short *)IORDEnabledLONG[65])=IORDEnabled[Port])!=0) {	//if ((GPIOC->BSRRL=IORDEnabled[Port])!=0) {	//zaroven provede EXWAIT_OFF
		//EXTIPRFC00
		return;
	}
	((fooport)IORDEnabledLONG[66+(Port>>2)])(Port);
}

void memwr(void) {
	register unsigned short Addr;
	register unsigned short Data;
	EXWAIT_ON();
	Addr=GET_MZADDR();
	Data=GET_MZDATA();
/*	if ((Addr<0x10F0)&&(Addr>=0x10D8)) {
		register unsigned short pos=(0x10F0-Addr)*40+38+0x1000;
		if ((Data&0xFF)>=0xA0) VRAM[pos]=((Data&0xFF)>>4)-9; else VRAM[pos]=((Data&0xFF)>>4)+0x20;
		pos++;
		if ((Data&0xF)>=0xA) VRAM[pos]=(Data&0xF)-9; else VRAM[pos]=(Data&0xF)+0x20;
		pos+=39;
		VRAM[pos++]=0;
		VRAM[pos]=0;
	}*/
/*		if (Addr>=0xA000) VRAM[0x1074]=(Addr>>12)-9; else VRAM[0x1074]=(Addr>>12)+0x20;
		if ((Addr&0xFFF)>=0xA00) VRAM[0x1075]=((Addr&0xFFF)>>8)-9; else VRAM[0x1075]=((Addr&0xFFF)>>8)+0x20;
		if ((Addr&0xFF)>=0xA0) VRAM[0x1076]=((Addr&0xFF)>>4)-9; else VRAM[0x1076]=((Addr&0xFF)>>4)+0x20;
		if ((Addr&0xF)>=0xA) VRAM[0x1077]=(Addr&0xF)-9; else VRAM[0x1077]=(Addr&0xF)+0x20;*/
  EXTIPRFC00
	Write[Addr>>12](Addr,Data);
	EXWAIT_OFF();
}

#ifdef MZ700_ONLY
extern const unsigned char ROM[0x4000];
#include <string.h>
void memrd(void) {
	register unsigned short Addr=GET_MZADDR();
//	if (Addr<=ROMON) {
/*		if (Addr>=0xA000) VRAM[0x1024]=(Addr>>12)-9; else VRAM[0x1024]=(Addr>>12)+0x20;
		Addr&=0x0FFF;
		if (Addr>=0xA00) VRAM[0x1025]=(Addr>>8)-9; else VRAM[0x1025]=(Addr>>8)+0x20;
		Addr&=0x00FF;
		if (Addr>=0xA0) VRAM[0x1026]=(Addr>>4)-9; else VRAM[0x1026]=(Addr>>4)+0x20;
		Addr&=0x000F;
		if (Addr>=0xA) VRAM[0x1027]=Addr-9; else VRAM[0x1027]=Addr+0x20;*/
//		EXWAIT_OFF();
//		EXTIPRFC00
//		return;
//	}
	if (Addr>ROMON) {
//		PUT_MZDATA(ROM[Addr&0x3FFF]);
		PUT_MZDATA(VRAM[Addr-0xC000]);
		EXTIPRFC00
/*	if ((GET_MZBUS()&(1<<(13-0)))!=0) {
		EXTIPRFC00
		MZDATA_OFF();
		return;
	}*/
//	__asm volatile ("cpsid i");
/*	__asm volatile ("isb");
	__asm volatile ("nop");
	__asm volatile ("nop");
	__asm volatile ("nop");
	__asm volatile ("nop");
	__asm volatile ("nop");
	__asm volatile ("nop");*/
//	__asm volatile ("isb");
	//EXTI->RTSR=(1<<8)|(1<<13)|(1<<14);					//reset,rd,wr
//	EXWAIT_OFF();
		while ((GET_MZBUS()&(1<<(13-0)))==0);
		MZDATA_OFF();
		return;
	}
	EXTIPRFC00
//	__asm volatile ("cpsie i");
	return;
}
void memrdm1(void) {
	register unsigned short Addr;
//	EXWAIT_ON();
	Addr=GET_MZADDR();
//	if (Addr<=ROMON) {
/*		if (Addr>=0xA000) VRAM[0x1024]=(Addr>>12)-9; else VRAM[0x1024]=(Addr>>12)+0x20;
		Addr&=0x0FFF;
		if (Addr>=0xA00) VRAM[0x1025]=(Addr>>8)-9; else VRAM[0x1025]=(Addr>>8)+0x20;
		Addr&=0x00FF;
		if (Addr>=0xA0) VRAM[0x1026]=(Addr>>4)-9; else VRAM[0x1026]=(Addr>>4)+0x20;
		Addr&=0x000F;
		if (Addr>=0xA) VRAM[0x1027]=Addr-9; else VRAM[0x1027]=Addr+0x20;*/
//		EXWAIT_OFF();
//		EXWAIT_OFF();
//		EXTIPRFC00
//		return;
//	}
	if (Addr>ROMON) {
//		PUT_MZDATA(ROM[Addr&0x3FFF]);
		PUT_MZDATA(VRAM[Addr-0xC000]);
		EXTIPRFC00
//	EXWAIT_OFF();
//	__asm volatile ("cpsid i");
/*	__asm volatile ("isb");
	__asm volatile ("nop");
	__asm volatile ("nop");
	__asm volatile ("nop");
	__asm volatile ("nop");
	__asm volatile ("nop");
	__asm volatile ("nop");*/
//	__asm volatile ("isb");
	//EXTI->RTSR=(1<<8)|(1<<13)|(1<<14);					//reset,rd,wr
//	EXWAIT_OFF();
		while ((GET_MZBUS()&(1<<(13-0)))==0);
		MZDATA_OFF();
//	__asm volatile ("cpsie i");
		return;
	}
//	EXWAIT_ON();
	EXTIPRFC00
//	EXWAIT_OFF();
//	{
//		xxxl++;if ((xxxl<0)||(xxxl>24)) xxxl=0;
//		xxxpos=xxxl*40+0x1024;
/*		if (Addr>=0xA000) VRAM[xxxpos]=(Addr>>12)-9; else VRAM[xxxpos]=(Addr>>12)+0x20;
		xxxpos++;
		if ((Addr&0x0FFF)>=0xA00) VRAM[xxxpos]=((Addr&0x0FFF)>>8)-9; else VRAM[xxxpos]=((Addr&0x0FFF)>>8)+0x20;
		xxxpos++;
		if ((Addr&0x0FF)>=0xA0) VRAM[xxxpos]=((Addr&0x0FF)>>4)-9; else VRAM[xxxpos]=((Addr&0x0FF)>>4)+0x20;
		xxxpos++;*/
//		if ((Addr&0x0F)>=0xA) VRAM[xxxpos]=(Addr&0x0F)-9; else VRAM[xxxpos]=(Addr&0x0F)+0x20;
//	}
	return;
}
void m1(void) {
	register unsigned short Addr=GET_MZADDR();
	register unsigned short data;
	register unsigned short mzbus;
	if (Addr>ROMON) {
		data=VRAM[Addr-0xC000];
		do {
			mzbus=GET_MZBUS();
			if ((mzbus&((1<<11)|(1<<13)))==0) {
//				if (Addr>ROMON) {
					PUT_MZDATA(data);
					EXTIPRFC00
					while ((GET_MZBUS()&(1<<(13-0)))==0);
					MZDATA_OFF();
					return;
//				}
//				EXTIPRFC00
//				return;
			}
		} while ((mzbus&(1<<15))==0);	//dokud trva M1
	}
	EXTIPRFC00
}
#endif
//void rdstop(void) {
//	MZDATA_OFF();
//	EXTI->RTSR=(1<<8);					//reset
//	EXTIPRFC00
//	return;
//}

#ifdef Z80_M1_SPY
void m1(void) {
	AddrM1=GET_MZADDR();
	EXWAIT_OFF();
  EXTIPRFC00
}
#endif

typedef void (*foo)(void);
static const foo pole[32+3]={
	noact,
	noact,
	noact,
	noact,

	noact,
	noact,
	noact,
	noact,

	noact,
	noact,
#ifdef Z80_M1_SPY
	m1,			//MREQ + RD + M1
	m1,			//RD + M1
#else
	memrdm1,	//noact,	//MREQ + RD + M1
	m1,
#endif

	noact,	//rdstop,	//noact,
	noact,	//rdstop,	//noact,
#ifdef Z80_M1_SPY
	m1,			//MREQ + M1
	m1,			//M1
#else
	m1,			//rdstop,	//noact,
	m1,			//noact,	//rdstop,	//noact,
#endif

	noact,
	noact,
	noact,
	noact,

	noact,
	iowr,		//IORQ + WR
	memwr,	//MREQ + WR
	noact,

	noact,
	iord,		//IORQ + RD
	memrd,	//noact,	//MREQ + RD
	noact,	//RD

	noact,	//rdstop,	//noact,
	noact,	//rdstop,	//noact,	//IORQ
	noact,	//rdstop,	//noact,	//MREQ
	noact,	//rdstop,	//noact,

	(foo)(&GET_MZBUS()),
	(foo)(&(GPIOC->BSRRH)),
	(foo)(&(EXTI->PR))
};

void EXTI15_10_IRQHandler()
{
//	register unsigned short mzbus=*((unsigned short *)(pole[32]));		//	register unsigned short mzbus=GET_MZBUS();
//	*(unsigned short *)(pole[33])=(1<<0);															//	EXWAIT_ON();
//	pole[mzbus>>11]();
/*	*(unsigned short *)(pole[33])=(1<<0);															//	EXWAIT_ON();
  *(unsigned short *)(pole[34])=0xFC00;										//EXTI->PR
	pole[(*((unsigned short *)(pole[32])))>>11]();*/
/*	EXWAIT_ON();
  EXTI->PR=0xFC00;										//EXTI->PR
	pole[GET_MZBUS()>>11]();*/
/*	register unsigned short mzbus=*((unsigned short *)(pole[32]));		//	register unsigned short mzbus=GET_MZBUS();
	*(unsigned short *)(pole[33])=(1<<0);															//	EXWAIT_ON();
  *(unsigned short *)(pole[34])=0xFC00;															//EXTI->PR
	pole[mzbus>>11]();*/
	pole[(*((unsigned short *)(pole[32])))>>11]();
}

int newline;

void EXTI9_5_IRQHandler()
{
/////	TIM2->CNT=4544;	//zaciname na radce 1 (poradove 20)
	TIM2->CNT=PAL_VIDEO_RESET_POSITION;	//zaciname na radce 1 (poradove 50)
	TIM5->CNT=0;			//zacatek radku
	newline=50;			//cislo radku
	MZDATA_OFF();
	EXWAIT_OFF();
	mzint_Init();	//EXINT_OFF
	emu_FDC_write(7,(unsigned int*)"\0\0\0\0");	//FDC INT OFF
	EXRESET_OFF();
	reset_event(EVENT_RAM_READ);
	reset_event(EVENT_RAM_WRITE);
	reset_event(EVENT_QD_READ);
	reset_event(EVENT_QD_WRITE);
	reset_event(EVENT_FDD_READ);
	reset_event(EVENT_FDD_WRITE);
	reset_event(EVENT_SIO_READ);
	reset_event(EVENT_SIO_WRITE);
	reset_event(EVENT_EMU_READ);
	reset_event(EVENT_EMU_WRITE);
	GDG_init();
	EXTI->PR = 0x0100;
}

void GDG_init() {
				int i;
#ifdef MZ700_ONLY
				memcpy(&VRAM[0],&ROM[0x1000],0x1000);
				memcpy(&VRAM[0x2000],&ROM[0x2000],0x2000);
#endif				
				for (i=0;i<16;i++)	{
					WriteM[MZ_700][i]=NoWr;
					WriteM[MZ_800][i]=NoWr;
					Write[i]=NoWr;
				}
				//novinka pro ZX emulaci, zdravim Spectristy, maji usetrenou praci ;-)
				Write[4]=WrZX_VRAM_45xx;Write[5]=WrZX_VRAM_45xx;
				//
				WriteM[MZ_800][8]=WrROM[MZ_800][8];
				WriteM[MZ_800][9]=WrROM[MZ_800][9];
				WriteM[MZ_800][10]=WrRAM[MZ_800][10];
				WriteM[MZ_800][11]=WrRAM[MZ_800][11];
				WriteM[MZ_800][12]=WrRAM[MZ_800][12];
				WriteM[MZ_800][13]=WrRAM[MZ_800][13];
				WriteM[MZ_700][8]=WrRAM[MZ_700][8];
				WriteM[MZ_700][9]=WrRAM[MZ_700][9];
				WriteM[MZ_700][10]=WrRAM[MZ_700][10];
				WriteM[MZ_700][11]=WrRAM[MZ_700][11];
				WriteM[MZ_700][12]=WrRAM[MZ_700][12];
				WriteM[MZ_700][13]=WrROM[MZ_700][13];
#ifdef MZ700_ONLY
				ROMON=0xE00F;
				MZ_graphic_mode=0x08;
				set_mode(MZ_700);
				memset(SYdata,0,sizeof(SYdata));
				KP80[1]=80;
				KP80[6]=25;
				KP80[9]=10-1;
#else					
				if ((GPIOC->IDR)&(1<<13)) {	//SW1 off
					MZ_graphic_mode=0x00;
					set_mode(MZ_800);
				} else {										//SW1 on
					MZ_graphic_mode=0x08;
					set_mode(MZ_700);
				}
				SCROLL_SOF = 0;// SCROLL_SW = 0; SCROLL_SSA = 0; SCROLL_SEA = 125<<6; SCROLL_SW = 125<<6;
				tab800_init_palete();
#endif				
				MZ_BORDER = 0;
				ZX_EMUL = 0;
}

void IOWRPortListAdd (int len, unsigned char* ports)
{
	while (len--) {
		IOWREnabled[ports[len]]=0;
	}
}

void IORDPortListAdd (int len, unsigned char* ports)
{
	while (len--) {
		IORDEnabled[ports[len]]=0;
	}
}

int IOGetEnabled(unsigned char n) {
	if ((IOWREnabled[n]==0)||(IORDEnabled[n]==0)) return 1;
	return 0;
}

