/*
 *    Copyright (c) 2012 by Bohumil Novacek <http://dzi.n.cz/8bit/>
 *
 *    Thies program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    MZ-800 Unicard is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MZ-800 Unicard; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "emu_DAC.h"
#include "stm32f4xx.h"
#include "ff.h"
#include <string.h>
#include "event.h"
#include "interrupt.h"
#include "monitor.h"
#define DBGLEVEL	DBGNON
//#define DBGLEVEL	(DBGNON | DBGFAT | DBGERR | DBGWAR | DBGINF)
#define USE_DBG_PRN	xprintf
#include "debug.h"

#define DAC_DHR12R2_ADDRESS    0x40007414
#define DAC_DHR12L1_ADDRESS    0x4000740C
#define DAC_DHR12L2_ADDRESS    0x40007418

extern   uint32_t SystemCoreClock;

/* Private function prototypes -----------------------------------------------*/
void TIM6_Config(unsigned long speed);
void TIM7_Config(void);

typedef struct WAV_S {
	uint8_t	RIFF[4];
	uint32_t filesize;
	uint8_t	WAVE[4];
	uint8_t	fmt_[4];
	uint32_t fmtlen;
	uint16_t format_type;
	uint16_t channels;
	uint32_t sample_rate;
	uint32_t data_rate;
	uint16_t sample_size;
	uint16_t bits_per_sample;
	uint8_t	data[4];
	uint32_t data_size;
} WAV_S;

static int state;
static FIL file;

#define READBUFSIZE	512UL
static unsigned char readbuf[READBUFSIZE];
static unsigned long readindex;
static unsigned long readindexMAX;
static WAV_S wave;

#define DMABUFSIZE	512UL
static unsigned short dmabuf1[DMABUFSIZE*2];
#define DMABUFSIZEPSG	2048UL
static unsigned short dmabuf2[DMABUFSIZEPSG*2];
static int mem_half1;
#ifndef DMA_DAC1_ON
static int DAC1_play_index,DAC2_play_index;
#endif
static int DAC2_time;
static int DAC2_index;
static signed long lastout;

/* Private functions ---------------------------------------------------------*/

int emu_DAC_read(unsigned char *data, int n)
{
	UINT rd;
	if (state==0) return 1;
	while (n>0) {
		if (readindex>=readindexMAX) { { if (state==1) state=2;}; return 2; }
		if (readindex&(READBUFSIZE-1)) {
			*data++=readbuf[readindex&(READBUFSIZE-1)];
		} else {
			int res=
			f_read( &file, readbuf, READBUFSIZE, &rd );
			if (res) { DBGPRINTF ( DBGERR, "Error f_read %d, readindex=%d, readindexMAX=%d \n", res, readindex, readindexMAX ); state=2; }
			*data++=readbuf[0];
		}
		readindex++;
		n--;
	}
	return 0;
}

void emu_DAC_init(int chnum)
{
  /* Preconfiguration before using DAC----------------------------------------*/
  GPIO_InitTypeDef GPIO_InitStructure;

  /* DMA1 clock and GPIOA clock enable (to be used with DAC) */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1 | RCC_AHB1Periph_GPIOA, ENABLE);

  /* DAC Periph clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);

  /* DAC channel 1 & 2 (DAC_OUT1 = PA.4)(DAC_OUT2 = PA.5) configuration */
//#ifdef USE_UNICARD_MK3B
  if (chnum==1) GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
//#else	
  else GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;
//#endif	
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

	state=0;
	DAC2_index=-1;
}

void emu_DAC_prepare_DMA_half(int hf) {
	int i=0;
	int n=DMABUFSIZE;
	if (hf) {
		i=DMABUFSIZE;
		n=DMABUFSIZE*2;
	}
	while (i<n) {
		unsigned long data;
		if (emu_DAC_read((unsigned char *)&data,wave.sample_size)) { dmabuf1[i]=0x8000; }
		else {
			if (wave.bits_per_sample==8) { dmabuf1[i]=(data<<8)&0xFF00; }
			else { dmabuf1[i]=(data+0x8000)&0xFFF0; }
		}
		i++;
	}
}

void emu_DAC_start(int emu_dac) {
	DAC_InitTypeDef  DAC_InitStructure;
#ifdef DMA_DAC1_ON
  DMA_InitTypeDef DMA_InitStructure;
#endif

	NVIC_InitTypeDef NVIC_InitStructure; // this is used to configure the NVIC (nested vector interrupt controller)

	if ((emu_dac!=DAC_Channel_1)&&(emu_dac!=DAC_Channel_2)) return;

	if (emu_dac==DAC_Channel_1) TIM6_Config(wave.sample_rate);	// DMA1 stream 5 channel 7 is triggered by timer 6.
	if (emu_dac==DAC_Channel_2) TIM7_Config();	// DMA1 stream 6 channel 7 is triggered by timer 7.

#ifdef DMA_DAC1_ON
	// Stop it and configure interrupts.
	if (emu_dac==DAC_Channel_1) DMA1_Stream5->CR&=~DMA_SxCR_EN;
	if (emu_dac==DAC_Channel_2) DMA1_Stream6->CR&=~DMA_SxCR_EN;
	if (emu_dac==DAC_Channel_1) NVIC_InitStructure.NVIC_IRQChannel = DMA1_Stream5_IRQn;		 // we want to configure the USART1 interrupts
	if (emu_dac==DAC_Channel_2) NVIC_InitStructure.NVIC_IRQChannel = DMA1_Stream6_IRQn;		 // we want to configure the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 6;// this sets the priority group of the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;		 // this sets the subpriority inside the group
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			 // the USART1 interrupts are globally enabled
	NVIC_Init(&NVIC_InitStructure);							 // the properties are passed to the NVIC_Init function which takes care of the low level stuff
#endif

	//DAC_DeInit();

  /* DAC channel1 Configuration */
  if (emu_dac==DAC_Channel_1) DAC_InitStructure.DAC_Trigger = DAC_Trigger_T6_TRGO;
  if (emu_dac==DAC_Channel_2) DAC_InitStructure.DAC_Trigger = DAC_Trigger_T7_TRGO;
  DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
  DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Disable;
  DAC_Init(emu_dac, &DAC_InitStructure);

  /* DMA1_Stream5 channel7 configuration **************************************/
  if (emu_dac==DAC_Channel_1) DMA_DeInit(DMA1_Stream5);
  if (emu_dac==DAC_Channel_2) DMA_DeInit(DMA1_Stream6);
#ifdef DMA_DAC1_ON
  DMA_InitStructure.DMA_Channel = DMA_Channel_7;
  if (emu_dac==DAC_Channel_1) DMA_InitStructure.DMA_PeripheralBaseAddr = DAC_DHR12L1_ADDRESS;
  if (emu_dac==DAC_Channel_1) DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&dmabuf1;
  if (emu_dac==DAC_Channel_1) DMA_InitStructure.DMA_BufferSize = DMABUFSIZE*2;
  if (emu_dac==DAC_Channel_2) DMA_InitStructure.DMA_PeripheralBaseAddr = DAC_DHR12L2_ADDRESS;
  if (emu_dac==DAC_Channel_2) DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&dmabuf2;
  if (emu_dac==DAC_Channel_2) DMA_InitStructure.DMA_BufferSize = DMABUFSIZEPSG*2;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
  DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  if (emu_dac==DAC_Channel_1) DMA_Init(DMA1_Stream5, &DMA_InitStructure);
  if (emu_dac==DAC_Channel_2) DMA_Init(DMA1_Stream6, &DMA_InitStructure);
  if (emu_dac==DAC_Channel_1) DMA_ITConfig(DMA1_Stream5, ((DMA_IT_TC)|(DMA_IT_HT)), ENABLE);
  if (emu_dac==DAC_Channel_2) DMA_ITConfig(DMA1_Stream6, ((DMA_IT_TC)|(DMA_IT_HT)), ENABLE);
//  DMA_FlowControllerConfig(DMA1_Stream5, DMA_FlowCtrl_Peripheral);

  /* Enable DMA1_Stream5 */
  if (emu_dac==DAC_Channel_1) DMA_Cmd(DMA1_Stream5, ENABLE);
  if (emu_dac==DAC_Channel_2) DMA_Cmd(DMA1_Stream6, ENABLE);
#else
  //TODO nastavit promenne
	DAC1_play_index=0;
	//TODO povolit preruseni od TIM6
	if (emu_dac==DAC_Channel_1) NVIC_InitStructure.NVIC_IRQChannel = TIM6_DAC_IRQn;		 // we want to configure the USART1 interrupts
	if (emu_dac==DAC_Channel_2) NVIC_InitStructure.NVIC_IRQChannel = TIM7_DAC_IRQn;		 // we want to configure the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 6;// this sets the priority group of the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;		 // this sets the subpriority inside the group
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			 // the USART1 interrupts are globally enabled
	NVIC_Init(&NVIC_InitStructure);							 // the properties are passed to the NVIC_Init function which takes care of the low level stuff
#endif

  /* Enable DAC Channel1 */
  DAC_Cmd(emu_dac, ENABLE);

#ifdef DMA_DAC1_ON
  /* Enable DMA for DAC Channel1 */
  DAC_DMACmd(emu_dac, ENABLE);
#else
	if (emu_dac==DAC_Channel_1) TIM6->DIER=TIM_DIER_UIE;	//int enable
	if (emu_dac==DAC_Channel_2) TIM7->DIER=TIM_DIER_UIE;	//int enable
#endif
}

#ifndef DMA_DAC1_ON
void TIM6_DAC_IRQHandler()
{
	TIM6->SR=0;
	*((unsigned short *)(DAC_DHR12L1_ADDRESS))=dmabuf1[DAC1_play_index++];
	if (DAC1_play_index==DMABUFSIZE) {
		mem_half1=0;
		set_event(EVENT_DAC1_WAV);
	} else if (DAC1_play_index==DMABUFSIZE*2) {
		mem_half1=1;
		set_event(EVENT_DAC1_WAV);
		DAC1_play_index=0;
	}
}
void TIM7_DAC_IRQHandler()
{
	TIM7->SR=0;
	*((unsigned short *)(DAC_DHR12L2_ADDRESS))=dmabuf2[DAC12play_index++];
	if (DAC2_play_index==DMABUFSIZEPSG) {
		mem_half2=0;
		set_event(EVENT_DAC2_WAV);
	} else if (DAC2_play_index==DMABUFSIZEPSG*2) {
		mem_half2=1;
		set_event(EVENT_DAC2_WAV);
		DAC2_play_index=0;
	}
}
#endif

int emu_DAC_PLAY(char *filename)
{
	int i;
	if (state) return -1;
	if ( FR_OK != f_open ( &file, filename, FA_READ )) return 1;
	state = 1;
	readindex=0;
	if (file.fsize<=44) { emu_DAC_STOP();return 2; }
	readindexMAX=file.fsize;
	for (i=0;i<44;i++) *((char *)&wave.RIFF[0])=0;
  emu_DAC_read(&wave.RIFF[0],44);
  if (memcmp(&wave.RIFF[0],"RIFF",4)) { emu_DAC_STOP();return 3; }
  if (memcmp(&wave.WAVE[0],"WAVE",4)) { emu_DAC_STOP();return 4; }
  if (memcmp(&wave.fmt_[0],"fmt",3)) { emu_DAC_STOP();return 5; }
  if (wave.format_type!=1) { emu_DAC_STOP();return 6; }
  if ((wave.channels<1)||(wave.channels>2)) { emu_DAC_STOP();return 7; }
  if ((wave.sample_rate<(SystemCoreClock/65535UL))||(wave.sample_rate>192000)) { emu_DAC_STOP();return 8; }
  if ((wave.sample_size!=1)&&(wave.sample_size!=2)&&(wave.sample_size!=4)) { emu_DAC_STOP();return 9; }
  if ((wave.bits_per_sample!=8)&&(wave.bits_per_sample!=16)) { emu_DAC_STOP();return 10; }
  if (memcmp(&wave.data[0],"data",4)) { emu_DAC_STOP();return 11; }
	if ((wave.data_size+44)<file.fsize) readindexMAX=wave.data_size+44;
	emu_DAC_prepare_DMA_half(0);
	emu_DAC_prepare_DMA_half(1);
	emu_DAC_start(DAC_Channel_1);
	return 0;
}

int emu_DAC_STOP(void)
{
	if (state) {
		TIM6->CR1&=~TIM_CR1_CEN; // Stop pixel clock.
#ifdef DMA_DAC1_ON
		DMA1_Stream5->CR=0; // Disable DMA.
#endif
		f_close(&file);
		state=0;
	}
	return 0;
}

#ifdef DMA_DAC1_ON
void DMA1_Stream5_IRQHandler()
{
	unsigned long tmp=DMA1->HISR;
	DMA1->HIFCR |= (DMA_HIFCR_CTCIF5|DMA_HIFCR_CHTIF5);
	if (tmp&DMA_HISR_HTIF5) {
		//emu_DAC_prepare_DMA_half(0);
		mem_half1=0;
		set_event(EVENT_DAC1_WAV);
	}
	if (tmp&DMA_HISR_TCIF5) {
		//emu_DAC_prepare_DMA_half(1);
		mem_half1=1;
		set_event(EVENT_DAC1_WAV);
	}
}

void DMA1_Stream6_IRQHandler()
{
	unsigned long tmp=DMA1->HISR;
	DMA1->HIFCR |= (DMA_HIFCR_CTCIF6|DMA_HIFCR_CHTIF6);
	if (tmp&DMA_HISR_HTIF6) {
		if (DAC2_index>(DMABUFSIZEPSG/2)) {	//jsme pomali, pridej
			TIM7->ARR = DAC2_SPEED_HI - 1L;
		} else {	//zrychlujeme
			TIM7->ARR = DAC2_SPEED_LOW - 1L;
		}
		if (DAC2_index>DMABUFSIZEPSG) DAC2_index=DMABUFSIZEPSG/2;
	}
	if (tmp&DMA_HISR_TCIF6) {
		if (DAC2_index>((DMABUFSIZEPSG*3)/2)) {	//jsme pomali, pridej
			TIM7->ARR = DAC2_SPEED_HI - 1L;
		} else {	//zrychlujeme
			TIM7->ARR = DAC2_SPEED_LOW - 1L;
		}
		if (DAC2_index<DMABUFSIZEPSG) DAC2_index=(DMABUFSIZEPSG*3)/2;
	}
}
#endif

void do_DACwav() {
	if (state) {
		emu_DAC_prepare_DMA_half(mem_half1);
		if (state==3) {
			emu_DAC_STOP();
		}
		if (state==2) state=3;
	}
}

void emu_DAC2_sync(int time) {
	if (DAC2_index>=0) {
		DAC2_time+=time;
		{
			register int i=DAC2_time/DAC2_SPEED;
			register int period=0;
			register int ct0val;
			DAC2_time%=DAC2_SPEED;
			switch (PSG_period[3]&3) {
				case	0:	period=16;break;
				case	1:	period=32;break;
				case	2:	period=64;break;
				case	3:	period=PSG_period[2];break;
			}
			ct0_update();
			ct0val=ct0_getvalue();
			while (i>0) {
				DAC2_index++;
				if (DAC2_index>=DMABUFSIZEPSG*2) DAC2_index=0;
				{
					register long output=0;

					if (PSG_period[0]) {
						if (PSG_last[0]++>=2*PSG_period[0]) PSG_last[0]=0;
						if (PSG_last[0]>=PSG_period[0]) {
							output-=PSG_volume[0];
						} else output+=PSG_volume[0];
					} else output+=PSG_volume[0];

					if (PSG_period[1]) {
						if (PSG_last[1]++>=2*PSG_period[1]) PSG_last[1]=0;
						if (PSG_last[1]>=PSG_period[1]) {
							output-=PSG_volume[1];
						} else output+=PSG_volume[1];
					} else output+=PSG_volume[1];

					if (PSG_period[2]) {
						if (PSG_last[2]++>=2*PSG_period[2]) PSG_last[2]=0;
						if (PSG_last[2]>=PSG_period[2]) {
							output-=PSG_volume[2];
						} else output+=PSG_volume[2];
					} else output+=PSG_volume[2];

					if (period) {
						if (PSG_last[3]++>=period) {
							PSG_last[3]=0;
							if (/*PSG_periodic*/PSG_period[3]&4) {
								PSG_noise = (PSG_noise >> 1) | ((((PSG_noise >> 1) ^ (PSG_noise))&1) << 15);
							} else {
								if (PSG_noise&1) PSG_noise &= 1;
								PSG_noise = (PSG_noise >> 1) | ((PSG_noise&1) << 15);
							}
							if (PSG_noise==0) PSG_noise=(1<<14);
						}
						if (PSG_noise&(1<<8)) output+=PSG_volume[3];
						else output-=PSG_volume[3];
					}
					
          output-=(8192-ct0val);

					lastout=lastout+(((output<<16)-lastout)>>6);	//low-pass 550Hz -3dB
					dmabuf2[DAC2_index]=(lastout>>16)+0x8000;
				}
				i--;
			} //while
		}
	} //if
}

void TIM6_Config(unsigned long speed)
{
  TIM_TimeBaseInitTypeDef    TIM_TimeBaseStructure;
  /* TIM6 Periph clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);

  /* Time base configuration */
  TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
  TIM_TimeBaseStructure.TIM_Period = /*0xFF*/ (((SystemCoreClock/speed)+1UL)/2UL)-1UL;
  TIM_TimeBaseStructure.TIM_Prescaler = 0;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInit(TIM6, &TIM_TimeBaseStructure);

  /* TIM6 TRGO selection */
  TIM_SelectOutputTrigger(TIM6, TIM_TRGOSource_Update);

  /* TIM6 enable counter */
  TIM_Cmd(TIM6, ENABLE);
}

void TIM7_Config()
{
  TIM_TimeBaseInitTypeDef    TIM_TimeBaseStructure;
  /* TIM6 Periph clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);

  /* Time base configuration */
  TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
  TIM_TimeBaseStructure.TIM_Period = /*0xFF*/ DAC2_SPEED-1UL;
  TIM_TimeBaseStructure.TIM_Prescaler = 0;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInit(TIM7, &TIM_TimeBaseStructure);

	TIM_ARRPreloadConfig(TIM7, ENABLE);

  /* TIM6 TRGO selection */
  TIM_SelectOutputTrigger(TIM7, TIM_TRGOSource_Update);

  /* TIM6 enable counter */
  TIM_Cmd(TIM7, ENABLE);
}

void emu_DAC_open() {
	int i=DMABUFSIZEPSG*2;
	for(;i--;) dmabuf2[i]=0x8000;
	lastout=0;
	PSG_period[0]=0;
	PSG_period[1]=0;
	PSG_period[2]=0;
	PSG_period[3]=3;
	DAC2_index=(DMABUFSIZEPSG*3)/2;
	DAC2_time=0;
	emu_DAC_start(DAC_Channel_2);
	IOWRPortListAdd(1,"\xF2");
}

