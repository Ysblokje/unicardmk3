/*
 *    Copyright (c) 2012 by Bohumil Novacek <http://dzi.n.cz/8bit/>
 *
 *    Thies program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    MZ-800 Unicard is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MZ-800 Unicard; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "interrupt.h"
#include "QD.h"
#include "pal.h"
#include "emu_MZFREPO.h"
#include "emu_RAMDISC.h"
#include "emu_SIO.h"
#include "event.h"
#include "mzint.h"
#include "emu_FDC.h"
#include "keyboard.h"
#include <string.h>

extern int MK3b;

unsigned char VRAM[0x8000];		//umisteni kopie 32KB VRAM
//unsigned char ZX_VRAM[0x1B00];	//umisteni kopie 8KB VRAM ZX Spectra (4000h-5FFFh)
unsigned long zx48tab[0x800];	//4*0x800 bytes - tabulka pro urychleni vykreslovani ZX video pameti

unsigned short PSG_noise;
short PSG_volume[4];
unsigned short PSG_period[4];
unsigned short PSG_last[4];
static byte LAST_F2;

static int PSG_VOLUME[16]={
	5461,	4338,	3445,	2737,
	2174,	1727,	1372,	1090,
	865,	687,	546,	434,
	345,	274,	217,	0
};

#define psg_set_volume(a,b)		{if (PSG_volume[a]>0) PSG_volume[a]=PSG_VOLUME[b];else PSG_volume[a]=-PSG_VOLUME[b];}
#define psg_set_period(a,b)		PSG_period[a]=b

int8_t (*reload_FDD)( uint8_t drive_id );

#ifdef Z80_M1_SPY
volatile unsigned short AddrM1;
#endif

byte MZ_mode;
byte MZ_graphic_mode;
word SCROLL_SSA;
word SCROLL_SEA;
word SCROLL_SW;
word SCROLL_SOF;

volatile int	QD_event_addr;
volatile int	QD_event_data;

volatile int	FDD_event_addr;
volatile int	FDD_event_data;

volatile int	RAM_event_addr;
volatile int	RAM_event_data;

volatile int	SIO_event_addr;
volatile int	SIO_event_data;

static int MEM_PROHIBITED;

static byte WRITE_CMD;
static byte WRITE_PLANE1;
static byte WRITE_PLANE2;
static byte WRITE_PLANE3;
static byte WRITE_PLANE4;
static byte WRITE_FRAME;

byte MZ_BORDER;
//byte ZX_EMUL;
byte ATARI_MOUSE;

void GDG_init(void);

void interrupt_init(int MK3b)
{
	if (MK3b) SYSCFG->EXTICR[0]=3;								//pin PD0 = PS/2 CLK
	SYSCFG->EXTICR[2]=3;								//pin PD8 = reset
	SYSCFG->EXTICR[3]=(3<<12)|(3<<8)|(3<<4)|3;	//pin PD15,PD14,PD13,PD12
	{
		NVIC_InitTypeDef NVIC_InitStructure;

		/* Configure the NVIC Preemption Priority Bits */
//		NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

		NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);

		NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
	}
	GDG_init();
#ifdef Z80_M1_SPY
	EXTI->FTSR=(1<<15)|(1<<14)|(1<<12);	//M1,WR,IORQ
	EXTI->RTSR=(1<<8)/*|(1<<13)*/;	//reset, RD nikoliv
	EXTI->IMR=(1<<8)|(1<<15)|(1<<14)/*|(1<<13)*/|(1<<12);
#else
	EXTI->FTSR=(1<<14)|(1<<12);	//WR,IORQ
	EXTI->RTSR=(1<<8);					//reset
	EXTI->IMR=(1<<8)|(1<<14)|(1<<12);
#endif
	if (MK3b) {
		//PS/2 keyboard
		EXTI_InitTypeDef EXTI_InitStructure;
		NVIC_InitTypeDef NVIC_InitStructure;
		/* Configure EXTI line */
		EXTI_InitStructure.EXTI_Line = EXTI_Line0;
		EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
		EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  
		EXTI_InitStructure.EXTI_LineCmd = ENABLE;
		EXTI_Init(&EXTI_InitStructure);

		NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;//7;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
	}
}

void NoWr(register word Addr, register byte Value) { EXTI->PR = 0xFC00; }

#define ram_write	NoWr

#define WrRAM_0xxx	ram_write
#define WrRAM_1xxx	ram_write
#define WrRAM_2xxx	ram_write
#define WrRAM_3xxx	ram_write
#define WrRAM_4xxx	ram_write
#define WrRAM_5xxx	ram_write
#define WrRAM_6xxx	ram_write
#define WrRAM_7xxx	ram_write
#define WrRAM_8xxx	ram_write
#define WrRAM_9xxx	ram_write
#define WrRAM_Axxx	ram_write
#define WrRAM_Bxxx	ram_write
#define WrRAM_Cxxx	ram_write
#define WrRAM_Dxxx	ram_write
#define WrRAM_Exxx	ram_write
#define WrRAM_Fxxx	ram_write

void WrZX_VRAM_45xx(register word Addr, register byte Value) {
//	Addr&=0x1FFF; if (Addr<0x1B00) ZX_VRAM[Addr]=Value;
  EXTI->PR = 0xFC00;
}

void WrVRAM_ABxx(register word Addr, register byte Value) {
	register short offset;
	if (!(MZ_graphic_mode&4)) { return; }
	offset=(Addr&1)<<13;
	Addr=((Addr>>1)&0x1FFF);
	if (SCROLL_SOF) {
		if ((Addr>=SCROLL_SSA)&&(Addr<SCROLL_SEA)) Addr=(((Addr+SCROLL_SOF-SCROLL_SSA)%(SCROLL_SW))+SCROLL_SSA)&0x1FFF;
	}
	Addr+=offset;
	switch (WRITE_CMD) {
	case 0: if (WRITE_PLANE1) VRAM[Addr]=Value;
			if (WRITE_PLANE3) VRAM[Addr+0x4000]=Value;
			break;
	case 1: if (WRITE_PLANE1) VRAM[Addr]^=Value;
			if (WRITE_PLANE3) VRAM[Addr+0x4000]^=Value;
			break;
	case 2: if (WRITE_PLANE1) VRAM[Addr]|=Value;
			if (WRITE_PLANE3) VRAM[Addr+0x4000]|=Value;
			break;
	case 3: if (WRITE_PLANE1) VRAM[Addr]&=~Value;
			if (WRITE_PLANE3) VRAM[Addr+0x4000]&=~Value;
			break;
	case 4:
	case 5:	if (WRITE_PLANE1) VRAM[Addr]=Value;else VRAM[Addr]=0;
			if (WRITE_PLANE3) VRAM[Addr+0x4000]=Value;else VRAM[Addr+0x4000]=0;
			break;
	case 6:
	case 7:	if (WRITE_PLANE1) VRAM[Addr]|=Value;else VRAM[Addr]&=~Value;
			if (WRITE_PLANE3) VRAM[Addr+0x4000]|=Value;else VRAM[Addr+0x4000]&=~Value;
			break;
	}
  EXTI->PR = 0xFC00;
}
void WrVRAM_89xx(register word Addr, register byte Value) {
	if (MZ_graphic_mode&4) { WrVRAM_ABxx(Addr, Value);return; }
	Addr&=0x1FFF;
	if (SCROLL_SOF) {
		if ((Addr>=SCROLL_SSA)&&(Addr<SCROLL_SEA)) Addr=(((Addr+SCROLL_SOF-SCROLL_SSA)%SCROLL_SW)+SCROLL_SSA)&0x1FFF;
	}
	switch (WRITE_CMD) {
	case 0:
				if (WRITE_PLANE1) VRAM[Addr]=Value;
				if (WRITE_PLANE2) VRAM[Addr+0x2000]=Value;
				if (WRITE_PLANE3) VRAM[Addr+0x4000]=Value;
				if (WRITE_PLANE4) VRAM[Addr+0x6000]=Value;
			break;
	case 1:
				if (WRITE_PLANE1) VRAM[Addr]^=Value;
				if (WRITE_PLANE2) VRAM[Addr+0x2000]^=Value;
				if (WRITE_PLANE3) VRAM[Addr+0x4000]^=Value;
				if (WRITE_PLANE4) VRAM[Addr+0x6000]^=Value;
			break;
	case 2:
				if (WRITE_PLANE1) VRAM[Addr]|=Value;
				if (WRITE_PLANE2) VRAM[Addr+0x2000]|=Value;
				if (WRITE_PLANE3) VRAM[Addr+0x4000]|=Value;
				if (WRITE_PLANE4) VRAM[Addr+0x6000]|=Value;
			break;
	case 3:
				if (WRITE_PLANE1) VRAM[Addr]&=~Value;
				if (WRITE_PLANE2) VRAM[Addr+0x2000]&=~Value;
				if (WRITE_PLANE3) VRAM[Addr+0x4000]&=~Value;
				if (WRITE_PLANE4) VRAM[Addr+0x6000]&=~Value;
			break;
	case 4:
	case 5:	if (MZ_graphic_mode==2) {
				if (WRITE_PLANE1) VRAM[Addr]=Value;else VRAM[Addr]=0;
				if (WRITE_PLANE2) VRAM[Addr+0x2000]=Value;else VRAM[Addr+0x2000]=0;
				if (WRITE_PLANE3) VRAM[Addr+0x4000]=Value;else VRAM[Addr+0x4000]=0;
				if (WRITE_PLANE4) VRAM[Addr+0x6000]=Value;else VRAM[Addr+0x6000]=0;
			} else {
				if (WRITE_FRAME) {
					if (WRITE_PLANE3) VRAM[Addr+0x4000]=Value;else VRAM[Addr+0x4000]=0;
					if (WRITE_PLANE4) VRAM[Addr+0x6000]=Value;else VRAM[Addr+0x6000]=0;
				} else {
					if (WRITE_PLANE1) VRAM[Addr]=Value;else VRAM[Addr]=0;
					if (WRITE_PLANE2) VRAM[Addr+0x2000]=Value;else VRAM[Addr+0x2000]=0;
				}
			}
			break;
	case 6:
	case 7:	if (MZ_graphic_mode==2) {
				if (WRITE_PLANE1) VRAM[Addr]|=Value;else VRAM[Addr]&=~Value;
				if (WRITE_PLANE2) VRAM[Addr+0x2000]|=Value;else VRAM[Addr+0x2000]&=~Value;
				if (WRITE_PLANE3) VRAM[Addr+0x4000]|=Value;else VRAM[Addr+0x4000]&=~Value;
				if (WRITE_PLANE4) VRAM[Addr+0x6000]|=Value;else VRAM[Addr+0x6000]&=~Value;
			} else {
				if (WRITE_FRAME) {
					if (WRITE_PLANE3) VRAM[Addr+0x4000]|=Value;else VRAM[Addr+0x4000]&=~Value;
					if (WRITE_PLANE4) VRAM[Addr+0x6000]|=Value;else VRAM[Addr+0x6000]&=~Value;
				} else {
					if (WRITE_PLANE1) VRAM[Addr]|=Value;else VRAM[Addr]&=~Value;
					if (WRITE_PLANE2) VRAM[Addr+0x2000]|=Value;else VRAM[Addr+0x2000]&=~Value;
				}
			}
			break;
	}
  EXTI->PR = 0xFC00;
}
void WrVRAM_Cxxx(register word Addr, register byte Value) { if (MZ_mode==MZ_700) { VRAM[(Addr|0x0000)&0x1FFF]=Value; }; EXTI->PR = 0xFC00; }
void WrVRAM_Dxxx(register word Addr, register byte Value) { if (MZ_mode==MZ_700) { if (!MEM_PROHIBITED) { VRAM[(Addr|0x0000)&0x1FFF]=Value; } }; EXTI->PR = 0xFC00; }

//P_WRITE	WriteM[2][16];
P_WRITE	Write[16];

void set_mode(int m) {
	MZ_mode = m;
/*	Write[8]=WriteM[MZ_mode][8];
	Write[9]=WriteM[MZ_mode][9];
	Write[10]=WriteM[MZ_mode][10];
	Write[11]=WriteM[MZ_mode][11];
	Write[12]=WriteM[MZ_mode][12];
	Write[13]=WriteM[MZ_mode][13];*/
//	PALETE_UPDATE = 32;
}

byte I8255_C;
static int I8253_CT0_HL;	//0 bude low, 1 bude hi
int I8253_CT0_SET;
int I8253_CT0_COUNT;
int I8253_CT0_OUT;
int I8253_GATE0;
int I8253_CT0_MODE;
//static int I8253_CT0_BUF;

#define	AOUT_DOWM_SAMPLE	14

#define NORMAL_SPEED    0	//(GLOBAL_SPEED_index==10)
static int CT0_period=0;
#ifdef NORMAL_SPEED
static char CT0_pulse[91];
#endif
static int CT0_index=0;
#define CT0_last	512
static int CT0_new=0;
static int CT0_mem=0;
int CT0_prumer=0;
#include "VGA.h"
int ct0_getvalue() {
    int res=0;
    if (NORMAL_SPEED) {
        if (CT0_index>71) CT0_index=71;
        CT0_prumer=((CT0_prumer*5)+CT0_pulse[CT0_index]*8192)/6;
        res=CT0_prumer/5;
        CT0_index++;
    } else {
        if (CT0_index) {
					res=CT0_prumer*8192/CT0_index;
					CT0_mem=res;
				} else {
					res=CT0_mem;
				}
    }
    return res;
}
void ct0_next() {
    if (NORMAL_SPEED) {
        if ((CT0_index<0)||(CT0_index>=71)) {
            CT0_period-=1136*5;
            if (CT0_period<0) {
                CT0_period=0;
            }
            if (CT0_period>8*80) {
                CT0_period=8*80;
            }
            memcpy(&CT0_pulse[0],&CT0_pulse[71],(90-71)*sizeof(char));
            CT0_index=0;
        } else {
        }
    } else {
			CT0_period%=1136;
      CT0_index=0;
      CT0_prumer=0;
    }
}
void ct0_newstate(int mode) {
    switch (mode) {
        case 0:
            I8253_CT0_OUT=0;    //negovany vystup
            break;
        case 2:
        case 3:
            I8253_CT0_OUT=1;
            break;
    }
}
void ct0_update() {
		register int n;
        CT0_new=(TIM2->CNT*5)%1136+CT0_last;//(R.cntLast-R.ICount)*5+CT0_last;
        //if (CT0_new>90*80) CT0_new=90*80;
				n=(CT0_new-((signed int)CT0_period))/16;
				if (n<=0) return;
				CT0_index+=n;
				CT0_period+=n*16;
        switch (I8253_CT0_MODE) {
			case 0:
           			if (I8253_GATE0||(MZ_mode==MZ_800)) {
									if ((I8255_C&1)&&(I8253_CT0_OUT)) {
										if (I8253_CT0_COUNT>=n) {
											CT0_prumer+=n;
										} else if (I8253_CT0_COUNT>=0) {
											CT0_prumer+=n;
											I8253_CT0_OUT=1;
										}
									} else if (I8255_C&1) {
										if (I8253_CT0_COUNT>=n) {
										} else if (I8253_CT0_COUNT>=0) {
											I8253_CT0_OUT=1;
											CT0_prumer+=(n-I8253_CT0_COUNT);
										}
									} else {
										if (I8253_CT0_COUNT>=n) {
										} else if (I8253_CT0_COUNT>=0) {
											I8253_CT0_OUT=1;
										}
									}
									I8253_CT0_COUNT-=n;
								}
				break;
			case 2:
           			if (I8253_GATE0||(MZ_mode==MZ_800)) {
									if ((I8255_C&1)/*&&(I8253_CT0_OUT)*/) {
										if (I8253_CT0_COUNT>=n) {
											CT0_prumer+=/*I8253_CT0_OUT**/n;
										} else if (I8253_CT0_COUNT>=0) {
											////CT0_prumer+=((I8255_C&1)?I8253_CT0_OUT*I8253_CT0_COUNT:0);
											//I8253_CT0_OUT=0;
											//CT0_prumer+=((I8255_C&1)?I8253_CT0_OUT*1:0);
											I8253_CT0_OUT=1;
											////CT0_prumer+=((I8255_C&1)?I8253_CT0_OUT*(n-(I8253_CT0_COUNT+1)):0);
											CT0_prumer+=/*I8253_CT0_OUT**/(n-1);
											I8253_CT0_COUNT+=I8253_CT0_SET;
										}
									} else {
										if (I8253_CT0_COUNT>=n) {
										} else if (I8253_CT0_COUNT>=0) {
											//I8253_CT0_OUT=0;
											I8253_CT0_OUT=1;
											I8253_CT0_COUNT+=I8253_CT0_SET;
										}
									}
									I8253_CT0_COUNT-=n;
								}
                break;
			case 3:
           			if (I8253_GATE0||(MZ_mode==MZ_800)) {
									 if (I8255_C&1) {
											if (I8253_CT0_COUNT>=I8253_CT0_SET/2+n) {
													CT0_prumer+=I8253_CT0_OUT*n;
											} else if (I8253_CT0_COUNT>=I8253_CT0_SET/2) {
													CT0_prumer+=I8253_CT0_OUT*(I8253_CT0_COUNT-I8253_CT0_SET/2);
													I8253_CT0_OUT=0;
											} else if (I8253_CT0_COUNT>=n) {
													CT0_prumer+=I8253_CT0_OUT*n;
											} else if (I8253_CT0_COUNT>=0) {
															CT0_prumer+=I8253_CT0_OUT*I8253_CT0_COUNT;
															I8253_CT0_OUT=1;
															CT0_prumer+=(n-I8253_CT0_COUNT);
															I8253_CT0_COUNT+=I8253_CT0_SET;
											}
										} else {
											if (I8253_CT0_COUNT>=I8253_CT0_SET/2+n) {
											} else if (I8253_CT0_COUNT>=I8253_CT0_SET/2) {
													I8253_CT0_OUT=0;
											} else if (I8253_CT0_COUNT>=n) {
											} else if (I8253_CT0_COUNT>=0) {
															I8253_CT0_OUT=1;
															I8253_CT0_COUNT+=I8253_CT0_SET;
											}
										}
										I8253_CT0_COUNT-=n;
                }
                break;
            default:
                CT0_prumer+=(I8255_C&1)*I8253_CT0_OUT*n;
                break;
		}
}
//#define WrROM_Exxx	NoWr
void WrROM_Exxx(register word Addr, register byte Value) {
	if ((MZ_mode==MZ_700)&&(!MEM_PROHIBITED)&&(Addr<=0xE008)) switch (Addr) {
		case	0xE002:	ct0_update();I8255_C=Value&0x0F;break;
 		case	0xE003: if ((Value&0x80)==0) {	//nastaveni bitu brany C
							register byte bit=(Value>>1)&0x7;
							if (bit<=3) {
								bit=(1<<bit);
                if (bit==0) ct0_update();
								if (Value&1) I8255_C|=bit;
								else I8255_C&=~bit;
							}
						}
						break;
		case	0xE004:	
					if (!I8253_CT0_HL) {I8253_CT0_SET=Value;I8253_CT0_HL=1;} else {
							ct0_update();
							if (I8253_CT0_HL==1) {
								I8253_CT0_COUNT=(I8253_CT0_SET|=(((word)Value)<<8));
								I8253_CT0_HL=0;
								I8253_CT0_MODE&=0x07;
                ct0_newstate(I8253_CT0_MODE);
							} else if (I8253_CT0_HL==0x10) {
								I8253_CT0_SET=(I8253_CT0_SET&0xFF00)|((word)Value);
								I8253_CT0_COUNT=I8253_CT0_SET;
								I8253_CT0_MODE&=0x07;
                ct0_newstate(I8253_CT0_MODE);
							} else if (I8253_CT0_HL==0x20) {
								I8253_CT0_SET=(I8253_CT0_SET&0x00FF)|(((word)Value)<<8);
								I8253_CT0_COUNT=I8253_CT0_SET;
								I8253_CT0_MODE&=0x07;
                ct0_newstate(I8253_CT0_MODE);
							}
//							tiskni_short(29,22,I8253_CT0_SET);
						}
						break;
		case	0xE007:	switch (Value&0xC0) {
							case 0x00: 
								ct0_update();
								I8253_CT0_HL=Value&0x30;
								I8253_CT0_MODE=(Value>>1)&7;if (I8253_CT0_MODE>5) I8253_CT0_MODE&=0x03;
								I8253_CT0_MODE+=8;
								if (I8253_CT0_HL==0x30) I8253_CT0_HL=0;
								ct0_newstate(I8253_CT0_MODE);
								break;
						}
						break;
		case	0xE008: I8253_GATE0=Value&1;
						break;
	}
  EXTI->PR = 0xFC00;
	return;
}

/*const*/ P_WRITE WrROM[2][16]={
/*700 mode*/	{NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,WrVRAM_Cxxx,WrVRAM_Dxxx,WrROM_Exxx,NoWr},
/*800 mode*/	{NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,NoWr,WrVRAM_89xx,WrVRAM_89xx,WrVRAM_ABxx,WrVRAM_ABxx,NoWr,NoWr,NoWr,NoWr}
};

/*const*/ P_WRITE WrRAM[2][16]={
/*700 mode*/	{WrRAM_0xxx,WrRAM_1xxx,WrRAM_2xxx,WrRAM_3xxx,WrRAM_4xxx,WrRAM_5xxx,WrRAM_6xxx,WrRAM_7xxx,WrRAM_8xxx,WrRAM_9xxx,WrRAM_Axxx,WrRAM_Bxxx,WrRAM_Cxxx,WrRAM_Dxxx,WrRAM_Exxx,WrRAM_Fxxx},
/*800 mode*/	{WrRAM_0xxx,WrRAM_1xxx,WrRAM_2xxx,WrRAM_3xxx,WrRAM_4xxx,WrRAM_5xxx,WrRAM_6xxx,WrRAM_7xxx,WrRAM_8xxx,WrRAM_9xxx,WrRAM_Axxx,WrRAM_Bxxx,WrRAM_Cxxx,WrRAM_Dxxx,WrRAM_Exxx,WrRAM_Fxxx}
};

/*__forceinline */void noact(void) {
//	EXWAIT_OFF();
  EXTI->PR = 0xFC00;
	return;
}

extern void tab800_init_palete(void);

void noport(register unsigned char Port) {
	EXWAIT_OFF();
  EXTI->PR = 0xFC00;
}

void iowrEMU(register unsigned char Port) {
	EMU_data=GET_MZDATA();
	EMU_addr=Port&3;
	if ((EMU_addr==0)&&(EMU_data==0x03)) {	//cmdSTSR lze rovnou, bez cekani na uvolneni main loopu
		emu_MZFREPO_write(EMU_addr,(unsigned int *)&EMU_data);
		EXWAIT_OFF();
		EXTI->PR = 0xFC00;
		return;
	}
  set_event(EVENT_EMU_WRITE);
	EXTI->PR = 0xFC00;
}

void iowrFDD(register unsigned char Port) {
  FDD_event_addr=Port&7;
  FDD_event_data=GET_MZDATA();
  set_event(EVENT_FDD_WRITE);
	EXTI->PR = 0xFC00;
}

void iowrSIO(register unsigned char Port) {
	SIO_event_data=GET_MZDATA();
	SIO_event_addr=Port&3;
  set_event(EVENT_SIO_WRITE);
	EXTI->PR = 0xFC00;
}

void iowrCC(register unsigned char Port) {
	EXWAIT_OFF();
  EXTI->PR = 0xFC00;
	switch (Port) {
		case 0xCC:	{
						register unsigned char Value=GET_MZDATA();
						WRITE_CMD=(Value>>5);
						WRITE_FRAME=(Value>>4)&1;
						WRITE_PLANE1=(Value&1);
						WRITE_PLANE2=(Value&2);
						WRITE_PLANE3=(Value&4);
						WRITE_PLANE4=(Value&8);
					}
					break;
		case 0xCE:	{
						register unsigned char Value=GET_MZDATA();SCROLL_SOF = 0;// SCROLL_SW = 0; SCROLL_SSA = 0; SCROLL_SEA = 125<<6; SCROLL_SW = 125<<6;
						if ((Value&0x08)&&(Write[14]==WrROM[MZ_700][14])) {
							Write[13]=WrROM[MZ_700][13];
						} else {
							Write[13]=WrRAM[MZ_700][13];
						}
						if ((MZ_mode==MZ_700)&&((Value&0x08)==0)) { //prechod z MZ700 do MZ800
							if (Write[12]==WrROM[MZ_700][12]) {
                                int i;
                                Write[12]=WrRAM[MZ_700][12];
                                for(i=8;i<12;i++) {
                                    Write[i]=WrROM[MZ_800][i];
                                }
							}
						}
						if ((MZ_mode==MZ_800)&&(Value&0x08)) {  //prechod z MZ800 do MZ700
							if (Write[8]==WrROM[MZ_800][8]) {
                                int i;
                                for(i=8;i<12;i++) {
                                    Write[i]=WrRAM[MZ_700][i];
                                }
                                Write[12]=WrROM[MZ_700][12];
							}
						}

						set_mode((Value&0x08)?MZ_700:MZ_800);MZ_graphic_mode=Value;
//						tab800_init_palete();
					}
					break;
		case 0xCF:
				{
					register unsigned char Value=GET_MZDATA();
					switch (GET_MZADDR()>>8) {
						case 1:	SCROLL_SOF = (SCROLL_SOF&0x1800)|(((unsigned short)Value)<<3);break;
						case 2:	SCROLL_SOF = (SCROLL_SOF&0x07F8)|(((unsigned short)(Value&0x03))<<11);break;
						case 3:	SCROLL_SW = (((unsigned short)(Value&0x7F))<<6);break;
						case 4:	SCROLL_SSA = (((unsigned short)(Value&0x7F))<<6);break;
						case 5:	SCROLL_SEA = (((unsigned short)(Value&0x7F))<<6);break;
						case 6:	MZ_BORDER = Value&0x0F; break;	//border MZ800
					}
				}
				break;
	}
}

void iowrD0(register word Port, register byte Value) {
	EXWAIT_OFF();
  EXTI->PR = 0xFC00;
	switch (Port&0xFF) {
		case 0xD2:	ct0_update();I8255_C=Value&0x0F;break;
		case 0xD3:	if ((Value&0x80)==0) {	//nastaveni bitu brany C
							register byte bit=(Value>>1)&0x7;
							if (bit<=3) {
								bit=(1<<bit);
                if (bit==0) ct0_update();
								if (Value&1) I8255_C|=bit;
								else I8255_C&=~bit;
							}
						} else {
							if (Value&0x08) I8255_C|=0xF0;
							else I8255_C&=0x0F;
							if (Value&0x01) I8255_C|=0x0F;
							else I8255_C&=0xF0;
						}
						break;
	}
}

void iowrD4(register word Port, register byte Value) {
	EXWAIT_OFF();
  EXTI->PR = 0xFC00;
	switch (Port&0xFF) {
		case 0xD4:	//printf("CT0 OUT %02X\r\n",Value);
					if (!I8253_CT0_HL) {I8253_CT0_SET=Value;I8253_CT0_HL=1;} else {
							ct0_update();
							if (I8253_CT0_HL==1) {
								I8253_CT0_COUNT=(I8253_CT0_SET|=(((word)Value)<<8));
								I8253_CT0_HL=0;
								I8253_CT0_MODE&=0x07;
								ct0_newstate(I8253_CT0_MODE);
							} else if (I8253_CT0_HL==0x10) {
								I8253_CT0_SET=(I8253_CT0_SET&0xFF00)|((word)Value);
								I8253_CT0_COUNT=I8253_CT0_SET;
								I8253_CT0_MODE&=0x07;
								ct0_newstate(I8253_CT0_MODE);
							} else if (I8253_CT0_HL==0x20) {
								I8253_CT0_SET=(I8253_CT0_SET&0x00FF)|(((word)Value)<<8);
								I8253_CT0_COUNT=I8253_CT0_SET;
								I8253_CT0_MODE&=0x07;
								ct0_newstate(I8253_CT0_MODE);
							}
						}
						break;
		case 0xD7:	switch (Value&0xC0) {
							case 0x00: /*printf("CT0 CMD %02X\r\n",Value);*/
                ct0_update();
								I8253_CT0_HL=Value&0x30;
								I8253_CT0_MODE=(Value>>1)&7;if (I8253_CT0_MODE>5) I8253_CT0_MODE&=0x03;
								I8253_CT0_MODE+=8;
								if (I8253_CT0_HL==0x30) I8253_CT0_HL=0;
								ct0_newstate(I8253_CT0_MODE);
								break;
						}
						break;
	}
}

void iowrE0(register unsigned char Port) {
	EXWAIT_OFF();
  EXTI->PR = 0xFC00;
	switch (Port) {
		case 0xE1:
				Write[13]=WrRAM[MZ_700][13];
				Write[14]=WrRAM[MZ_700][14];
				break;
		case 0xE3:
				if (MZ_mode==MZ_700) {
					Write[13]=WrROM[MZ_700][13];
				}
				Write[14]=WrROM[MZ_700][14];
				break;
	}
}

void iowrE4(register unsigned char Port) {
	EXWAIT_OFF();
  EXTI->PR = 0xFC00;
	switch (Port) {
		case 0xE4:
				if (MZ_mode==MZ_800) {
						Write[8]=WrROM[MZ_800][8];
						Write[9]=WrROM[MZ_800][9];
						Write[10]=WrROM[MZ_800][10];
						Write[11]=WrROM[MZ_800][11];
						Write[12]=WrRAM[MZ_700][12];
						Write[13]=WrRAM[MZ_700][13];
				}
				if (MZ_mode==MZ_700) {
						Write[8]=WrRAM[MZ_700][12];
						Write[9]=WrRAM[MZ_700][12];
						Write[10]=WrRAM[MZ_700][12];
						Write[11]=WrRAM[MZ_700][12];
						Write[12]=WrRAM[MZ_700][12];
						Write[13]=WrROM[MZ_700][13];
				}
				Write[14]=WrROM[MZ_700][14];
				MEM_PROHIBITED=0;
				break;
		case 0xE5:	//TODO prohibited
				MEM_PROHIBITED=1;
				break;
		case 0xE6:
				MEM_PROHIBITED=0;
				break;
	}
}

void iowrF0(register unsigned char Port) {
	switch (Port) {
		case 0xF0:  {
						register unsigned char Value=GET_MZDATA();
						tab800_set_palete(Value>>4,Value&15);
						//MZ_BORDER=Value&15;	//TODO smazat
					//puts_shell("F0=");puthex(Value,4);puts_shell("\r\n");
					}
					break;

		case 0xF2:	{	//PSG
						register unsigned char Value=GET_MZDATA();
						if (Value&0x80) {
							if (Value&0x10) psg_set_volume((Value>>5)&0x3,Value&0xF);
							LAST_F2 = Value;
							if ((Value&0x70)==0x60) psg_set_period(3,(Value&0x0F));
						} else {
							if (LAST_F2&0x10) {
								psg_set_volume((LAST_F2>>5)&0x3,Value&0xF);
							}
							else psg_set_period((LAST_F2>>5)&0x3,((Value<<4)|(LAST_F2&0xF))&0x3FF);
						}
					}
					break;
	}
	EXWAIT_OFF();
	EXTI->PR = 0xFC00;
}

void iowrE8(register unsigned char Port) {
	switch (Port) {
		case	0xE9:
		case	0xEA:
		case	0xEB:
		  RAM_event_addr=GET_MZADDR();	//B.N. 20.3.2012 - xxEB port
		  RAM_event_data=GET_MZDATA();
		  set_event(EVENT_RAM_WRITE);
			break;
		default:
			EXWAIT_OFF();
			break;
	}
	EXTI->PR = 0xFC00;
}

void iowrF8(register unsigned char Port) {
	switch (Port) {
		case	0xF8:
		case	0xFA:
		  RAM_event_addr=Port;
		  RAM_event_data=GET_MZDATA();
		  set_event(EVENT_RAM_WRITE);
			break;
		default:
			EXWAIT_OFF();
			break;
	}
	EXTI->PR = 0xFC00;
}

void iowrZX(register unsigned char Port) {
/*	if ((GET_MZADDR()==(((int)'Z')<<8))&&(GET_MZDATA()=='X')) {
		ZX_EMUL=1;
	} else {
		ZX_EMUL=0;
	}*/
	EXWAIT_OFF();
	EXTI->PR = 0xFC00;
}

void iowrQD(register unsigned char Port) {
  QD_event_addr=Port&3;
  QD_event_data=GET_MZDATA();
  set_event(EVENT_QD_WRITE);
	EXTI->PR = 0xFC00;
}

void iordE0(register unsigned char Port) {
	EXWAIT_OFF();
  EXTI->PR = 0xFC00;
	switch (Port) {
		case 0xE0:
				if (MZ_mode==MZ_700) {
					Write[12]=WrROM[MZ_700][12];
				}
				if (MZ_mode==MZ_800) {
					Write[8]=WrROM[MZ_800][8];
					Write[9]=WrROM[MZ_800][9];
					Write[10]=WrROM[MZ_800][10];
					Write[11]=WrROM[MZ_800][11];
				}
				break;

		case 0xE1:
				if (MZ_mode==MZ_700) {
					Write[12]=WrRAM[MZ_700][12];
				}
				if (MZ_mode==MZ_800) {
					Write[8]=WrRAM[MZ_800][8];
					Write[9]=WrRAM[MZ_800][9];
					Write[10]=WrRAM[MZ_800][10];
					Write[11]=WrRAM[MZ_800][11];
				}
				break;
	}
}

void iordQD(register unsigned char Port) {
	//prepnout na vystup, nastavit hodnotu
  QD_event_addr=Port&3;
	set_event(EVENT_QD_READ);
  EXTI->PR = 0xFC00;
}

void iordFDD(register unsigned char Port) {
	switch (Port) {
		case 0x58:
		case 0x59:
		case 0x5A:
		case 0x5B:
		case 0xD8:
		case 0xD9:
		case 0xDA:
		case 0xDB:
			//prepnout na vystup, nastavit hodnotu
		  FDD_event_addr=Port&7;
			set_event(EVENT_FDD_READ);
			break;
		default:
			EXWAIT_OFF();
			break;
	}
  EXTI->PR = 0xFC00;
}

void iordE8(register unsigned char Port) {
	unsigned char io_data;
	switch (Port) {
		case 0xEA:
			if (MZDATA_BUSY) {
				RAM_event_addr=GET_MZADDR();
				set_event(EVENT_RAM_READ);
			} else if (emu_RAMDISC_read_interrupt(&io_data)) {
				MZDATA_INT(io_data);
			} else {
				RAM_event_addr=GET_MZADDR();
				set_event(EVENT_RAM_READ);
			}
			break;
		case 0xEC:
			RAM_event_addr=GET_MZADDR();
			set_event(EVENT_RAM_READ);
			break;
		default:
			EXWAIT_OFF();
			break;
	}
  EXTI->PR = 0xFC00;
}

#define IRC(a)	((((a)&2)?(a)^1:(a))&0x03)
#define	K_RESET		4
extern char kb_state;

void iord70(register unsigned char Port) {
	switch (Port&0xFF) {
		case 0x71:
			if ((mouse==0)&&((kb_state&K_RESET)==0)) {kb_state=K_RESET;keyb_timeout=200000;}
			if (mouse_x_final>0) {
				mouse_x_final--;
				mouse_x_count--;
			} else if (mouse_x_final<0) {
				mouse_x_final++;
				mouse_x_count++;
			}
			if (mouse_y_final>0) {
				mouse_y_final--;
				mouse_y_count++;
			} else if (mouse_y_final<0) {
				mouse_y_final++;
				mouse_y_count--;
			}
			ATARI_MOUSE=(ATARI_MOUSE&0xF0)|(IRC((mouse_y_count>>1)&3)<<2)|(IRC((mouse_x_count>>1)&3));
			MZDATA_INT(ATARI_MOUSE);
		default:
			EXWAIT_OFF();
			break;
	}
  EXTI->PR = 0xFC00;
}

/*void iordF0(register unsigned char Port) {
	switch (Port&0xFF) {
		case 0xF3:
			if ((mouse==0)&&((kb_state&K_RESET)==0)) {kb_state=K_RESET;keyb_timeout=200000;}
			if (mouse_x_final>0) {
				mouse_x_final--;
				mouse_x_count--;
			} else if (mouse_x_final<0) {
				mouse_x_final++;
				mouse_x_count++;
			}
			if (mouse_y_final>0) {
				mouse_y_final--;
				mouse_y_count++;
			} else if (mouse_y_final<0) {
				mouse_y_final++;
				mouse_y_count--;
			}
			ATARI_MOUSE=(ATARI_MOUSE&0xF0)|(IRC((mouse_y_count>>2)&3)<<2)|(IRC((mouse_x_count>>2)&3));
			MZDATA_INT(ATARI_MOUSE);
		default:
			EXWAIT_OFF();
			break;
	}
  EXTI->PR = 0xFC00;
}*/

void iordF8(register unsigned char Port) {
	unsigned char io_data;
	switch (Port) {
		case 0xF8:
			RAM_event_addr=GET_MZADDR();
			set_event(EVENT_RAM_READ);
			break;
		case 0xF9:
			if (MZDATA_BUSY) {
				RAM_event_addr=GET_MZADDR();
				set_event(EVENT_RAM_READ);
			} else if (emu_RAMDISC_read_interrupt(&io_data)) {
				MZDATA_INT(io_data);
			} else {
				RAM_event_addr=GET_MZADDR();
				set_event(EVENT_RAM_READ);
			}
			break;
		default:
			EXWAIT_OFF();
			break;
	}
  EXTI->PR = 0xFC00;
}

void iordEMU(register unsigned char Port) {
	unsigned char io_data;
	switch (Port&3) {
		case 1:	//data
			if (MZDATA_BUSY) {
				EMU_addr=Port&3;
				set_event(EVENT_EMU_READ);
			} else if (emu_MZFREPO_read_interrupt(&io_data)) {
				MZDATA_INT(io_data);
			} else {
				EMU_addr=Port&3;
				set_event(EVENT_EMU_READ);
			}
			break;
		default:
			EMU_addr=Port&3;
			set_event(EVENT_EMU_READ);
		break;
	}
  EXTI->PR = 0xFC00;
}

void iordSIO(register unsigned char Port) {
	SIO_event_addr=Port&3;
	set_event(EVENT_SIO_READ);
  EXTI->PR = 0xFC00;
}

static void * IOWREnabledLONG[(256/4)+3+(256/4)]=
{
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)(&GET_MZADDR()),
	(void *)(&(GPIOC->BSRRL)),
	(void *)(&(EXTI->PR)),

	noport,//iowrZX,	/* 00-03 */
	noport,	/* 04-07 */
	noport,	/* 08-0B */
	noport,	/* 0C-0F */

	noport,	/* 10-13 */
	noport,	/* 14-17 */
	noport,	/* 18-1B */
	noport,	/* 1C-1F */

	noport,	/* 20-23 */
	noport,	/* 24-27 */
	noport,	/* 28-2B */
	noport,	/* 2C-2F */

	noport,	/* 30-33 */
	noport,	/* 34-37 */
	noport,	/* 38-3B */
	noport,	/* 3C-3F */

	noport,	/* 40-43 */
	noport,	/* 44-47 */
	noport,	/* 48-4B */
	noport,	/* 4C-4F */

	iowrEMU,	/* 50-53 */
	noport,	/* 54-57 */
	iowrFDD,	/* 58-5B */
	iowrFDD,	/* 5C-5F */

	noport,	/* 60-63 */
	noport,	/* 64-67 */
	noport,	/* 68-6B */
	noport,	/* 6C-6F */

	noport,	/* 70-73 */
	noport,	/* 74-77 */
	noport,	/* 78-7B */
	noport,	/* 7C-7F */

	noport,	/* 80-83 */
	noport,	/* 84-87 */
	noport,	/* 88-8B */
	noport,	/* 8C-8F */

	noport,	/* 90-93 */
	noport,	/* 94-97 */
	noport,	/* 98-9B */
	noport,	/* 9C-9F */

	noport,	/* A0-A3 */
	noport,	/* A4-A7 */
	noport,	/* A8-AB */
	noport,	/* AC-AF */

	iowrSIO,	/* B0-B3 */
	noport,	/* B4-B7 */
	noport,	/* B8-BB */
	noport,	/* BC-BF */

	noport,	/* C0-C3 */
	noport,	/* C4-C7 */
	noport,	/* C8-CB */
	iowrCC,	/* CC-CF */

	iowrD0,	/* D0-D3 */
	iowrD4,	/* D4-D7 */
	iowrFDD,	/* D8-DB */
	iowrFDD,	/* DC-DF */

	iowrE0,	/* E0-E3 */
	iowrE4,	/* E4-E7 */
	iowrE8,	/* E8-EB */
	noport,	/* EC-EF */

	iowrF0,	/* F0-F3 */
	iowrQD,	/* F4-F7 */
	iowrF8,	/* F8-FB */
	noport	/* FC-FF */

};
static void * IORDEnabledLONG[(256/4)+3+(256/4)]=
{
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,(void *)0x01010101,
	(void *)(&GET_MZADDR()),
	(void *)(&(GPIOC->BSRRL)),
	(void *)(&(EXTI->PR)),

	noport,	/* 00-03 */
	noport,	/* 04-07 */
	noport,	/* 08-0B */
	noport,	/* 0C-0F */

	noport,	/* 10-13 */
	noport,	/* 14-17 */
	noport,	/* 18-1B */
	noport,	/* 1C-1F */

	noport,	/* 20-23 */
	noport,	/* 24-27 */
	noport,	/* 28-2B */
	noport,	/* 2C-2F */

	noport,	/* 30-33 */
	noport,	/* 34-37 */
	noport,	/* 38-3B */
	noport,	/* 3C-3F */

	noport,	/* 40-43 */
	noport,	/* 44-47 */
	noport,	/* 48-4B */
	noport,	/* 4C-4F */

	iordEMU,	/* 50-53 */
	noport,	/* 54-57 */
	iordFDD,	/* 58-5B */
	iordFDD,	/* 5C-5F */

	noport,	/* 60-63 */
	noport,	/* 64-67 */
	noport,	/* 68-6B */
	noport,	/* 6C-6F */

	iord70,	/* 70-73 */
	noport,	/* 74-77 */
	noport,	/* 78-7B */
	noport,	/* 7C-7F */

	noport,	/* 80-83 */
	noport,	/* 84-87 */
	noport,	/* 88-8B */
	noport,	/* 8C-8F */

	noport,	/* 90-93 */
	noport,	/* 94-97 */
	noport,	/* 98-9B */
	noport,	/* 9C-9F */

	noport,	/* A0-A3 */
	noport,	/* A4-A7 */
	noport,	/* A8-AB */
	noport,	/* AC-AF */

	iordSIO,	/* B0-B3 */
	noport,	/* B4-B7 */
	noport,	/* B8-BB */
	noport,	/* BC-BF */

	noport,	/* C0-C3 */
	noport,	/* C4-C7 */
	noport,	/* C8-CB */
	noport,	/* CC-CF */

	noport,	/* D0-D3 */
	noport,	/* D4-D7 */
	iordFDD,	/* D8-DB */
	iordFDD,	/* DC-DF */

	iordE0,	/* E0-E3 */
	noport,	/* E4-E7 */
	iordE8,	/* E8-EB */
	noport,	/* EC-EF */

	noport,	/* F0-F3 */
	iordQD,	/* F4-F7 */
	iordF8,	/* F8-FB */
	noport	/* FC-FF */

};

#define IOWREnabled 	((unsigned char *)IOWREnabledLONG)
#define IORDEnabled 	((unsigned char *)IORDEnabledLONG)

void IO_tabs_init(void) {
	int i;
	for (i=0;i<256;i++)	{
		IOWREnabled[i]=1;
		IORDEnabled[i]=1;
	}
}

typedef void (*fooport)(register unsigned char Port);

void iowr(void) {
	register unsigned char Port;
	*((unsigned short *)((int)IOWREnabledLONG[65]+2))=(1<<0);
	Port=*((unsigned char *)IOWREnabledLONG[64]);		//register unsigned char Port=GET_MZADDR();
	if ((*((unsigned short *)IOWREnabledLONG[65])=IOWREnabled[Port])!=0) {	//if ((GPIOC->BSRRL=IOWREnabled[Port])!=0) {	//zaroven provede EXWAIT_OFF
		/*EXTI->PR*/ *((unsigned short *)IOWREnabledLONG[66]) = 0xFC00;
		return;
	}
	((fooport)IOWREnabledLONG[67+(Port>>2)])(Port);
}

void iord(void) {
	register unsigned char Port;
	*((unsigned short *)((int)IORDEnabledLONG[65]+2))=(1<<0);
	Port=*((unsigned char *)IORDEnabledLONG[64]);		//register unsigned char Port=GET_MZADDR();
	if ((*((unsigned short *)IORDEnabledLONG[65])=IORDEnabled[Port])!=0) {	//if ((GPIOC->BSRRL=IORDEnabled[Port])!=0) {	//zaroven provede EXWAIT_OFF
		/*EXTI->PR*/ *((unsigned short *)IORDEnabledLONG[66]) = 0xFC00;
		return;
	}
	((fooport)IORDEnabledLONG[67+(Port>>2)])(Port);
}

void memwr(void) {
	register unsigned short Addr=GET_MZADDR();
//	register unsigned short Data=GET_MZDATA();
////	EXWAIT_OFF();
//	Write[Addr>>12](Addr,Data);
	Write[Addr>>12](Addr,GET_MZDATA());
//!!!  EXTI->PR = 0xFC00;
}

#ifdef Z80_M1_SPY
void m1(void) {
	AddrM1=GET_MZADDR();
	EXWAIT_OFF();
  EXTI->PR = 0xFC00;
}
#endif

typedef void (*foo)(void);
static foo pole[32+2]={
	noact,
	noact,
	noact,
	noact,

	noact,
	noact,
	noact,
	noact,

	noact,
	noact,
#ifdef Z80_M1_SPY
	m1,			//MREQ + RD + M1
	m1,			//RD + M1
#else
	noact,
	noact,
#endif

	noact,
	noact,
#ifdef Z80_M1_SPY
	m1,			//MREQ + M1
	m1,			//M1
#else
	noact,
	noact,
#endif

	noact,
	noact,
	noact,
	noact,

	noact,
	iowr,		//IORQ + WR
	memwr,	//MREQ + WR
	noact,

	noact,
	iord,		//IORQ + RD
	noact,	//MREQ + RD
	noact,	//RD

	noact,
	noact,	//IORQ
	noact,	//MREQ
	noact,

	(foo)(&GET_MZBUS()),
	(foo)(&(GPIOC->BSRRH))
};

void EXTI15_10_IRQHandler()
{
	/*register unsigned short mzbus=*((unsigned short *)(pole[32]));*/			register unsigned short mzbus=*((unsigned short *)(&(GET_MZBUS())));
//	*(unsigned short *)(pole[33])=(1<<0);															//	EXWAIT_ON();
	pole[mzbus>>11]();
/*		switch(mzbus>>11) {
			case 0x15:	//IOWR
				{
					register unsigned char Port;
					*((unsigned short *)((int)IOWREnabledLONG[65]+2))=(1<<0);
					Port=*((unsigned char *)IOWREnabledLONG[64]);		//register unsigned char Port=GET_MZADDR();
					if ((*((unsigned short *)IOWREnabledLONG[65])=IOWREnabled[Port])!=0) {	//if ((GPIOC->BSRRL=IOWREnabled[Port])!=0) {	//zaroven provede EXWAIT_OFF
						EXTI->PR = 0xFC00;
						return;
					}
					((fooport)IOWREnabledLONG[66+(Port>>2)])(Port);
				}
				break;
			case 0x16:	//MEMWR
				{
					register unsigned short Addr=GET_MZADDR();
					register unsigned short Data=GET_MZDATA();
				//	EXWAIT_OFF();
					EXTI->PR = 0xFC00;
					Write[Addr>>12](Addr,Data);
				}
				break;
			case 0x19:	//IORD
				{
					register unsigned char Port;
					*((unsigned short *)((int)IORDEnabledLONG[65]+2))=(1<<0);
					Port=*((unsigned char *)IORDEnabledLONG[64]);		//register unsigned char Port=GET_MZADDR();
					if ((*((unsigned short *)IORDEnabledLONG[65])=IORDEnabled[Port])!=0) {	//if ((GPIOC->BSRRL=IORDEnabled[Port])!=0) {	//zaroven provede EXWAIT_OFF
						EXTI->PR = 0xFC00;
						return;
					}
					((fooport)IORDEnabledLONG[66+(Port>>2)])(Port);
				}
				break;
			default:
				EXTI->PR = 0xFC00;
				break;
		}*/
/*	if ((mzbus&(1<<12))==0) {					//io
		if ((mzbus&(1<<13))==0) {					//iord
			register unsigned char Port=*((unsigned char *)IORDEnabledLONG[64]);		//register unsigned char Port=GET_MZADDR();
			if ((*((unsigned short *)IORDEnabledLONG[65])=IORDEnabled[Port])!=0) {	//if ((GPIOC->BSRRL=IORDEnabled[Port])!=0) {	//zaroven provede EXWAIT_OFF
				EXTI->PR = 0xFC00;
				return;
			}
			((fooport)IORDEnabledLONG[66+(Port>>2)])(Port);
			return;
		} else if ((mzbus&(1<<14))==0) {	//iowr
			register unsigned char Port=*((unsigned char *)IOWREnabledLONG[64]);		//register unsigned char Port=GET_MZADDR();
			if ((*((unsigned short *)IOWREnabledLONG[65])=IOWREnabled[Port])!=0) {	//if ((GPIOC->BSRRL=IOWREnabled[Port])!=0) {	//zaroven provede EXWAIT_OFF
				EXTI->PR = 0xFC00;
				return;
			}
			((fooport)IOWREnabledLONG[66+(Port>>2)])(Port);
			return;
		}
	} else if ((mzbus&(9<<11))==0) {	//memwr
		register unsigned short Addr=GET_MZADDR();
		register unsigned short Data=GET_MZDATA();
		EXWAIT_OFF();
		EXTI->PR = 0xFC00;
		Write[Addr>>12](Addr,Data);
		return;
	}
	EXWAIT_OFF();
	EXTI->PR = 0xFC00;*/
}

int newline;

void EXTI9_5_IRQHandler()
{
/////	TIM2->CNT=4544;	//zaciname na radce 1 (poradove 20)
	TIM2->CNT=PAL_VIDEO_RESET_POSITION;	//zaciname na radce 1 (poradove 50)
	TIM5->CNT=0;			//zacatek radku
	newline=50;			//cislo radku
	MZDATA_OFF();
	EXWAIT_OFF();
	mzint_Init();	//EXINT_OFF
	emu_FDC_write(7,(unsigned int*)"\0\0\0\0");	//FDC INT OFF
	EXRESET_OFF();
	reset_event(EVENT_RAM_READ);
	reset_event(EVENT_RAM_WRITE);
	reset_event(EVENT_QD_READ);
	reset_event(EVENT_QD_WRITE);
	reset_event(EVENT_FDD_READ);
	reset_event(EVENT_FDD_WRITE);
	reset_event(EVENT_SIO_READ);
	reset_event(EVENT_SIO_WRITE);
	reset_event(EVENT_EMU_READ);
	reset_event(EVENT_EMU_WRITE);
	GDG_init();
	EXTI->PR = 0x0100;
}

void GDG_init() {
				int i;
				for (i=0;i<16;i++)	{
					//WriteM[MZ_700][i]=NoWr;
					//WriteM[MZ_800][i]=NoWr;
					Write[i]=NoWr;
				}
				//novinka pro ZX emulaci, zdravim Spectristy, maji usetrenou praci ;-)
/*				Write[4]=WrZX_VRAM_45xx;Write[5]=WrZX_VRAM_45xx;
				//
				WriteM[MZ_800][8]=WrROM[MZ_800][8];
				WriteM[MZ_800][9]=WrROM[MZ_800][9];
				WriteM[MZ_800][10]=WrRAM[MZ_800][10];
				WriteM[MZ_800][11]=WrRAM[MZ_800][11];
				WriteM[MZ_800][12]=WrRAM[MZ_800][12];
				WriteM[MZ_800][13]=WrRAM[MZ_800][13];
				WriteM[MZ_700][8]=WrRAM[MZ_700][8];
				WriteM[MZ_700][9]=WrRAM[MZ_700][9];
				WriteM[MZ_700][10]=WrRAM[MZ_700][10];
				WriteM[MZ_700][11]=WrRAM[MZ_700][11];
				WriteM[MZ_700][12]=WrRAM[MZ_700][12];
				WriteM[MZ_700][13]=WrROM[MZ_700][13];*/
				if (((GPIOC->IDR)&(1<<13))||(MK3b)) {	//SW1 on
					MZ_graphic_mode=0x08;
					set_mode(MZ_700);
					Write[8]=WrRAM[MZ_700][12];
					Write[9]=WrRAM[MZ_700][12];
					Write[10]=WrRAM[MZ_700][12];
					Write[11]=WrRAM[MZ_700][12];
					Write[12]=WrRAM[MZ_700][12];
					Write[13]=WrROM[MZ_700][13];
				} else {										//SW1 off
					MZ_graphic_mode=0x00;
					set_mode(MZ_800);
					Write[8]=WrROM[MZ_800][8];
					Write[9]=WrROM[MZ_800][9];
					Write[10]=WrROM[MZ_800][10];
					Write[11]=WrROM[MZ_800][11];
					Write[12]=WrRAM[MZ_700][12];
					Write[13]=WrRAM[MZ_700][13];
				}
				Write[14]=WrROM[MZ_700][14];
				MEM_PROHIBITED=0;

				SCROLL_SOF = 0;// SCROLL_SW = 0; SCROLL_SSA = 0; SCROLL_SEA = 125<<6; SCROLL_SW = 125<<6;
				tab800_init_palete();
				MZ_BORDER = 0;
//				ZX_EMUL = 0;
				ATARI_MOUSE=0xFF;
				if ((mouse==0)&&((kb_state&K_RESET)==0)) {kb_state=K_RESET;keyb_timeout=200000;}
				I8253_CT0_HL=0;
				I8253_CT0_SET=0;
				I8253_CT0_COUNT=0;
				I8253_CT0_MODE=0;
				I8253_CT0_OUT=0;
				I8253_GATE0=0;
				I8255_C=0xFF;
}

void IOWRPortListAdd (int len, unsigned char* ports)
{
	while (len--) {
		IOWREnabled[ports[len]]=0;
	}
}

void IORDPortListAdd (int len, unsigned char* ports)
{
	while (len--) {
		IORDEnabled[ports[len]]=0;
	}
}

int IOGetEnabled(unsigned char n) {
	if ((IOWREnabled[n]==0)||(IORDEnabled[n]==0)) return 1;
	return 0;
}

