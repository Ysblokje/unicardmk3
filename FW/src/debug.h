/*
 * debug.h
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    MZ-800 Unicard is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MZ-800 Unicard; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 *  Created on: 11.10.2010
 *      Author: ladmanj
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#define DBGNON		0
#define DBGFAT		1
#define DBGERR		2
#define DBGWAR		4
#define DBGINF		8

#ifndef DBGLEVEL
#define DBGLEVEL	0
#endif

#ifndef USE_DBG_PRN
#warning using standard printf function, replace it by #_define USE_DBG_PRN your_prinft
#define USE_DBG_PRN	printf
#endif


#define DBGNON__DEBUGPRINTF__(...)

#define __DEBUGPRINTF__X(level,...)	{ USE_DBG_PRN("%s\t%s\t%d: ", #level ,__FILE__, __LINE__); USE_DBG_PRN(__VA_ARGS__); }

#if (DBGLEVEL & DBGFAT)
#define DBGFAT__DEBUGPRINTF__(...)	__DEBUGPRINTF__X(DBGFAT,__VA_ARGS__)
#else
#define DBGFAT__DEBUGPRINTF__(...)
#endif

#if (DBGLEVEL & DBGERR)
#define DBGERR__DEBUGPRINTF__(...)	__DEBUGPRINTF__X(DBGERR,__VA_ARGS__)
#else
#define DBGERR__DEBUGPRINTF__(...)
#endif

#if (DBGLEVEL & DBGWAR)
#define DBGWAR__DEBUGPRINTF__(...)	__DEBUGPRINTF__X(DBGWAR,__VA_ARGS__)
#else
#define DBGWAR__DEBUGPRINTF__(...)
#endif

#if (DBGLEVEL & DBGINF)
#define DBGINF__DEBUGPRINTF__(...)	__DEBUGPRINTF__X(DBGINF,__VA_ARGS__)
#else
#define DBGINF__DEBUGPRINTF__(...)
#endif

#define DBGPRINTF(level,...)		level##__DEBUGPRINTF__(__VA_ARGS__)

#endif /* DEBUG_H_ */
