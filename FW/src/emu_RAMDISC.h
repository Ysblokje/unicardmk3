#ifndef _EMU_RAMDISC
#define _EMU_RAMDISC

#include "ff.h"

int emu_RAMDISC_Init( void);

int emu_RAMDISC_write( int i_addroffset, unsigned int *io_data);

void emu_RAMDISC_read( int i_addroffset, unsigned int *io_data);
void emu_RAMDISC_preread(void);

int emu_RAMDISC_read_interrupt(unsigned char *io_data);

void emu_RAMDISC_timeout(void);

typedef struct {
	int ready;
  FIL fh;		// filehandle k /unicard/ramdisc.dat
  uint16_t	offset;		// aktualni ukazatel adresy v RAMdisku
  uint8_t	bank;		// prave nastavena banka
  uint8_t	bank_mask;		// prave nastavena banka
  uint8_t	peziktst;	// hokuspokus - pri PEZIK testu vracime vzdy jinou hodnotu
  uint8_t	dirty;	// zmeneno ?
  uint8_t	timeout;	// vyprsel timeout zapisu ?
  uint32_t	buff_id;	// stranka bufferu
  uint32_t	addr;	// aktualni obsah bufferu
  uint32_t	preread_addr;	// stranka bufferu
  uint8_t	preread_ready;	// stranka bufferu
} RAMdisc_s;

extern RAMdisc_s RAMdisc;

#ifdef __cplusplus
 }
#endif /* __cplusplus */

#endif

