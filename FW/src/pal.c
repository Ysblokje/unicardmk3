/*
 *    Copyright (c) 2012 by Bohumil Novacek <http://dzi.n.cz/8bit/>
 *
 *    Thies program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    MZ-800 Unicard is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MZ-800 Unicard; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "pal.h"
#include "event.h"
#include "interrupt.h"
#include "VGA.h"
#include <string.h>

#define TO_RGB(a)	(((a&2)?0x020000:0)|((a&4)?0x000200:0)|((a&1)?0x000002:0)|((a&8)?0x010101:0))

#define	ascii	((unsigned char *)&(VRAM[/*0x200*/0]))			//CG-RAM
extern unsigned char framebuffer0[160*MAX_Y_PIXEL];
extern unsigned char framebuffer1[404*(FRAME_BUFFER_SIZE+16)+8];
extern unsigned char borderbuffer1[300];
//extern unsigned char line_palete[200*16];
unsigned char line_palete[200*8];

#define GDC_text	((unsigned char *)&(VRAM[/*0x3000*/0x1000]))	//VRAM pro mod MZ700

/*const */unsigned char BARVA[8]={0x00,0x03,0xC0,0xC3,0x30,0x33,0xF0,0xF3};			//barvy mod MZ700
/*const */unsigned char BARVA16[16]={0x00,0x02,0x80,0x82,0x20,0x22,0xA0,0xA2,		//barvy mod MZ800
						0x51,0x53,0xD1,0xD3,0x71,0x73,0xF1,0xF3};
/*const *//*char BARVA16x16[256]={	//tabulka scitani barev (soucet dvou 16ti barevnych bodu do jednoho 64 barevneho) pro mod 640x200
0x00,0x01,0x40,0x41,0x10,0x11,0x50,0x51,0x51,0x52,0x91,0x92,0x61,0x62,0xA1,0xA2,
0x01,0x02,0x41,0x42,0x11,0x12,0x51,0x52,0x52,0x53,0x92,0x93,0x62,0x63,0xA2,0xA3,
0x40,0x41,0x80,0x81,0x50,0x51,0x90,0x91,0x91,0x92,0xD1,0xD2,0xA1,0xA2,0xE1,0xE2,
0x41,0x42,0x81,0x82,0x51,0x52,0x91,0x92,0x92,0x93,0xD2,0xD3,0xA2,0xA3,0xE2,0xE3,
0x10,0x11,0x50,0x51,0x20,0x21,0x60,0x61,0x61,0x62,0xA1,0xA2,0x71,0x72,0xB1,0xB2,
0x11,0x12,0x51,0x52,0x21,0x22,0x61,0x62,0x62,0x63,0xA2,0xA3,0x72,0x73,0xB2,0xB3,
0x50,0x51,0x90,0x91,0x60,0x61,0xA0,0xA1,0xA1,0xA2,0xE1,0xE2,0xB1,0xB2,0xF1,0xF2,
0x51,0x52,0x91,0x92,0x61,0x62,0xA1,0xA2,0xA2,0xA3,0xE2,0xE3,0xB2,0xB3,0xF2,0xF3,
0x51,0x52,0x91,0x92,0x61,0x62,0xA1,0xA2,0x51,0x52,0x91,0x92,0x61,0x62,0xA1,0xA2,
0x52,0x53,0x92,0x93,0x62,0x63,0xA2,0xA3,0x52,0x53,0x92,0x93,0x62,0x63,0xA2,0xA3,
0x91,0x92,0xD1,0xD2,0xA1,0xA2,0xE1,0xE2,0x91,0x92,0xD1,0xD2,0xA1,0xA2,0xE1,0xE2,
0x92,0x93,0xD2,0xD3,0xA2,0xA3,0xE2,0xE3,0x92,0x93,0xD2,0xD3,0xA2,0xA3,0xE2,0xE3,
0x61,0x62,0xA1,0xA2,0x71,0x72,0xB1,0xB2,0x61,0x62,0xA1,0xA2,0x71,0x72,0xB1,0xB2,
0x62,0x63,0xA2,0xA3,0x72,0x73,0xB2,0xB3,0x62,0x63,0xA2,0xA3,0x72,0x73,0xB2,0xB3,
0xA1,0xA2,0xE1,0xE2,0xB1,0xB2,0xF1,0xF2,0xA1,0xA2,0xE1,0xE2,0xB1,0xB2,0xF1,0xF2,
0xA2,0xA3,0xE2,0xE3,0xB2,0xB3,0xF2,0xF3,0xA2,0xA3,0xE2,0xE3,0xB2,0xB3,0xF2,0xF3
};*/

//prepocitavaci tabulky pro urychleni vykresleni radku
static unsigned short tab[0x400];
//static unsigned long tabsuc[0x400];
static short bit701[256];
static short bit702[256];
static unsigned long tab8_32[256];
static unsigned short tab8_16[256];
//static unsigned short tab816[256];
/*#define	tab816b	tab816
static uint64_t tab640[256];
static unsigned short tab644a[256];
static unsigned char tab644b[16];
static unsigned short tab644c[256];*/
//static unsigned long tab800[256];

static unsigned char tab2x4_8[256];

//tabulka aktualni 16ti barevne palety
static unsigned char tab320_palete[16]={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
//tabulka 4barevne palety
static unsigned char *tab800_palete=&tab320_palete[0];

//volatile char PALETE_UPDATE=0;				//priznak zmeny palety 0 beze zmeny, 1-16 jen jedna paleta zmenena, 32 vsechny
static volatile char PALETE_SWITCH;		//registr prepinace pri editovani 16ti barevne palety

static volatile int ZX_KURZOR_COUNT;

//reset palety
void tab800_init_palete() {
//	register int i=16;
	PALETE_SWITCH = 0;
/*	tab800_palete=&tab320_palete[PALETE_SWITCH];
	while(i--) tab320_palete[i]=i;
//	PALETE_UPDATE = 32;*/
}

//nastaveni palety
void tab800_set_palete(unsigned char palete, unsigned char color) {
	if (palete&0x04) {
		register int i=(color<<2)&0x0C;
/*		if (i!=PALETE_SWITCH) {
			//prekopirovat paletu na novou pozici
			tab320_palete[i]=tab800_palete[0];
			tab320_palete[i+1]=tab800_palete[1];
			tab320_palete[i+2]=tab800_palete[2];
			tab320_palete[i+3]=tab800_palete[3];
			//default do stare pozice
			tab800_palete[0]=PALETE_SWITCH;
			tab800_palete[1]=PALETE_SWITCH+1;
			tab800_palete[2]=PALETE_SWITCH+2;
			tab800_palete[3]=PALETE_SWITCH+3;
*/			//ukazovatka na novou paletu
			PALETE_SWITCH=i;
/*			tab800_palete=&tab320_palete[PALETE_SWITCH];
			//PALETE_UPDATE = 32;	//nechat na cely snimek
		}*/
	} else {
		color&=15;palete&=3;
		if (tab800_palete[palete]!=color) {
			tab800_palete[palete]=color;
		}
	}
}

//zmena tabulek vykreslovani radku pri zmene palety
/*void tab800_update_palete() {
	switch (MZ_graphic_mode) {
	case MZ_GRAPH_320x200_4C:
	case MZ_GRAPH_320x200_4C_FRAMEB:
		{
			register int i;
			unsigned char a[4];
			unsigned char *buf=(unsigned char *) &tab800[0];
			a[0]=BARVA16[tab800_palete[0]];
			a[1]=BARVA16[tab800_palete[1]];
			a[2]=BARVA16[tab800_palete[2]];
			a[3]=BARVA16[tab800_palete[3]];
			for(i=0;i<256;i++) {
				*buf++ = a[(i&1)|((i>>3)&2)];
				*buf++ = a[((i>>1)&1)|((i>>4)&2)];
				*buf++ = a[((i>>2)&1)|((i>>5)&2)];
				*buf++ = a[((i>>3)&1)|((i>>6)&2)];
			}
		}
		break;
	case MZ_GRAPH_640x200:
	case MZ_GRAPH_640x200_FRAMEB:
		{
			register int i,x;
			unsigned long a[2];
			unsigned char *buf=(unsigned char *) &tab640[0];
			a[0]=TO_RGB(tab800_palete[0]);
			a[1]=TO_RGB(tab800_palete[1]);
			for(i=0;i<256;i++) {
				x=(5*a[i&1]+3*a[(i>>1)&1]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
				x=(2*a[(i>>1)&1]+5*a[(i>>2)&1]+a[(i>>3)&1]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
				x=(4*a[(i>>3)&1]+4*a[(i>>4)&1]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
				x=(a[(i>>4)&1]+5*a[(i>>5)&1]+2*a[(i>>6)&1]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
				x=(3*a[(i>>6)&1]+5*a[(i>>7)&1]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
				buf+=3;
			}
		}
		break;
	case MZ_GRAPH_640x200_4C:
		{
			register int i,x;
			unsigned long a[16];
			unsigned char *buf;
			for(i=16;i--;) a[i]=TO_RGB(tab800_palete[i]);
			buf=(unsigned char *) &tab644a[0];
			for(i=0;i<256;i++) {
				x=(5*a[(i&1)|((i>>3)&2)]+3*a[((i>>1)&1)|((i>>4)&2)]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
				x=(2*a[((i>>1)&1)|((i>>4)&2)]+5*a[((i>>2)&1)|((i>>5)&2)]+a[((i>>3)&1)|((i>>6)&2)]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
			}
			buf=(unsigned char *) &tab644b[0];
			for(i=0;i<16;i++) {
				x=(4*a[(i&1)|((i>>1)&2)]+4*a[((i>>1)&1)|((i>>2)&2)]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
			}
			buf=(unsigned char *) &tab644c[0];
			for(i=0;i<256;i++) {
				x=(a[(i&1)|((i>>3)&2)]+5*a[((i>>1)&1)|((i>>4)&2)]+2*a[((i>>2)&1)|((i>>5)&2)]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
				x=(3*a[((i>>2)&1)|((i>>5)&2)]+5*a[((i>>3)&1)|((i>>6)&2)]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
			}
		}
		break;
	case MZ_GRAPH_320x200_16C:
		{
			int i;
			for(i=0;i<256;i++) {
				tab816[i] = BARVA16[tab320_palete[(i&1)|((i>>1)&2)|((i>>2)&4)|((i>>3)&8)]]|
							((BARVA16[tab320_palete[((i>>1)&1)|((i>>2)&2)|((i>>3)&4)|((i>>4)&8)]])<<8);
//				tab8_32[i] = (i&3)|((i<<6)&0x300)|((i<<12)&0x30000)|((i<<18)&0x3000000);
			}
		}
		break;

	}
}*/

//zmena tabulek pri zmene pouze jedne barvy v palete
/*void tab800_update_palete_partial(int palete) {
	switch (MZ_graphic_mode) {
	case MZ_GRAPH_320x200_4C:
	case MZ_GRAPH_320x200_4C_FRAMEB:
		{
			register int i;
			unsigned char a[4];
			unsigned char *buf=(unsigned char *) &tab800[0];
			a[0]=BARVA16[tab800_palete[0]];
			a[1]=BARVA16[tab800_palete[1]];
			a[2]=BARVA16[tab800_palete[2]];
			a[3]=BARVA16[tab800_palete[3]];
			for(i=0;i<256;i++) {
				*buf++ = a[(i&1)|((i>>3)&2)];
				*buf++ = a[((i>>1)&1)|((i>>4)&2)];
				*buf++ = a[((i>>2)&1)|((i>>5)&2)];
				*buf++ = a[((i>>3)&1)|((i>>6)&2)];
			}
		}
		break;
	case MZ_GRAPH_640x200:
	case MZ_GRAPH_640x200_FRAMEB:
		{
			register int i,x;
			unsigned long a[2];
			unsigned char *buf=(unsigned char *) &tab640[0];
			a[0]=TO_RGB(tab800_palete[0]);
			a[1]=TO_RGB(tab800_palete[1]);
			for(i=0;i<256;i++) {
				x=(5*a[i&1]+3*a[(i>>1)&1]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
				x=(2*a[(i>>1)&1]+5*a[(i>>2)&1]+a[(i>>3)&1]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
				x=(4*a[(i>>3)&1]+4*a[(i>>4)&1]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
				x=(a[(i>>4)&1]+5*a[(i>>5)&1]+2*a[(i>>6)&1]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
				x=(3*a[(i>>6)&1]+5*a[(i>>7)&1]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
				buf+=3;
			}
		}
		break;
	case MZ_GRAPH_640x200_4C:
		{
			register int i,x;
			unsigned long a[16];
			unsigned char *buf;
			for(i=16;i--;) a[i]=TO_RGB(tab800_palete[i]);
			buf=(unsigned char *) &tab644a[0];
			for(i=0;i<256;i++) {
				x=(5*a[(i&1)|((i>>3)&2)]+3*a[((i>>1)&1)|((i>>4)&2)]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
				x=(2*a[((i>>1)&1)|((i>>4)&2)]+5*a[((i>>2)&1)|((i>>5)&2)]+a[((i>>3)&1)|((i>>6)&2)]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
			}
			buf=(unsigned char *) &tab644b[0];
			for(i=0;i<16;i++) {
				x=(4*a[(i&1)|((i>>1)&2)]+4*a[((i>>1)&1)|((i>>2)&2)]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
			}
			buf=(unsigned char *) &tab644c[0];
			for(i=0;i<256;i++) {
				x=(a[(i&1)|((i>>3)&2)]+5*a[((i>>1)&1)|((i>>4)&2)]+2*a[((i>>2)&1)|((i>>5)&2)]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
				x=(3*a[((i>>2)&1)|((i>>5)&2)]+5*a[((i>>3)&1)|((i>>6)&2)]+0x040404)&0x181818;*buf++=((x>>13)|(x>>7)|(x>>3));
			}
		}
		break;
	case MZ_GRAPH_320x200_16C:
		{
			int i;
			for(i=(palete<<4);i<(palete<<4)+16;i++) {
				tab816[i] = BARVA16[tab320_palete[(i&1)|((i>>1)&2)|((i>>2)&4)|((i>>3)&8)]]|
							((BARVA16[tab320_palete[((i>>1)&1)|((i>>2)&2)|((i>>3)&4)|((i>>4)&8)]])<<8);
//				tab8_32[i] = (i&3)|((i<<6)&0x300)|((i<<12)&0x30000)|((i<<18)&0x3000000);
			}
			for(i=(palete);i<(palete)+256;i+=16) {
				tab816[i] = BARVA16[tab320_palete[(i&1)|((i>>1)&2)|((i>>2)&4)|((i>>3)&8)]]|
							((BARVA16[tab320_palete[((i>>1)&1)|((i>>2)&2)|((i>>3)&4)|((i>>4)&8)]])<<8);
//				tab8_32[i] = (i&3)|((i<<6)&0x300)|((i<<12)&0x30000)|((i<<18)&0x3000000);
			}
		}
		break;

	}
}*/

//inicializace prepocitavacich tabulek
void pal_init(void) {
		int i;
		unsigned char a,b;
		unsigned char *buf=(unsigned char *)&tab[0];
		for(i=0;i<0x400;i++) {
			a=(i>>4)&7;if (a) a|=8;
			b=(i)&7;if (b) b|=8;
			*buf++ = ((i&0x08)?a:b) | (((i&0x80)?a:b)<<4);
			*buf++ = ((i&0x100)?a:b) | (((i&0x200)?a:b)<<4);
		}
#ifdef MZ700_ONLY
		buf=(unsigned char *)&tabsuc[0];
		for(i=0;i<0x400;i++) {
			a=(i>>4)&7;if (a) a|=8;
			b=(i)&7;if (b) b|=8;
			*buf++ = ((i&0x08)?a:b);
			*buf++ = ((i&0x80)?a:b);
			*buf++ = ((i&0x100)?a:b);
			*buf++ = ((i&0x200)?a:b);
		}
#endif		
		for(i=0;i<256;i++) {
			bit701[i]=((i&0x01)<<3)|((i&0x02)<<6)|((i&0x04)<<6)|((i&0x08)<<6);
			bit702[i]=((i&0x10)>>1)|((i&0x20)<<2)|((i&0x40)<<2)|((i&0x80)<<2);
		}
		for(i=0;i<2048;i++) /*GDC_atb[i]*/GDC_text[i+2048]=0x71;
		for(i=0;i<2048;i++) GDC_text[i]=0;//i;
		for(i=0;i<256;i++) {
//			tab816[i] = BARVA16[(i&1)|((i>>1)&2)|((i>>2)&4)|((i>>3)&8)]|
//						((BARVA16[((i>>1)&1)|((i>>2)&2)|((i>>3)&4)|((i>>4)&8)])<<8);
//			tab8_32[i] = (i&3)|((i<<6)&0x300)|((i<<12)&0x30000)|((i<<18)&0x3000000);
				tab8_32[i] = (i&1)|((i<<3)&0x10)|((i<<6)&0x100)|((i<<9)&0x1000)|((i<<12)&0x10000)|((i<<15)&0x100000)|((i<<18)&0x1000000)|((i<<21)&0x10000000);
				tab8_16[i] = (i&1)|((i<<1)&0x4)|((i<<2)&0x10)|((i<<3)&0x40)|((i<<4)&0x100)|((i<<5)&0x400)|((i<<6)&0x1000)|((i<<7)&0x4000);
				{
					int h=TO_RGB(((i>>4)&15));
					int l=TO_RGB((i&15));
					int db=((l&3)-(h&3)+1)>>1;
					int dg=((l&0x300)-(h&0x300)+0x100)>>1;
					int dr=((l&0x30000)-(h&0x30000)+0x10000)>>1;
					int d=(db&3)|(dg&0x300)|(dr&0x30000);
					d+=h;
					tab2x4_8[i]=(d&3)|((d>>4)&0x30)|((d>>10)&0xC0);
				}
		}

		//ZX table ...
		{
			unsigned short t;
			unsigned char a,b;
			unsigned char *buf=(unsigned char *)&zx48tab[0];
			for(i=0;i<0x800;i++) {
				b=(i>>3)&7;if (b) b|=((i>>3)&8);
				a=(i)&7;if (a) a|=((i>>3)&8);
				t=(i>>7)&15;
				*buf++ = ((t&0x08)?BARVA16[a]:BARVA16[b]);
				*buf++ = ((t&0x04)?BARVA16[a]:BARVA16[b]);
				*buf++ = ((t&0x02)?BARVA16[a]:BARVA16[b]);
				*buf++ = ((t&0x01)?BARVA16[a]:BARVA16[b]);
			}
		}
}

//registry scrollingu
#define PWM_SCROLL_SSA	SCROLL_SSA
#define PWM_SCROLL_SEA	SCROLL_SEA
#define PWM_SCROLL_SW		SCROLL_SW
#define PWM_SCROLL_SOF	SCROLL_SOF

//vykresleni jednoho TV radku do bufferu, ale jako ZX Spectrum ;-)
/*void ZX_pal_line(int line) {
	register unsigned long *buf=(unsigned long*)&framebuffer1[404*line+4];
	{
		unsigned long border=BARVA16[MZ_BORDER&0x0F]*0x01010101;
		register int i=8;
		register unsigned char *text=&ZX_VRAM[(((line&0xC0)|((line<<3)&0x38)|((line>>3)&0x07))<<5)];
		register unsigned char *col=&ZX_VRAM[(line>>3)*32+0x1800];
		register unsigned short t;
		register unsigned char a;
		*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
		*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
		*buf++=border;*buf++=border;
		while (i--) {
			a=*col++;if (a&0x80) { if (ZX_KURZOR_COUNT<=17) a=(a&0x40)|((a<<3)|((a>>3)&0x07)); } a&=0x7F;t=*text++;*buf++ = zx48tab[((t<<3)&0x780)|a];*buf++ = zx48tab[((t<<7)&0x780)|a];
			a=*col++;if (a&0x80) { if (ZX_KURZOR_COUNT<=17) a=(a&0x40)|((a<<3)|((a>>3)&0x07)); } a&=0x7F;t=*text++;*buf++ = zx48tab[((t<<3)&0x780)|a];*buf++ = zx48tab[((t<<7)&0x780)|a];
			a=*col++;if (a&0x80) { if (ZX_KURZOR_COUNT<=17) a=(a&0x40)|((a<<3)|((a>>3)&0x07)); } a&=0x7F;t=*text++;*buf++ = zx48tab[((t<<3)&0x780)|a];*buf++ = zx48tab[((t<<7)&0x780)|a];
			a=*col++;if (a&0x80) { if (ZX_KURZOR_COUNT<=17) a=(a&0x40)|((a<<3)|((a>>3)&0x07)); } a&=0x7F;t=*text++;*buf++ = zx48tab[((t<<3)&0x780)|a];*buf++ = zx48tab[((t<<7)&0x780)|a];
		}
		*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
		*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
		*buf++=border;*buf++=border;
		if (line==191) {
				ZX_KURZOR_COUNT--;
			if (ZX_KURZOR_COUNT<=0) {
				ZX_KURZOR_COUNT=34;			//cca 1.5Hz
			}
		}
	}
}*/

void palete_line(int line) {
	//border
	if ((line>=0)&&(line<300)) {
		borderbuffer1[line]=MZ_BORDER&0x0F;
	}
	if ((line<50)||(line>=250)) {
		return;
	}
	line-=50;
	memcpy(&line_palete[line<<3],tab800_palete,4);
	line_palete[(line<<3)+4]=PALETE_SWITCH;
	line_palete[(line<<3)+5]=MZ_graphic_mode;
}

//vykresleni jednoho TV radku do bufferu
void pal_line(int line) {
/*	if ((ZX_EMUL)&&(line>=50)&&(line<242)) {
//		ZX_pal_line(line-50);
		return;
	}*/
	if ((line<50)||(line>=MAX_Y_PIXEL+50)) {
		return;
	}
	line-=50;
	{
//		unsigned long border=BARVA16[MZ_BORDER&0x0F]*0x01010101;register unsigned long *buf=(unsigned long*)&framebuffer1[404*line+4];
//		register unsigned long *buf=(unsigned long*)&framebuffer0[160*line];
		if (MZ_mode==MZ_800) {
			if ((MZ_graphic_mode&0x0E)==0) {	//320*200, 4 barvy
				register unsigned long *buf=(unsigned long*)&framebuffer0[160*line];
				register unsigned short Addr=line*40;
				if ((PWM_SCROLL_SOF)&&((Addr>=PWM_SCROLL_SSA)&&(Addr<PWM_SCROLL_SEA))&&(PWM_SCROLL_SW)) {
					register int i=10;
					Addr=(((Addr+PWM_SCROLL_SOF-PWM_SCROLL_SSA)%PWM_SCROLL_SW)+PWM_SCROLL_SSA)&0x1FFF;
					while (i--) {
						register unsigned char *bitmap=(unsigned char *)&VRAM[Addr|((MZ_graphic_mode&1)<<14)];
						register unsigned long a,b;
						Addr+=4;if (Addr>=PWM_SCROLL_SEA) Addr-=PWM_SCROLL_SW;
						Addr&=0x1FFF;
						b=tab8_32[bitmap[0x2000]];
						a=tab8_32[(*bitmap++)];
						*buf++ = a|(b<<1);
						b=tab8_32[bitmap[0x2000]];
						a=tab8_32[(*bitmap++)];
						*buf++ = a|(b<<1);
						b=tab8_32[bitmap[0x2000]];
						a=tab8_32[(*bitmap++)];
						*buf++ = a|(b<<1);
						b=tab8_32[bitmap[0x2000]];
						a=tab8_32[(*bitmap++)];
						*buf++ = a|(b<<1);
					}
				} else {	//320*240, 4 barvy, bez scrollingu
					register unsigned char *bitmap=(unsigned char *)&VRAM[Addr|((MZ_graphic_mode&1)<<14)];
					register unsigned long a,b;
					register int i=10;
					while (i--) {
						b=tab8_32[bitmap[0x2000]];
						a=tab8_32[(*bitmap++)];
						*buf++ = a|(b<<1);
						b=tab8_32[bitmap[0x2000]];
						a=tab8_32[(*bitmap++)];
						*buf++ = a|(b<<1);
						b=tab8_32[bitmap[0x2000]];
						a=tab8_32[(*bitmap++)];
						*buf++ = a|(b<<1);
						b=tab8_32[bitmap[0x2000]];
						a=tab8_32[(*bitmap++)];
						*buf++ = a|(b<<1);
					}
				}
			}	//320x200 4color
			else if (MZ_graphic_mode==MZ_GRAPH_320x200_16C) {
				register unsigned long *buf=(unsigned long*)&framebuffer0[160*line];
				register unsigned short Addr=line*40;
				if ((PWM_SCROLL_SOF)&&((Addr>=PWM_SCROLL_SSA)&&(Addr<PWM_SCROLL_SEA))&&(PWM_SCROLL_SW)) {
					register int i=10;
					Addr=(((Addr+PWM_SCROLL_SOF-PWM_SCROLL_SSA)%PWM_SCROLL_SW)+PWM_SCROLL_SSA)&0x1FFF;
					while (i--) {
						register unsigned char *bitmap=(unsigned char *)&VRAM[Addr|((MZ_graphic_mode&1)<<14)];
						register unsigned long a,b,c,d;
						Addr+=4;if (Addr>=PWM_SCROLL_SEA) Addr-=PWM_SCROLL_SW;
						Addr&=0x1FFF;
						d=tab8_32[bitmap[0x6000]];
						c=tab8_32[bitmap[0x4000]];
						b=tab8_32[bitmap[0x2000]];
						a=tab8_32[(*bitmap++)];
						*buf++ = a|(b<<1)|(c<<2)|(d<<3);
						d=tab8_32[bitmap[0x6000]];
						c=tab8_32[bitmap[0x4000]];
						b=tab8_32[bitmap[0x2000]];
						a=tab8_32[(*bitmap++)];
						*buf++ = a|(b<<1)|(c<<2)|(d<<3);
						d=tab8_32[bitmap[0x6000]];
						c=tab8_32[bitmap[0x4000]];
						b=tab8_32[bitmap[0x2000]];
						a=tab8_32[(*bitmap++)];
						*buf++ = a|(b<<1)|(c<<2)|(d<<3);
						d=tab8_32[bitmap[0x6000]];
						c=tab8_32[bitmap[0x4000]];
						b=tab8_32[bitmap[0x2000]];
						a=tab8_32[(*bitmap++)];
						*buf++ = a|(b<<1)|(c<<2)|(d<<3);
					}
				} else {	//320*240, 16 barev, bez scrollingu
					register unsigned char *bitmap=(unsigned char *)&VRAM[Addr|((MZ_graphic_mode&1)<<14)];
					register unsigned long a,b,c,d;
					register int i=10;
					while (i--) {
						d=tab8_32[bitmap[0x6000]];
						c=tab8_32[bitmap[0x4000]];
						b=tab8_32[bitmap[0x2000]];
						a=tab8_32[(*bitmap++)];
						*buf++ = a|(b<<1)|(c<<2)|(d<<3);
						d=tab8_32[bitmap[0x6000]];
						c=tab8_32[bitmap[0x4000]];
						b=tab8_32[bitmap[0x2000]];
						a=tab8_32[(*bitmap++)];
						*buf++ = a|(b<<1)|(c<<2)|(d<<3);
						d=tab8_32[bitmap[0x6000]];
						c=tab8_32[bitmap[0x4000]];
						b=tab8_32[bitmap[0x2000]];
						a=tab8_32[(*bitmap++)];
						*buf++ = a|(b<<1)|(c<<2)|(d<<3);
						d=tab8_32[bitmap[0x6000]];
						c=tab8_32[bitmap[0x4000]];
						b=tab8_32[bitmap[0x2000]];
						a=tab8_32[(*bitmap++)];
						*buf++ = a|(b<<1)|(c<<2)|(d<<3);
					}
				}
/*			} else if (MZ_graphic_mode&MZ_GRAPH_640x200) {	//640x200
				register unsigned short Addr=line*40;
				if (PWM_SCROLL_SOF) {
					if ((Addr>=PWM_SCROLL_SSA)&&(Addr<PWM_SCROLL_SEA)) if (PWM_SCROLL_SW) Addr=(((Addr+PWM_SCROLL_SOF-PWM_SCROLL_SSA)%PWM_SCROLL_SW)+PWM_SCROLL_SSA)&0x1FFF;
				}
				if (MZ_graphic_mode&MZ_GRAPH_320x200_16C) {	//4 barvy
					register unsigned char *bitmap=(unsigned char *)&VRAM[Addr];
					register unsigned char *bitmap2=(unsigned char *)&VRAM[Addr|0x2000];
					register unsigned char *sbuf=(void *)buf;
					register unsigned char *tabtmp;
					register unsigned char a,b;
					register int i=10;
					while (i--) {
#define movx(a,b)	{tabtmp=(unsigned char *)&tab644a[(a&0x0F)|((b<<4)&0xF0)];*sbuf++ = *tabtmp++;*sbuf++ = *tabtmp++;*sbuf++ = tab644b[((a>>3)&0x03)|((b>>1)&0x0C)];tabtmp=(unsigned char *)&tab644c[((a>>4)&0x0F)|(b&0xF0)];*sbuf++ = *tabtmp++;*sbuf++ = *tabtmp++;}
						b=bitmap[0x4000];a=*bitmap++;movx(a,b);
						b=bitmap2[0x4000];a=*bitmap2++;movx(a,b);
						b=bitmap[0x4000];a=*bitmap++;movx(a,b);
						b=bitmap2[0x4000];a=*bitmap2++;movx(a,b);
						b=bitmap[0x4000];a=*bitmap++;movx(a,b);
						b=bitmap2[0x4000];a=*bitmap2++;movx(a,b);
						b=bitmap[0x4000];a=*bitmap++;movx(a,b);
						b=bitmap2[0x4000];a=*bitmap2++;movx(a,b);
					}
				} else { //2 barvy
					register unsigned char *bitmap=(unsigned char *)&VRAM[Addr|((MZ_graphic_mode&1)<<14)];
					register unsigned char *bitmap2=(unsigned char *)&VRAM[Addr|0x2000|((MZ_graphic_mode&1)<<14)];
					register unsigned char *sbuf=(void *)buf;
					register int i=10;
#define mov5(a,b)	{memcpy((unsigned char *)(a),&tab640[*b++],5);a+=5;}
					while (i--) {
						mov5(sbuf,bitmap);mov5(sbuf,bitmap2);mov5(sbuf,bitmap);mov5(sbuf,bitmap2);
						mov5(sbuf,bitmap);mov5(sbuf,bitmap2);mov5(sbuf,bitmap);mov5(sbuf,bitmap2);
					}
				}*/
			} else if (MZ_graphic_mode&MZ_GRAPH_640x200) {	//640x200
				register unsigned short *buf=(unsigned short*)&framebuffer0[160*line];
				register unsigned short Addr=line*40;
				if (PWM_SCROLL_SOF) {
					if ((Addr>=PWM_SCROLL_SSA)&&(Addr<PWM_SCROLL_SEA)) if (PWM_SCROLL_SW) Addr=(((Addr+PWM_SCROLL_SOF-PWM_SCROLL_SSA)%PWM_SCROLL_SW)+PWM_SCROLL_SSA)&0x1FFF;
				}
				if (MZ_graphic_mode&MZ_GRAPH_320x200_16C) {	//4 barvy
					register unsigned char *bitmap=(unsigned char *)&VRAM[Addr];
					register int i=10;
					while (i--) {
						*buf++=tab8_16[*bitmap]|(tab8_16[bitmap[0x4000]]<<1);*buf++=tab8_16[bitmap[0x2000]]|(tab8_16[bitmap[0x6000]]<<1);bitmap++;
						*buf++=tab8_16[*bitmap]|(tab8_16[bitmap[0x4000]]<<1);*buf++=tab8_16[bitmap[0x2000]]|(tab8_16[bitmap[0x6000]]<<1);bitmap++;
						*buf++=tab8_16[*bitmap]|(tab8_16[bitmap[0x4000]]<<1);*buf++=tab8_16[bitmap[0x2000]]|(tab8_16[bitmap[0x6000]]<<1);bitmap++;
						*buf++=tab8_16[*bitmap]|(tab8_16[bitmap[0x4000]]<<1);*buf++=tab8_16[bitmap[0x2000]]|(tab8_16[bitmap[0x6000]]<<1);bitmap++;
					}
				} else { //2 barvy
					register unsigned char *bitmap=(unsigned char *)&VRAM[Addr|((MZ_graphic_mode&1)<<14)];
					register int i=10;
					while (i--) {
						*buf++=tab8_16[*bitmap];*buf++=tab8_16[bitmap[0x2000]];bitmap++;
						*buf++=tab8_16[*bitmap];*buf++=tab8_16[bitmap[0x2000]];bitmap++;
						*buf++=tab8_16[*bitmap];*buf++=tab8_16[bitmap[0x2000]];bitmap++;
						*buf++=tab8_16[*bitmap];*buf++=tab8_16[bitmap[0x2000]];bitmap++;
					}
				}
			} else {
				//TODO nepodporovany rezim
			}
		} else if (MZ_mode==MZ_700) {
			register unsigned short *buf=(unsigned short*)&framebuffer0[160*line];
			register unsigned char *text=&GDC_text[(line>>3)*40];
			register unsigned short t;
			register unsigned char a;//,b;
			register int i=10;
//			*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
			while (i--) {
				a=text[2048];
				t=(*text++)|(((a)<<1)&0x100);
				t=ascii[(t<<3)|(line&7)];
				*buf++ = tab[bit701[t]|(a&0x77)];
				*buf++ = tab[bit702[t]|(a&0x77)];

				a=text[2048];
				t=(*text++)|(((a)<<1)&0x100);
				t=ascii[(t<<3)|(line&7)];
				*buf++ = tab[bit701[t]|(a&0x77)];
				*buf++ = tab[bit702[t]|(a&0x77)];

				a=text[2048];
				t=(*text++)|(((a)<<1)&0x100);
				t=ascii[(t<<3)|(line&7)];
				*buf++ = tab[bit701[t]|(a&0x77)];
				*buf++ = tab[bit702[t]|(a&0x77)];

				a=text[2048];
				t=(*text++)|(((a)<<1)&0x100);
				t=ascii[(t<<3)|(line&7)];
				*buf++ = tab[bit701[t]|(a&0x77)];
				*buf++ = tab[bit702[t]|(a&0x77)];
			}
//			buf=(unsigned long*)&framebuffer1[404*line+364];*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
#ifdef MZ700_ONLY
		} else if (MZ_mode==MZ_80c) {
		} else if (MZ_mode==MZ_SUC) {
#endif			
		}
	}
}

void pal_buf0_buf1(int line) {
	if (/*line&*/1) {
//		line=line/2;
		line=line-50;
		if ((line>=0)&&(line<MAX_Y_PIXEL)) {
			unsigned long border=BARVA16[borderbuffer1[line+50]&0x0F]*0x01010101;register unsigned long *buf=(unsigned long*)&framebuffer1[404*(line%FRAME_BUFFER_SIZE)+4];
			register unsigned long *buf0=(unsigned long*)&framebuffer0[/*(state_mp3)?0:*/160*line];
			register unsigned char *bufc;
			register unsigned long a;//,b;
			register int i=10;
			unsigned char paleta[16];
			register int j;
			if (((MZ_mode==MZ_700)||(MZ_mode==MZ_800))/*||((line>=192)&&(INFO_TEXT))*/) {
				unsigned char gm=line_palete[(line<<3)+5];
//				if ((line>=192)&&(INFO_TEXT)) {
//					gm=0x08;
//				}
				if (gm<8) {
					if (gm & MZ_GRAPH_640x200) {
						j=16;while(j--) paleta[j]=line_palete[(line<<3)|(j&3)];
						bufc=(unsigned char *)buf;
						while (i--) {
							a=*buf0++;
							*bufc++=tab2x4_8[(paleta[a&0x03]<<4)|paleta[(a>>2)&0x03]];a>>=4;
							*bufc++=BARVA16[paleta[a&0x03]];a>>=2;
							*bufc++=tab2x4_8[(paleta[a&0x03]<<4)|paleta[(a>>2)&0x03]];a>>=4;
							*bufc++=BARVA16[paleta[a&0x03]];a>>=2;
							*bufc++=tab2x4_8[paleta[a&0x03]|(paleta[(a>>2)&0x03]<<4)];a>>=4;
							*bufc++=tab2x4_8[(paleta[a&0x03]<<4)|paleta[(a>>2)&0x03]];a>>=4;
							*bufc++=BARVA16[paleta[a&0x03]];a>>=2;
							*bufc++=tab2x4_8[(paleta[a&0x03]<<4)|paleta[(a>>2)&0x03]];a>>=4;
							*bufc++=BARVA16[paleta[a&0x03]];a>>=2;
							*bufc++=tab2x4_8[paleta[a&0x03]|(paleta[(a>>2)&0x03]<<4)];
							a=*buf0++;
							*bufc++=tab2x4_8[(paleta[a&0x03]<<4)|paleta[(a>>2)&0x03]];a>>=4;
							*bufc++=BARVA16[paleta[a&0x03]];a>>=2;
							*bufc++=tab2x4_8[(paleta[a&0x03]<<4)|paleta[(a>>2)&0x03]];a>>=4;
							*bufc++=BARVA16[paleta[a&0x03]];a>>=2;
							*bufc++=tab2x4_8[paleta[a&0x03]|(paleta[(a>>2)&0x03]<<4)];a>>=4;
							*bufc++=tab2x4_8[(paleta[a&0x03]<<4)|paleta[(a>>2)&0x03]];a>>=4;
							*bufc++=BARVA16[paleta[a&0x03]];a>>=2;
							*bufc++=tab2x4_8[(paleta[a&0x03]<<4)|paleta[(a>>2)&0x03]];a>>=4;
							*bufc++=BARVA16[paleta[a&0x03]];a>>=2;
							*bufc++=tab2x4_8[paleta[a&0x03]|(paleta[(a>>2)&0x03]<<4)];
							a=*buf0++;
							*bufc++=tab2x4_8[(paleta[a&0x03]<<4)|paleta[(a>>2)&0x03]];a>>=4;
							*bufc++=BARVA16[paleta[a&0x03]];a>>=2;
							*bufc++=tab2x4_8[(paleta[a&0x03]<<4)|paleta[(a>>2)&0x03]];a>>=4;
							*bufc++=BARVA16[paleta[a&0x03]];a>>=2;
							*bufc++=tab2x4_8[paleta[a&0x03]|(paleta[(a>>2)&0x03]<<4)];a>>=4;
							*bufc++=tab2x4_8[(paleta[a&0x03]<<4)|paleta[(a>>2)&0x03]];a>>=4;
							*bufc++=BARVA16[paleta[a&0x03]];a>>=2;
							*bufc++=tab2x4_8[(paleta[a&0x03]<<4)|paleta[(a>>2)&0x03]];a>>=4;
							*bufc++=BARVA16[paleta[a&0x03]];a>>=2;
							*bufc++=tab2x4_8[paleta[a&0x03]|(paleta[(a>>2)&0x03]<<4)];
							a=*buf0++;
							*bufc++=tab2x4_8[(paleta[a&0x03]<<4)|paleta[(a>>2)&0x03]];a>>=4;
							*bufc++=BARVA16[paleta[a&0x03]];a>>=2;
							*bufc++=tab2x4_8[(paleta[a&0x03]<<4)|paleta[(a>>2)&0x03]];a>>=4;
							*bufc++=BARVA16[paleta[a&0x03]];a>>=2;
							*bufc++=tab2x4_8[paleta[a&0x03]|(paleta[(a>>2)&0x03]<<4)];a>>=4;
							*bufc++=tab2x4_8[(paleta[a&0x03]<<4)|paleta[(a>>2)&0x03]];a>>=4;
							*bufc++=BARVA16[paleta[a&0x03]];a>>=2;
							*bufc++=tab2x4_8[(paleta[a&0x03]<<4)|paleta[(a>>2)&0x03]];a>>=4;
							*bufc++=BARVA16[paleta[a&0x03]];a>>=2;
							*bufc++=tab2x4_8[paleta[a&0x03]|(paleta[(a>>2)&0x03]<<4)];
						}
						return;
					} else {
						if (gm==MZ_GRAPH_320x200_16C) {
							unsigned char sw=line_palete[(line<<3)+4];
							j=16;while(j--) { if ((j&0x0C)==sw) paleta[j]=BARVA16[line_palete[(line<<3)|(j&3)]]; else paleta[j]=BARVA16[j]; }
						} else {
							j=4;while(j--) paleta[j]=BARVA16[line_palete[(line<<3)|(j&15)]];
						}
					}
				} else {
					memcpy(paleta,BARVA16,16);
				}
				*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
				bufc=(unsigned char *)buf;
				while (i--) {
						a=*buf0++;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];
						a=*buf0++;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];
						a=*buf0++;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];
						a=*buf0++;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];a>>=4;
						*bufc++=paleta[a&0x0F];
				}
				buf=(unsigned long *)bufc;
				*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
			} //MZ_mode==MZ_800
		}
	}
}

#ifdef MZ700_ONLY
unsigned char KP80[32];
#endif			

//#ifdef USE_UNICARD_MK3B
#include "../lcd/stm32f429i_discovery_lcd.h"
void pallcd(int line) {
	if (/*line&*/1) {
//		line=line/2;
		unsigned long border=(borderbuffer1[line]&0x0F)*0x01010101;
		//LTDC_Layer1->DCCR=((line&8)?0xFFFFFFFF:0);
//		LTDC_Layer1->CLUTWR = bordertab[border&0x0F];
		border=0x18181818;
//		memset(LCD_FRAME_BUFFER,line>>8,800);
#ifdef MZ700_ONLY
		if (MZ_mode==MZ_80c) {
			line=line-25;
			if ((line>=0)&&(line<((KP80[9]&15)+1)*KP80[6])) {
				register unsigned long *buf=(unsigned long*)&LCD_FRAME_BUFFER[0];
				register const unsigned long *offset=&SYfont[(line%((KP80[9]&15)+1))<<1];
				register int addr=((line/((KP80[9]&15)+1))*KP80[1])+(KP80[12]<<8)+KP80[13];
				register int n=KP80[1];
				*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
				*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
				while(n>0) {
					register unsigned short uc=SYdata[addr&0x07FF]<<5;
					*buf++=offset[uc++];
					*buf++=offset[uc];
					addr++;
					n--;
				}
				*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
				*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
				return;
			}
			memset(LCD_FRAME_BUFFER,border,800);
			return;
		}
#endif			
		line=line-50;
		if ((line>=0)&&(line<MAX_Y_PIXEL)) {
			register unsigned long *buf=(unsigned long*)&LCD_FRAME_BUFFER[80];
			register unsigned long *buf0=(unsigned long*)&framebuffer0[/*(state_mp3)?0:*/160*line];
			register unsigned short *bufs;
			register unsigned long a;//,b;
			register int i=10;
			unsigned short paleta[16];
			register int j;
			/////memset(LCD_FRAME_BUFFER,line,804);
#ifdef MZ700_ONLY
			if (MZ_mode==MZ_SUC) {
				register unsigned char *text=&GDC_text[(line>>3)*40];
				register unsigned short t;
				register unsigned char a;//,b;
//				register int i=10;
//				*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
//				*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
				while (i--) {
					a=text[2048+1024];
					t=(*(text+1024))|(((a)<<1)&0x100);
					t=ascii[(t<<3)|(line&7)];
					*buf++ = tabsuc[bit701[t]|(a&0x77)];
					*buf++ = tabsuc[bit702[t]|(a&0x77)];

					a=text[2048];
					t=(*text++)|(((a)<<1)&0x100);
					t=ascii[(t<<3)|(line&7)];
					*buf++ = tabsuc[bit701[t]|(a&0x77)];
					*buf++ = tabsuc[bit702[t]|(a&0x77)];

					a=text[2048+1024];
					t=(*(text+1024))|(((a)<<1)&0x100);
					t=ascii[(t<<3)|(line&7)];
					*buf++ = tabsuc[bit701[t]|(a&0x77)];
					*buf++ = tabsuc[bit702[t]|(a&0x77)];

					a=text[2048];
					t=(*text++)|(((a)<<1)&0x100);
					t=ascii[(t<<3)|(line&7)];
					*buf++ = tabsuc[bit701[t]|(a&0x77)];
					*buf++ = tabsuc[bit702[t]|(a&0x77)];

					a=text[2048+1024];
					t=(*(text+1024))|(((a)<<1)&0x100);
					t=ascii[(t<<3)|(line&7)];
					*buf++ = tabsuc[bit701[t]|(a&0x77)];
					*buf++ = tabsuc[bit702[t]|(a&0x77)];

					a=text[2048];
					t=(*text++)|(((a)<<1)&0x100);
					t=ascii[(t<<3)|(line&7)];
					*buf++ = tabsuc[bit701[t]|(a&0x77)];
					*buf++ = tabsuc[bit702[t]|(a&0x77)];

					a=text[2048+1024];
					t=(*(text+1024))|(((a)<<1)&0x100);
					t=ascii[(t<<3)|(line&7)];
					*buf++ = tabsuc[bit701[t]|(a&0x77)];
					*buf++ = tabsuc[bit702[t]|(a&0x77)];

					a=text[2048];
					t=(*text++)|(((a)<<1)&0x100);
					t=ascii[(t<<3)|(line&7)];
					*buf++ = tabsuc[bit701[t]|(a&0x77)];
					*buf++ = tabsuc[bit702[t]|(a&0x77)];
				}
//				*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
//				*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
			} else
#endif			
			if (((MZ_mode==MZ_700)||(MZ_mode==MZ_800))/*||((line>=192)&&(INFO_TEXT))*/) {
				unsigned char gm=line_palete[(line<<3)+5];
//				if ((line>=192)&&(INFO_TEXT)) {
//					gm=0x08;
//				}
//				memset(LCD_FRAME_BUFFER+400,gm,400);
				if (gm<8) {
					if (gm & MZ_GRAPH_640x200) {
						register unsigned char *bufc;
						j=16;while(j--) paleta[j]=line_palete[(line<<3)|(j&3)];
//						*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
//						*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
						bufc=(unsigned char *)buf;
						while (i--) {
							a=*buf0++;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];
							a=*buf0++;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];
							a=*buf0++;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];
							a=*buf0++;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];a>>=2;
							*bufc++=paleta[a&0x03];
						}
						buf=(unsigned long *)bufc;
//						*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
//						*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
						return;
					} else {
						if (gm==MZ_GRAPH_320x200_16C) {
							unsigned char sw=line_palete[(line<<3)+4];
							j=16;while(j--) { if ((j&0x0C)==sw) paleta[j]=line_palete[(line<<3)|(j&3)]*0x0101; else paleta[j]=j*0x0101; }
						} else {
							j=4;while(j--) paleta[j]=line_palete[(line<<3)|(j&15)]*0x0101;
						}
					}
				} else {
					j=16;while(j--) { paleta[j]=j*0x0101; }
				}
//				*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
//				*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
				bufs=(unsigned short *)buf;
				while (i--) {
						a=*buf0++;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];
						a=*buf0++;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];
						a=*buf0++;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];
						a=*buf0++;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];a>>=4;
						*bufs++=paleta[a&0x0F];
				}
				buf=(unsigned long *)bufs;
//				*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
//				*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;*buf++=border;
			} //MZ_mode==MZ_800
		} else if (line==200) {
/*			memset(LCD_FRAME_BUFFER,GPIOE->IDR,50);
			memset(LCD_FRAME_BUFFER+50,(GPIOE->IDR)>>4,50);
			memset(LCD_FRAME_BUFFER+100,(GPIOE->IDR)>>8,50);
			memset(LCD_FRAME_BUFFER+150,(GPIOE->IDR)>>12,50);
			memset(LCD_FRAME_BUFFER+200,GPIOB->IDR,50);
			memset(LCD_FRAME_BUFFER+250,(GPIOB->IDR)>>4,50);
			memset(LCD_FRAME_BUFFER+300,(GPIOD->IDR)>>8,50);
			memset(LCD_FRAME_BUFFER+350,(GPIOD->IDR)>>12,50);
			memset(LCD_FRAME_BUFFER+400,newline,400);*/
			memset(LCD_FRAME_BUFFER,border,800);
		}
	}
}
//#endif

void TIM2_IRQHandler()
{
		TIM2->SR=0;
		TIM5->EGR=1;	//nulovat
		newline=0;
		TIM5->SR=0;
}

void TIM5_IRQHandler()
{
		TIM5->SR=0;
		newline++;
		if (newline>=312) newline=0;
		set_event(EVENT_VGA);
}

