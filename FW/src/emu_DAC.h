#ifndef _EMU_DAC
#define _EMU_DAC

#define DMA_DAC1_ON		//use DMA for DAC output generation

#define DAC2_SPEED	((((SystemCoreClock*16)/3546875)+1UL)/2UL)
#define DAC2_SPEED_LOW	((((SystemCoreClock*16)/3546875)+2UL)/2UL)
#define DAC2_SPEED_HI	((((SystemCoreClock*16)/3546875)-0UL)/2UL)

extern void emu_DAC_init(int);
extern void emu_DAC_open(void);
extern int emu_DAC_PLAY(char *filename);
extern int emu_DAC_STOP(void);
extern void do_DACwav(void);
extern void emu_DAC2_sync(int time);

#endif
