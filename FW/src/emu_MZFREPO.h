#ifndef _EMU_REPO
#define _EMU_REPO

#define EMU_READ		1
#define EMU_WRITE		2

extern volatile int EMU_event;
extern volatile int EMU_addr;
extern volatile unsigned long EMU_data;

int emu_MZFREPO_init( void);

int emu_MZFREPO_write( int i_addroffset, unsigned int *io_data);

int emu_MZFREPO_read( int i_addroffset, unsigned int *io_data);

void emu_MZFREPO_preread(void);
int emu_MZFREPO_read_interrupt(unsigned char *io_data);

#endif

