/*
 *    Copyright (c) 2012 by Bohumil Novacek <http://www.dzi.n.cz/8bit>
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    MZ-800 Unicard is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MZ-800 Unicard; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


/*
 * Servisni manual karty se Z80SIO: http://www.8bity.cz/files/Sharp/mz1e24_sm_gb.pdf
 *
 * Manual Z80SIO: http://www.hartetechnologies.com/manuals/Zilog/Zilog%20Z80-SIO%20Technical%20Manual.pdf
 *
 */


#include <string.h>

#include "interrupt.h"

//#include "ff.h"
#include "monitor.h"
#include "mzint.h"
#include "USARTshell.h"

#define DBGLEVEL	(DBGNON /*(DBGFAT | DBGERR | DBGWAR | DBGINF*/)
//#define DBGLEVEL	(DBGNON | DBGFAT | DBGERR | DBGWAR | DBGINF)

#define USE_DBG_PRN	xprintf
#include "debug.h"
//#define DBGPRINTF(a,...)	{}

#define RETURN_SIO_ERR 1
#define RETURN_SIO_OK  0

// definice bool
#define true	1
#define false	0

USART_TypeDef* USARTx;

/*
 * Struktura informaci udrzovanych pro kazdou FD radic.
 *
 *
 */
typedef struct {
    uint8_t      COMMAND;      // zde je ulozen SIO command, ktery jsme prijali na portu 0xd8
/*    uint8_t      MOTOR;         // bit0 a bit1 urcuje FD mechaniku se kterou prave pracujeme
                                                // bit7 zapina motor mechaniky (pouzivame pri cteni registru sektoru)
    uint8_t      DENSITY;
    uint8_t      EINT;         // 1 => INT rezim je povolen, 0 => zakazan
                                                // v pripade povoleneho INT rezimu rika radic signalem /INT,
                                                // ze ma pro MZ-800 pripraveny data

    uint16_t      DATA_COUNTER;      // Pri vykonavani prikazu cteni a zapisu sektoru, cteni adresy
                                                // sektoru je do tohoto citace vlozena velikost dat, ktere budou prijimany,
                                                // nebo odesilany. Tzn. velukost sektoru, nebo pocet bajt pri cteni adresy.
                                                // Pri kazdem cteni, nebo zapisu je tato hodnota snizovana.
                                                // Pokud je v tomto citaci nenulova hodnota, tak to znamena, ze radic ocekava
                                                // nejaka data v pripade zapisu, nebo ma pripravena data k odberu.

    uint8_t      MULTIBLOCK_RW;      // 1 - znamena, ze posledni prikaz cteni/zapisu sektoru byl multiblokovy
                                                // tzn., ze az precteme / zapiseme posledni bajt aktualniho sektoru,
                                                // tak automaticky prejdeme na dalsi a pokracujeme dokud neprijde prikaz preruseni,
                                                // nebo dokud uz na stope neexistuje sektor s nasledujicim poradovym cislem
                                                // 0 - znamena obycejne cteni / zapis, ktere konci s poslednim bajtem sektoru.

    uint8_t      STATUS_SCRIPT;      // Scenar podle ktereho se ma chovat status registr - viz. cteni status registru

    uint8_t      waitForInt;      // Pokud jsme v rezimu INT, tak si zde radic pocita za jak dlouho
                                                // poslat dalsi interrupt - viz. SIO.waitForInt()

    uint8_t      write_track_stage;   // Pokud se formatuje, tak zde je ulozena uroven vychazejici z prijate znacky
                                                // 0 - zacatek zapisu stopy (prijimame data pro WRITE TRACK)
                                                // 1 - dorazila znacka indexu (0xfc)
                                                // 2 - dorazila znacka adresy (0xfe)
                                                // 3 - znacka dat sektoru (0xfb)
                                                // 4 - konec dat
                                                // 5 - konec stopy

    uint16_t      write_track_counter;   // Nuluje se vzdy pri zmene znacky, takze podle nej identifikujeme
                                                // kde se prave nachazime.

    uint8_t      reading_status_counter;   // pri cteni a zapisu sektoru pocitame kolikrat po sobe se
                                                // cetl regSTATUS bez toho, aniz by se mezi tim pracovalo s regDATA
                                                // pokud vice, nez 5x, tak se chovame jako kdyby uz byl konec sektoru.
                                                // Tohle cteni dat bez skutecneho cteni pouzivaji nektere programy jako
                                                // verifikaci ulozenych dat.
*/
} SIOontroller_s;

SIOontroller_s SIO[2];

extern volatile unsigned short AddrM1;

void init_USART6(uint32_t baudrate){

	/* This is a concept that has to do with the libraries provided by ST
	 * to make development easier the have made up something similar to
	 * classes, called TypeDefs, which actually just define the common
	 * parameters that every peripheral needs to work correctly
	 *
	 * They make our life easier because we don't have to mess around with
	 * the low level stuff of setting bits in the correct registers
	 */
	GPIO_InitTypeDef GPIO_InitStruct; // this is for the GPIO pins used as TX and RX
	USART_InitTypeDef USART_InitStruct; // this is for the USART1 initilization
	NVIC_InitTypeDef NVIC_InitStructure; // this is used to configure the NVIC (nested vector interrupt controller)

	/* enable APB2 peripheral clock for USART1
	 * note that only USART1 and USART6 are connected to APB2
	 * the other USARTs are connected to APB1
	 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART6, ENABLE);

	/* enable the peripheral clock for the pins used by
	 * USART1, PB6 for TX and PB7 for RX
	 */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

	/* This sequence sets up the TX and RX pins
	 * so they work correctly with the USART1 peripheral
	 */
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7; // Pins 6 (TX) and 7 (RX) are used
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF; 			// the pins are configured as alternate function so the USART peripheral has access to them
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;		// this defines the IO speed and has nothing to do with the baudrate!
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;			// this defines the output type as push pull mode (as opposed to open drain)
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;			// this activates the pullup resistors on the IO pins
	GPIO_Init(GPIOC, &GPIO_InitStruct);					// now all the values are passed to the GPIO_Init() function which sets the GPIO registers

	/* The RX and TX pins are now connected to their AF
	 * so that the USART1 can take over control of the
	 * pins
	 */
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_USART6); //
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_USART6);

	/* Now the USART_InitStruct is used to define the
	 * properties of USART1
	 */
	USART_InitStruct.USART_BaudRate = baudrate;				// the baudrate is set to the value we passed into this init function
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;// we want the data frame size to be 8 bits (standard)
	USART_InitStruct.USART_StopBits = USART_StopBits_1;		// we want 1 stop bit (standard)
	USART_InitStruct.USART_Parity = USART_Parity_No;		// we don't want a parity bit (standard)
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None; // we don't want flow control (standard)
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx; // we want to enable the transmitter and the receiver
	USART_Init(USART6, &USART_InitStruct);					// again all the properties are passed to the USART_Init function which takes care of all the bit setting


	/* Here the USART1 receive interrupt is enabled
	 * and the interrupt controller is configured
	 * to jump to the USART1_IRQHandler() function
	 * if the USART1 receive interrupt occurs
	 */
	USART_ITConfig(USART6, USART_IT_RXNE, ENABLE); // enable the USART1 receive interrupt

	NVIC_InitStructure.NVIC_IRQChannel = USART6_IRQn;		 // we want to configure the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 12;// this sets the priority group of the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;		 // this sets the subpriority inside the group
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			 // the USART1 interrupts are globally enabled
	NVIC_Init(&NVIC_InitStructure);							 // the properties are passed to the NVIC_Init function which takes care of the low level stuff

	// finally this enables the complete USART1 peripheral
	USART_Cmd(USART6, ENABLE);
}

void init_USART2(uint32_t baudrate){

	/* This is a concept that has to do with the libraries provided by ST
	 * to make development easier the have made up something similar to
	 * classes, called TypeDefs, which actually just define the common
	 * parameters that every peripheral needs to work correctly
	 *
	 * They make our life easier because we don't have to mess around with
	 * the low level stuff of setting bits in the correct registers
	 */
	GPIO_InitTypeDef GPIO_InitStruct; // this is for the GPIO pins used as TX and RX
	USART_InitTypeDef USART_InitStruct; // this is for the USART2 initilization

	/* enable APB1 peripheral clock for USART2
	 * note that only USART1 and USART6 are connected to APB2
	 * the other USARTs are connected to APB1
	 */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	/* enable the peripheral clock for the pins used by
	 * USART2, PD6 for TX and PD5 for RX
	 */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	/* This sequence sets up the TX and RX pins
	 * so they work correctly with the USART2 peripheral
	 */
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6; // Pins 5 (TX) and 6 (RX) are used
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF; 			// the pins are configured as alternate function so the USART peripheral has access to them
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;		// this defines the IO speed and has nothing to do with the baudrate!
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;			// this defines the output type as push pull mode (as opposed to open drain)
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;			// this activates the pullup resistors on the IO pins
	GPIO_Init(GPIOD, &GPIO_InitStruct);					// now all the values are passed to the GPIO_Init() function which sets the GPIO registers

	/* The RX and TX pins are now connected to their AF
	 * so that the USART2 can take over control of the
	 * pins
	 */
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource5, GPIO_AF_USART2);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource6, GPIO_AF_USART2);

	/* Now the USART_InitStruct is used to define the
	 * properties of USART2
	 */
	USART_InitStruct.USART_BaudRate = baudrate;				// the baudrate is set to the value we passed into this init function
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;// we want the data frame size to be 8 bits (standard)
	USART_InitStruct.USART_StopBits = USART_StopBits_1;		// we want 1 stop bit (standard)
	USART_InitStruct.USART_Parity = USART_Parity_No;		// we don't want a parity bit (standard)
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None; // we don't want flow control (standard)
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx; // we want to enable the transmitter and the receiver
	USART_Init(USART2, &USART_InitStruct);					// again all the properties are passed to the USART_Init function which takes care of all the bit setting

	// finally this enables the complete USART2 peripheral
	USART_Cmd(USART2, ENABLE);
}

/*
 * SIO_Init() - vynulovani vsech hodnot ve strukture SIO a primountovani
 * DSK souboru podle konfigurace v /unicard/fd[0-3].cfg
 *
 */
int emu_SIO_Init ( USART_TypeDef* u )
{
//	DBGPRINTF ( DBGINF, "emu_SIO_Init(): mounting drive %d failed\n", i );
	SIO[0].COMMAND=0;
	SIO[1].COMMAND=0;
	USARTx = u;
//#ifdef USE_UNICARD_MK3B
	if (u==USART2) init_USART2(9600);
//#else	
	else init_USART6(9600);
//#endif
	return 1;
}

void emu_SIO_set_bps(/*USART_TypeDef* USARTx, */int USART_BaudRate) {
//#ifdef USE_UNICARD_MK3B
//  USART_TypeDef* USARTx = USART2;
//#else
//  USART_TypeDef* USARTx = USART6;
//#endif
  uint32_t tmpreg = 0x00, apbclock = 0x00;
  uint32_t integerdivider = 0x00;
  uint32_t fractionaldivider = 0x00;
  RCC_ClocksTypeDef RCC_ClocksStatus;
  /*---------------------------- USART BRR Configuration -----------------------*/
  /* Configure the USART Baud Rate */
  RCC_GetClocksFreq(&RCC_ClocksStatus);

  if ((USARTx == USART1) || (USARTx == USART6))
  {
    apbclock = RCC_ClocksStatus.PCLK2_Frequency;
  }
  else
  {
    apbclock = RCC_ClocksStatus.PCLK1_Frequency;
  }

  /* Determine the integer part */
  if ((USARTx->CR1 & USART_CR1_OVER8) != 0)
  {
    /* Integer part computing in case Oversampling mode is 8 Samples */
    integerdivider = ((25 * apbclock) / (2 * (USART_BaudRate)));
  }
  else /* if ((USARTx->CR1 & USART_CR1_OVER8) == 0) */
  {
    /* Integer part computing in case Oversampling mode is 16 Samples */
    integerdivider = ((25 * apbclock) / (4 * (USART_BaudRate)));
  }
  tmpreg = (integerdivider / 100) << 4;

  /* Determine the fractional part */
  fractionaldivider = integerdivider - (100 * (tmpreg >> 4));

  /* Implement the fractional part in the register */
  if ((USARTx->CR1 & USART_CR1_OVER8) != 0)
  {
    tmpreg |= ((((fractionaldivider * 8) + 50) / 100)) & ((uint8_t)0x07);
  }
  else /* if ((USARTx->CR1 & USART_CR1_OVER8) == 0) */
  {
    tmpreg |= ((((fractionaldivider * 16) + 50) / 100)) & ((uint8_t)0x0F);
  }

  /* Write to USART BRR register */
  USARTx->BRR = (uint16_t)tmpreg;
}

//#ifdef USE_UNICARD_MK3B
//	#define USARTx	USART2
//#else
//	#define USARTx	USART6
//#endif
/* main write function called from request decoder in main.c
 *
 */
int emu_SIO_write( int i_addroffset, unsigned int *io_data)
{
   unsigned char off = i_addroffset&0x03;
//   	DBGPRINTF(DBGINF, "SIO write (M1=0x%x) 0x%x, 0x%x\n",AddrM1,off,*io_data);
    switch( off) {
    case 0 :
			//data serial
			//USARTx->DR = *io_data;
			USART_SendData(USARTx, *io_data);
      break;
    case 2 :
			//data USB
			USART1_SendChar(*io_data);
			//xprintf("%c",*io_data);
      break;
    case 1 :
    case 3 :
			//command
			switch (SIO[off>>1].COMMAND) {
				case 0: SIO[off>>1].COMMAND=(*io_data)&0x07;
								break;
				case 1: SIO[off>>1].COMMAND=0;
								break;
				case 2: SIO[off>>1].COMMAND=0;
								break;
				case 3: SIO[off>>1].COMMAND=0;
								if (off==1) {
								  unsigned short tmpreg = USARTx->CR1;
									/* Clear World Length */
									tmpreg &= ~(USART_CR1_M);
									switch (((*io_data)>>6)&3) {
										case	0:	//5bit
										case	2:	//6bit
										case	1:	//7bit
										default:	//8bit
											//TODO smula, umime jenom 8 a 9 bit, takze osm a basta ;-)
											tmpreg |= USART_WordLength_8b;
											break;
									}
									/* Write to USART CR1 */
									USARTx->CR1 = tmpreg;
									//if (((*io_data)&0x01) //RX enable
									//if (((*io_data)&0x20) //auto CTS enable
								}
								break;
				case 4: SIO[off>>1].COMMAND=0;
								if (off==1) {
								  unsigned short tmpreg = USARTx->CR1;
									/* Clear World Length */
									tmpreg &= ~(USART_CR1_PS|USART_CR1_PCE);
									if ((*io_data)&1) {
										if ((*io_data)&2) {
											tmpreg |= USART_Parity_Even;
										} else tmpreg |= USART_Parity_Odd;
									} else tmpreg |= USART_Parity_No;
									/* Write to USART CR1 */
									USARTx->CR1 = tmpreg;
									//STOP bity ...
									tmpreg = USARTx->CR2;
									/* Clear STOP[13:12] bits */
									tmpreg &= ~(USART_CR2_STOP);
									/* Configure the USART Stop Bits, Clock, CPOL, CPHA and LastBit :
									Set STOP[13:12] bits according to USART_StopBits value */
									switch (((*io_data)>>2)&3) {
										case	1:	tmpreg |= USART_StopBits_1; break;
										case	2:	tmpreg |= USART_StopBits_1_5; break;
										default:	tmpreg |= USART_StopBits_2; break;
									}
									/* Write to USART CR2 */
									USARTx->CR2 = (uint16_t)tmpreg;
								}
								break;
				case 5: SIO[off>>1].COMMAND=0;
								break;
				case 6: SIO[off>>1].COMMAND=0;
								break;
				default:
								SIO[off>>1].COMMAND=0;
								break;
			}
      break;

    default:
       break;
    }

  return 0;
}

#define SIO_READ0_RXCHAR		0x01
#define SIO_READ0_TXEMPTY		0x04
#define SIO_READ0_DCD				0x08
#define SIO_READ0_CTS				0x20

#define SIO_READ1_ALLSENT		0x01

#include "usbd_cdc_vcp.h"
extern uint32_t APP_Rx_ptr_in;	//z vysilaciho bufferu VCP na USB
extern uint32_t APP_Rx_ptr_out;

/* main read function called from request decoder in main.c
 *
 */
int emu_SIO_read( int i_addroffset, unsigned int *io_data)
{
   unsigned char off = i_addroffset&0x03;

//   	DBGPRINTF(DBGINF, "SIO read (Addr=0x%x) 0x%x\n",AddrM1,off);
    switch(off) {
    case 0 :
			//data serial read
			*io_data = USARTx->DR;
      break;
    case 2 :
			//data USB read
			*io_data = USART1_getc();
      break;
    case 1 :
			//register
			switch(SIO[0].COMMAND) {
				case 0:
					*io_data=SIO_READ0_DCD|SIO_READ0_CTS/*|SIO_READ0_TXEMPTY*/;
					if (USARTx->SR & 0x00000080) *io_data|=SIO_READ0_TXEMPTY;
					if (USARTx->SR & 0x00000020) *io_data|=SIO_READ0_RXCHAR;		//RX not empty
					break;
				case 1:
					if (USARTx->SR & 0x00000080) {
						*io_data=SIO_READ1_ALLSENT;	//all sent
					} else {
						*io_data=0;	//jeste nic
					}
					break;
				case 2:
					break;
				default:
					*io_data=1;
					break;
			}
			SIO[0].COMMAND=0;
			break;
    case 3 :
			//register
			switch(SIO[1].COMMAND) {
				case 0:
					*io_data=SIO_READ0_DCD;
					if (APP_Rx_ptr_in>=APP_Rx_ptr_out) {
						if (APP_Rx_ptr_in-APP_Rx_ptr_out<APP_RX_DATA_SIZE-1) *io_data=SIO_READ0_DCD|SIO_READ0_CTS|SIO_READ0_TXEMPTY;
					} else if (APP_Rx_ptr_out-APP_Rx_ptr_in>1) *io_data=SIO_READ0_DCD|SIO_READ0_CTS|SIO_READ0_TXEMPTY;
					if ((!is_USARTshell_active())&&(USHELL_RX_input!=USHELL_RX_output)) *io_data|=SIO_READ0_RXCHAR;
					break;
				case 1:
					if (APP_Rx_ptr_in==APP_Rx_ptr_out) {
						*io_data=SIO_READ1_ALLSENT;	//all sent
					} else {
						*io_data=0;	//jeste nic
					}
					break;
				case 2:
					break;
				default:
					*io_data=1;
					break;
			}
			SIO[1].COMMAND=0;
			break;
    default:
       break;
    }

  return RETURN_SIO_OK;
}

/*
 * SIO_doInterrupt() - pokud je to potreba, tak posleme do MZ-800 signal /INT
 *
 * Tato fce. je volana vzdy pri ukonceni zpracovavaneho pozadavku na Unicard.
 *
 */
/*void emu_SIO_doInterrupt ( void ) {

    if ( ! SIO.EINT ) return; // radic neni v rezimu INT

    // Mame pripravena data ke cteni, nebo ocekavame data k zapisu?
#if FAT_WRITE_SUPPORT
    if ( ( SIO.DATA_COUNTER && ( SIO.COMMAND >> 5 == 0b011 || SIO.COMMAND >> 5 == 0b010 || SIO.COMMAND == 0x3f ) ) ||
    ( SIO.COMMAND == 0x0f || SIO.COMMAND == 0x0b ) ) {
#else
    if ( SIO.DATA_COUNTER && ( SIO.COMMAND >> 5 == 0b011 || SIO.COMMAND == 0x3f ) ) {
#endif

        // Signal /INT neposilame neustale, ale jen jednou za cas ...
        SIO.waitForInt++;

        // ... tedy po kazdem 2 pozadavku na Unicard
        if ( SIO.waitForInt > 2 ) {


        	DBGPRINTF(DBGINF, "SIO_doInterrupt(): SharpINT => ON!\n");


        	hal_SetSharpINT ();
        };

    };
}
#endif

void emu_SIO_mzint ( void ) {

    if ( ! SIO.EINT ) return; // radic neni v rezimu INT

    // Mame pripravena data ke cteni, nebo ocekavame data k zapisu?
#if FAT_WRITE_SUPPORT
    if ( ( SIO.DATA_COUNTER && ( SIO.COMMAND >> 5 == 0x03 || SIO.COMMAND >> 5 == 0x02 || SIO.COMMAND == 0x3f ) ) ||
    ( SIO.COMMAND == 0x0f || SIO.COMMAND == 0x0b ) ) {
#else
    if ( SIO.DATA_COUNTER && ( SIO.COMMAND >> 5 == 0b011 || SIO.COMMAND == 0x3f ) ) {
#endif

        // Signal /INT neposilame neustale, ale jen jednou za cas ...
        SIO.waitForInt++;

        // ... tedy po kazdem 2 pozadavku na Unicard
        if ( SIO.waitForInt > 2 ) {


        	DBGPRINTF(DBGINF, "SIO_doInterrupt(): SharpINT => ON!\n");


        	mzint_SetInterrupt ( mzintSIO );
        };

    };
}
*/

// this is the interrupt request handler (IRQ) for ALL USART1 interrupts
void USART6_IRQHandler(void){

	// check if the USART1 receive interrupt flag was set
	if( USART_GetITStatus(USART6, USART_IT_RXNE) ){

		char t = USART6->DR; // the character from the USART1 data register is saved in t

		//USART1_SendChar(t);	/////smazat //TODO
		
	}
}

// this is the interrupt request handler (IRQ) for ALL USART1 interrupts
void USART2_IRQHandler(void){

	// check if the USART1 receive interrupt flag was set
	if( USART_GetITStatus(USART2, USART_IT_RXNE) ){

		char t = USART2->DR; // the character from the USART1 data register is saved in t

		//USART1_SendChar(t);	/////smazat //TODO
		
	}
}

