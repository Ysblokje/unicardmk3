#include "stm32f4xx_eth.h"

#define	RMII_TX_GPIO_GROUPB	1
#define	RMII_TX_GPIO_GROUPG	2

#undef MII_MODE
#define RMII_MODE
#define RMII_TX_GPIO_GROUP	RMII_TX_GPIO_GROUPB

static void NVIC_Configuration(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;

    /* Enable the Ethernet global Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = ETH_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE/*ENABLE*/;
    NVIC_Init(&NVIC_InitStructure);
}

/*
* GPIO Configuration for ETH
*/
static void GPIO_Configuration(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /* Enable SYSCFG clock */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOC, ENABLE);

#if defined(MII_MODE)
		SYSCFG_ETH_MediaInterfaceConfig(SYSCFG_ETH_MediaInterface_MII);
#elif defined(RMII_MODE)
		SYSCFG_ETH_MediaInterfaceConfig(SYSCFG_ETH_MediaInterface_RMII);
#endif /* RMII_MODE */

		ETH->MACMIIAR = (ETH->MACMIIAR & (~ETH_MACMIIAR_CR)) | ETH_MACMIIAR_CR_Div102;
	
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

    /* config MDIO and MDC. */
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_ETH); /* config ETH_MDIO */
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource1, GPIO_AF_ETH); /* config ETH_MDC */
    /* config PA2: MDIO */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    /* config PC1: MDC */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    /* Ethernet pins configuration ************************************************/
#if defined(MII_MODE)
/*
ETH_MDIO ------------> PA2
ETH_MDC -------------> PC1

ETH_MII_CRS ---------> PA0
ETH_MII_COL ---------> PA3

ETH_MII_RX_CLK ------> PA1
ETH_MII_RX_ER -------> PB10
ETH_MII_RX_ER -------> PI10
ETH_MII_RX_DV -------> PA7
ETH_MII_RXD0 --------> PC4
ETH_MII_RXD1 --------> PC5
ETH_MII_RXD2 --------> PB0
ETH_MII_RXD3 --------> PB1

ETH_MII_TX_EN -------> PB11
ETH_MII_TX_EN -------> PG11
ETH_MII_TX_CLK ------> PC3
ETH_MII_TXD0 --------> PB12
ETH_MII_TXD0 --------> PG13
ETH_MII_TXD1 --------> PB13
ETH_MII_TXD1 --------> PG14
ETH_MII_TXD2 --------> PC2
ETH_MII_TXD3 --------> PB8
ETH_MII_TXD3 -------> PE2
*/

#error insert MII GPIO initial.
#elif defined(RMII_MODE)
/*
ETH_MDIO ------------> PA2
ETH_MDC -------------> PC1

ETH_RMII_REF_CLK ----> PA1

ETH_RMII_CRS_DV -----> PA7
ETH_RMII_RXD0 -------> PC4
ETH_RMII_RXD1 -------> PC5

ETH_RMII_TX_EN ------> PG11
ETH_RMII_TXD0 -------> PG13
ETH_RMII_TXD1 -------> PG14

ETH_RMII_TX_EN ------> PB11
ETH_RMII_TXD0 -------> PB12
ETH_RMII_TXD1 -------> PB13
*/
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource1, GPIO_AF_ETH); /* RMII_REF_CLK */
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_ETH); /* RMII_CRS_DV */

    /* configure PA1:RMII_REF_CLK, PA7:RMII_CRS_DV. */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_7;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_PinAFConfig(GPIOC, GPIO_PinSource4, GPIO_AF_ETH); /* RMII_RXD0 */
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource5, GPIO_AF_ETH); /* RMII_RXD1 */

    /* configure PC4:RMII_RXD0, PC5:RMII_RXD1. */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

# if RMII_TX_GPIO_GROUP == RMII_TX_GPIO_GROUPB
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

    GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_ETH); /* RMII_TX_EN */
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource12, GPIO_AF_ETH); /* RMII_TXD0 */
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_ETH); /* RMII_TXD1 */

    /* configure PB11:RMII_TX_EN, PB12:RMII_TXD0, PB13:RMII_TXD1 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
# elif RMII_TX_GPIO_GROUP == RMII_TX_GPIO_GROUPG
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG, ENABLE);

    GPIO_PinAFConfig(GPIOG, GPIO_PinSource11, GPIO_AF_ETH); /* RMII_TX_EN */
    GPIO_PinAFConfig(GPIOG, GPIO_PinSource13, GPIO_AF_ETH); /* RMII_TXD0 */
    GPIO_PinAFConfig(GPIOG, GPIO_PinSource14, GPIO_AF_ETH); /* RMII_TXD1 */

    /* configure PG11:RMII_TX_EN, PG13:RMII_TXD0, PG14:RMII_TXD1 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_13 | GPIO_Pin_14;
    GPIO_Init(GPIOG, &GPIO_InitStructure);
# else
# error RMII_TX_GPIO_GROUP setting error!
# endif /*RMII_TX_GPIO_GROUP */
#endif /* RMII_MODE */
}

//--------------------------------------------------------------------------------

void ETH_Init() {
	GPIO_Configuration();
	NVIC_Configuration();
}


