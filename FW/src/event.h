/*
 *    Copyright (c) 2012 by Bohumil Novacek <http://dzi.n.cz/8bit/>
 *
 *    Thies program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    MZ-800 Unicard is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MZ-800 Unicard; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef EVENT_H_
#define EVENT_H_

#define EVENT_VGA				0
#define EVENT_QD_READ		1
#define EVENT_QD_WRITE	2
#define EVENT_FDD_READ	3
#define EVENT_FDD_WRITE	4
#define EVENT_EMU_READ	5
#define EVENT_EMU_WRITE	6
#define EVENT_RAM_READ	7
#define EVENT_RAM_WRITE	8
#define EVENT_SIO_READ	9
#define EVENT_SIO_WRITE	10
#define EVENT_USART_SHELL	11
#define EVENT_DAC1_WAV		12
#define EVENT_DAC2_WAV		13
#define EVENT_ETH_POOL		14

#define EVENT_RAM_PREREAD	21
#define EVENT_REPO_PREREAD	22

typedef unsigned long t_event;

void init_event(void);	//inicializace pri startu
t_event peek_event(void);	//precte aktualni stav
void set_event(t_event evt);		//pridani eventu
void reset_event(t_event evt);	//odebrani eventu

#endif /* EVENT_H_ */
