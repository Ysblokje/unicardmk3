/*
 *    Copyright (c) 2012 by Bohumil Novacek <http://dzi.n.cz/8bit/>
 *
 *    Thies program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    MZ-800 Unicard is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MZ-800 Unicard; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/*
 * event.c
 *
 * Event manazer
 *
 * Set_event, reset_event (s atomickou operaci) a peek_event pro seznam aktualnich eventu
 *
 */

#include "event.h"

volatile static t_event CN__EVENT;	//nutno na 0x200xxxxx kvuli bit_bangingu

t_event peek_event() {
	t_event res;
		res = CN__EVENT;
	return res;
}

void init_event() { CN__EVENT = 0; }		//inicializace pri startu

void set_event(t_event evt) {	
	*((unsigned long *)(0x22000000 | ((((unsigned long)(&CN__EVENT))&0x000FFFFF)<<5) | (evt<<2))) = 1;
}

void reset_event(t_event evt) {	
	*((unsigned long *)(0x22000000 | ((((unsigned long)(&CN__EVENT))&0x000FFFFF)<<5) | (evt<<2))) = 0;
}

