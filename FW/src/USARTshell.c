/*
 *    Copyright (c) 2009 by Michal Hucik <http://www.ordoz.com>
 *    Copyright (c) 2012 by Bohumil Novacek <http://www.dzi.n.cz/8bit>
 *
 *    Thies program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    MZ-800 Unicard is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MZ-800 Unicard; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <string.h>

//#include "hal.h"
#include "stm32f4xx.h"
#include "ff.h"
#include "monitor.h"
#include "interrupt.h"
#include "event.h"
#include "emu_DAC.h"

extern struct netif gnetif;

//#include "usart1.h"

#include "crctab.h"

#define DBGLEVEL	(DBGNON /*| DBGFAT | DBGERR | DBGWAR | DBGINF*/)
//#define DBGLEVEL	(DBGNON | DBGFAT | DBGERR | DBGWAR | DBGINF)

#define USE_DBG_PRN	xprintf
#include "debug.h"

#define RETURN_OK  0
#define RETURN_ERR 1

#define bool unsigned char

// definice bool
#define true		1
#define false		0

/*
 *
 * USARTshell bezi na USB,
 *
 * Pokud existuje soubor /unicard/ushelldisabled.cfg, tak je USB k dispozici
 * pro Sharpa jako emulovane SIO kanal B.
 *
 * Po startu se USARTshell vzdy ozve hlasenim "Unicard - USART Shell" a na dalsim radku
 * oznami reset prostredi "OK:RESET".
 *
 * Chovani USARTshellu je ovlivnitelne ze Sharpa pres API MZFREPO:
 *
 *	cmdUSHELLON	- zapnuti shellu, vypnuti emu_SIO kanalu B
 *	cmdUSHELLOFF	- vypnuti shellu, zapnuti emu_SIO kanalu B
 *	cmdUSART1BPS	- nastaveni BPS pro USART6
 *
 * Pri vkladani prikazu je na USB vraceno s kazdym znakem jeho echo, takze v terminalu
 * bude videt co se do unikarty pise.
 *
 * Zadost o provedeni vkladaneho prikazu se provede zaslanim znaku s hodnotou
 * mensi jak 0x20 a vetsi, nez 0x00. Po vlozeni takoveho znaku posle unikarta
 * znak '\n' (0x0d, 0x0a).
 *
 *
 * Prikazy USARTshellu:
 * ====================
 *
 *
 *	\x00
 *		Jediny binarni prikaz.
 *		Provede reset vstupniho prikazoveho bufferu.
 *		Pokud se cte soubor, nebo adresar, tak bude zavren.
 *		Po vykonani prikazu unikarta odpovi "\nOK:RESET\n".
 *
 *
 *	MKDIR <path>
 *		Vytvorit adresar. Odpoved "OK:MKDIR <path>\n" pokud se jej podarilo
 *		vytvorit, jinak "ERR:MKDIR <path>\n".
 *
 *
 *	UNLINK <filepath>
 *		Smazat soubor/adresar. "OK:UNLINK <filepath>\n" pokud se smazal,
 *		nebo "ERR:UNLINK <filepath>\n" pri chybe.
 *
 *
 *	FILELIST <path>
 *		Vypise obsah adresare stejnym zpusobem, jakym to dela MZFREPO jen s
 *		tim rozdilem, ze mezi polozkami posila 0x0d, 0x0a.
 *		Pri uspesnem otevreni adresare vraci "OK:FILELIST <path>\n", pak
 *		nasleduje seznam souboru. Na konci seznamu posle "OK:FILELIST_DONE\n".
 *		Pokud se nepodarilo adresar otevrit, tak vraci "ERR:FILELIST <path>\n".
 *		Pokud pri nacitani jednotlivych polozek adresare dojde k chybe,
 *		tak unikarta posle "ERR:READDIR\n"
 *
 *	READDIR <path>
 *		Precteni polozek adresare v binarni strukture FILINFO.
 *
 *	READ <filepath>
 *		Otevre soubor pro cteni. Vraci "OK:READ <filepath>\n". Potom nasleduje
 *		jiz v binarnim tvaru jako DWORD pocet bajtu, ktere budou poslany a
 *		za nimi je poslan obsah souboru.
 *		Po uspesnem odeslani nasleduje hlaseni o uzavreni souboru a CRC32 "OK:CLOSE 0x????????\n".
 *		Pokud pri cteni souboru dojde k chybe, tak posle "ERR:READ\n" a dalsi data jiz neposila.
 *
 *
 *	OPEN <filepath>
 *	NEW <filepath>
 *		Otevre soubor pro zapis. NEW - zaklada vzdy novy soubor, pokud
 *		jiz existuje, tak vrati "ERR:NEW <filepath>\n", jinak "OK:NEW <filepath>\n".
 *		Pri pouziti OPEN bude existujici soubor prepsan, pokud neexistuje, tak
 *		se vytvori novy a nasleduje OK:, nebo ERR:.
 *		Po uspesnem otevreni souboru prejde vstup USARTshellu do binarniho rezimu
 *		ve kterem ocekava jako DWORD pocet bajtu, ktere budou do souboru zapsany.
 *		Po prijeti techto 4 bajtu unikarta posle HEX ASCII hodnotu cisla, ktere
 *		prijala, napr. "OK:SIZE 0xdeadbeef\n". Dalsi bajty uz jsou ulozeny do
 *		otevreneho souboru.
 *		Po uspesnem prenosu oznami uzavreni souboru a CRC32 "OK:CLOSE 0x????????\n".
 *		Pokud pri zapisu dojde k chybe, tak unikarta posle "ERR:WRITE\n" a
 *		ukonci binarni rezim.
 *		Pokud dele nez 2 sekundy neprijdou zadna data, tak unikarta soubor
 *		uzavre a prejde zpet do prikazoveho rezimu s hlasenim "ERR:DATA_TIMEOUT\n".
 *
 *
 *	FDDREMOUNT <0..3>
 *		Odmountuje prislusnou FD mechaniku a zavola remount, ktery se provede
 *		standardne podle obsahu /unicard/fd<0..3>.cfg
 *
 *	DATE
 *	DATE YYYY-MM-DD HH:MM:SS
 *		Bez parametru vrati aktualni cas. Pokud jsou uvedeny parametry,
 *		tak se pokusi podle nich nastavit datum a cas v RTC a vrati takovy
 *		cas, jaky se po teto zmene vyskytuje v RTC.
 *
 *
*/

#define USHELL_BUF_SIZE 128

typedef struct {
    bool	active;		// prepinani mezi USARTshell a emu_SIO
    uint8_t	mode;		// rezim: 0 - prikazovy, 1-4 - dword size, 5 - file data
    uint8_t	*buf;
    uint8_t	buf_byte [ USHELL_BUF_SIZE ];
    uint8_t	buf_count;
    uint32_t	fsize;
    FIL		fh;
    DIR		dh;
    bool	filelist;
    bool	read_file;	// true - znamena, ze mame otevreny soubor, ktery posilame ven
} USARTshell_s;

static USARTshell_s USARTshell;

#define USHELL_RX_SIZE	2048
static uint8_t USHELL_RX_Buffer[USHELL_RX_SIZE];
int USHELL_RX_input;
int USHELL_RX_output;

extern __IO uint32_t  EthStatus;
extern u16_t SET_Handler(int iIndex, char *pcInsert, int iInsertLen);

#include "usbd_cdc_vcp.h"
extern CDC_IF_Prop_TypeDef  APP_FOPS;
void USART1_SendChar(unsigned char c)
{
	APP_FOPS.pIf_DataTx(0,c);
}
uint16_t USART1_DataRX(uint8_t* Buf, uint32_t Len)
{
	if (Len>0) {
		while(Len>0) {
			USHELL_RX_Buffer[USHELL_RX_input]=*Buf++;
			if (USHELL_RX_input>=USHELL_RX_SIZE-1) {
				USHELL_RX_input=0;
			} else USHELL_RX_input++;
			Len--;
		}
		set_event(EVENT_USART_SHELL);
	}
	return 0;
}
int USART1_getc(void)
{
	int res=USHELL_RX_Buffer[USHELL_RX_output];
	if (USHELL_RX_output>=USHELL_RX_SIZE-1) {
		USHELL_RX_output=0;
	} else USHELL_RX_output++;
	return res;
}
void USART1_init(void) {
	USHELL_RX_input=0;
	USHELL_RX_output=0;
	APP_FOPS.pIf_DataRx=USART1_DataRX;
}

void USARTshell_on ( void ) {
    DBGPRINTF ( DBGINF, "USARTshell_on()\n" );
    USARTshell.dh.fs = 0;
    USARTshell.read_file = false;
    USARTshell.mode = 0;
    USARTshell.active = true;
    USARTshell.buf = USARTshell.buf_byte;
    USARTshell.buf_count = sizeof ( USARTshell.buf_byte );
    USART1_init ();
    xprintf ( "\nUnicard - USART Shell\nOK:RESET\n" );
}

void USARTshell_off ( void ) {
    DBGPRINTF ( DBGINF, "USARTshell_off()\n" );
    USARTshell.active = false;
    USART1_init ();
}

/* USARTshell_init()
 *
 */
int USARTshell_init ( void ) {
    FIL ff_file;
    DBGPRINTF ( DBGINF, "USARTshell_init()\n" );
    if ( FR_OK == f_open ( &ff_file, "/unicard/ushelldisabled.cfg", FA_READ ) ) {
        f_close ( &ff_file );
        USARTshell_off();
    } else {
        USARTshell_on();
    };
    return ( RETURN_OK );
}

/*
 *
 * Pokud je USART shell povolen, tak vraci true.
 */
uint8_t is_USARTshell_active ( void ) {
    return ( USARTshell.active );
}



/*
 *
 *
 */
void ReadDirField ( void ) {

    FILINFO finfo;
    char lfname [ _MAX_LFN ];

		set_event(EVENT_USART_SHELL);

    finfo.lfname = lfname;
    finfo.lfsize = _MAX_LFN;

    ReadDirField_NEXT_FIELD:

    if ( FR_OK != f_readdir ( &USARTshell.dh, &finfo ) ) {
        xprintf ( "ERR:READDIR\n" );
        USARTshell.dh.fs = 0;
        return;
    };

    // vynechat "."
    if ( ( finfo.fname[0] == '.' ) && ( finfo.fname[1] == '\x00' ) ) goto ReadDirField_NEXT_FIELD;

    // Posledni polozka?
    if ( finfo.fname[0] == '\x00' ) {
        if ( USARTshell.filelist ) {
            xprintf ( "OK:FILELIST_DONE\n" );
        } else {
            xprintf ( "OK:READDIR_DONE\n" );
        };
        USARTshell.dh.fs = 0;
        return;
    };

    if ( USARTshell.filelist ) {

        if ( finfo.lfname[0] ) {
            xprintf ( "%s", finfo.lfname );
        } else {
            xprintf ( "%s", finfo.fname );
        };

        if ( 0x10 == ( finfo.fattrib & 0x10 ) ) {
            xputc ( '/' );
        };

        xprintf ( "\n%d\n", finfo.fsize );
    } else {
        uint8_t i;
        char *c = (char *) &finfo;
        for ( i = 0; i < 22; i++ ) {
            USART1_SendChar ( c[0] );
            c++;
        };
        USART1_SendChar ( (uint8_t) strlen ( finfo.lfname ) );
        for ( i = 0; i < _MAX_LFN; i++ ) {
            USART1_SendChar ( finfo.lfname[i] );
        };
    };

}


unsigned int filecrc;

/*
 *
 *
 */
void ReadFile ( void ) {
    uint8_t i, c;
    unsigned int ff_readlen;

		set_event(EVENT_USART_SHELL);

    i = 32;

    while ( i-- ) {

        if ( FR_OK != f_read ( &USARTshell.fh, &c, 1, &ff_readlen ) ) {
            xprintf ( "ERR:READ\n" );
            USARTshell.read_file = false;
            return;
        };

        // konec souboru?
        if ( 1 != ff_readlen ) {
            xprintf ( "OK:CLOSE 0x%08lx\n", filecrc );
            USARTshell.read_file = false;
            return;
        };

        filecrc = UPDC32 ( c, filecrc );

        USART1_SendChar ( c );
    };
}

uint8_t a2n ( char c ) {
    if ( ( c >= 0x30 ) && ( c <= 0x39 ) ) return ( c - 0x30 );
    return ( 0 );
}

#define HAL_RTC_RAW_TIME	100		//2*50 => 2sec

unsigned long transferTimeout = 0;

void USARTshell_timeout( void ) {
	if (transferTimeout) {
		transferTimeout--;
		if (transferTimeout==0) {
	    	if ( USARTshell.mode ) {
        	    USARTshell.mode = 0;	// zpet do prikazoveho modu
        	    xprintf ( "ERR:DATA_TIMEOUT\n" );
        	    f_close ( &USARTshell.fh );
        	    USARTshell.buf = USARTshell.buf_byte;
        	    USARTshell.buf_count = sizeof ( USARTshell.buf_byte );
	    	};
		}
	}
}

//prikaz mouse
extern char kb_state;
extern int keyb_timeout;
extern char mouse;
extern unsigned char znak;

/*
 *
 *
 */
void do_USARTshell ( void ) {

    uint8_t MaxCalls = 32;	// zpracujeme max. 32 bajtu - pak jdeme zkontrolovat zda nevola Sharp

    if ( ! USARTshell.active ) return;

    // Pokud je otevreny adresar, tak vypiseme jednu polozku
    if ( USARTshell.dh.fs ) ReadDirField ();

    // Pokud se cte soubor, tak z nej kus posleme
    if ( USARTshell.read_file ) ReadFile ();

    while (/* USART1_rbufStatus() & 0xff */USHELL_RX_input!=USHELL_RX_output) {	// dokud mame neco v bufferu

        char c = USART1_getc();

        // Prichozi znak zastavi FILELIST a READ
        USARTshell.dh.fs = 0;
        USARTshell.read_file = false;

        if ( USARTshell.mode == 0 ) { // cekani na prikaz: 0x00 - reset; TXTcmd,0x0d - command

            if ( 0x20 <= c ) {	// ulozit znak do prikazoveho bufferu

                USARTshell.buf[0] = c;
                USARTshell.buf++;
                USARTshell.buf_count--;
                xputc ( c );
                if ( ! USARTshell.buf_count ) {
                    xprintf ("\nERR:BUFOVFL!\n" );
                    USARTshell.buf = USARTshell.buf_byte;
                    USARTshell.buf_count = sizeof ( USARTshell.buf_byte );
                };

            } else {		// prikazy USARTshellu

                if ( c == 0x00 ) {
                    xprintf ("OK:RESET\n" );

                } else {

                    xputc ('\n');
                    USARTshell.buf[0] = '\x00';

                    if ( strcmp( (char*) USARTshell.buf_byte, "DATE" ) == 0 ) {
//                        RTC_t rtc;
//                        rtc_getdatetime ( &rtc );
//                        xprintf ( "OK:DATE %d-%02d-%02d %02d:%02d:%02d\n", rtc.year, rtc.month, rtc.mday, rtc.hour, rtc.min, rtc.sec );
												RTC_DateTypeDef rtc_date;
												RTC_TimeTypeDef rtc_time;
												RTC_GetDate(RTC_Format_BIN,&rtc_date);
												RTC_GetTime(RTC_Format_BIN,&rtc_time);
                        xprintf ( "OK:DATE %d-%02d-%02d %02d:%02d:%02d\n", rtc_date.RTC_Year+2000, ((rtc_date.RTC_Month & 0x10)?((rtc_date.RTC_Month&0x0F)+10):(rtc_date.RTC_Month)), rtc_date.RTC_Date, rtc_time.RTC_Hours, rtc_time.RTC_Minutes, rtc_time.RTC_Seconds );

                    } else if ( strncmp( (char*) USARTshell.buf_byte, "DATE ", 5 ) == 0 ) {
												RTC_DateTypeDef rtc_date;
												RTC_TimeTypeDef rtc_time;
                        uint8_t *buf = USARTshell.buf_byte + 5;
                        if ( strlen ( (char *) buf ) == 19 ) {
                            rtc_date.RTC_Year = /*a2n( buf[0] ) * 1000 + a2n( buf[1] ) * 100 +*/ a2n( buf[2] ) * 10 + a2n( buf[3] );
                            rtc_date.RTC_Month = a2n( buf[5] ) * 10 + a2n( buf[6] );
                            rtc_date.RTC_Date = a2n( buf[8] ) * 10 + a2n( buf[9] );
														rtc_date.RTC_WeekDay = 0;
														rtc_time.RTC_H12 = RTC_H12_AM;
                            rtc_time.RTC_Hours = a2n( buf[11] ) * 10 + a2n( buf[12] );
                            rtc_time.RTC_Minutes = a2n( buf[14] ) * 10 + a2n( buf[15] );
                            rtc_time.RTC_Seconds = a2n( buf[17] ) * 10 + a2n( buf[18] );

														RTC_SetDate(RTC_Format_BIN,&rtc_date);
														RTC_SetTime(RTC_Format_BIN,&rtc_time);
														RTC_GetDate(RTC_Format_BIN,&rtc_date);
														RTC_GetTime(RTC_Format_BIN,&rtc_time);
														xprintf ( "OK:DATE %d-%02d-%02d %02d:%02d:%02d\n", rtc_date.RTC_Year+2000, ((rtc_date.RTC_Month & 0x10)?((rtc_date.RTC_Month&0x0F)+10):(rtc_date.RTC_Month)), rtc_date.RTC_Date, rtc_time.RTC_Hours, rtc_time.RTC_Minutes, rtc_time.RTC_Seconds );

                        } else {
                            xprintf ( "ERR:DATE_SET %s\n", buf );
                        };
                    } else if ( strncmp( (char*) USARTshell.buf_byte, "GDG", 3 ) == 0 ) {
											extern byte MZ_mode;
											extern byte MZ_graphic_mode;
                      xprintf ( "GDG:MZ_mode=%d,MZ_graphic_mode=%d\n", MZ_mode, MZ_graphic_mode );
                    } else if ( strncmp( (char*) USARTshell.buf_byte, "GDD", 3 ) == 0 ) {
											int i;
											for(i=0;i<0x8000;i+=0x7F0) {
												xprintf("DUMP %04X:",i);
												for(;!(i&16);i++) {
													xprintf(" %02X",VRAM[i]);
												}
												xprintf("\n");
											}
                      xprintf ( "GDG:MZ_mode=%d,MZ_graphic_mode=%d\n", MZ_mode, MZ_graphic_mode );
                    } else if ( strncmp( (char*) USARTshell.buf_byte, "MKDIR ", 6 ) == 0 ) {
                        USARTshell.buf = USARTshell.buf_byte + 6;
                        if ( FR_OK != f_mkdir ( (char*) USARTshell.buf ) ) {
                            xprintf ( "ERR:MKDIR %s\n", USARTshell.buf );
                        } else {
                            xprintf ( "OK:MKDIR %s\n", USARTshell.buf );
                        };

                    } else if ( strncmp( (char*) USARTshell.buf_byte, "UNLINK ", 7 ) == 0 ) {
                        USARTshell.buf = USARTshell.buf_byte + 7;
                        if ( FR_OK != f_unlink ( (char*) USARTshell.buf ) ) {
                            xprintf ( "ERR:UNLINK %s\n", USARTshell.buf );
                        } else {
                            xprintf ( "OK:UNLINK %s\n", USARTshell.buf );
                        };

                    } else if ( strncmp( (char*) USARTshell.buf_byte, "FILELIST ", 9 ) == 0 ) {
                        USARTshell.buf = USARTshell.buf_byte + 9;
                        if ( FR_OK != f_opendir ( &USARTshell.dh, (char*) USARTshell.buf ) ) {
                            xprintf ( "ERR:FILELIST %s\n", USARTshell.buf );
                            USARTshell.dh.fs = 0;
                        } else {
                            xprintf ( "OK:FILELIST %s\n", USARTshell.buf );
                            USARTshell.filelist = true;
                        };

                    } else if ( strncmp( (char*) USARTshell.buf_byte, "READDIR ", 8 ) == 0 ) {
                        USARTshell.buf = USARTshell.buf_byte + 8;
                        if ( FR_OK != f_opendir ( &USARTshell.dh, (char*) USARTshell.buf ) ) {
                            xprintf ( "ERR:READDIR %s\n", USARTshell.buf );
                            USARTshell.dh.fs = 0;
                        } else {
                            xprintf ( "OK:READDIR %s\n", USARTshell.buf );
                            USARTshell.filelist = false;
                        };

                    } else if ( strncmp( (char*) USARTshell.buf_byte, "READ ", 5 ) == 0 ) {
                        USARTshell.buf = USARTshell.buf_byte + 5;
                        if ( FR_OK != f_open ( &USARTshell.fh, (char*) USARTshell.buf, FA_READ ) ) {
                            xprintf ( "ERR:READ %s\n", USARTshell.buf );
                        } else {
                            char *c = (char*) &USARTshell.fh.fsize;
                            uint8_t i;
                            xprintf ( "OK:READ %s\n", USARTshell.buf );
                            USARTshell.read_file = true;
                            // poslat DWORD size
                            for ( i = 0; i < 4; i++ ) {
                                USART1_SendChar ( *(char *) c++ );
                            };
                            filecrc = 0;
                        };

                    } else if ( strncmp( (char*) USARTshell.buf_byte, "OPEN ", 5 ) == 0 ) {
                        USARTshell.buf = USARTshell.buf_byte + 5;
                        if ( FR_OK != f_open ( &USARTshell.fh, (char*) USARTshell.buf, FA_WRITE | FA_CREATE_ALWAYS ) ) {
                            xprintf ( "ERR:OPEN %s\n", USARTshell.buf );
                        } else {
                            xprintf ( "OK:OPEN %s\n", USARTshell.buf );
                            USARTshell.mode = 1;	// ted budeme cekat na dword vyjadrujici size
                            USARTshell.fsize = 0;
                            transferTimeout = HAL_RTC_RAW_TIME;
                        };

                    } else if ( strncmp( (char*) USARTshell.buf_byte, "NEW ", 4 ) == 0 ) {
                        USARTshell.buf = USARTshell.buf_byte + 4;
                        if ( FR_OK != f_open ( &USARTshell.fh, (char*) USARTshell.buf, FA_WRITE | FA_CREATE_NEW ) ) {
                            xprintf ( "ERR:NEW %s\n", USARTshell.buf );
                        } else {
                            xprintf ( "OK:NEW %s\n", USARTshell.buf );
                            USARTshell.mode = 1;	// ted budeme cekat na dword vyjadrujici size
                            USARTshell.fsize = 0;
                            transferTimeout = HAL_RTC_RAW_TIME;
                        };

                    } else if ( strncmp( (char*) USARTshell.buf_byte, "FDDREMOUNT ", 11 ) == 0 ) {
                        bool remount_err = true;
                        USARTshell.buf = USARTshell.buf_byte + 11;
                        if ( ( '\x00' == USARTshell.buf[1] ) && ( 0x30 <= USARTshell.buf[0] ) && ( 0x33 >= USARTshell.buf[0] ) ) {
                            // pujde reload FDD ?
                            if ( reload_FDD ) {
                                if ( -1 != reload_FDD ( ( USARTshell.buf[0] - 0x30 ) ) ) {
                                    remount_err = false;
                                } else {
                                    DBGPRINTF(DBGERR, "FDDREMOUNT - FDD%d remount error!\n", ( USARTshell.buf[0] - 0x30 )  );
                                };
                            } else {
                                DBGPRINTF ( DBGERR, "FDDREMOUNT - No FDD remount function\n" );
                            };
                        };
                        if ( remount_err ) {
                            xprintf ( "ERR:FDDREMOUNT %s\n", USARTshell.buf );
                        } else {
                            xprintf ( "OK:FDDREMOUNT %s\n", USARTshell.buf );
                        };


                    } else if ( strncmp( (char*) USARTshell.buf_byte, "PLAY ", 5 ) == 0 ) {
												int res;
                        USARTshell.buf = USARTshell.buf_byte + 5;
                        if ( ( res = emu_DAC_PLAY( (char*) USARTshell.buf ) ) != 0) {
                            xprintf ( "ERR:PLAY %s\n", USARTshell.buf );
                            xprintf ( "ERR:PLAY %d\n", res );
                        } else {
                            xprintf ( "OK:PLAY %s\n", USARTshell.buf );
                        };

                    } else if ( strncmp( (char*) USARTshell.buf_byte, "STOP", 4 ) == 0 ) {
                        emu_DAC_STOP();
                        xprintf ( "OK:STOP\n");
                    } else if ( strncmp( (char*) USARTshell.buf_byte, "STATE", 4 ) == 0 ) {
                        xprintf ( "OK:STATE (%04X)=%02X, IN=%02X, OUT=%02X\n",GET_MZADDR(),GET_MZDATA(),(GPIOD->IDR)>>8,(GPIOC->IDR)&0x0D);
                    } else if ( strncmp( (char*) USARTshell.buf_byte, "LANINFO", 7 ) == 0 ) {
												xprintf ( "KSZ8031:        " );
												if (PHY_ETH_OK) {
													xprintf ( "OK\n" );
												} else {
													xprintf ( "ERR\n" );
												}
												xprintf ( "PHY_ID:         " );
												{
													char s[32];
													int len=SET_Handler(14,s,32);
													s[len]=0;
													xprintf("%s\n",s);
												}
												xprintf ( "LINK:           " );
												if (gnetif.flags & NETIF_FLAG_LINK_UP/*EthStatus & ETH_LINK_FLAG*/) {
													xprintf ( "UP\n" );
												} else {
													xprintf ( "DOWN\n" );
												}
												xprintf ( "HWADDR:         " );
												{
													char s[32];
													int len=SET_Handler(0,s,32);
													s[len]=0;
													xprintf("%s\n",s);
												}
												xprintf ( "IPCFG:          " );
												xprintf ( "STATIC\n" );
												xprintf ( "IPADDR:         " );
												{
													char s[32];
													int len=SET_Handler(1,s,32);
													s[len]=0;
													xprintf("%s\n",s);
												}
												xprintf ( "NETMASK:        " );
												{
													char s[32];
													int len=SET_Handler(4,s,32);
													s[len]=0;
													xprintf("%s\n",s);
												}
												xprintf ( "GATEWAY:        " );
												{
													char s[32];
													int len=SET_Handler(5,s,32);
													s[len]=0;
													xprintf("%s\n",s);
												}
												xprintf("OK:LANINFO_DONE\n");
                    } else if ( strncmp( (char*) USARTshell.buf_byte, "MOUSE", 5 ) == 0 ) {
												xprintf ( "STATE:%d\n",kb_state );
												xprintf ( "TMOUT:%d\n",keyb_timeout );
												xprintf ( "MOUSE:%d\n",mouse );
												xprintf ( "ZNAK :%d\n",znak );
												xprintf ( "ATARI:%d\n",ATARI_MOUSE );
                    } else {
                        xprintf ("ERR:UNKNOWN '%s'\n", USARTshell.buf_byte );

                    };
                    USARTshell.buf = USARTshell.buf_byte;
                    USARTshell.buf_count = sizeof ( USARTshell.buf_byte );
                };	// if ( prikaz )
            };  // if (znak do bufferu, nebo vykonat prikaz)


        } else if ( 5 > USARTshell.mode ) {	// prichazeji nam jednotlive bajty k size
            USARTshell.fsize += c << ( (USARTshell.mode-1)*8 );
            USARTshell.mode++;
            if ( 5 == USARTshell.mode ) {
                xprintf ( "OK:SIZE 0x%08lx\n", USARTshell.fsize );
            };
            transferTimeout = HAL_RTC_RAW_TIME;
            filecrc = 0;

        } else {	// cmd_mode==5: prichazeji nam data pro ulozeni do filete

            USARTshell.buf[0] = c;
            USARTshell.buf++;
            USARTshell.buf_count--;
            USARTshell.fsize--;

            if ( ( ! USARTshell.buf_count ) || ( ! USARTshell.fsize) ) {
                uint8_t i;

                unsigned int ff_wlen;
                uint8_t savesize = sizeof ( USARTshell.buf_byte ) - USARTshell.buf_count;
                if ( FR_OK != f_write ( &USARTshell.fh, USARTshell.buf_byte, savesize, &ff_wlen ) ) {
                    xprintf ( "ERR:WRITE\n" );
                    f_close ( &USARTshell.fh );
                    USARTshell.mode = 0;	// zpet do prikazoveho modu
                    USARTshell.buf = USARTshell.buf_byte;
                    USARTshell.buf_count = sizeof ( USARTshell.buf_byte );
                } else {
                    f_sync ( &USARTshell.fh );
                };

                for ( i = 0; i < savesize; i++ ) {
                    //filecrc = updcrc ( USARTshell.buf_byte[i], filecrc );
                    filecrc = UPDC32 ( USARTshell.buf_byte[i], filecrc );
                };

                USARTshell.buf = USARTshell.buf_byte;
                USARTshell.buf_count = sizeof ( USARTshell.buf_byte );
                if ( ! USARTshell.fsize ) {
                    //filecrc = updcrc ( 0, updcrc ( 0, filecrc ) );
                    f_close ( &USARTshell.fh );
                    //xprintf ( "OK:CLOSE 0x%04x\n", filecrc & 0xffff );
                    xprintf ( "OK:CLOSE 0x%08lx\n", filecrc );
                    USARTshell.mode = 0;	// zpet do prikazoveho modu
                };
            };
            transferTimeout = HAL_RTC_RAW_TIME;
        };

				set_event(EVENT_USART_SHELL);

        MaxCalls--;
        if ( ! MaxCalls ) break; // Jsme tu moc dlouho, zkontrolujem zda nas nevola Sharp

    };

}

