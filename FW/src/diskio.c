/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2007        */
/*-----------------------------------------------------------------------*/
/* This is a stub disk I/O module that acts as front end of the existing */
/* disk I/O modules and attach it to FatFs module with common interface. */
/*-----------------------------------------------------------------------*/

#include "diskio.h"

#include <string.h> // memcpy

#include "stm32f4xx.h"
#include "stm32f4_discovery_sdio_sd.h"

#include "misc.h"

#define BLOCK_SIZE            512 /* Block Size in Bytes */

#include "monitor.h"
#define DBGLEVEL	(DBGNON | DBGFAT | DBGERR | DBGWAR | DBGINF)
#define USE_DBG_PRN	xprintf
#include "debug.h"

/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */

DSTATUS disk_initialize (
        BYTE drv                                /* Physical drive nmuber (0..) */
)
{
        SD_Error  Status;

//	printf("disk_initialize %d\n", drv);

 	/* Supports only single drive */
        if (drv)
        {
                return STA_NOINIT;
        }
/*-------------------------- SD Init ----------------------------- */
  Status = SD_Init();
        if (Status!=SD_OK )
        {
//					puts("Initialization Fail");
                return STA_NOINIT;

        }
        else
        {
				NVIC_InitTypeDef NVIC_InitStructure;
                // SDIO Interrupt ENABLE
                NVIC_InitStructure.NVIC_IRQChannel = SDIO_IRQn;
                NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;
                NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
                NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
                NVIC_Init(&NVIC_InitStructure);
                // DMA2 STREAMx Interrupt ENABLE
                NVIC_InitStructure.NVIC_IRQChannel = SD_SDIO_DMA_IRQn;
                NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 6;
                NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
                NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
                NVIC_Init(&NVIC_InitStructure);

                return RES_OK;
        }

}



/*-----------------------------------------------------------------------*/
/* Return Disk Status                                                    */

DSTATUS disk_status (
        BYTE drv                /* Physical drive nmuber (0..) */
)
{
	DSTATUS stat = 0;

	if (SD_Detect() != SD_PRESENT)
		stat |= STA_NODISK;

	// STA_NOTINIT - Subsystem not initailized
	// STA_PROTECTED - Write protected, MMC/SD switch if available

	return(stat);
}

//extern void disk_callback(void);

/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */

DRESULT disk_read (
        BYTE drv,               /* Physical drive nmuber (0..) */
        BYTE *buff,             /* Data buffer to store read data */
        DWORD sector,   				/* Sector address (LBA) */
        BYTE count              /* Number of sectors to read (1..255) */
)
{
	SD_Error Status;

	int pokusy;

//	disk_callback();
	
//	printf("disk_read %d %p %10d %d\n",drv,buff,sector,count);

	if (SD_Detect() != SD_PRESENT)
		return(RES_NOTRDY);

	if ((DWORD)buff & 3) // DMA Alignment failure, do single up to aligned buffer
	{
		DRESULT res = RES_OK;
		DWORD scratch[BLOCK_SIZE / 4]; // Alignment assured, you'll need a sufficiently big stack

		while(count--)
		{
			res = disk_read(drv, (void *)scratch, sector++, 1);

			if (res != RES_OK)
				break;

			memcpy(buff, scratch, BLOCK_SIZE);

			buff += BLOCK_SIZE;
		}

		return(res);
	}

#ifndef SD_POLLING_MODE
	Status = SD_ReadBlockFIXED(buff, sector, BLOCK_SIZE/*, count*/); // 4GB Compliant
	if (Status == SD_OK)
	{
		SDTransferState State;

		Status = SD_WaitReadOperation(); // Check if the Transfer is finished

		while((State = SD_GetStatus()) == SD_TRANSFER_BUSY); // BUSY, OK (DONE), ERROR (FAIL)

		if ((State == SD_TRANSFER_ERROR) || (Status != SD_OK))
			return(RES_ERROR);
		else
			return(RES_OK);
#else
//  Status = SD_ReadMultiBlocksFIXED(buff, sector, BLOCK_SIZE, count); // 4GB Compliant
	pokusy = 1000;
	do {
		Status = SD_ReadBlockFIXED(buff, sector, BLOCK_SIZE/*, count*/); // 4GB Compliant
		pokusy--;
	} while ((/*(Status==SD_CMD_RSP_TIMEOUT)||*/(Status==SD_RX_OVERRUN)||(Status==SD_ILLEGAL_CMD))&&(pokusy>0));

	if (Status == SD_OK)
	{
			count--;
			buff += BLOCK_SIZE;
			sector++;
			if (count<=0) return(RES_OK);
			else return disk_read(drv, buff, sector, count);
#endif
	}
	else {
		DBGPRINTF ( DBGERR, "Error disk_read %d\n", Status );
		return(RES_ERROR);
	}
}

/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */

#if _READONLY == 0
DRESULT disk_write (
        BYTE drv,                       /* Physical drive nmuber (0..) */
        const BYTE *buff,       /* Data to be written */
        DWORD sector,           /* Sector address (LBA) */
        BYTE count                      /* Number of sectors to write (1..255) */
)
{
	SD_Error Status;

//	printf("disk_write %d %p %10d %d\n",drv,buff,sector,count);

	if (SD_Detect() != SD_PRESENT)
		return(RES_NOTRDY);

	if ((DWORD)buff & 3) // DMA Alignment failure, do single up to aligned buffer
	{
		DRESULT res = RES_OK;
		DWORD scratch[BLOCK_SIZE / 4]; // Alignment assured, you'll need a sufficiently big stack

		while(count--)
		{
			memcpy(scratch, buff, BLOCK_SIZE);

			res = disk_write(drv, (void *)scratch, sector++, 1);

			if (res != RES_OK)
				break;

			buff += BLOCK_SIZE;
		}

		return(res);
	}

  Status = SD_WriteMultiBlocksFIXED((uint8_t *)buff, sector, BLOCK_SIZE, count); // 4GB Compliant
//  Status = SD_WriteBlockFIXED((uint8_t *)buff, sector, BLOCK_SIZE/*, count*/); // 4GB Compliant

	if (Status == SD_OK)
	{
//#ifndef SD_POLLING_MODE		//TODO write zatim neumi bez DMA !!!
		SDTransferState State;

		Status = SD_WaitWriteOperation(); // Check if the Transfer is finished

		while((State = SD_GetStatus()) == SD_TRANSFER_BUSY); // BUSY, OK (DONE), ERROR (FAIL)

		if ((State == SD_TRANSFER_ERROR) || (Status != SD_OK))
			return(RES_ERROR);
		else
			return(RES_OK);
/*#else
			count--;
			buff += BLOCK_SIZE;
			sector++;
			if (count<=0) return(RES_OK);
			else return disk_read(drv, (BYTE *)buff, sector, count);
#endif*/
	}
	else
		return(RES_ERROR);
}
#endif /* _READONLY */




/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */

DRESULT disk_ioctl (
        BYTE drv,               /* Physical drive nmuber (0..) */
        BYTE ctrl,              /* Control code */
        void *buff              /* Buffer to send/receive control data */
)
{
        return RES_OK;
}

#include "stm32f4xx_rtc.h"

/*-----------------------------------------------------------------------*/
/* Get current time                                                      */
/*-----------------------------------------------------------------------*/
DWORD get_fattime(void){
	RTC_DateTypeDef date;
	RTC_TimeTypeDef time;
	RTC_GetDate(RTC_Format_BIN,&date);
	RTC_GetTime(RTC_Format_BIN,&time);
	return	 (((date.RTC_Year - 1980 + 2000)&(0x7F))<<25)
			|((((date.RTC_Month & 0x10)?((date.RTC_Month&0x0F)+10):(date.RTC_Month))&(0x0F))<<21)
			|(((date.RTC_Date)&(0x1F))<<16)
			|(((time.RTC_Hours)&(0x1F))<<11)
			|(((time.RTC_Minutes)&(0x3F))<<5)
			|(((time.RTC_Seconds)&(0x3E))>>1)
			;
}

