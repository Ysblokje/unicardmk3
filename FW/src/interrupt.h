#ifndef __INTERRUPT_H__
#define __INTERRUPT_H__

#include "stm32f4xx.h"
#include "pal.h"

#define MZBUS_M1			(0x7C)
#define MZBUS_WR			(0xBC)
#define MZBUS_RD			(0xDC)
#define MZBUS_IORQ		(0xEC)
#define MZBUS_MREQ		(0xF4)
#define MZBUS_HALT		(0xF8)

#define MZBUS_IEI 		(0x02)
#define MZBUS_RESET		(0x01)

#define EXWAIT_OFF()	{ GPIOC->BSRRL=(1<<0); }
#define EXWAIT_ON()		{ GPIOC->BSRRH=(1<<0); }
#define EXRESET_OFF()	{ GPIOC->BSRRL=(1<<3); }
#define EXRESET_ON()	{ GPIOC->BSRRH=(1<<3); }
#define EXINT_OFF()		{ GPIOC->BSRRL=(1<<2); }
#define EXINT_ON()		{ GPIOC->BSRRH=(1<<2); }
#define GET_MZBUS()		(GPIOD->IDR)

#define GET_MZADDR()	(GPIOE->IDR)
#define GET_MZDATA()	((GPIOB->IDR)&0xFF)

#ifdef MZ700_ONLY
#define PUT_MZDATA(a)	{ GPIOB->MODER|=0x5555;GPIOB->ODR=(a);/*GPIOB->MODER=0x00065555;*/ }
#else
#define PUT_MZDATA(a)	{ GPIOB->MODER|=0x5555;GPIOB->BSRRH=0xFF;GPIOB->BSRRL=(a);/*GPIOB->MODER=0x00065555;*/ }
#endif
#define MZDATA_OFF()	{ GPIOB->MODER&=0xFFFF0000; }

#define hal_ResetSharpINT()	EXINT_OFF()
#define hal_SetSharpINT()		EXINT_ON()

/*ukazatel na funkci, ktera reloaduje floppy disky */
extern int8_t (*reload_FDD)( uint8_t drive_id );

extern void IO_tabs_init(void);
extern void interrupt_init(int MK3b);

extern void IOWRPortListAdd(int len, unsigned char* ports);
extern void IORDPortListAdd(int len, unsigned char* ports);

extern volatile int	QD_event_addr;
extern volatile int	QD_event_data;

extern volatile int	FDD_event_addr;
extern volatile int	FDD_event_data;

extern volatile int	RAM_event_addr;
extern volatile int	RAM_event_data;

extern volatile int	SIO_event_addr;
extern volatile int	SIO_event_data;

extern volatile char PALETE_UPDATE;

typedef void (*P_WRITE)(register word, register byte);

extern byte MZ_mode;
extern byte MZ_graphic_mode;
extern word SCROLL_SSA;
extern word SCROLL_SEA;
extern word SCROLL_SW;
extern word SCROLL_SOF;
extern byte MZ_BORDER;
//extern byte ZX_EMUL;
extern byte ATARI_MOUSE;

extern unsigned short PSG_noise;
extern short PSG_volume[4];
extern unsigned short PSG_period[4];
extern unsigned short PSG_last[4];

extern void ct0_update(void);
extern int ct0_getvalue(void/*int i, int ii*/);
extern void ct0_next(void);
extern void ct0_newstate(int mode);

#endif

