/*
 *    Copyright (c) 2009 by Michal Hucik <http://www.ordoz.com>
 *    Copyright (c) 2012 by Bohumil Novacek <http://dzi.n.cz/8bit/>
 *
 *    Thies program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    MZ-800 Unicard is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MZ-800 Unicard; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>

#include "interrupt.h"
#include "ff.h"

#include "emu_RAMDISC.h"
#include "event.h"

#define RETURN_EMU_OK  0
#define RETURN_EMU_ERR 1

unsigned char RAMDISK[RAMDISK_SIZE*2];	//vyrovnavaci buffer pro stranku ramdisku

/*
 *
 *

 Rezim RD:
 =========


 -W 0xe9 - stranka 0x00 - 0xff
 RW 0xea - I/O data + increment addr
 -W 0xeb - zapis adresy, DATA = LowBYTE, Addr(8-15) = HiBYTE (na unicard jen dolni bajt)
 R- 0xf8 - R - reset adresy a stranky, W - HiBYTE


 Rezim SRAM:
 ===========

 R- 0xf8 - reset adresy
 R- 0xf9 - read + inc
 -W 0xfa - write + inc

 *
 *
 */

#include "main.h"

RAMdisc_s RAMdisc;

int emu_RAMDISC_seek ( unsigned long addr ) {
	UINT s1;
	if (RAMdisc.dirty) {
		f_lseek ( &RAMdisc.fh, RAMdisc.addr );
		f_write( &RAMdisc.fh, &RAMDISK[RAMdisc.buff_id], RAMDISK_SIZE, &s1 );
		RAMdisc.dirty = 0;
		if (RAMdisc.preread_addr == addr) {
			RAMdisc.preread_ready = 0;
		}
	}
	if ((RAMdisc.preread_ready == 1)&&(RAMdisc.preread_addr == addr ))	{ //uz mam nacteno
		RAMdisc.buff_id ^= RAMDISK_SIZE;
		RAMdisc.preread_ready = 0;
		RAMdisc.addr = addr;
		return ( RETURN_EMU_OK );
	} else if ( FR_OK != f_lseek ( &RAMdisc.fh, addr ) ) {
		f_close( &RAMdisc.fh);
    return ( RETURN_EMU_ERR );
  }
	f_read( &RAMdisc.fh, &RAMDISK[RAMdisc.buff_id], RAMDISK_SIZE, &s1 );
	RAMdisc.addr = addr;
  return ( RETURN_EMU_OK );
};

/*
 * Volani jednou za TV snimek (50Hz), vyprsi-li timeout, zapise zmeneny buffer ramdisku
 *
 */
void emu_RAMDISC_timeout() {
	UINT s1;
	if (RAMdisc.timeout) {
		RAMdisc.timeout--;
		if (RAMdisc.timeout==0) {
			if (RAMdisc.dirty) {
				f_lseek ( &RAMdisc.fh, RAMdisc.addr/*( ( RAMdisc.bank << 16 ) | (RAMdisc.offset & RAMDISK_MASK_HI) )*/ );	//kde se prave nalezame
				f_write( &RAMdisc.fh, &RAMDISK[RAMdisc.buff_id], RAMDISK_SIZE, &s1 );
				RAMdisc.dirty = 0;
			}
		}
	}
}

/*
 * Cteni aktualniho byte ramdisku, pokud je potreba nacte novou stranku do bufferu
 *
 */
void rd_read1(unsigned char *sync1/*, UINT *s1*/)
{
//	*sync1=RAMDISK[RAMdisc.offset&RAMDISK_MASK_LOW];
//	unsigned char mem=RAMDISK[(RAMdisc.offset&RAMDISK_MASK_LOW)|RAMdisc.buff_id];
	if ((( RAMdisc.bank << 16 ) | (RAMdisc.offset & RAMDISK_MASK_HI)) != RAMdisc.addr) {
		emu_RAMDISC_seek(( RAMdisc.bank << 16 ) | (RAMdisc.offset & RAMDISK_MASK_HI));
	} else if (
						(!(peek_event()&(1<<EVENT_RAM_PREREAD)))
						&&
						(
							(RAMdisc.preread_ready==0)
							||(RAMdisc.preread_addr != (( RAMdisc.bank << 16 ) | ((RAMdisc.offset&RAMDISK_MASK_HI)&0x0000FFFFUL)) + RAMDISK_SIZE)
						)
					) {	//pokud uz neni zadano, precteno nebo nesedi adresa
							RAMdisc.preread_ready = 0;
							RAMdisc.preread_addr = (( RAMdisc.bank << 16 ) | ((RAMdisc.offset&RAMDISK_MASK_HI)&0x0000FFFFUL)) + RAMDISK_SIZE;
							set_event(EVENT_RAM_PREREAD);
					}
//	*s1=1;
	*sync1=RAMDISK[(RAMdisc.offset&RAMDISK_MASK_LOW)|RAMdisc.buff_id];
	RAMdisc.offset++;
}

/*
 * Zapis do bufferu ramdisku, pokud je potreba, ulozi a nacte novou stranku, jinak nastavi timeout pro zapis
 *
 */
void rd_write1(unsigned char sync1/*, UINT *s1*/)
{
	if ((( RAMdisc.bank << 16 ) | (RAMdisc.offset & RAMDISK_MASK_HI)) != RAMdisc.addr) {
		emu_RAMDISC_seek(( RAMdisc.bank << 16 ) | (RAMdisc.offset & RAMDISK_MASK_HI));
	} else if (
						(!(peek_event()&(1<<EVENT_RAM_PREREAD)))
						&&
						(
							(RAMdisc.preread_ready==0)
							||(RAMdisc.preread_addr != (( RAMdisc.bank << 16 ) | ((RAMdisc.offset&RAMDISK_MASK_HI)&0x0000FFFFUL)) + RAMDISK_SIZE)
						)
					) {	//pokud uz neni zadano, precteno nebo nesedi adresa
							RAMdisc.preread_ready = 0;
							RAMdisc.preread_addr = (( RAMdisc.bank << 16 ) | ((RAMdisc.offset&RAMDISK_MASK_HI)&0x0000FFFFUL)) + RAMDISK_SIZE;
							set_event(EVENT_RAM_PREREAD);
	}
	if (RAMDISK[(RAMdisc.offset&RAMDISK_MASK_LOW)|RAMdisc.buff_id]!=sync1) {
		RAMDISK[(RAMdisc.offset&RAMDISK_MASK_LOW)|RAMdisc.buff_id]=sync1;
		RAMdisc.dirty = 1;
		RAMdisc.timeout = 50; //nezmeni-li nekdo stranku, zapis nejpozdeji po jedne sekunde
	}
	RAMdisc.offset++;
//	if ((RAMdisc.offset&RAMDISK_MASK_LOW)==0) {
//		emu_RAMDISC_seek(( RAMdisc.bank << 16 ) | (RAMdisc.offset & RAMDISK_MASK_HI));
//	}
//	*s1=1;
}

/*
 * Inicializace ramdisku pri startu
 *
 */
int emu_RAMDISC_Init ( void ) {
	UINT s1;

	RAMdisc.ready = 0;
  RAMdisc.dirty = 0;
	RAMdisc.timeout = 0;

  if ( FR_OK != f_open( &RAMdisc.fh, "/unicard/ramdisc.dat", FA_READ|FA_WRITE ) ) {
    return ( 0 );
  };

	if (RAMdisc.fh.fsize<0x00010000) return 0;	//prilis maly, alespon 64k
	else if (RAMdisc.fh.fsize<0x00020000) RAMdisc.bank_mask = 0x00;	//64k
	else if (RAMdisc.fh.fsize<0x00040000) RAMdisc.bank_mask = 0x01;	//128k
	else if (RAMdisc.fh.fsize<0x00080000) RAMdisc.bank_mask = 0x03;	//256k
	else if (RAMdisc.fh.fsize<0x00100000) RAMdisc.bank_mask = 0x07;	//512k
	else if (RAMdisc.fh.fsize<0x00200000) RAMdisc.bank_mask = 0x0F;	//1M
	else if (RAMdisc.fh.fsize<0x00400000) RAMdisc.bank_mask = 0x1F;	//2M
	else if (RAMdisc.fh.fsize<0x00800000) RAMdisc.bank_mask = 0x3F;	//4M
	else if (RAMdisc.fh.fsize<0x01000000) RAMdisc.bank_mask = 0x7F;	//8M
	else RAMdisc.bank_mask = 0xFF;	//16M ale uz staci :-)

	f_read( &RAMdisc.fh, RAMDISK, RAMDISK_SIZE, &s1 );
	RAMdisc.addr = 0;

//	f_close( &RAMdisc.fh);

  RAMdisc.bank = 0;
  RAMdisc.offset = 0;
	RAMdisc.ready=1;

	RAMdisc.buff_id = 0;
	RAMdisc.preread_ready = 0;
	
  return ( 1 );
};

/*
 *
 * 0xe9 - set bank
 * 0xfa
 * 0xea	- write data
 * 0xeb - set LowBYTE addr
 * 0xf8	- set HiBYTE addr
 *
 * 0xec - PEZIK (ignorujeme)
 *
 */
int emu_RAMDISC_write( int i_addroffset, unsigned int *io_data)
{
//  unsigned int ff_writelen;

	if (!RAMdisc.ready) {return 0;}

	switch (i_addroffset&0xFF) {
		case 0xE9:
			// nastavit banku
			{
//				uint8_t	last = RAMdisc.bank;
				RAMdisc.bank = (*io_data)&RAMdisc.bank_mask;
//				if (last != RAMdisc.bank) {
//					emu_RAMDISC_seek(( RAMdisc.bank << 16 ) | (RAMdisc.offset & RAMDISK_MASK_HI));
//				}
			}
			break;
		case 0xEA:
		case 0xFA:
			rd_write1(*io_data/*,&ff_writelen*/);
			break;
		case 0xF8:
			// offset HiBYTE
			i_addroffset = (i_addroffset&0xFF) | ( *io_data << 8 );
			*io_data = ( RAMdisc.offset & 0x00ff );
			//tady spravne chybi break ! :-)
		case 0xEB:
			{
//				uint16_t last = RAMdisc.offset;
				*io_data = (*io_data & 0xFF)|(i_addroffset&0xFF00);				//Low=D0-7, High=A8-15
				RAMdisc.offset = *io_data;
//				if ((last & RAMDISK_MASK_HI) != (RAMdisc.offset & RAMDISK_MASK_HI)) {
//					emu_RAMDISC_seek(( RAMdisc.bank << 16 ) | (RAMdisc.offset & RAMDISK_MASK_HI));
//				}
			}
			break;
		case 0xF9:
			// offset LowBYTE
			RAMdisc.offset = ( RAMdisc.offset & 0xff00 ) | (*io_data & 0xFF);	//buffer je snad vetsi jak 256B, takze neni potreba menit segment
			break;
	}
  return ( RETURN_EMU_OK );
};

// 0 = 
int emu_RAMDISC_read_interrupt(unsigned char *io_data) {
//	if (RAMdisc.ready) {
		if ((( RAMdisc.bank << 16 ) | (RAMdisc.offset & RAMDISK_MASK_HI)) != RAMdisc.addr) {
			if ((RAMdisc.preread_ready)&&(!RAMdisc.dirty)&&((( RAMdisc.bank << 16 ) | (RAMdisc.offset & RAMDISK_MASK_HI)) == RAMdisc.preread_addr)) {
				RAMdisc.buff_id ^= RAMDISK_SIZE;
				RAMdisc.preread_ready = 0;
				RAMdisc.addr = RAMdisc.preread_addr;
			} else return 0;
		}
		if (
						(!(peek_event()&(1<<EVENT_RAM_PREREAD)))
						&&
						(
							(RAMdisc.preread_ready==0)
							||(RAMdisc.preread_addr != (( RAMdisc.bank << 16 ) | ((RAMdisc.offset&RAMDISK_MASK_HI)&0x0000FFFFUL)) + RAMDISK_SIZE)
						)
					) {	//pokud uz neni zadano, precteno nebo nesedi adresa
							RAMdisc.preread_ready = 0;
							RAMdisc.preread_addr = (( RAMdisc.bank << 16 ) | ((RAMdisc.offset&RAMDISK_MASK_HI)&0x0000FFFFUL)) + RAMDISK_SIZE;
							set_event(EVENT_RAM_PREREAD);
		}
		*io_data=RAMDISK[(RAMdisc.offset&RAMDISK_MASK_LOW)|RAMdisc.buff_id];
		RAMdisc.offset++;
		return 1;
//		return 0;
}

void emu_RAMDISC_preread(void) {
	unsigned int s1;
	if ((RAMdisc.preread_ready == 0)&&(RAMdisc.addr != RAMdisc.preread_addr)) {
		if (FR_OK != f_lseek ( &RAMdisc.fh, RAMdisc.preread_addr )) return;
		if (FR_OK != f_read( &RAMdisc.fh, &RAMDISK[RAMdisc.buff_id ^ RAMDISK_SIZE], RAMDISK_SIZE, &s1 )) return;
		if (s1==RAMDISK_SIZE) RAMdisc.preread_ready = 1;
	}
}

/*
 *
 * 0xea - read data
 * 0xf8 - reset addr
 * 0xec - PEZIK (vracime nahodne hodnoty)
 * 0xf9 - read data + increment addr
 *
 */
void emu_RAMDISC_read( int i_addroffset, unsigned int *io_data)
{
//  unsigned int ff_readlen;
//	if (!RAMdisc.ready) { MZDATA_RETURN(i_addroffset); return;}
	if (RAMdisc.ready) switch (i_addroffset&0xFF) {
		case 0xEA:
		case 0xF9:
			rd_read1((unsigned char *)io_data/*,&ff_readlen*/);
			break;
		case 0xF8:
			// reset banky a adresy
			{
//				uint8_t	lastbnk = RAMdisc.bank;
//				uint16_t lastofs = RAMdisc.offset;
				RAMdisc.offset = 0x0000;
				RAMdisc.bank = 0x00;
//				if ((lastbnk != RAMdisc.bank)||((lastofs & RAMDISK_MASK_HI) != (RAMdisc.offset & RAMDISK_MASK_HI))) {
//					emu_RAMDISC_seek(( RAMdisc.bank << 16 ) | (RAMdisc.offset & RAMDISK_MASK_HI));
//				}
//				MZDATA_SEND(i_addroffset);
				*io_data=i_addroffset;
			}
			break;
		case 0xEC:
			// pseudo nahodna data kvuli PEZIK testu
//			MZDATA_SEND(RAMdisc.peziktst);
			*io_data=RAMdisc.peziktst;
			RAMdisc.peziktst+=3;
			//RAMdisc.peziktst++;
			//RAMdisc.peziktst++;
			break;
		default:
			*io_data=i_addroffset;
			break;
	}
//  return ( RETURN_EMU_OK );
//	MZDATA_RETURN(i_addroffset);
	return;
};

