/*
 *    Copyright (c) 2012 by Bohumil Novacek <http://dzi.n.cz/8bit/>
 *
 *    Thies program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    MZ-800 Unicard is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MZ-800 Unicard; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __MAIN_H
#define __MAIN_H

#include "stm32f4_discovery.h"

void TimingDelay_Decrement(void);

extern unsigned char VRAM[0x8000];		//umisteni kopie 32KB VRAM
extern unsigned char ZX_VRAM[0x1B00];	//umisteni kopie 8KB VRAM ZX Spectra (4000h-5FFFh)
extern unsigned long zx48tab[0x800];	//4*0x800 bytes - tabulka pro urychleni vykreslovani ZX video pameti

#define RAMDISK_SIZE	(0x0400)
extern unsigned char RAMDISK[RAMDISK_SIZE*2];
#define RAMDISK_MASK_LOW	((unsigned short)(RAMDISK_SIZE-1))
#define RAMDISK_MASK_HI		((unsigned short)(0xFFFF^RAMDISK_MASK_LOW))

#define REPODISK_SIZE	(0x1000)
extern unsigned char REPODISK[REPODISK_SIZE*2];
#define REPODISK_MASK_LOW	((unsigned long)(REPODISK_SIZE-1))
#define REPODISK_MASK_HI		((unsigned long)(~REPODISK_MASK_LOW))

//extern volatile int MZDATA_BUSY;
#define MZDATA_BUSY	0
extern char PHY_ETH_OK;

//predcasne ukonceni obsluhy portu read, mame hodnotu
#define MZDATA_SEND(data)	\
		{ if (!MZDATA_BUSY) {	/*MZDATA_BUSY = 1;*/ \
		PUT_MZDATA((data)&0xFF); \
		__asm volatile ("cpsid i"); \
		EXWAIT_OFF(); \
		while ((GET_MZBUS()&((1<<(13-0))|(1<<(12-0))))!=((1<<(13-0))|(1<<(12-0)))); \
		MZDATA_OFF(); \
		__asm volatile ("cpsie i"); \
		} }

//konec obsluhy portu read
#define MZDATA_RETURN(data)	\
		{ if (!MZDATA_BUSY) { PUT_MZDATA((data)&0xFF); \
		__asm volatile ("cpsid i"); \
		EXWAIT_OFF(); \
		while ((GET_MZBUS()&((1<<(13-0))|(1<<(12-0))))!=((1<<(13-0))|(1<<(12-0)))); \
		MZDATA_OFF(); \
		__asm volatile ("cpsie i"); \
		} /*else MZDATA_BUSY = 0;*/ }

//obsluha portu read v preruseni
#define MZDATA_INT(data)	\
		{ PUT_MZDATA((data)&0xFF); \
		EXWAIT_OFF(); \
		while ((GET_MZBUS()&((1<<(13-0))|(1<<(12-0))))!=((1<<(13-0))|(1<<(12-0)))); \
		MZDATA_OFF(); } \

#include "stm32f4xx.h"
#include "stm32f4x7_eth_bsp.h"

//#define USE_LCD        /* enable LCD  */
//#define USE_DHCP       /* enable DHCP, if disabled static address is used */
   
/* Uncomment SERIAL_DEBUG to enables retarget of printf to serial port (COM1 on STM32 evalboard) 
   for debug purpose */   
//#define SERIAL_DEBUG 

 
/* MAC ADDRESS: MAC_ADDR0:MAC_ADDR1:MAC_ADDR2:MAC_ADDR3:MAC_ADDR4:MAC_ADDR5 */
//#define MAC_ADDR0   2
//#define MAC_ADDR1   0
//#define MAC_ADDR2   0
//#define MAC_ADDR3   0
//#define MAC_ADDR4   0
//#define MAC_ADDR5   0

extern unsigned char MAC_ADDR[6];
 
/*Static IP ADDRESS: IP_ADDR0.IP_ADDR1.IP_ADDR2.IP_ADDR3 */
//#define IP_ADDR0   192
//#define IP_ADDR1   168
//#define IP_ADDR2   1
//#define IP_ADDR3   10

extern unsigned char IP_ADDR[4];

/*NETMASK*/
//#define NETMASK_ADDR0   255
//#define NETMASK_ADDR1   255
//#define NETMASK_ADDR2   255
//#define NETMASK_ADDR3   0

extern unsigned char NETMASK_ADDR[4];

/*Gateway Address*/
//#define GW_ADDR0   192
//#define GW_ADDR1   168
//#define GW_ADDR2   1
//#define GW_ADDR3   20

extern unsigned char GW_ADDR[4];

/* MII and RMII mode selection, for STM324xG-EVAL Board(MB786) RevB ***********/
#define RMII_MODE  // User have to provide the 50 MHz clock by soldering a 50 MHz
                     // oscillator (ref SM7745HEV-50.0M or equivalent) on the U3
                     // footprint located under CN3 and also removing jumper on JP5. 
                     // This oscillator is not provided with the board. 
                     // For more details, please refer to STM3240G-EVAL evaluation
                     // board User manual (UM1461).

                                     
//#define MII_MODE

/* Uncomment the define below to clock the PHY from external 25MHz crystal (only for MII mode) */
#ifdef 	MII_MODE
 #define PHY_CLOCK_MCO
#endif

#ifdef 	RMII_MODE
	#define	RMII_TX_GPIO_GROUPB	1
	#define	RMII_TX_GPIO_GROUPG	2
	#define RMII_TX_GPIO_GROUP	RMII_TX_GPIO_GROUPB
#endif	


/* STM324xG-EVAL jumpers setting
    +==========================================================================================+
    +  Jumper |       MII mode configuration            |      RMII mode configuration         +
    +==========================================================================================+
    +  JP5    | 2-3 provide 25MHz clock by MCO(PA8)     |  Not fitted                          +
    +         | 1-2 provide 25MHz clock by ext. Crystal |                                      +
    + -----------------------------------------------------------------------------------------+
    +  JP6    |          2-3                            |  1-2                                 +
    + -----------------------------------------------------------------------------------------+
    +  JP8    |          Open                           |  Close                               +
    +==========================================================================================+
  */
   
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void Time_Update(void);  
void Delay(uint32_t nCount);

#endif /* __MAIN_H */

