/*
 *    Copyright (c) 2009 by Michal Hucik <http://www.ordoz.com>
 *    Copyright (c) 2012 by Bohumil Novacek <http://dzi.n.cz/8bit/>
 *
 *    Thies program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    MZ-800 Unicard is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MZ-800 Unicard; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <string.h>

#include "stm32f4xx.h"
#define bool unsigned char
//#define DBGPRINTF(a,...)	{}

#include "interrupt.h"
#include "ff.h"
//#include "ffconf.h"
#include "monitor.h"

#include "emu_MZFREPO.h"
#include "emu_SIO.h"
#include "revision.h"
#include "USARTshell.h"
//#include "usart1.h"
#include "mzint.h"

#include "event.h"

//#define DBGLEVEL	(DBGNON /*| DBGFAT | DBGERR | DBGWAR | DBGINF*/)
#define DBGLEVEL	(/*DBGNON | DBGFAT |*/ DBGERR /*| DBGWAR | DBGINF*/)
//#define DBG_GETCPUTC	1

#define USE_DBG_PRN	xprintf
#include "debug.h"

#define RETURN_FDC_OK  0
#define RETURN_FDC_ERR 1


#define RETURN_EMU_OK  0
#define RETURN_EMU_ERR 1


/*
 *
 * Prikazy prijate pres CMD port
 * =============================
 *
 * Prikazy se posilaji na CMD port. Vsechny jsou jednobajtove a pokud
 * nevyzaduji dalsi parametr, tak jsou okamzite vykonany.
 *
 * Pokud je kdykoliv vlozen prikaz, tak se tim automaticky zrusi vykonavani
 * predchoziho prikazu (napr. pokud ceka na vstupni parametry,
 * nebo na odebrani vystupnich dat).
 *
 * V pripade vlozeni prikazu STSR se pouze vynuluje ukazatel statusu (viz. nize) a
 * nedojde k zadnemu ovlivneni predchoziho prikazu, ani obsahu statusu.
 *
 *
 * Vstup:
 *
 * Pokud prikaz vyzaduje dalsi parametry, tak jsou ocekavany v
 * predem urcenem poradi na DATA portu a prikaz bude vykonan okmzite po
 * vlozeni vsech parametru.
 *
 * Pokud je vstupnim parametrem string, tak za jeho konec je povazovano
 * prijeti libovolneho znaku s hodnotou mensi nez 0x20.
 *
 * Pokud jsou vstupnim parametrem dva retezce, tak za oddelovac je povazovan
 * libovolny jeden znak s hodnotou mensi nez 0x20.
 *
 * Pokud je vstupnim parametrem retezec, tak tento vstup prochazi uvnitr
 * unikarty I/O translatorem ASCII.
 *
 * Pokud je parametrem soubor, nebo adresar (string), tak je mozne pouzit
 * absolutni cestu, ktera zacina od korene "/". Nebo relativni, ktera zacne od CWD.
 * Pouziti relativni cesty neni vhodne u prikazu FDDMOUNT, protoze se pak v teto podobe
 * ulozi do konfigurace. Pri startu unikarty se nastavi CWD=/ a relativni cesta k DSK nemusi
 * byt platna.
 *
 *
 * Vystup:
 *
 * Pokud se ocekava, ze vystupem prikazu budou nejake data,
 * tak jsou k dispozici na dataportu.
 *
 * Pokud jsou vystupni hodnotou retezce, tak jsou vzdy oddeleny a ukonceny znakem 0x0d.
 *
 * Pokud unikarta nema zadne data, ktere by mohla poslat, tak na datovem portu vraci 0x00.
 *
 * Pokud je vystupnim parametrem retezec, tak tento vystup prochazi uvnitr
 * unikarty I/O translatorem ASCII.
 *
 *
 * Pokud je pres OPEN otevren nejaky soubor, tak zapis a cteni dataportu je povazovano
 * za pokus o getc(), nebo putc().
 *
 * Pokud je pres OPEN otevren nejaky soubor a vlozime prikaz, ktery ocekava parametry,
 * nebo vraci nejaka data, tak tento prikaz ziska prioritu v pristupu na dataport.
 * To plati do doby, dokud neni prikaz kompletne vykonan, nebo stornovan prikazem STORNO.
 *
 *
 * Ciselne hodnoty WORD (2B)  a DWORD (4) jsou predavany od nejnizsiho bajtu k nejvyssimu.
 *
 * Struktura FILINFO:
 *
 *	DWORD	-	4B, File size
 *	WORD	-	2B, Last modified date
 *	WORD	-	2B, Last modified time
 *	BYTE	-	1B, Attribute
 *	TCHAR	-	13B, Short file name (8.3 format), zakonceno 0x00
 *	BYTE	-	1B, LFN strlen
 *	TCHAR	-	32B, LFN, zakonceno 0x00
 *
 *
 *
 * Status kody:
 * ============
 *
 * Status repozitare obsahuje 4 bajty a lze jej kdykoliv ziskat prectenim CMD portu.
 *
 * Prectenim jednoho bajtu ze statusu se interne zvedne pozice ukazatele na dalsi bajt
 * a jakmile jsou precteny vsechny ctyri bajty, tak zustane ukazatel "zaparkovan" a pri dalsim
 * cteni vraci unikarta misto statusu 0x00.
 *
 * Po kazde I/O operaci DATA portu, nebo pri zapisu prikazu se vzdy v unikarte aktualizuje
 * stav, ktery je sdelovan statusem a zaroven se take vzdy resetuje ukazatel statusu
 * na prvni bajt.
 *
 * Pozici ukazatele lze kdykoliv resetovat prikazem STSR, ktery zaroven jako jediny prikaz
 * zadnym zpusobem neovlivnije obsah statusu a ani neprerusi vykonavani jiz aktivniho prikazu.
 * Je tedy bezpecne jej kdykoliv pouzit.
 *
 *
 * Vyznam jednotlivych bajtu:
 *
 * 1. bajt - master status byte:
 *
 *	0. bit	- BUSY/READY		- pokud je bit vynulovan, tak to znamena, ze je
 *					repozitar ve stavu READY
 *
 *					- pokud je nastaven, tak to znamena, ze je repozitar
 *					ve stavu BUSY - tedy je prave aktivni nejaky prikaz,
 *					ktery vyzaduje vlozeni dalsich parametru
 *					pres DATA port
 *
 *					- s vlozenim posledniho parametru, nebo pretecenim
 *					vstupniho zasobniku se tento bit opet vynuluje a repozitar
 *					prejde do stavu READY (Pokud doslo k preteceni zasobniku,
 *					tak se zaroven status ERROR.)
 *
 *					- s vlozenim prikazu STORNO prejde repozitar do stavu READY
 *
 *
 *
 *	1. bit 	- CMD_OUTPUT		- jestlize je nastaven, tak to znamena, ze mame v zasobniku
 *					nejaka data, ktera jsou vystupem z posledniho prikazu
 *
 *					- s prectenim posledniho bajtu, nebo vlozenim prikazu STORNO
 *					se tento bit vynuluje
 *
 *
 *
 *	2. bit	- READDIR		- pokud je nastaven, tak to znamena, ze mame otevren
 *					adresar prikazem READDIR, nebo FILELIST a pokud nejsou
 *					v zasobniku zadna data k vyzvednuti (CMD_OUTPUT), tak
 *					v tuto chvili ctenim z DATA portu ziskame odpovidajici
 *					data z otevreneho adresare
 *
 *					- s prectenim posledniho bajtu z posledni polozky adresare
 *					se tento status bit vynuluje
 *
 *					- pokud je vlozen prikaz NEXT a jiz neni k dispozici
 *					zadna dalsi polozka adresare, tak se tento bit vynuluje
 *
 *
 *
 *	3. bit	- READ_FILE		- pokud je nastaven, tak to znamena, ze mame otevren
 *					nejaky soubor pro cteni a pokud nejsou v zasobniku
 * 					zasobniku zadna data k vyzvednuti (CMD_OUTPUT),
 *					tak v tuto chvili ctenim z DATA portu provedeme
 *					fget() - precteme bajt z otevreneho souboru
 *
 *
 *
 *	4. bit	- WRITE_FILE		- pokud je nastaven, tak to znamena, ze mame otevren
 *                                      nejaky soubor pro zapis a poku je repozitar READY,
 *					tak zapisem na DATA port provedeme fput() - zapiseme
 *					bajt do otevreneho souboru
 *
 *
 *
 *	5. bit 	- EOF			- pokud mame otevreny nejaky soubor a pozice ukazatele
 *					se momentalne nachazi na jeho konci, tak se nam nastavi
 *					tento bit
 *
 *
 *	6. bit	- nepouzito
 *
 *
 *	7. bit	- ERROR			- nastaven, pokud doslo k chybe, coz muze nastat:
 *
 *					- vlozeni prikazu (pripadne vlozenim jeho posledniho
 *                                      parametru, cimz je prikaz spusten)
 *
 *                                      - pretecenim vstupniho bufferu pri vkladani parametru
 *                                      typu string
 *
 *					- zapisem/ctenim otevreneho souboru
 *
 *                                      - prectenim posledniho bajty polozky adresare (to interne
 *					aktivuje prikaz NEXT pro nacteni dalsi polozky z FAT
 *					do vystupniho bufferu)
 *
 *                                      - pri neocekavane I/O operaci na DATA portu
 *
 *
 * 	Pozn.: Status bity READDIR a READ_FILE/WRITE_FILE se v tuto chvili navzajem vylucuji.
 *
 *
 * 2. bajt - hodnota posledniho aktivniho prikazu
 *
 *	Pozn.:	V pripade cteni/zapisu do souboru pres DATA port se zde jako hodnota projevi
 *		interni prikazy INTGETC a INTPUTC.
 *
 *
 * 3. bajt - v pripade, ze je v prvnim master status bajtu nastaven ERROR, tak zde muze
 *	     byt error kod z unikarty, napr. pri preteceni bufferu, spatne hodnote parametru, atd...
 *           (v tuto chvili jeste neni implementovano a vraci 0x00)
 *
 *	   - v pripade, ze vkladame parametry k prikazu (BUSY) je zde informace o zbyvajici
 *	     velikosti volne pameti ve vstupnim bufferu
 *
 *	   - v pripade, ze cteme vystupni data z prikazu, tak je zde zbyvajici pocet
 *	     bajtu ve vystupnim bufferu
 *
 *	   - v pripade, ze cteme polozku adresare otevreneho pres READDIR, nebo FILELIST,
 *	     tak je zde zbyvajici pocet bajtu aktualni polozky, ktera je ulozena v bufferu
 *
 * 4. bajt - FatFS result
 *
 *	Zde je ulozen navratovy kod z posledni zavolane FatFS operace. Ma smysl se
 *	o nej zajimat jen v pripade, ze byl v prvnim master status bajtu nastaven ERROR.
 *	viz. FatFS - http://elm-chan.org/fsw/ff/en/rc.html
 */


/*
 *
 * Seznam prikazu repozitare
 *
 */


// Prace s repozitarem:
#define	cmdRESET	0x00	// Reset celeho repozitare. Nastaveni ASCII.
                                // Sync a uzavreni otevrenych souboru.
                                // Uzavreni otevrenych adresaru. CWD se nastavi na korenovy adresar.

#define	cmdASCII	0x01	// I/O translator nastavime na ASCII.

#define	cmdSHASCII	0x02	// I/O translator nastavime na SharpASCII.

#define	cmdSTSR		0x03	// Ukazatel statusu se nastavi na zacatek.
                                // Jinym zpusobem se na statusu neprojevi. Aktualni prikaz nebude prerusen.

#define	cmdSTORNO	0x04	// Okamzite ukonceni prave vkladaneho,
                                // nebo vykonavaneho prikazu (takovy maly reset pro uvolneni data portu).

#define cmdREV		0x05	// Sdeli revizi firmware v txt tvaru.
                                //
                                // Vystup:	string

#define cmdREVD		0x06	// Sdeli revizi firmware v binarnim tvaru.
                                //
                                // Vystup:	DWORD

// Konfigurace a nastaveni unikarty:
#define	cmdFDDMOUNT	0x10	// Nastavi DSK image pro prislusnou mechaniku a ulozi konfiguraci.
                                // Pokud uz je v mechanice nejaky DSK, tak provede jeho sync a odpoji se.
                                //
                                // Vstup:	1B - cislo mechaniky
                                //		string - DSK image path/filename (pokud je parametr prazdny, tak se mechanika odmountuje a zustane prazdna)

#define cmdINTCALLER	0x11	// Sdeli kdo je volajicim interruptu. Kody volajicich periferii jsou definovany v mzint.h
                                //
                                // Vystup:	1B - mzintFLAG

// Prace s filesystemem:
#define	cmdGETFREE	0x20	// Sdeli celkovy pocet a pocet volnych sektoru na disku.
                                //
                                // Vystup:	DWORD - celkovy pocet sektoru
                                //		DWORD - pocet volnych sektoru

#define	cmdCHDIR	0x21	// Zmena CWD.
                                //
                                // Vstup:	string - path, nebo dirname

#define	cmdGETCWD	0x22	// Sdeli aktualni CWD.
                                //
                                // Vystup:	string, 0x0d

// Prace se soubory a adresari:
#define	cmdSTAT		0x30	// Informace o souboru, nebo adresari.
                                //
                                // Vstup:	string
                                // Vystup:	binarni struktura FILINFO

#define	cmdUNLINK	0x31	// Smazat soubor, nebo adresar.
                                //
                                // Vstup:	string

#define	cmdCHMOD	0x32	// Zmena atributu souboru, nebo adresare.
                                //
                                // Vstup:	1B - atributy
                                //		1B - maska
                                //		string - filename/dirname
                                //
                                //
                                // 	0x01	AM_RDO	Read only
                                // 	0x02	AM_HID	Hidden
                                // 	0x04	AM_SYS	System
                                // 	0x20	AM_ARC	Archive
                                //
                                // Napr. nastavit AM_RDO a smazat AM_ARC (ostatni atributy zustanou nezmeneny):
                                //
                                //	attr = AM_RDO
                                //	mask = AM_RDO | AM_ARC
                                //

#define	cmdUTIME	0x33	// Nastaveni casove znacky souboru.
                                //
                                // Vstup:	6B - D,M,Y-1980,H,M,S
                                //		string - filename/dirname

#define	cmdRENAME	0x34	// Prejmenovani souboru, nebo adresare.
                                //
                                // Vstup:	string - oldname
                                //		string - newname

// Prace s adresarem:
#define	cmdMKDIR	0x40	// Vytvori adresar.
                                //
                                // Vstup:	string - dirname

#define	cmdREADDIR	0x41	// Otevre adresar a pripravi na data port binarni vystup pro cteni.
                                // Pokud byl otevreny nejaky soubor, tak se provede sync() a zavre se.
                                //
                                // Vstup:	string - dirname
                                // Vystup:	FILINFO - binarni struktura pro kazdou polozku adresare

#define	cmdFILELIST	0x42	// Otevre adresar a pripravi na data port textovy vystup pro cteni.
                                // Pokud byl otevreny nejaky soubor, tak se provede sync() a zavre se.
                                //
                                // Vstup:	string - dirname
                                // Vystup:	string filename[/],0x0d,string size,0x0d

#define	cmdNEXT		0x43	// Provede okamzity presun na dalsi polozku adresare v prave
                                // vykonavanem READDIR, nebo FILELIST.


// Prace se souborem:
#define	cmdOPEN		0x50	// Otevre soubor v pozadovanem rezimu.
                                //
                                // Vstup:	1B - rezim viz. FatFS
                                //		string - filename
                                //
                                //	0x00	FA_OPEN_EXISTING
                                //	0x01	FA_READ
                                //	0x02	FA_WRITE
                                //	0x04	FA_CREATE_NEW
                                //	0x08	FA_CREATE_ALWAYS
                                //	0x10	FA_OPEN_ALWAYS
                                //	0x20	FILE_SHASCII_CNV
                                //

#define	cmdSEEK		0x51	// Zmena pozice v otevrenem souboru.
                                //
                                // Vstup:	1B - rezim
                                //			0x00 - od zacatku
                                //			0x01 - od konce
                                //			0x02 - relativne nahoru
                                //			0x03 - relativne dolu
                                //		DWORD - pocet bajtu o ktere se ma ukazatel posunout

#define	cmdTRUNC	0x52	// Zkrati prave otevreny soubor na velikost odpovidajici soucasne pozici ukazatele.

#define	cmdSYNC		0x53	// Provede sync() prave otevreneho souboru.

#define	cmdCLOSE	0x54	// Provede sync() a zavre prave otevreny soubor.

#define	cmdTELL		0x55	// Sdeli pozici ukazatele v prave otevrenem souboru.
                                //
                                // Vystup:	DWORD

#define	cmdSIZE		0x56	// Sdeli velikost prave otevreneho souboru.
                                //
                                // Vystup:	DWORD

// Prace s RTC:
#define	cmdRTCSETD	0x60	// Nastaveni datumu.
                                //
                                // Vstup:	3B - D,M,Y-1980

#define	cmdRTCSETT	0x61	// Nastaveni casu.
                                //
                                // Vstup:	3B - H,M,S

#define	cmdRTCGETD	0x62	// Vrati aktualni datum.
                                //
                                // Vystup:	3B - D,M,Y-1980

#define	cmdRTCGETT	0x63	// Vrati aktualni cas.
                                //
                                // Vystup:	3B - H,M,S

// USART:
#define cmdUSHELLON	0x70	// Zapne USARTshell, vypne emu_SIO.

#define cmdUSHELLOFF	0x71	// Vypne USARTshell, zapne emu_SIO.

#define	cmdUSARTBPS	0x72	// Nastaveni BPS pro USART1.
                                //
                                // Vstup:	DWORD - rychlost


// Interni prikazy - provadi se jinak, nez pres CMD port:

#define cmdINTGETC		0xf0	// Getchar - precteni 1B na data portu z otevreneho souboru.

#define cmdINTPUTC		0xf1	// Putchar - zapis 1B na data port do otevreneho souboru.


// Faze vykonavani prikazu
#define phsDONE		0x00	// hotovo
#define phsPARAMRQ	0x01	// cekame na parametry
#define phsDOUTRQ	0x02	// cekame na vyzvednuti vystupnich dat
// interni stavy
#define phsSTART	0x03	// prikaz byl prave vlozen
#define phsPARAM	0x04	// prijali jsme 1B parametru
#define phsPARAMOK	0x05	// vsechny parametry byly vlozeny, GO!
#define phsWORK		0x06	// pracovni cyklus (nacteni dalsi polozky adresare :-)

// definice bool
#define true		1
#define false		0

#define PARAM_BUFFER_SIZE	255
//#define FILE_BUFFER_SIZE	255
#define FILE_BUFFER_SIZE	55	// FILINFO 23+_MAX_LFN=55, FILELIST 32+20=52

struct MZFrepo_s {
    uint8_t		cmd;			// posledni aktivni prikaz
    uint8_t		cmd_phase;		// faze vykonavani prikazu
    bool		cnv_cmd;		// budeme provadet SharpASCII konverzi?

    uint8_t		ff_res;			// navratovy kod z posledni FatFS operace

    uint8_t		sts_pos;		// pozice ukazatele cteni statusu
    bool		sts_err;		// pokud je true, tak budeme ve statusu reportovat ERROR

    uint8_t		*buf;			// ukazatel I/O bufferu pro parametry a vystup z prikazu
    uint8_t		buf_count;		// pocet zbyvajicich bajtu
    uint8_t		buf_byte[PARAM_BUFFER_SIZE];

    uint8_t		*filebuf;		// ukazatel I/O bufferu pro OPEN, READDIR a FILELIST
    uint8_t		filebuf_count;		// pocet zbyvajicich bajtu
    uint8_t		filebuf_byte[FILE_BUFFER_SIZE];

    FIL			fh;			// filehandle pro praci se souborem (OPEN)
    uint8_t		file_mode;		// tady si ukladame rezim v jakem je soubor otevren

	  uint32_t	offset;		// aktualni ukazatel adresy v RAMdisku
  uint8_t	dirty;	// zmeneno ?
//  uint8_t	timeout;	// vyprsel timeout zapisu ?
  uint32_t	buff_id;	// stranka bufferu
  uint32_t	addr;	// aktualni obsah bufferu
  uint32_t	preread_addr;	// stranka bufferu
  uint32_t	preread_ready;	// stranka bufferu

    DIR			dh;			// dirhandle pro READDIR a FILELIST
} MZFrepo;

unsigned char REPODISK[REPODISK_SIZE*2];	//vyrovnavaci buffer pro stranku ramdisku

int emu_MZFREPO_read_interrupt(unsigned char *io_data) {
        if ( phsDOUTRQ == MZFrepo.cmd_phase ) return 0;
				if ( MZFrepo.dh.fs ) return 0;
				if (!( MZFrepo.fh.fs )) return 0;
					
		if (((MZFrepo.offset & REPODISK_MASK_HI)) != MZFrepo.addr) {
			if ((MZFrepo.preread_ready)&&(!MZFrepo.dirty)&&(((MZFrepo.offset & REPODISK_MASK_HI)) == MZFrepo.preread_addr)) {
				MZFrepo.buff_id ^= REPODISK_SIZE;
				MZFrepo.preread_ready = 0;
				MZFrepo.addr = MZFrepo.preread_addr;
			} else return 0;
		} else if (
						(!(peek_event()&(1<<EVENT_REPO_PREREAD)))
						&&
						(
							(MZFrepo.preread_ready==0)
							||(MZFrepo.preread_addr != (MZFrepo.offset&REPODISK_MASK_HI) + REPODISK_SIZE)
						)
					) {	//pokud uz neni zadano, precteno nebo nesedi adresa
							MZFrepo.preread_ready = 0;
							MZFrepo.preread_addr = (MZFrepo.offset&REPODISK_MASK_HI) + REPODISK_SIZE;
							set_event(EVENT_REPO_PREREAD);
					}
		if (MZFrepo.offset>=MZFrepo.fh.fsize) *io_data=0;
		else { *io_data=REPODISK[(MZFrepo.offset&REPODISK_MASK_LOW)|MZFrepo.buff_id];MZFrepo.offset++; }
		return 1;
}

void emu_MZFREPO_preread(void) {
	unsigned int s1;
	if ((MZFrepo.preread_ready == 0)&&(MZFrepo.addr != MZFrepo.preread_addr)) {
		if (FR_OK != f_lseek ( &MZFrepo.fh, MZFrepo.preread_addr )) return;
		if (FR_OK != f_read( &MZFrepo.fh, &REPODISK[MZFrepo.buff_id ^ REPODISK_SIZE], REPODISK_SIZE, &s1 )) return;
		if ((s1>0)&&(s1<=REPODISK_SIZE)) MZFrepo.preread_ready = s1;
	}
}

void emu_RAMDISC_fsync() {
	UINT s1;
	if (MZFrepo.dirty) {
		f_lseek ( &MZFrepo.fh, MZFrepo.addr );	//kde se prave nalezame
		if (MZFrepo.offset>=MZFrepo.addr+REPODISK_SIZE)
			f_write( &MZFrepo.fh, &RAMDISK[MZFrepo.buff_id], REPODISK_SIZE, &s1 );
		else if (MZFrepo.offset>MZFrepo.addr)
			f_write( &MZFrepo.fh, &REPODISK[MZFrepo.buff_id], MZFrepo.offset-MZFrepo.addr, &s1 );	//TODO neuplny paket
		MZFrepo.dirty = 0;
	}
}

uint8_t emu_REPODISC_seek ( unsigned long addr ) {
	UINT s1;
	if (MZFrepo.dirty) {
		f_lseek ( &MZFrepo.fh, MZFrepo.addr );
		if (MZFrepo.offset>=MZFrepo.addr+REPODISK_SIZE)
			f_write( &MZFrepo.fh, &REPODISK[MZFrepo.buff_id], REPODISK_SIZE, &s1 );	//TODO neuplny paket
		else if (MZFrepo.offset>MZFrepo.addr)
			f_write( &MZFrepo.fh, &REPODISK[MZFrepo.buff_id], MZFrepo.offset-MZFrepo.addr, &s1 );	//TODO neuplny paket
		else {//TODO neni co zapisovat ?
		};
		MZFrepo.dirty = 0;
		if (MZFrepo.preread_addr == (addr & REPODISK_MASK_HI)) {
			MZFrepo.preread_ready = 0;
		}
	}
	if ((MZFrepo.preread_ready >= 1)&&(MZFrepo.preread_addr == (addr & REPODISK_MASK_HI) ))	{ //uz mam nacteno
		MZFrepo.buff_id ^= REPODISK_SIZE;
		MZFrepo.preread_ready = 0;
		MZFrepo.addr = (addr & REPODISK_MASK_HI);
		MZFrepo.offset = addr;
		return ( FR_OK );
	} else if ( FR_OK != f_lseek ( &MZFrepo.fh, addr & REPODISK_MASK_HI) ) {
		f_close( &MZFrepo.fh);
    return ( FR_INVALID_PARAMETER );
  }
	f_read( &MZFrepo.fh, &REPODISK[MZFrepo.buff_id], REPODISK_SIZE, &s1 );
	MZFrepo.addr = (addr & REPODISK_MASK_HI);
	MZFrepo.offset = addr;
  return ( FR_OK );
};

uint8_t repo_read1(unsigned char *sync1/*, UINT *s1*/)
{
	uint8_t res = FR_OK;
	if ((MZFrepo.offset & REPODISK_MASK_HI) != MZFrepo.addr) {
		res = emu_REPODISC_seek(MZFrepo.offset);
	} else if (
						(!(peek_event()&(1<<EVENT_REPO_PREREAD)))
						&&
						(
							(MZFrepo.preread_ready==0)
							||(MZFrepo.preread_addr != (MZFrepo.offset&REPODISK_MASK_HI) + REPODISK_SIZE)
						)
					) {	//pokud uz neni zadano, precteno nebo nesedi adresa
							MZFrepo.preread_ready = 0;
							MZFrepo.preread_addr = (MZFrepo.offset&REPODISK_MASK_HI) + REPODISK_SIZE;
							set_event(EVENT_REPO_PREREAD);
					}
//	*s1=1;
	if (MZFrepo.offset>=MZFrepo.fh.fsize) *sync1=0;
	else { *sync1=REPODISK[(MZFrepo.offset&REPODISK_MASK_LOW)|MZFrepo.buff_id];MZFrepo.offset++; }
	return res;
}

uint8_t repo_write1(unsigned char sync1/*, UINT *s1*/)
{
	uint8_t res = FR_OK;
	if ((MZFrepo.offset & REPODISK_MASK_HI) != MZFrepo.addr) {
		res = emu_REPODISC_seek(MZFrepo.offset);
	} else if (
						(!(peek_event()&(1<<EVENT_REPO_PREREAD)))
						&&
						(
							(MZFrepo.preread_ready==0)
							||(MZFrepo.preread_addr != (MZFrepo.offset&REPODISK_MASK_HI) + REPODISK_SIZE)
						)
					) {	//pokud uz neni zadano, precteno nebo nesedi adresa
							MZFrepo.preread_ready = 0;
							MZFrepo.preread_addr = (MZFrepo.offset&REPODISK_MASK_HI) + REPODISK_SIZE;
							set_event(EVENT_REPO_PREREAD);
					}
	if (REPODISK[(MZFrepo.offset&REPODISK_MASK_LOW)|MZFrepo.buff_id]!=sync1) {
		REPODISK[(MZFrepo.offset&REPODISK_MASK_LOW)|MZFrepo.buff_id]=sync1;
		MZFrepo.dirty = 1;
//		MZFrepo.timeout = 50; //nezmeni-li nekdo stranku, zapis nejpozdeji po jedne sekunde
	}
	MZFrepo.offset++;
	if (MZFrepo.offset>MZFrepo.fh.fsize) MZFrepo.fh.fsize=MZFrepo.offset;
	return res;
}

/* Konverzni tabulka Sharp ASCII
 *
 */
const uint8_t SharpASCII [ 49 ] = {
    '_',' ','e',' ','~',' ','t','g',
    'h',' ','b','x','d','r','p','c',
    'q','a','z','w','s','u','i',' ',
    ' ','k','f','v',' ',' ',' ','j',
    'n',' ',' ','m',' ',' ',' ','o',
    'l',' ',' ',' ',' ','y','{',' ',
    '|' };


/* FromSHASCII() - konverze jednoho znaku z Sharp ASCII do ASCII
 *
 * Vstup: uint8_t c - znak
 *
 * Vystup: uint8_t c - konvertovany znak
 *
 */
uint8_t FromSHASCII ( uint8_t c ) {
    if ( c <= 0x5d ) return ( c );
    if ( c == 0x80 ) return ( '}' );
    if ( c < 0x90 || c > 0xc0 ) return ( ' ' ); // z neznamych znaku udelame ' '
    return ( SharpASCII [ c - 0x90 ] );
}


/* ToSHASCII() - konverze jednoho znaku z ASCII do Sharp ASCII
 *
 * Vstup: uint8_t c - znak
 *
 * Vystup: uint8_t c - konvertovany znak
 *
 */
uint8_t ToSHASCII ( uint8_t c ) {
    uint8_t i;

    if ( c <= 0x5d ) return ( c );
    if ( c == '}' ) return ( 0x80 );
    for ( i = 0; i < sizeof ( SharpASCII ); i++ ) {
        if ( c == SharpASCII [ i ] ) {
            return ( i + 0x90 );
        };
    };
    return ( ' ' );  // z neznamych znaku udelame ' '
}


/* long2dec() - konverze uint32_t na retezec s cislem v desitkove soustave
 *
 * Vstup:
 *	uint32_t l	cislo, ktere chceme konvertovat do txt
 *	char* dec	ukazatel na misto kam chceme txt vlozit
 *
 * Vraci: char* dec	ukazatel na misto s textem
 *
 */
char* long2dec ( uint32_t l, char* dec ) {
    uint32_t num = 1000000;
    uint8_t started = 0;

    dec [ 0 ] = '\0';

    while ( num > 0 ) {
        uint8_t b = l / num;
        if ( b > 0 || started || num == 1 ) {
            uint8_t i = strlen ( dec );
            dec [ i ] = '0' + b;
            i++;
            dec [ i ] = '\0';
            started = 1;
        };
        l -= b * num;
        num /= 10;
    };
    return ( dec );
}


/* OutputData
 *
 * Vystavi data z *MZFrepo.buf pro vystup. Pokud txt_data = true,
 * tak provede konverzi ASCII pokud je pozadovana a za daty prida 0x0d.
 */
uint8_t OutputData ( bool txt_data ) {
    uint8_t ret_val;

    MZFrepo.sts_err = false;
    ret_val = MZFrepo.buf [0];
    if ( txt_data && ( ret_val == '\x00' ) ) {
        ret_val = '\x0d';
    };

    if ( MZFrepo.cnv_cmd && txt_data ) {
        uint8_t cnv_val = ToSHASCII ( ret_val );
        DBGPRINTF ( DBGINF, "OutputData() - 0x%02lx, cnv: 0x%02lx\n", ret_val, cnv_val );
        ret_val = cnv_val;
    } else {
        DBGPRINTF ( DBGINF, "OutputData() - 0x%02lx\n", ret_val );
    };
    MZFrepo.buf++;
    MZFrepo.buf_count--;
    if ( MZFrepo.buf_count ) {
        MZFrepo.cmd_phase = phsDOUTRQ;
    } else {
        MZFrepo.cmd_phase = phsDONE;
    };
    return ( ret_val );
}

char *g_param_format;

/* InputParams
 *
 *
 * V g_param_format ocekava strukturu parametru:
 *	B - bajt
 *	S - string (ukoncen znakem mensim jak 0x20)
 *
 * Vraci pocet parametru, ktere zbyva nacist, nebo -1 pri preteceni bufferu.
 */
int8_t InputParams ( uint8_t phase, uint8_t inputByte ) {

    MZFrepo.cmd_phase = phsPARAMRQ;

    if ( phase == phsSTART ) {
        uint8_t i;
        MZFrepo.sts_err = false;
        MZFrepo.buf = MZFrepo.buf_byte;
        MZFrepo.buf_count = PARAM_BUFFER_SIZE;
        for ( i = 0; i < strlen ( g_param_format ); i++ ) {
            if ( 'S' == g_param_format[i] ) {
                MZFrepo.buf_count--;		// pro kazdy retezec musime udelat misto pro 0x00
            };
        };
    } else {
        if ( 'B' == g_param_format[0] ) {
            DBGPRINTF ( DBGINF, "InputParams ( BYTE ) - 0x%02lx\n", inputByte );
            MZFrepo.buf[0] = inputByte;
            MZFrepo.buf++;
            MZFrepo.buf_count--;
            g_param_format++;
        } else {
            if ( MZFrepo.cnv_cmd ) {
                DBGPRINTF ( DBGINF, "InputParams ( STRING ) - 0x%02lx", inputByte );
                inputByte = ToSHASCII ( inputByte );
                DBGPRINTF ( DBGINF, ", cnv=0x%02lx\n", inputByte );
            } else {
                DBGPRINTF ( DBGINF, "InputParams ( STRING ) - 0x%02lx\n", inputByte );
            };
            if ( inputByte >= 0x20 ) {
                MZFrepo.buf[0] = inputByte;
                MZFrepo.buf_count--;
            } else {
                MZFrepo.buf[0] = '\x00';
                g_param_format++;
            };
            MZFrepo.buf++;
        };
    };

    if ( ! MZFrepo.buf_count ) {
        DBGPRINTF ( DBGERR, "InputParams() - parameters exceeded buffer size!\n" );
        // TODO: unicard err code: BUFFER_EXCEEDED
        return ( -1 );
    };
    return ( strlen ( g_param_format ) );
}


/* do_cmdUSARTBPS()
 *
 * Nastaveni BPS pro USART1.
 */
void do_cmdUSARTBPS ( uint8_t cmd_phase ) {

    if ( phsSTART == cmd_phase ) {
        DBGPRINTF ( DBGINF, "do_cmdUSARTBPS ( START )\n" );

        g_param_format = "BBBB";
        InputParams ( cmd_phase, 0 );

    } else {
        // zpracovani prikazu
        uint32_t new_bps;
        DBGPRINTF ( DBGINF, "do_cmdUSARTBPS ( PARAMOK )\n" );
        MZFrepo.cmd_phase = phsDONE;

        memcpy ( &new_bps, ( MZFrepo.buf_byte ), 4 );
        DBGPRINTF ( DBGINF, "do_cmdUSARTBPS: new_bps=0x%08lx\n", new_bps );
		emu_SIO_set_bps( new_bps );
        MZFrepo.sts_err = false;
    };
}


/* CloseDir()
 *
 * Pokud je v MZFrepo.dh otevreny adresar, tak ten dirhandle zlikvidujeme.
 */
void CloseDir ( void ) {
    DBGPRINTF ( DBGINF, "CloseDir()\n" );
    memset ( &MZFrepo.dh, 0x00, sizeof ( DIR ) );
}


bool g_rtc_set_date;
/* do_cmdRTCSETD()
 *
 * Provede zmenu nastaveni datumu D, M, R - 1980.
 *
 * do_cmdRTCSETT()
 *
 * Provede zmenu nastaveni casu H, M, S.
 */
void do_cmdRTCSET ( uint8_t cmd_phase ) {

    if ( phsSTART == cmd_phase ) {
        if ( g_rtc_set_date ) {
            DBGPRINTF ( DBGINF, "do_cmdRTCSETD ( START )\n" );
        } else {
            DBGPRINTF ( DBGINF, "do_cmdRTCSETT ( START )\n" );
        };

        g_param_format = "BBB";
        InputParams ( cmd_phase, 0 );

    } else {
        // zpracovani prikazu
        MZFrepo.cmd_phase = phsDONE;
        if ( g_rtc_set_date ) {
						RTC_DateTypeDef rtc; //RTC_t rtc;
            rtc.RTC_Date = MZFrepo.buf_byte[0];
            rtc.RTC_Month = MZFrepo.buf_byte[1];
            rtc.RTC_Year = MZFrepo.buf_byte[2] + 1980 - 2000;	//0-99
						rtc.RTC_WeekDay = 0;
            RTC_SetDate(RTC_Format_BIN, &rtc);//rtc_setdate ( &rtc );
//            DBGPRINTF ( DBGINF, "do_cmdRTCSETD ( PARAMOK ) new date: %d.%d.%d\n", rtc.mday, rtc.month, rtc.year );
        } else {
						RTC_TimeTypeDef rtc; //RTC_t rtc;
			rtc.RTC_H12 = RTC_H12_AM;
            rtc.RTC_Hours = MZFrepo.buf_byte[0];
            rtc.RTC_Minutes = MZFrepo.buf_byte[1];
            rtc.RTC_Seconds = MZFrepo.buf_byte[2];
            RTC_SetTime(RTC_Format_BIN, &rtc);//rtc_settime ( &rtc );
//            DBGPRINTF ( DBGINF, "do_cmdRTCSETT ( PARAMOK ) new time: %02ld:%02ld:%02ld\n", rtc.hour, rtc.min, rtc.sec );
        };
        MZFrepo.sts_err = false;
    };
}



/* do_cmdCLOSE()
 *
 * Pokud je v MZFrepo.fh otevreny soubor, tak provedeme f_sync a f_close a
 * vypneme konverzi ASCII pro fput a fget.
 */
uint8_t do_cmdCLOSE ( void ) {
    DBGPRINTF ( DBGINF, "do_cmdCLOSE()\n" );
    if ( MZFrepo.fh.fs ) {
        DBGPRINTF ( DBGFAT, " - f_sync\n" );
				emu_RAMDISC_fsync();
        MZFrepo.ff_res = f_sync ( &MZFrepo.fh );
        if ( FR_OK != MZFrepo.ff_res ) {
            DBGPRINTF ( DBGERR, "do_cmdCLOSE() - f_sync error! 0x%02lx\n", MZFrepo.ff_res );
            return ( RETURN_EMU_ERR );
        };
        DBGPRINTF ( DBGFAT, " - f_close\n" );
        MZFrepo.ff_res = f_close ( &MZFrepo.fh );
        if ( FR_OK != MZFrepo.ff_res ) {
            DBGPRINTF ( DBGERR, "do_cmdCLOSE() - f_close error! 0x%02lx\n", MZFrepo.ff_res );
            return ( RETURN_EMU_ERR );
        };
    };
    memset ( &MZFrepo.fh, 0x00, sizeof ( FIL ) );
    MZFrepo.file_mode = 0x00;
    MZFrepo.sts_err = false;
    return ( RETURN_EMU_OK );
}


/* do_cmdSEEK()
 *
 * Provede zmenu pozice v otevrenem souboru.
 */
void do_cmdSEEK ( uint8_t cmd_phase ) {

    if ( phsSTART == cmd_phase ) {
        DBGPRINTF ( DBGINF, "do_cmdSEEK ( START )\n" );

        g_param_format = "BBBBB";
        InputParams ( cmd_phase, 0 );

    } else {
        // zpracovani prikazu
        uint32_t offset;
        DBGPRINTF ( DBGINF, "do_cmdSEEK ( PARAMOK )\n" );
        MZFrepo.cmd_phase = phsDONE;

        if ( 3 < MZFrepo.buf_byte[0] ) {
            // TODO: unicard err code PARAM_ERR
            return;
        };

        memcpy ( &offset, ( MZFrepo.buf_byte + 1 ), 4 );
        DBGPRINTF ( DBGINF, "do_cmdSEEK: mode=%d, offset=0x%08lx\n", MZFrepo.buf_byte[0], offset );
        switch ( MZFrepo.buf_byte[0] ) {
            case 1:
                offset = f_size ( &MZFrepo.fh ) - offset;
                break;
            case 2:
                offset = f_tell ( &MZFrepo.fh ) + offset;
                break;
            case 3:
                offset = f_tell ( &MZFrepo.fh ) - offset;
                break;
        };
        DBGPRINTF ( DBGFAT, " - f_lseek 0x%08lx\n", offset );
        MZFrepo.ff_res = f_lseek ( &MZFrepo.fh, offset );
        if ( FR_OK != MZFrepo.ff_res ) {
            DBGPRINTF ( DBGERR, "do_cmdSEEK() - f_lseek error! 0x%02lx\n", MZFrepo.ff_res );
            return;
        };
        MZFrepo.sts_err = false;
    };
}



/* do_cmdOPEN()
 *
 * Otevre soubor pro dalsi pouziti.
 */
void do_cmdOPEN ( uint8_t cmd_phase ) {

    if ( phsSTART == cmd_phase ) {
        DBGPRINTF ( DBGINF, "do_cmdOPEN ( START )\n" );

        CloseDir();		// zavreme adresar, pokud byl otevreny

        g_param_format = "BS";
        InputParams ( cmd_phase, 0 );

    } else {
        // zpracovani prikazu
        DBGPRINTF ( DBGINF, "do_cmdOPEN ( PARAMOK )\n" );
        MZFrepo.cmd_phase = phsDONE;
        MZFrepo.buf = MZFrepo.buf_byte + 1;
        DBGPRINTF ( DBGFAT, " - f_open '%s', mode: 0x%02lx\n", MZFrepo.buf, MZFrepo.buf_byte[0] );
        MZFrepo.file_mode = MZFrepo.buf_byte[0];
        MZFrepo.ff_res = f_open ( &MZFrepo.fh, (char*) MZFrepo.buf, ( MZFrepo.file_mode & 0xdf ) );
        if ( FR_OK != MZFrepo.ff_res ) {
            DBGPRINTF ( DBGERR, "do_cmdOPEN() - f_open error! 0x%02lx\n", MZFrepo.ff_res );
            return;
        };
			  MZFrepo.dirty = 0;
				MZFrepo.offset = 0;
				MZFrepo.buff_id = 0;
				MZFrepo.preread_ready = 0;
				{
					unsigned int s1;
					if (MZFrepo.fh.fsize>0) f_read( &MZFrepo.fh, &REPODISK[MZFrepo.buff_id], REPODISK_SIZE, &s1 );
				}
        MZFrepo.sts_err = false;
    };
}


bool g_dir_filelist_mode;
/* do_cmdREADDIR()
 *
 * Otevre adresar pro dalsi pouziti v binarnim, nebo txt rezimu.
 */
void do_cmdREADDIR ( uint8_t cmd_phase ) {

    if ( phsSTART == cmd_phase ) {
        DBGPRINTF ( DBGINF, "do_cmdREADDIR ( START, txt=%d )\n", g_dir_filelist_mode );

        do_cmdCLOSE();	// Zavreme soubor, pokud byl otevreny

        g_param_format = "S";
        InputParams ( cmd_phase, 0 );

    } else if ( phsPARAMOK == cmd_phase ) {
        // zpracovani prikazu - otevreni adresare
        DBGPRINTF ( DBGINF, "do_cmdREADDIR ( PARAMOK, txt=%d )\n", g_dir_filelist_mode );
        MZFrepo.cmd_phase = phsDONE;
        DBGPRINTF ( DBGFAT, " - f_opendir '%s' \n", MZFrepo.buf_byte );
        MZFrepo.ff_res = f_opendir ( &MZFrepo.dh, (char*) MZFrepo.buf_byte );
        if ( FR_OK != MZFrepo.ff_res ) {
            DBGPRINTF ( DBGERR, "do_cmdREADDIR() - f_opendir error! 0x%02lx\n", MZFrepo.ff_res );
            return;
        };

        // pripravime prvni polozku adresare
        do_cmdREADDIR ( phsWORK );

    } else {
        // nacteni polozky adresare
        FILINFO finfo;
        DBGPRINTF ( DBGINF, "do_cmdREADDIR ( WORK, txt=%d )\n", g_dir_filelist_mode );

        if ( true == g_dir_filelist_mode ) {
            MZFrepo.cmd = cmdFILELIST;
        } else {
            MZFrepo.cmd = cmdREADDIR;
        };
        MZFrepo.cmd_phase = phsDONE;

        DBGPRINTF ( DBGFAT, " - f_readdir\n" );

        finfo.lfname = (char*) MZFrepo.filebuf_byte + 23;	// DWORD+WORD+WORD+BYTE+TCHAR[13]+1
        finfo.lfsize = _MAX_LFN;

        do_cmdREADDIR_NEXT_FIELD:

        MZFrepo.ff_res = f_readdir ( &MZFrepo.dh, &finfo );
        if ( FR_OK != MZFrepo.ff_res ) {
            DBGPRINTF ( DBGERR, "do_cmdREADDIR() - f_readdir error! 0x%02lx\n", MZFrepo.ff_res );
            return;
        };

        // vynechat "."
        if ( ( finfo.fname[0] == '.' ) && ( finfo.fname[1] == '\x00' ) ) goto do_cmdREADDIR_NEXT_FIELD;

        MZFrepo.sts_err = false;

        if ( finfo.fname[0] == '\x00' ) {
            // dalsi polozka uz neni
            DBGPRINTF ( DBGINF, "do_cmdREADDIR ( WORK, txt=%d ) - EOF\n", g_dir_filelist_mode );
            CloseDir();
            return;
        };

        if ( false == g_dir_filelist_mode ) {
            // READDIR
            memcpy ( MZFrepo.filebuf_byte, &finfo, 22 ); // DWORD+WORD+WORD+BYTE+TCHAR[13]
            MZFrepo.filebuf_byte[22] = strlen ( finfo.lfname );
            //MZFrepo.filebuf_count = 23 + MZFrepo.filebuf_byte[22];
            MZFrepo.filebuf_count = 23 + _MAX_LFN;
            MZFrepo.filebuf = MZFrepo.filebuf_byte;

        } else {
            // FILELIST
            char s_len[20];
            long2dec ( finfo.fsize, s_len);
            if ( finfo.lfname[0] ) {
                strcpy ( (char*) MZFrepo.filebuf_byte, finfo.lfname );
            } else {
                strcpy ( (char*) MZFrepo.filebuf_byte, finfo.fname );
            };
            if ( 0x10 == ( finfo.fattrib & 0x10 ) ) {
                strcat ( (char*) MZFrepo.filebuf_byte, "/" );
            };
            strcat( (char*) MZFrepo.filebuf_byte, "\x0d" );
            strcat( (char*) MZFrepo.filebuf_byte,  s_len );
            strcat( (char*) MZFrepo.filebuf_byte, "\x0d");
            MZFrepo.filebuf_count = strlen ( (char*) MZFrepo.filebuf_byte );
        };
        MZFrepo.filebuf = MZFrepo.filebuf_byte;
    };
}

/* do_cmdMKDIR()
 *
 * Smaze soubor, nebo adresar.
 */
void do_cmdMKDIR ( uint8_t cmd_phase ) {

    if ( phsSTART == cmd_phase ) {
        DBGPRINTF ( DBGINF, "do_cmdMKDIR ( START )\n" );

        g_param_format = "S";
        InputParams ( cmd_phase, 0 );

    } else {
        // zpracovani prikazu
        DBGPRINTF ( DBGINF, "do_cmdMKDIR ( PARAMOK )\n" );
        MZFrepo.cmd_phase = phsDONE;
        DBGPRINTF ( DBGFAT, " - f_mkdir '%s'\n", MZFrepo.buf_byte );
        MZFrepo.ff_res = f_mkdir ( (char*) MZFrepo.buf_byte );
        if ( FR_OK != MZFrepo.ff_res ) {
            DBGPRINTF ( DBGERR, "do_cmdMKDIR() - f_mkdir error! 0x%02lx\n", MZFrepo.ff_res );
            return;
        };
        MZFrepo.sts_err = false;
    };
}



/* do_cmdRENAME()
 *
 * Prejmenovani souboru, nebo adresare.
 */
void do_cmdRENAME ( uint8_t cmd_phase ) {

    if ( phsSTART == cmd_phase ) {
        DBGPRINTF ( DBGINF, "do_cmdRENAME ( START )\n" );

        g_param_format = "SS";
        InputParams ( cmd_phase, 0 );

    } else {
        // zpracovani prikazu
        DBGPRINTF ( DBGINF, "do_cmdRENAME ( PARAMOK )\n" );
        MZFrepo.cmd_phase = phsDONE;
        MZFrepo.buf = MZFrepo.buf_byte + strlen ( (char*) MZFrepo.buf_byte ) + 1;
        DBGPRINTF ( DBGFAT, " - f_rename old='%s', new='%s'\n", (char*) MZFrepo.buf_byte, (char*) MZFrepo.buf);
        MZFrepo.ff_res = f_rename ( (char*) MZFrepo.buf_byte, (char*) MZFrepo.buf );
        if ( FR_OK != MZFrepo.ff_res ) {
            DBGPRINTF ( DBGERR, "do_cmdRENAME() - f_rename error! 0x%02lx\n", MZFrepo.ff_res );
            return;
        };
        MZFrepo.sts_err = false;
    };
}


/* do_cmdUTIME()
 *
 * Zmena casove znacky souboru, nebo adresare.
 */
void do_cmdUTIME ( uint8_t cmd_phase ) {

    if ( phsSTART == cmd_phase ) {
        DBGPRINTF ( DBGINF, "do_cmdUTIME ( START )\n" );

        g_param_format = "BBBBBBS";
        InputParams ( cmd_phase, 0 );

    } else {
        // zpracovani prikazu
        FILINFO fno;
        DBGPRINTF ( DBGINF, "do_cmdUTIME ( PARAMOK )\n" );
        MZFrepo.cmd_phase = phsDONE;
        MZFrepo.buf = MZFrepo.buf_byte + 6;
        DBGPRINTF ( DBGFAT, " - f_utime '%s', day: 0x%02lx, month: 0x%02lx, year: 0x%02lx, hour: 0x%02lx, min: 0x%02lx, sec: 0x%02lx\n", MZFrepo.buf, MZFrepo.buf_byte[0], MZFrepo.buf_byte[1], MZFrepo.buf_byte[2], MZFrepo.buf_byte[3], MZFrepo.buf_byte[4], MZFrepo.buf_byte[5] );
        fno.fdate = (WORD) ( ( MZFrepo.buf_byte[2] * 512U ) | MZFrepo.buf_byte[1] * 32U | MZFrepo.buf_byte[0] );
        fno.ftime = (WORD) ( MZFrepo.buf_byte[3] * 2048U | MZFrepo.buf_byte[4] * 32U | MZFrepo.buf_byte[5] / 2U);
        MZFrepo.ff_res = f_utime ( (char*) MZFrepo.buf, &fno );
        if ( FR_OK != MZFrepo.ff_res ) {
            DBGPRINTF ( DBGERR, "do_cmdUTIME() - f_utime error! 0x%02lx\n", MZFrepo.ff_res );
            return;
        };
        MZFrepo.sts_err = false;
    };
}



/* do_cmdCHMOD()
 *
 * Zmena atributu souboru, nebo adresare.
 */
void do_cmdCHMOD ( uint8_t cmd_phase ) {

    if ( phsSTART == cmd_phase ) {
        DBGPRINTF ( DBGINF, "do_cmdCHMOD ( START )\n" );

        g_param_format = "BBS";
        InputParams ( cmd_phase, 0 );

    } else {
        // zpracovani prikazu
        DBGPRINTF ( DBGINF, "do_cmdCHMOD ( PARAMOK )\n" );
        MZFrepo.cmd_phase = phsDONE;
        MZFrepo.buf = MZFrepo.buf_byte + 2;
        DBGPRINTF ( DBGFAT, " - f_chmod '%s', attr: 0x%02lx, msk: 0x%02lx\n", MZFrepo.buf, MZFrepo.buf_byte[0], MZFrepo.buf_byte[1] );
        MZFrepo.ff_res = f_chmod ( (char*) MZFrepo.buf, MZFrepo.buf_byte[0], MZFrepo.buf_byte[1] );
        if ( FR_OK != MZFrepo.ff_res ) {
            DBGPRINTF ( DBGERR, "do_cmdCHMOD() - f_chmod error! 0x%02lx\n", MZFrepo.ff_res );
            return;
        };
        MZFrepo.sts_err = false;
    };
}


/* do_cmdUNLINK()
 *
 * Smaze soubor, nebo adresar.
 */
void do_cmdUNLINK ( uint8_t cmd_phase ) {

    if ( phsSTART == cmd_phase ) {
        DBGPRINTF ( DBGINF, "do_cmdUNLINK ( START )\n" );

        g_param_format = "S";
        InputParams ( cmd_phase, 0 );

    } else {
        // zpracovani prikazu
        DBGPRINTF ( DBGINF, "do_cmdUNLINK ( PARAMOK )\n" );
        MZFrepo.cmd_phase = phsDONE;
        DBGPRINTF ( DBGFAT, " - f_unlink '%s'\n", MZFrepo.buf_byte );
        MZFrepo.ff_res = f_unlink ( (char*) MZFrepo.buf_byte );
        if ( FR_OK != MZFrepo.ff_res ) {
            DBGPRINTF ( DBGERR, "do_cmdUNLINK() - f_unlink error! 0x%02lx\n", MZFrepo.ff_res );
            return;
        };
        MZFrepo.sts_err = false;
    };
}


/* do_cmdSTAT()
 *
 * Vrati FILINFO o souboru/adresari.
 */
void do_cmdSTAT ( uint8_t cmd_phase ) {

    if ( phsSTART == cmd_phase ) {
        DBGPRINTF ( DBGINF, "do_cmdSTAT ( START )\n" );

        g_param_format = "S";
        InputParams ( cmd_phase, 0 );

    } else {
        // zpracovani prikazu
        FILINFO finfo;
        DBGPRINTF ( DBGINF, "do_cmdSTAT ( PARAMOK )\n" );
        MZFrepo.cmd_phase = phsDONE;
        DBGPRINTF ( DBGFAT, " - f_stat '%s'\n", MZFrepo.buf_byte );
        finfo.lfname = (char*) MZFrepo.buf_byte + 23;	// DWORD+WORD+WORD+BYTE+TCHAR[13]+1
        finfo.lfsize = _MAX_LFN;
        MZFrepo.ff_res = f_stat ( (char*) MZFrepo.buf_byte, &finfo );
        if ( FR_OK != MZFrepo.ff_res ) {
            DBGPRINTF ( DBGERR, "do_cmdSTAT() - f_stat error! 0x%02lx\n", MZFrepo.ff_res );
            return;
        };
        memcpy ( MZFrepo.buf_byte, &finfo, 22 ); // DWORD+WORD+WORD+BYTE+TCHAR[13]
        MZFrepo.buf_byte[22] = strlen ( finfo.lfname );
        //MZFrepo.buf_count = 23 + MZFrepo.buf_byte[22];
        MZFrepo.buf_count = 23 + _MAX_LFN;
        MZFrepo.buf = MZFrepo.buf_byte;
        MZFrepo.cmd_phase = phsDOUTRQ;
        MZFrepo.sts_err = false;
    };
}

/* do_cmdCHDIR()
 *
 * Provede zmenu CWD.
 */
void do_cmdCHDIR ( uint8_t cmd_phase ) {

    if ( phsSTART == cmd_phase ) {
        DBGPRINTF ( DBGINF, "do_cmdCHDIR ( START )\n" );

        g_param_format = "S";
        InputParams ( cmd_phase, 0 );

    } else {
        // zpracovani prikazu
        DBGPRINTF ( DBGINF, "do_cmdCHDIR ( PARAMOK )\n" );
        MZFrepo.cmd_phase = phsDONE;
        DBGPRINTF ( DBGFAT, " - f_chdir '%s'\n", MZFrepo.buf_byte );
        MZFrepo.ff_res = f_chdir ( (char*) MZFrepo.buf_byte );
        if ( FR_OK != MZFrepo.ff_res ) {
            DBGPRINTF ( DBGERR, "do_cmdCHDIR() - f_chdir error! 0x%02lx\n", MZFrepo.ff_res );
            return;
        };
        MZFrepo.sts_err = false;
    };
}



/* do_cmdGETFREE()
 *
 * Vrati celkovy pocet sektoru a pocet volnych sektoru.
 */
void do_cmdGETFREE ( uint8_t cmd_phase ) {
    uint32_t /*fre_clust, */fre_sect, tot_sect;
    DWORD fre_clust;
    FATFS *fs;

    if ( phsSTART == cmd_phase ) {
        // Get volume information and free clusters of drive
        MZFrepo.ff_res = f_getfree ( "0:", &fre_clust, &fs );
        if ( FR_OK != MZFrepo.ff_res ) {
            MZFrepo.cmd_phase = phsDONE;
            DBGPRINTF ( DBGERR, "do_cmdGETFREE - f_getfree failed! 0x%02lx\n", MZFrepo.ff_res );
            return;
        };
        // Get total sectors and free sectors
        tot_sect = ( fs->n_fatent - 2 ) * fs->csize;
        fre_sect = fre_clust * fs->csize;
        DBGPRINTF ( DBGINF, "do_cmdGETFREE ( START ) - tot_sect=0x%08lx, free_sect=0x%08lx, free_c=0x%08lx\n", tot_sect, fre_sect, fre_clust );

        memcpy ( MZFrepo.buf_byte, &tot_sect, sizeof ( uint32_t ) );
        memcpy ( (MZFrepo.buf_byte + 4), &fre_sect, sizeof ( uint32_t ) );

        MZFrepo.buf = MZFrepo.buf_byte;
        MZFrepo.buf_count = 8;		// 2x uint32_t
        MZFrepo.cmd_phase = phsDOUTRQ;
        MZFrepo.sts_err = false;
    };
}


/* do_cmdFDDMOUNT()
 *
 * Provede remount FDD a ulozi konfiguraci.
 */
void do_cmdFDDMOUNT ( uint8_t cmd_phase ) {

    if ( phsSTART == cmd_phase ) {
        DBGPRINTF ( DBGINF, "do_cmdFDDMOUNT ( START )\n" );

        g_param_format = "BS";
        InputParams ( cmd_phase, 0 );

    } else {
        DIR ff_dir;
        char filename[18];
        char zn[2];
        // zpracovani prikazu
        DBGPRINTF ( DBGINF, "do_cmdFDDMOUNT ( PARAMOK )\n" );
        MZFrepo.buf = MZFrepo.buf_byte + 1;
        DBGPRINTF ( DBGINF, "do_cmdFDDMOUNT ( EXEC ) - 0x%02lx, DSK='%s'\n", MZFrepo.buf_byte[0], MZFrepo.buf );
        MZFrepo.cmd_phase = phsDONE;

        if ( 3 < MZFrepo.buf_byte[0] ) {
            DBGPRINTF ( DBGERR, "do_cmdFDDMOUNT() - invalid FDD id '0x%02lx'!\n", MZFrepo.buf_byte[0] );
            // TODO: unicard err code: INVALID_PARAMETER
            return;
        };

        DBGPRINTF ( DBGFAT, " - f_opendir: /unicard\n" );
        if  ( FR_OK !=  f_opendir (&ff_dir, "/unicard" ) ) {
            DBGPRINTF ( DBGWAR, "do_cmdFDDMOUNT() - not exist directory /unicard!\n" );
            MZFrepo.ff_res = f_mkdir ( "/unicard" );
            if ( FR_OK != MZFrepo.ff_res ) {
                DBGPRINTF ( DBGERR, "do_cmdFDDMOUNT() - f_mkdir failed! 0x%02lx\n", MZFrepo.ff_res );
                // TODO: unicard err code: MKDIR?
                return;
            };
        };

        // vytvorime jmeno konfiguraku k FDD
        strcpy ( filename, "/unicard/fd" );
        zn[0] = ( MZFrepo.buf_byte[0] + 0x30 ); zn[1] = 0x00;
        strcat ( filename, zn );
        strcat ( filename, ".cfg" );

        // Nebyla uvedena filepath, takze smazeme konfigurak pokud existuje.
        if ( MZFrepo.buf[0] == '\0' ) {
            DBGPRINTF ( DBGFAT, " - f_unlink: %s\n", filename );
            MZFrepo.ff_res = f_unlink ( filename );
            if ( FR_OK != MZFrepo.ff_res ) {
                DBGPRINTF ( DBGERR, "do_cmdFDDMOUNT() - f_unlink '%s' failed! 0x%02lx\n", filename, MZFrepo.ff_res );
                return;
            };
        } else {
            // otevreme soubor fdX.cfg pro zapis
            static FIL ff_file;
            static unsigned int ff_readlen;
            DBGPRINTF ( DBGFAT, " - f_open: '%s'\n", filename );
            MZFrepo.ff_res = f_open ( &ff_file, filename, FA_WRITE | FA_CREATE_ALWAYS );
            if ( FR_OK != MZFrepo.ff_res ) {
                DBGPRINTF ( DBGERR, "do_cmdFDDMOUNT() - f_open '%s' failed! 0x%02lx\n", filename, MZFrepo.ff_res );
                return;
            };
            // zapsat konfiguraci
            DBGPRINTF ( DBGFAT, " - f_write: '%s'\n", filename );
            MZFrepo.ff_res = f_write ( &ff_file, (uint8_t*) MZFrepo.buf, strlen ( (char*) MZFrepo.buf ), &ff_readlen );
            if ( FR_OK != MZFrepo.ff_res ) {
                DBGPRINTF(DBGERR, "do_cmdFDDMOUNT() - f_write '%s' failed! 0x%02lx\n", filename, MZFrepo.ff_res);
                f_close ( &ff_file );
                return;
            };
            if ( ff_readlen != strlen ( (char*) MZFrepo.buf ) ) {
                DBGPRINTF(DBGERR, "do_cmdFDDMOUNT() - f_write '%s' failed! 0x%02lx\n", filename, MZFrepo.ff_res);
                f_close ( &ff_file );
                return;
            };

            DBGPRINTF ( DBGFAT, " - f_sync: '%s'\n", filename );
            MZFrepo.ff_res = f_sync ( &ff_file );
            if ( FR_OK != MZFrepo.ff_res ) {
                DBGPRINTF(DBGERR, "do_cmdFDDMOUNT() - f_sync '%s' failed! 0x%02lx\n", filename, MZFrepo.ff_res);
                f_close ( &ff_file );
                return;
            };
            DBGPRINTF ( DBGFAT, " - f_close: '%s'\n", filename );
            MZFrepo.ff_res = f_close ( &ff_file );
            if ( FR_OK != MZFrepo.ff_res ) {
                DBGPRINTF(DBGERR, "do_cmdFDDMOUNT() - f_close '%s' failed! 0x%02lx\n", filename, MZFrepo.ff_res);
                f_close ( &ff_file );
                return;
            };
        };

        // pujde reload FDD ?
        if ( reload_FDD ) {	/////
            DBGPRINTF ( DBGINF, "do_cmdFDDMOUNT() - remount FDD\n" );
            if ( -1 == reload_FDD ( MZFrepo.buf_byte[0] ) ) {
                // TODO: unicard err code: ????
                DBGPRINTF(DBGERR, "do_cmdFDDMOUNT() - FDD%d remount error!\n", MZFrepo.buf_byte[0] );
                return;
            };
        } else {
            // TODO: unicard err code: ????
            DBGPRINTF ( DBGERR, "do_cmdFDDMOUNT() - No FDD remount function\n" );
            return;
        };

        // uspesne dokonceni prikazu
        MZFrepo.sts_err = false;
    };
}



/* do_cmdRESET()
 *
 * Ukonci aktivni prikaz, nastavi CWD=/, zavola do_cmdCLOSE() a vypne konverzi ASCII u prikazu.
 */
uint8_t do_cmdRESET ( void ) {
    DBGPRINTF ( DBGINF, "do_cmdRESET()\n" );
    MZFrepo.cmd_phase = phsDONE;
    MZFrepo.cnv_cmd = false;
    MZFrepo.ff_res = f_chdir ("/");
    if ( FR_OK != MZFrepo.ff_res ) {
        DBGPRINTF ( DBGERR, "do_cmdRESET - f_chdir '/' error! 0x%02lx\n", MZFrepo.ff_res );
        return ( RETURN_EMU_ERR );
    };
    CloseDir();
    if ( RETURN_EMU_ERR == do_cmdCLOSE() ) return ( RETURN_EMU_ERR );
    MZFrepo.sts_err = false;
    return ( RETURN_EMU_OK );
}


/* emu_MZFREPO_init()
 *
 * Inicializace repozitare pri startu unikarty
 */
int emu_MZFREPO_init ( void ) {
  MZFrepo.dirty = 0;
//	RAMdisc.timeout = 0;
  MZFrepo.offset = 0;
	MZFrepo.buff_id = 0;
	MZFrepo.preread_ready = 0;
    DBGPRINTF ( DBGINF, "emu_MZFREPO_init()\n" );
    MZFrepo.sts_pos = 0x00;
    MZFrepo.sts_err = true;
    MZFrepo.cmd = cmdRESET;
    return ( do_cmdRESET() );
}



/* emu_MZFREPO_write
 *
 * Connection is like following:
 * offset = 0 - command register
 * offset = 1 - data register
 */
int emu_MZFREPO_write ( int i_addroffset, unsigned int *io_data ) {

    MZFrepo.sts_pos = 0x00;

    // data port
    if ( i_addroffset & 0x01 ) {

        MZFrepo.sts_err = true;

        if ( phsPARAMRQ == MZFrepo.cmd_phase ) {

            int8_t res = InputParams ( phsPARAM, *io_data );
            // zapis parametru k prikazum
            DBGPRINTF ( DBGINF, "do_cmd'0x%02lx' ( BYTE )\n", MZFrepo.cmd );
            if ( -1 == res ) {			// preteceni bufferu
                MZFrepo.cmd_phase = phsDONE;
                return ( RETURN_EMU_ERR );

            } else if ( 0 == res ) {		// vsechny parametry jsou prijate - jdeme neco vykonat

                switch ( MZFrepo.cmd ) {

                    case cmdFDDMOUNT:
                        do_cmdFDDMOUNT ( phsPARAMOK );
                        break;

                    case cmdCHDIR:
                        do_cmdCHDIR ( phsPARAMOK );
                        break;

                    case cmdSTAT:
                        do_cmdSTAT ( phsPARAMOK );
                        break;

                    case cmdUNLINK:
                        do_cmdUNLINK ( phsPARAMOK );
                        break;

                    case cmdCHMOD:
                        do_cmdCHMOD ( phsPARAMOK );
                        break;

                    case cmdUTIME:
                        do_cmdUTIME ( phsPARAMOK );
                        break;

                    case cmdRENAME:
                        do_cmdRENAME ( phsPARAMOK );
                        break;

                    case cmdMKDIR:
                        do_cmdMKDIR ( phsPARAMOK );
                        break;

                    case cmdREADDIR:
                    case cmdFILELIST:
                        do_cmdREADDIR ( phsPARAMOK );
                        break;

                    case cmdOPEN:
                        do_cmdOPEN ( phsPARAMOK );
                        break;

                    case cmdSEEK:
                        do_cmdSEEK ( phsPARAMOK );
                        break;

                    case cmdRTCSETD:
                    case cmdRTCSETT:
                        do_cmdRTCSET ( phsPARAMOK );
                        break;

                    case cmdUSARTBPS:
                        do_cmdUSARTBPS ( phsPARAMOK );
                        break;

                };

            } else {			// pokracujeme v prijimani parametru
                MZFrepo.sts_err = false;
                return ( RETURN_EMU_OK );
            };

        } else if ( MZFrepo.fh.fs ) {

            unsigned int ff_writelen;
            uint8_t put_val = *io_data;
            // cmdINTPUTC: zapis do otevreneho souboru (TODO: dopsat cache)
            MZFrepo.cmd = cmdINTPUTC;

            if ( MZFrepo.file_mode & 0x20 ) {
                uint8_t cnv_val = ToSHASCII ( put_val );
#if DBG_GETCPUTC
                DBGPRINTF ( DBGINF, "cmdINTPUTC: - 0x%02lx, cnv: 0x%02lx\n", put_val, cnv_val );
#endif
                put_val = cnv_val;
#if DBG_GETCPUTC
            } else {
                DBGPRINTF ( DBGINF, "cmdINTPUTC: - 0x%02lx\n", put_val );
#endif
            };

//            MZFrepo.ff_res = f_write ( &MZFrepo.fh, &put_val, 1, &ff_writelen );

						MZFrepo.ff_res =  repo_write1(put_val);
						ff_writelen = 1;
						
            if ( FR_OK != MZFrepo.ff_res ) {
                DBGPRINTF ( DBGERR, "cmdINTPUTC: - f_write error! 0x%02lx\n", MZFrepo.ff_res );
                return ( RETURN_EMU_ERR );
            };

            if ( 1 != ff_writelen ) {
                DBGPRINTF ( DBGERR, "cmdINTPUTC: - unknown write error!\n" );
                // TODO: unicard err code WRITE_ERR
                return ( RETURN_EMU_ERR );
            };

            MZFrepo.sts_err = false;

        } else {
            // prijali jsme data, ktere neni kam ulozit
            // TODO: unicard err code DATA_LOST
        };



    // cmd port
    } else {

        //
        // Status pointer = 0 (prazdny prikaz)
        //
        if ( cmdSTSR == *io_data ) {
            DBGPRINTF ( DBGINF, "do_cmdSTSR: reset sts pointer\n" );
            return ( RETURN_EMU_OK );
        };

        MZFrepo.sts_err = true;

        MZFrepo.cmd = *io_data;

        switch ( *io_data ) {


            //
            // Prace s repozitarem:
            //
            case cmdRESET:
                do_cmdRESET();
                break;

            case cmdASCII:
                DBGPRINTF ( DBGINF, "do_cmdASCII: cnv/OFF\n" );
                MZFrepo.cnv_cmd = false;
                MZFrepo.cmd_phase = phsDONE;
                MZFrepo.sts_err = false;
                break;

            case cmdSHASCII:
                DBGPRINTF ( DBGINF, "do_cmdSHASCII: cnv/ON\n" );
                MZFrepo.cnv_cmd = true;
                MZFrepo.cmd_phase = phsDONE;
                MZFrepo.sts_err = false;
                break;

            case cmdSTORNO:
                DBGPRINTF ( DBGINF, "do_cmdSTORNO\n" );
                MZFrepo.cmd_phase = phsDONE;
                MZFrepo.sts_err = false;
                break;

            case cmdREV:
                MZFrepo.sts_err = false;
                MZFrepo.buf = (uint8_t*) FIRMWARE_REVISION;
                DBGPRINTF ( DBGINF, "do_cmdREV ( START ) - rev: '%s'\n", MZFrepo.buf );
                MZFrepo.buf_count = strlen ( FIRMWARE_REVISION ) + 1;	// + 0x0d
                MZFrepo.cmd_phase = phsDOUTRQ;
                break;

            case cmdREVD:
                MZFrepo.sts_err = false;
                MZFrepo.buf = (uint8_t*) &FIRMWARE_REVISION_DWORD;
                DBGPRINTF ( DBGINF, "do_cmdREVD ( START ) - rev: 0x%04lx\n", (uint32_t*) MZFrepo.buf );
                MZFrepo.buf_count = 4;
                MZFrepo.cmd_phase = phsDOUTRQ;
                break;


            //
            // Konfigurace a nastaveni unikarty:
            //
            case cmdFDDMOUNT:
                do_cmdFDDMOUNT ( phsSTART );
                break;

            case cmdINTCALLER:
                MZFrepo.sts_err = false;
                MZFrepo.buf_byte[0] = mzint_getFLAG ();
                MZFrepo.buf = MZFrepo.buf_byte;
                DBGPRINTF ( DBGINF, "do_cmdINTCALLER ( START ) - mzintFLAG: 0x%02x\n", MZFrepo.buf_byte[0] );
                MZFrepo.buf_count = 1;
                MZFrepo.cmd_phase = phsDOUTRQ;
                break;

            //
            // Prace s filesystemem:
            //
            case cmdGETFREE:
                do_cmdGETFREE ( phsSTART );
                break;

            case cmdCHDIR:
                do_cmdCHDIR ( phsSTART );
                break;

            case cmdGETCWD:
                MZFrepo.ff_res = f_getcwd ( (char*) MZFrepo.buf_byte, PARAM_BUFFER_SIZE );
                if ( FR_OK != MZFrepo.ff_res ) {
                    DBGPRINTF ( DBGERR, "do_cmdGETCWD - f_getcwd error! 0x%02lx\n", MZFrepo.ff_res );
                    MZFrepo.cmd_phase = phsDONE;
                    break;
                };
                MZFrepo.buf = MZFrepo.buf_byte + 2;	// odrizneme 2 znaky "0:"
                DBGPRINTF ( DBGINF, "do_cmdGETCWD: '%s'\n", MZFrepo.buf );
                MZFrepo.buf_count = strlen ( (char*) MZFrepo.buf ) + 1;	// + 0x0d
                MZFrepo.cmd_phase = phsDOUTRQ;
                MZFrepo.sts_err = false;
                break;

            //
            // Prace se soubory a adresari:
            //
            case cmdSTAT:
                do_cmdSTAT ( phsSTART );
                break;

            case cmdUNLINK:
                do_cmdUNLINK ( phsSTART );
                break;

            case cmdCHMOD:
                do_cmdCHMOD ( phsSTART );
                break;

            case cmdUTIME:
                do_cmdUTIME ( phsSTART );
                break;

            case cmdRENAME:
                do_cmdRENAME ( phsSTART );
                break;

            //
            // Prace s adresarem:
            //
            case cmdMKDIR:
                do_cmdMKDIR ( phsSTART );
                break;

            case cmdREADDIR:
                g_dir_filelist_mode = false;
                do_cmdREADDIR ( phsSTART );
                break;

            case cmdFILELIST:
                g_dir_filelist_mode = true;
                do_cmdREADDIR ( phsSTART );
                break;

            case cmdNEXT:
                do_cmdREADDIR ( phsWORK );
                break;

            //
            // Prace se souborem:
            //
            case cmdOPEN:
                do_cmdOPEN ( phsSTART );
                break;

            case cmdSEEK:
                do_cmdSEEK ( phsSTART );
                break;

            case cmdTRUNC:
                DBGPRINTF ( DBGINF, "do_cmdTRUNC - pos: 0x%08lx\n", f_tell ( &MZFrepo.fh ) );
                MZFrepo.cmd_phase = phsDONE;
								emu_RAMDISC_fsync();
                MZFrepo.ff_res = f_truncate ( &MZFrepo.fh );
                if ( FR_OK != MZFrepo.ff_res ) {
                    DBGPRINTF ( DBGERR, "do_cmdTRUNC - f_truncate error! 0x%02lx\n", MZFrepo.ff_res );
                    break;
                };
                MZFrepo.sts_err = false;
                break;

            case cmdSYNC:
                MZFrepo.cmd_phase = phsDONE;
								emu_RAMDISC_fsync();
								MZFrepo.ff_res = f_sync ( &MZFrepo.fh );
                if ( FR_OK != MZFrepo.ff_res ) {
                    DBGPRINTF ( DBGERR, "do_cmdSYNC - f_sync error! 0x%02lx\n", MZFrepo.ff_res );
                    break;
                };
                DBGPRINTF ( DBGINF, "do_cmdSYNC\n" );
                MZFrepo.sts_err = false;
                break;

            case cmdCLOSE:
                if ( RETURN_EMU_OK == do_cmdCLOSE() ) MZFrepo.sts_err = false;
                MZFrepo.cmd_phase = phsDONE;
                break;

            case cmdTELL:
//                MZFrepo.buf = (uint8_t*) &MZFrepo.fh.fptr;
                MZFrepo.buf = (uint8_t*) &MZFrepo.offset;
                DBGPRINTF ( DBGINF, "do_cmdTELL: 0x%08lx\n", MZFrepo.fh.fptr );
                MZFrepo.buf_count = 4;
                MZFrepo.cmd_phase = phsDOUTRQ;
                MZFrepo.sts_err = false;
                break;

            case cmdSIZE:
                MZFrepo.buf = (uint8_t*) &MZFrepo.fh.fsize;
                DBGPRINTF ( DBGINF, "do_cmdFSIZE: 0x%08lx\n", MZFrepo.fh.fsize );
                MZFrepo.buf_count = 4;
                MZFrepo.cmd_phase = phsDOUTRQ;
                MZFrepo.sts_err = false;
                break;

            //
            // Prace s RTC:
            //
            case cmdRTCSETD:
                g_rtc_set_date = true;
                do_cmdRTCSET ( phsSTART );
                break;

            case cmdRTCSETT:
                g_rtc_set_date = false;
                do_cmdRTCSET ( phsSTART );
                break;

            case cmdRTCGETD:
            case cmdRTCGETT:
                if ( MZFrepo.cmd == cmdRTCGETD ) {
									  RTC_DateTypeDef rtc;
										RTC_GetDate(RTC_Format_BIN,&rtc);
//                    DBGPRINTF ( DBGINF, "do_cmdRTCGETD: %d.%d.%d\n", rtc.mday, rtc.month, rtc.year );
                    MZFrepo.buf_byte[0] = rtc.RTC_Date;
                    MZFrepo.buf_byte[1] = ((rtc.RTC_Month & 0x10)?((rtc.RTC_Month&0x0F)+10):(rtc.RTC_Month));
                    MZFrepo.buf_byte[2] = rtc.RTC_Year - 1980 + 2000;
                } else {
									  RTC_TimeTypeDef rtc;
										RTC_GetTime(RTC_Format_BIN,&rtc);
//                    DBGPRINTF ( DBGINF, "do_cmdRTCGETT: %02ld:%02ld.%02ld\n", rtc.hour, rtc.min, rtc.sec );
                    MZFrepo.buf_byte[0] = rtc.RTC_Hours;
                    MZFrepo.buf_byte[1] = rtc.RTC_Minutes;
                    MZFrepo.buf_byte[2] = rtc.RTC_Seconds;
                };
                MZFrepo.buf = MZFrepo.buf_byte;
                MZFrepo.buf_count = 3;
                MZFrepo.cmd_phase = phsDOUTRQ;
                MZFrepo.sts_err = false;
                break;

            //
            // USART:
            //
            case cmdUSHELLON:
                mzint_ResInterrupt ( mzintSIORX | mzintSIOTX );
                if ( ! is_USARTshell_active() ) {
                    USARTshell_on ();
                };
                MZFrepo.sts_err = false;
                MZFrepo.cmd_phase = phsDONE;
                break;

            case cmdUSHELLOFF:
                if ( is_USARTshell_active() ) {
                    USARTshell_off ();
                };
                MZFrepo.sts_err = false;
                MZFrepo.cmd_phase = phsDONE;
                break;

            case cmdUSARTBPS:
                do_cmdUSARTBPS ( phsSTART );
                break;

            default:
                // TODO: unicard err code UNKNOWN_COMMAND
                DBGPRINTF ( DBGERR, "UNKNOWN COMMAND: 0x%02lx\n", MZFrepo.cmd );
                break;
        };
    };

    return ( RETURN_EMU_OK );
}

/* emu_MZFREPO_read
 *
 * Connection is like following:
 * offset = 0 - command register
 * offset = 1 - data register
 */
int emu_MZFREPO_read ( int i_addroffset, unsigned int *io_data ) {


    // data port
    if ( i_addroffset & 0x01 ) {

        MZFrepo.sts_pos = 0x00;
        MZFrepo.sts_err = true;

        if ( phsDOUTRQ == MZFrepo.cmd_phase ) {

            // cteni vystupu z prikazu
            switch ( MZFrepo.cmd ) {

                case cmdREV:
                    DBGPRINTF ( DBGINF, "do_cmdREV: ( BYTE )\n" );
                    *io_data = OutputData ( true );
                    break;

                case cmdREVD:
                    DBGPRINTF ( DBGINF, "do_cmdREVD: ( BYTE )\n" );
                    *io_data = OutputData ( false );
                    break;

                case cmdINTCALLER:
                    DBGPRINTF ( DBGINF, "do_cmdINTCALLER: ( BYTE )\n" );
                    *io_data = OutputData ( false );
                    break;

                case cmdGETFREE:
                    DBGPRINTF ( DBGINF, "do_cmdGETFREE: ( BYTE )\n" );
                    *io_data = OutputData ( false );
                    break;

                case cmdGETCWD:
                    DBGPRINTF ( DBGINF, "do_cmdGETCWD: ( BYTE )\n" );
                    *io_data = OutputData ( true );
                    break;

                case cmdSTAT:
                    DBGPRINTF ( DBGINF, "do_cmdSTAT: ( BYTE )\n" );
                    *io_data = OutputData ( false );
                    break;

                case cmdTELL:
                    DBGPRINTF ( DBGINF, "do_cmdTELL: ( BYTE )\n" );
                    *io_data = OutputData ( false );
                    break;

                case cmdSIZE:
                    DBGPRINTF ( DBGINF, "do_cmdSIZE: ( BYTE )\n" );
                    *io_data = OutputData ( false );
                    break;

                case cmdRTCGETD:
                    DBGPRINTF ( DBGINF, "do_cmdRTCGETD: ( BYTE )\n" );
                    *io_data = OutputData ( false );
                    break;

                case cmdRTCGETT:
                    DBGPRINTF ( DBGINF, "do_cmdRTCGETT: ( BYTE )\n" );
                    *io_data = OutputData ( false );
                    break;
            };

        } else if ( MZFrepo.dh.fs ) {

            // cteni polozek adresare
            uint8_t ret_val = MZFrepo.filebuf [0];
            if ( MZFrepo.cnv_cmd && g_dir_filelist_mode ) {
                uint8_t cnv_val = ToSHASCII ( ret_val );
                DBGPRINTF ( DBGINF, "OutputData() - 0x%02lx, cnv: 0x%02lx\n", ret_val, cnv_val );
                ret_val = cnv_val;
            } else {
                DBGPRINTF ( DBGINF, "OutputData() - 0x%02lx\n", ret_val );
            };
            *io_data = ret_val;
            MZFrepo.filebuf++;
            MZFrepo.filebuf_count--;
            if ( MZFrepo.filebuf_count ) {
                MZFrepo.sts_err = false;
            } else {
                // nacist dalsi polozku adresare
                do_cmdREADDIR ( phsWORK );
            };

        } else if ( MZFrepo.fh.fs ) {

            unsigned int ff_readlen;
            uint8_t ret_val;
            // cmdINTGETC: cteni z otevreneho souboru (TODO: dopsat cache)
            MZFrepo.cmd = cmdINTGETC;
            //MZFrepo.ff_res = f_read ( &MZFrepo.fh, &ret_val, 1, &ff_readlen );
					
						MZFrepo.ff_res = repo_read1(&ret_val);
						ff_readlen = 1;

            if ( FR_OK != MZFrepo.ff_res ) {
                DBGPRINTF ( DBGERR, "cmdINTGETC: - f_read error! 0x%02lx\n", MZFrepo.ff_res );
                return ( RETURN_EMU_ERR );
            };
            MZFrepo.sts_err = false;
            if ( 1 != ff_readlen ) {
                // EOF - neni co cist
                *io_data = 0x00;
                DBGPRINTF ( DBGWAR, "cmdINTGETC: - read on EOF!\n", io_data );
            } else {
                if ( MZFrepo.file_mode & 0x20 ) {
                    uint8_t cnv_val = ToSHASCII ( ret_val );
#if DBG_GETCPUTC
                    DBGPRINTF ( DBGINF, "cmdINTGETC: - 0x%02lx, cnv: 0x%02lx\n", ret_val, cnv_val );
#endif
                    ret_val = cnv_val;
#if DBG_GETCPUTC
                } else {
                    DBGPRINTF ( DBGINF, "cmdINTGETC: - 0x%02lx\n", ret_val );
#endif
                };
                *io_data = ret_val;
            };

        } else {
            // nemame zadne data pro vyber
            // TODO: unicard err code NO_DATA
            DBGPRINTF ( DBGWAR, "DataP: NO DATA!\n", io_data );
            *io_data = 0x00;
        };

    // status port
    } else {

        switch ( MZFrepo.sts_pos ) {

            case 0:
                *io_data = 0x00;
                if ( phsPARAMRQ == MZFrepo.cmd_phase ) *io_data |= 0x01;	// 0. bit - BUSY(paramRQ)=1

                if ( phsDOUTRQ == MZFrepo.cmd_phase ) *io_data |= 0x02;		// 1. bit - CMD_OUTPUT=1

                if ( MZFrepo.dh.fs ) *io_data |= 0x04;				// 2. bit - READDIR_DATA=1

                if ( MZFrepo.fh.fs) {
                    if ( MZFrepo.file_mode & FA_READ ) *io_data |= 0x08;	// 3. bit - FA_READ=1

                    if ( MZFrepo.file_mode & FA_WRITE ) *io_data |= 0x10;	// 4. bit - FA_WRITE=1
                };

                if ( MZFrepo.fh.fs) {
//                    if ( MZFrepo.fh.fsize == MZFrepo.fh.fptr ) *io_data |= 0x20;// 5. bit - EOF=1
                    if ( MZFrepo.fh.fsize <= MZFrepo.offset ) *io_data |= 0x20;// 5. bit - EOF=1
                };

                // 6. bit - nepouzito

                if ( true == MZFrepo.sts_err ) *io_data |= 0x80;		// 7. bit - ERROR=1

                break;

            case 1:
                *io_data = MZFrepo.cmd;						// kod posledniho vlozeneho prikazu
                break;

            case 2:
                if ( true == MZFrepo.sts_err ) {
                    *io_data = 0x00;						// err code unikarty - dopsat! ;)
                } else if ( ( phsDOUTRQ == MZFrepo.cmd_phase ) || ( phsPARAMRQ == MZFrepo.cmd_phase ) ) {
                    *io_data = MZFrepo.buf_count;				// tady je obcas zbyvajici pocet bajtu z prikazu k vyzvednuti
                } else if ( MZFrepo.dh.fs || MZFrepo.fh.fs) {
                    *io_data = MZFrepo.filebuf_count;				// zbyvajici miste ve filebufferu
                } else {
                    *io_data = 0x00;						// zadny err code, ani data k predani ci vyzvednuti
                };
                break;

            case 3:
                *io_data = MZFrepo.ff_res;					// navratovy kod posledni FatFS operace
                break;

            default:
                *io_data = 0x00;
        };

        DBGPRINTF ( DBGINF, "STS pos=%d, sts=0x%02lx\n", MZFrepo.sts_pos, *io_data );

        if ( MZFrepo.sts_pos != 255 ) {
            MZFrepo.sts_pos++;
            if ( MZFrepo.sts_pos == 4 ) MZFrepo.sts_pos = 255;
        };
    };

    return ( RETURN_EMU_OK );
}

