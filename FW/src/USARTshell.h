
#ifndef __USART_SHELL_H__
#define __USART_SHELL_H__

extern int USHELL_RX_input;
extern int USHELL_RX_output;

extern void USARTshell_on ( void );

extern void USARTshell_off ( void );

extern int USARTshell_init ( void );

extern uint8_t is_USARTshell_active ( void );

extern void do_USARTshell ( void );

extern void USARTshell_timeout( void );

extern void USART1_SendChar(unsigned char c);

extern int USART1_getc(void);

#endif

