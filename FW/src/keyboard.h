#ifndef KEYBOARD_H_
#define KEYBOARD_H_

#include "stm32f4_discovery.h"

extern char mouse_x_count;
extern char mouse_y_count;
extern char mouse_x_final;
extern char mouse_y_final;

extern char mouse;

extern int keyb_timeout;
extern void keyb_init(void);
//extern char keyb_bit;
extern /*__INLINE*/ void put_bit(unsigned char bitin);
//extern /*__INLINE*/ void put_bit_vga(void);
extern /*__INLINE*/ void put_timeout_vga(void);

//extern unsigned char vga_key;
extern unsigned char keyboardbit;

#endif /* KEYBOARD_H_ */

