#ifndef _MZINT
#define _MZINT

#define mzintFDC	(1 << 0)
#define mzintSIORX	(1 << 1)
#define mzintSIOTX	(1 << 2)

#include "stm32f4xx.h"

extern void mzint_SetInterrupt ( uint8_t icaller );

extern void mzint_ResInterrupt ( uint8_t icaller );

extern void mzint_Init ( void );

extern uint8_t mzint_getFLAG ( void );

#endif

