/*
 *    Copyright (c) 2009 by Michal Hucik <http://www.ordoz.com>
 *    Copyright (c) 2012 by Bohumil Novacek <http://dzi.n.cz/8bit/>
 *
 *    Thies program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    MZ-800 Unicard is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MZ-800 Unicard; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "interrupt.h"
#include "mzint.h"

#define DBGLEVEL	(DBGNON /*| DBGFAT | DBGERR | DBGWAR | DBGINF*/)
//#define DBGLEVEL	(DBGNON | DBGFAT | DBGERR | DBGWAR | DBGINF)

//#include "debug.h"


static uint8_t mzint_FLAG;

void mzint_SetInterrupt ( uint8_t icaller ) {
    mzint_FLAG |= icaller;
    if ( mzint_FLAG ) {
        EXINT_ON();
    } else {
        EXINT_OFF();
    };
}

void mzint_ResInterrupt ( uint8_t icaller ) {
    mzint_FLAG &= ~icaller;
    if ( mzint_FLAG ) {
        EXINT_ON();
    } else {
        EXINT_OFF();
    };
}

void mzint_Init ( void ) {
    mzint_FLAG = 0;
        EXINT_OFF();
}

uint8_t mzint_getFLAG ( void ) { 
    return ( mzint_FLAG );
}
