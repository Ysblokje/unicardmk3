/*
 *    Copyright (c) 2012 by Bohumil Novacek <http://dzi.n.cz/8bit/>
 *
 *    Thies program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    MZ-800 Unicard is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MZ-800 Unicard; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __VGA_H__
#define __VGA_H__

#include "stm32f4xx.h"

void IntializeVGAScreenMode400x240(uint8_t *framebuffer);

#define FRAME_BUFFER_SIZE	2
extern volatile signed int Line;
#define MAX_Y_PIXEL	200

extern void InitializeVGAPort(void);
extern void InitializeALLPorts(void);
extern void InitializeVGAHorizontalSync(void);
extern void Initialize3546875Hz(void);
extern int VGA_event;

extern void Initialize429Ports(void);

#define VGAHorizontalSyncStartInterrupt 1 // Overflow interrupt
#define VGAHorizontalSyncEndInterrupt 2 // Output compare 1 interrupt
#define VGAVideoStartInterrupt 4 // Output compare 2 interrupt

#define RaiseVGAVSYNCLine() { GPIOB->BSRRL=(1<<9); }
#define LowerVGAVSYNCLine() { GPIOB->BSRRH=(1<<9); }

extern   uint32_t SystemCoreClock;
extern unsigned char borderbuffer1[300];

//#define VGA640x480x60Hz
#define VGA800x600x56Hz	//pouze po pretaktovani na 216MHz
//#define VGA800x600x60Hz	//pouze po pretaktovani na 240MHz

#ifdef VGA640x480x60Hz
#define Pixel_clock			25175
#define HSync_pulse			96
#define HBack_porch			48
#define Whole_line			800
#define Hsync_positive	0

#define Front_porch			10
#define Sync_pulse			2
#define Whole_frame			525
#define Vsync_positive	0
#endif

#ifdef VGA640x480x73Hz
#define Pixel_clock			31500
#define HSync_pulse			40
#define HBack_porch			128
#define Whole_line			832
#define Hsync_positive	0

#define Front_porch			9
#define Sync_pulse			2
#define Whole_frame			520
#define Vsync_positive	0
#endif

#ifdef VGA640x480x75Hz
#define Pixel_clock			31500
#define HSync_pulse			64
#define HBack_porch			120
#define Whole_line			840
#define Hsync_positive	0

#define Front_porch			1
#define Sync_pulse			3
#define Whole_frame			500
#define Vsync_positive	0
#endif

#ifdef VGA800x600x56Hz
#define Pixel_clock			36000
#define HSync_pulse			72
#define HBack_porch			128
#define Whole_line			1024
#define Hsync_positive	1

#define Front_porch			1
#define Sync_pulse			2
#define Whole_frame			625
#define Vsync_positive	1
#endif

#ifdef VGA800x600x60Hz
#define Pixel_clock			40000
#define HSync_pulse			128
#define HBack_porch			88
#define Whole_line			1056
#define Hsync_positive	1

#define Front_porch			1
#define Sync_pulse			4
#define Whole_frame			628
#define Vsync_positive	1
#endif

#endif
