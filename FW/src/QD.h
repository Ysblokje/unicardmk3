#ifndef _QD_H_
#define _QD_H_

#define FILEMGR_FILE2 "/unicard/mzfloader.mzq"
#define FILEMGR_FILE "RDLOADER.MZQ"

#define QD_NO_DISC 		0
#define QD_DISC_READY 	1
#define QD_HEAD_HOME 	2
#define QD_FILE2 		4
#define QD_ERR	 		8

extern int QD_init(void);
extern int QD_init_ERR(void);
extern void QD_write( register unsigned short i_addroffset, register unsigned char io_data);
extern unsigned char QD_read( register unsigned short i_addroffset);

#endif
