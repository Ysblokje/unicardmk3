#ifndef __PAL_H__
#define __PAL_H__

#include "stm32f4xx.h"
#include "integer.h"
#include "main.h"

#define MZ_700	0
#define MZ_800	1
#define ZX_48k	2	//:-)

#define MZ_80c	4
#define MZ_SUC	8

#define MZ_GRAPH_320x200_4C			0
#define MZ_GRAPH_320x200_4C_FRAMEB	1
#define MZ_GRAPH_320x200_16C		2
#define MZ_GRAPH_640x200			4
#define MZ_GRAPH_640x200_FRAMEB		5
#define MZ_GRAPH_640x200_4C			6

//#define	PAL_VIDEO_RESET_POSITION	11360
#define	PAL_VIDEO_RESET_POSITION	11177 //33

extern unsigned char KP80[32];	//registry SY6845E
extern unsigned char SYdata[0x0800];
extern const unsigned long SYfont[0x2000];

extern unsigned char BARVA16[16];
extern int newline;

extern void pal_init(void);
extern void pal_line(int line);
extern void pal_write(unsigned short addr, unsigned char data);
extern void pal_buf0_buf1(int line);
extern void pallcd(int line);

extern void tab800_update_palete(void);
extern void tab800_update_palete_partial(int palete);

extern void set_cursor(char x,char y);
extern void putb(char c);
//extern void puthex(unsigned int value, int chr);
extern void tab800_set_palete(unsigned char palete, unsigned char color);

#endif
