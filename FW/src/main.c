/*
 *    Copyright (c) 2012 by Bohumil Novacek <http://dzi.n.cz/8bit/>
 *
 *    Thies program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    MZ-800 Unicard is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MZ-800 Unicard; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "main.h"
#include "VGA.h"
#include "pal.h"
#include "QD.h"
#include "interrupt.h"
#include "emu_MZFREPO.h"
#include "emu_RAMDISC.h"
#include "emu_FDC.h"
#include "emu_SIO.h"
#include "emu_DAC.h"
#include <string.h>
#include "misc.h"
#include "stm32f4xx_gpio.h"
#include "event.h"
#include "ff.h"
#include "keyboard.h"

//#ifdef USE_UNICARD_MK3B
#include "../lcd/stm32f4xx_ltdc.h"
#include "../lcd/stm32f429i_discovery_lcd.h"
//#endif

#include "usbd_cdc_core.h"
#include "usbd_usr.h"
#include "usbd_desc.h"
#include "usbd_cdc_vcp.h"

#include "USARTshell.h"

#include "stm32f4x7_eth.h"
#include "netconf.h"
#include "main.h"
#include "httpd.h"

//#include "stm32f4xx_eth.h"

#include "monitor.h"
#define DBGLEVEL	0 //(DBGFAT | DBGERR /*| DBGWAR | DBGINF*/)
#define USE_DBG_PRN	xprintf
#include "debug.h"

GPIO_InitTypeDef GPIO_InitStructure;
//static __IO uint32_t TimingDelay;

//#define SYSTEMTICK_PERIOD_MS  10
#define SYSTEMTICK_PERIOD_MS  1
__IO uint32_t LocalTime = 0; /* this variable is used to create a time reference incremented by 10ms */
uint32_t timingdelay;

extern   uint32_t SystemCoreClock;

static FATFS fat;
static DIR Dir;

static int line;
//static int vgaline;

volatile int EMU_addr;
volatile unsigned long EMU_data;

//volatile int MZDATA_BUSY;

#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment = 4
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
__ALIGN_BEGIN USB_OTG_CORE_HANDLE  USB_OTG_dev __ALIGN_END;

 __IO uint32_t TimingDelay;

//unsigned char framebuffer0[320*200];
unsigned char framebuffer0[160*MAX_Y_PIXEL];
unsigned long framebuffer1[(404*(FRAME_BUFFER_SIZE+16))/4+2];
unsigned char borderbuffer1[300];
//unsigned char line_palete[200*16];
//unsigned char palete0[16];

unsigned char MAC_ADDR[6];
unsigned char IP_ADDR[4];
unsigned char NETMASK_ADDR[4];
unsigned char GW_ADDR[4];
char PHY_ETH_OK;
static int phy_eth_timeout;

void Delay(__IO uint32_t nTime);

extern CDC_IF_Prop_TypeDef  APP_FOPS;
void hal_uart_put( unsigned char c) { APP_FOPS.pIf_DataTx(0,c); }
unsigned char hal_uart_get( void) {	return 0; }

void rtc_init()
{		//TODO zatim bez Sharpa
		RTC_InitTypeDef rtc;
		/* Enable the PWR clock */
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
		/* Allow access to RTC */
		PWR_BackupAccessCmd(ENABLE);
		/* Enable the LSE OSC */
		RCC_LSEConfig(RCC_LSE_ON);
		/* Wait till LSE is ready */
		while(RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
		{
		}
		/* Select the RTC Clock Source */
		RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
		/* Enable the RTC Clock */
		RCC_RTCCLKCmd(ENABLE);
		RTC_WaitForSynchro();
		RTC_StructInit(&rtc);
		RTC_Init(&rtc);
}

int is_alphanum(char c) {
	if ((c>='A')&&(c<='Z')) return 1;
	if ((c>='a')&&(c<='z')) return 1;
	if ((c>='0')&&(c<='9')) return 1;
	if (c=='_') return 1;
	return 0;
}

int is_hex(char c) {
	if ((c>='A')&&(c<='F')) return 0x100+10+c-'A';
	if ((c>='a')&&(c<='f')) return 0x100+10+c-'a';
	if ((c>='0')&&(c<='9')) return 0x100+c-'0';
	return 0;
}

int is_comment(char c) {
	if (c=='#') return 1;
	if (c==';') return 1;
	return 0;
}

unsigned int getip(char *s) {
	int res;
	unsigned char *b=(unsigned char *)&res;
	int i,cifer;
	for(i=0;i<4;i++) {
		cifer=0;
		b[i]=0;
		while (*s) {
			if ((*s>='0')&&(*s<='9')) {
				int x;
				if (cifer==3) break;					//ctvrtou uz nezadavet
				x=b[i]*10+(*s)-'0';
				if (x>255) return 0xFFFFFFFF;	//moc velke cislo
				b[i]=x;
				cifer++;
			}
			if (*s=='.') {
				s++;
				break;
			}
			s++;
		}
		if (!cifer) break;
	}
	if (i==4) return res;
	return 0xFFFFFFFF;
}

void empty_tag(char *tag) {
	//TODO zpracovani tagu bez hodnoty
	return;
}

void tagval(char *tag, char *val) {
	//TODO zpracovani tagu
	if (strstr(tag,"HWADDR")) {
		int i=0;
		char *s=val;
		while (*s) {
			while ((*s)&&(is_hex(*s)==0)) s++;
			if (*s==0) return;	//narazili jsme
			tag[i++]=*s++;
			if (i==12) break;
		}
		for(i=0;i<12;i+=2) {
			MAC_ADDR[i>>1]=((is_hex(tag[i])&15)<<4)|(is_hex(tag[i+1])&15);
		}
	} else if (strstr(tag,"IPADDR")) {
		unsigned int ip=getip(val);
		if (ip!=0xFFFFFFFF) {
			memcpy(IP_ADDR,&ip,4);
		}
	} else if (strstr(tag,"NETMASK")) {
		unsigned int ip=getip(val);
		if (ip!=0xFFFFFFFF) {
			memcpy(NETMASK_ADDR,&ip,4);
		}
	} else if (strstr(tag,"GATEWAY")) {
		unsigned int ip=getip(val);
		if (ip!=0xFFFFFFFF) {
			memcpy(GW_ADDR,&ip,4);
		}
	}
	return;
}

void ETH_filecfg_radek(char *s) {
	char *tag;
	char *tagk;
	char *val;
	while ((*s)&&(*s<=' ')) s++;
	tag=s;												//zacatek tagu
	if (is_comment(*s))	return; 				//komentar
	while (is_alphanum(*s)) s++;
	tagk=s;												//konec tagu
	while ((*s)&&(*s<=' ')) s++;
	if ((*s==0)||(is_comment(*s)))	{	//konec nebo komentar ihned za tagem
		*tagk=0;
		empty_tag(tag);
		return;
	}
	if (*s!='=') return;					//neni = za tagem
	*tagk=0;
	*s++;
	while ((*s)&&(*s<=' ')) s++;					//mezery za rovnitkem
	val=s;
	while ((*s)&&(!is_comment(*s))) s++;	//hodnota az po konec nebo komentar
	*s=0;
	tagval(tag,val);
}

void ETH_filecfg() {
	FIL file_eth;
	int i=0;
	unsigned int j;
	char line[256];
	if (FR_OK == f_open( &file_eth, "/unicard/network.cfg", FA_READ)) {
		f_read(&file_eth, &line[i] , 1, &j );
		while (j==1) {
			if ((line[i]>='a')&&(line[i]<='z')) line[i]+='A'-'a';
			if ((line[i]==0x0D)||(line[i]==0x0A)) {
				line[i]=0;
				ETH_filecfg_radek(line);
				i=-1;
			}
			if (i<255) i++;
			f_read(&file_eth, &line[i] , 1, &j );
		}
	}
}

int init_periferii() {
	int res=0;
	int is429=1;
	line=0;									//radek odkud se prave vykresluje VGA
//	vgaline=0;							//radek odkud se prave vykresluje VGA
	if (SysTick_Config((SystemCoreClock / 1000)*SYSTEMTICK_PERIOD_MS))
  {
    /* Capture error */
    while (1);
  }

	/* Configure the NVIC Preemption Priority Bits */
//	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	NVIC_SetPriority (SysTick_IRQn, 255);

	InitializeALLPorts();//IO porty

	//presmerovani debug hlasek
	APP_FOPS.pIf_Init();
	xfunc_in = hal_uart_get;
	xfunc_out = hal_uart_put;

	init_event();				//event manager
	rtc_init();					//realny cas
	pal_init();					//vykreslovani obrazu do bufferu
	keyb_init();

	{ int i; for(i=0;i<sizeof(fat);i++) ((char *)((void *)&fat))[i]=0; }
	f_mount(0,&fat);
	{ int i; for(i=0;i<sizeof(Dir);i++) ((char *)((void *)&Dir))[i]=0; }
	f_opendir(&Dir, "/");
	IO_tabs_init();								//nulovat tabulky povolenych portu periferii
	if (!f_opendir(&Dir, "/")) {

		QD_event_addr=0;
		QD_event_data=0;
		if (QD_init())	{				//QD disk
			IOWRPortListAdd(4,"\xF4\xF5\xF6\xF7");
			IORDPortListAdd(4,"\xF4\xF5\xF6\xF7");
		}

		IOWRPortListAdd(4,"\xD2\xD3\xD4\xD7");		//MZ700 Audio

		FDD_event_addr=0;
		FDD_event_data=0;
		emu_FDC_Init();			//FDD
//		if ((GPIOA->IDR)&(1<<15)) {	//SW2 off
//			IOWRPortListAdd(8,"\xD8\xD9\xDA\xDB\xDC\xDD\xDE\xDF");
//			IORDPortListAdd(4,"\xD8\xD9\xDA\xDB");
//		} else {										//SW2 on
//			IOWRPortListAdd(8,"\x58\x59\x5A\x5B\x5C\x5D\x5E\x5F");
//			IORDPortListAdd(4,"\x58\x59\x5A\x5B");
//		}

		RAM_event_addr=0;
		RAM_event_data=0;
		if (emu_RAMDISC_Init())	{	//RAM disk
			IOWRPortListAdd(5,"\xE9\xEA\xEB\xF8\xFA");
			IORDPortListAdd(4,"\xEA\xEC\xF8\xF9");
		}

	} else {
		QD_event_addr=0;
		QD_event_data=0;
		QD_init_ERR();
		IOWRPortListAdd(4,"\xF4\xF5\xF6\xF7");
		IORDPortListAdd(4,"\xF4\xF5\xF6\xF7");
	}

	SIO_event_addr=0;
	SIO_event_data=0;
//	emu_SIO_Init();			//Z80SIO
	IOWRPortListAdd(4,"\xB0\xB1\xB2\xB3");
	IORDPortListAdd(4,"\xB0\xB1\xB2\xB3");

	emu_MZFREPO_init();	//repository
	IOWRPortListAdd(4,"\x50\x51\x52\x53");
	IORDPortListAdd(4,"\x50\x51\x52\x53");

#ifdef MZ700_ONLY
	//SY6845E
	IORDPortListAdd(3,"\x70\x72\x73");
	IOWRPortListAdd(3,"\x71\x72\x73");
#endif	

	memset(framebuffer1,0,404*(FRAME_BUFFER_SIZE+16)+8);	//konec cerny, vsechno cerne
	{
		int i;for(i=0;i<16;i++) memset(((char *)framebuffer1)+404*(FRAME_BUFFER_SIZE+i)+4,BARVA16[i],400);
	}
//#ifdef USE_UNICARD_MK3B
  LCD_DeInit();
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART7, ENABLE);	//jen STM32F429
#define UART7_BASE (APB1PERIPH_BASE + 0x07800)
#define UART7              ((USART_TypeDef *) UART7_BASE)
	UART7->BRR=0x55;
	if ((UART7->BRR & 0xFF) != 0x55) is429=0;
	
	UART7->BRR=0;
	if ((UART7->BRR & 0xFF) != 0) is429=0;

  /* LCD initiatization */
  if (is429) {
		//IORDPortListAdd(1,"\xF3");	//ATARI mouse
		{
			FIL ff_file;
			if ( FR_OK == f_open ( &ff_file, "/unicard/mouse.cfg", FA_READ ) ) {
					f_close ( &ff_file );
					IORDPortListAdd(1,"\x71");	//ATARI mouse
			}
		}
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART7, DISABLE);
		LCD_Init();
  
		/* LCD Layer initiatization */
		LCD_LayerInit();
			
		/* Enable the LTDC */
		LTDC_Cmd(ENABLE);
		
		/* Enable the LTDC CLUT */
		{
			LTDC_CLUT_InitTypeDef LTDC_CLUT_Struct;
			int i;
			LTDC_CLUTCmd(LTDC_Layer1,ENABLE);
			for(i=0;i<256;i++) {
				LTDC_CLUTStructInit(&LTDC_CLUT_Struct);
				if (i&1) LTDC_CLUT_Struct.LTDC_BlueValue|=0x80;
				if (i&2) LTDC_CLUT_Struct.LTDC_GreenValue=0x10;
				if (i&4) LTDC_CLUT_Struct.LTDC_GreenValue|=0x40;
				if (i&8) LTDC_CLUT_Struct.LTDC_BlueValue|=0x40;
				LTDC_CLUT_Struct.LTDC_CLUTAdress=i;
				LTDC_CLUTInit(LTDC_Layer1,&LTDC_CLUT_Struct);
			}
		}

		/* Set LCD foreground layer */
		LCD_SetLayer(LCD_BACKGROUND_LAYER);
		Initialize3546875Hz();
		emu_SIO_Init(USART2);			//Z80SIO

		InitializeALLPorts();//paranoia, radsi jeste jednou nastavit porty
		Initialize429Ports();

		IOWRPortListAdd(8,"\xD8\xD9\xDA\xDB\xDC\xDD\xDE\xDF");
		IORDPortListAdd(4,"\xD8\xD9\xDA\xDB");

		#ifdef MZ700_ONLY
			IOWRPortListAdd(10,"\xCE\xCF\xE0\xE1\xE2\xE3\xE4\xE5\xE6\xF0");
			//IORDPortListAdd(0,"\x00");
		#else
			IOWRPortListAdd(9,"\xCC\xCE\xCF\xE1\xE3\xE4\xE5\xE6\xF0");
		//	IOWRPortListAdd(11,"\xCC\xCD\xCE\xCF\xE1\xE3\xE4\xE5\xE6\xF0\x00"); //TODO smazat
			IORDPortListAdd(2,"\xE0\xE1");
		#endif	

		emu_DAC_init(1);
		res=1;	//STM32F429
	} else {
//#else
//		InitializeVGAPort();//obraz na VGA, spusteni citacu
		IntializeVGAScreenMode400x240(((uint8_t *)framebuffer1)+2);	//buffer ven
		emu_SIO_Init(USART6);			//Z80SIO

		InitializeALLPorts();//paranoia, radsi jeste jednou nastavit porty

		if ((GPIOA->IDR)&(1<<15)) {	//SW2 off
			IOWRPortListAdd(8,"\xD8\xD9\xDA\xDB\xDC\xDD\xDE\xDF");
			IORDPortListAdd(4,"\xD8\xD9\xDA\xDB");
		} else {										//SW2 on
			IOWRPortListAdd(8,"\x58\x59\x5A\x5B\x5C\x5D\x5E\x5F");
			IORDPortListAdd(4,"\x58\x59\x5A\x5B");
		}
		
		#ifdef MZ700_ONLY
			IOWRPortListAdd(10,"\xCE\xCF\xE0\xE1\xE2\xE3\xE4\xE5\xE6\xF0");
			//IORDPortListAdd(0,"\x00");
		#else
			IOWRPortListAdd(10,"\xCC\xCE\xCF\xE1\xE3\xE4\xE5\xE6\xF0\x00");
		//	IOWRPortListAdd(11,"\xCC\xCD\xCE\xCF\xE1\xE3\xE4\xE5\xE6\xF0\x00"); //TODO smazat
			IORDPortListAdd(2,"\xE0\xE1");
		#endif	

		emu_DAC_init(2);

	}
//#endif	

	//ETH_Init();
	MAC_ADDR[0]=0x00;
	MAC_ADDR[1]=0x1C;
	MAC_ADDR[2]=0xEE;
	MAC_ADDR[3]=(*((unsigned long *)(0x1FFF7A10)))^(*((unsigned long *)(0x1FFF7A10))>>24)^(*((unsigned long *)(0x1FFF7A14))>>16)^(*((unsigned long *)(0x1FFF7A18))>>8);
	MAC_ADDR[4]=(*((unsigned long *)(0x1FFF7A10))>>8)^(*((unsigned long *)(0x1FFF7A14)))^(*((unsigned long *)(0x1FFF7A14))>>24)^(*((unsigned long *)(0x1FFF7A18))>>16);
	MAC_ADDR[5]=(*((unsigned long *)(0x1FFF7A10))>>16)^(*((unsigned long *)(0x1FFF7A14))>>8)^(*((unsigned long *)(0x1FFF7A18)))^(*((unsigned long *)(0x1FFF7A18))>>24);

	IP_ADDR[0]=192;
	IP_ADDR[1]=168;
	IP_ADDR[2]=1;
	IP_ADDR[3]=10;
	NETMASK_ADDR[0]=255;
	NETMASK_ADDR[1]=255;
	NETMASK_ADDR[2]=255;
	NETMASK_ADDR[3]=0;
	GW_ADDR[0]=192;
	GW_ADDR[1]=168;
	GW_ADDR[2]=1;
	GW_ADDR[3]=20;

	ETH_filecfg();

	/* configure ethernet (GPIOs, clocks, MAC, DMA) */
  ETH_BSP_Config();

	PHY_ETH_OK = 0;
	phy_eth_timeout = 0;

	if ((PHY_ID&0xFFFFFC00)==0x00221400) {

		PHY_ETH_OK = 1;
		phy_eth_timeout = 50;

	  /* Initilaize the LwIP stack */
		LwIP_Init();
  
		/* Http webserver Init */
		httpd_init();
		
	}

	//init virtualniho seriaku na USB
  USBD_Init(&USB_OTG_dev,
            USB_OTG_FS_CORE_ID,
            &USR_desc,
            &USBD_CDC_cb,
            (USBD_Usr_cb_TypeDef *)&USR_cb);

	USARTshell_init();	//start shellu

/*	MZDATA_BUSY = 0;*/

	return res;
}

void SHARP_reset_sync() {
#ifdef MZ700_ONLY
#else
		while (GPIOD->IDR & (1<<8)) {	//konec resetu
			if (peek_event()&(1<<EVENT_USART_SHELL)) {	reset_event(EVENT_USART_SHELL); do_USARTshell(); }
		}
		Delay(25);
		EXRESET_ON();
		while (!(GPIOD->IDR & (1<<8))) {	//zacatek resetu
			if (peek_event()&(1<<EVENT_USART_SHELL)) {	reset_event(EVENT_USART_SHELL); do_USARTshell(); }
		}
#endif		
		EXRESET_OFF();
//		TIM2->CNT=4544;	//zaciname na radce 1 (poradove 20)
		TIM2->CNT=PAL_VIDEO_RESET_POSITION;	//zaciname na radce 1 (poradove 50)
}

void RUN_RAM_read() {
		unsigned int data;
		volatile unsigned char mzbus;
		emu_RAMDISC_read(RAM_event_addr,&data);
		PUT_MZDATA(data&0xFF);
		__asm volatile ("cpsid i");
		__asm volatile ("isb");
		EXWAIT_OFF();
		while ((GET_MZBUS()&((1<<(13-0))|(1<<(12-0))))!=((1<<(13-0))|(1<<(12-0))));
		MZDATA_OFF();
		__asm volatile ("cpsie i");
}

void RUN_RAM_preread() {
		emu_RAMDISC_preread();
}

void RUN_REPO_preread() {
		emu_MZFREPO_preread();
}

void RUN_QD_read() {
		volatile unsigned char mzbus;
		mzbus=QD_read(QD_event_addr);
		PUT_MZDATA(mzbus);
		__asm volatile ("cpsid i");
		__asm volatile ("isb");
		EXWAIT_OFF();
		while ((GET_MZBUS()&((1<<(13-0))|(1<<(12-0))))!=((1<<(13-0))|(1<<(12-0))));
		MZDATA_OFF();
		__asm volatile ("cpsie i");
}

void RUN_FDD_read() {
		unsigned int data;
		volatile unsigned char mzbus;
		emu_FDC_read(FDD_event_addr,&data);
		PUT_MZDATA(data&0xFF);
		__asm volatile ("cpsid i");
		__asm volatile ("isb");
		EXWAIT_OFF();
		while ((GET_MZBUS()&((1<<(13-0))|(1<<(12-0))))!=((1<<(13-0))|(1<<(12-0))));
		MZDATA_OFF();
		__asm volatile ("cpsie i");
		emu_FDC_mzint();
}

void RUN_SIO_read() {
		unsigned int data;
		volatile unsigned char mzbus;
		emu_SIO_read(SIO_event_addr,&data);
		PUT_MZDATA(data&0xFF);
		__asm volatile ("cpsid i");
		__asm volatile ("isb");
		EXWAIT_OFF();
		while ((GET_MZBUS()&((1<<(13-0))|(1<<(12-0))))!=((1<<(13-0))|(1<<(12-0))));
		MZDATA_OFF();
		__asm volatile ("cpsie i");
}

void RUN_EMU_read() {
		unsigned int data;
		volatile unsigned char mzbus;
		emu_MZFREPO_read(EMU_addr,&data);
		PUT_MZDATA(data&0xFF);
		__asm volatile ("cpsid i");
		__asm volatile ("isb");
		EXWAIT_OFF();
		while ((GET_MZBUS()&((1<<(13-0))|(1<<(12-0))))!=((1<<(13-0))|(1<<(12-0))));
		MZDATA_OFF();
		__asm volatile ("cpsie i");
}

extern void palete_line(int line);

void RUN_VGA_event() {
		if (newline!=line) {
			line++;
			if (line>=312) {
				line=0;
			}
			pal_line(line);	//TODO smazat podminku !!!
			palete_line(newline);
			emu_DAC2_sync(SystemCoreClock/(2*15611)); //SystemCoreClock / Freq HSYNC
			if (newline!=line) set_event(EVENT_VGA);	//pokud bude potreba zobrazit vice radku
			else set_event(EVENT_ETH_POOL);						//mam dost casu, muzu si hrat s ETHernetem
			if (line==280) {
				//TODO timeout zapisu RAM disku, zatim takhle, jednou za snimek zavolame
				emu_RAMDISC_timeout();
				USARTshell_timeout();
				if (phy_eth_timeout) {
					phy_eth_timeout--;
					if (phy_eth_timeout==0) {
						phy_eth_timeout = 50;
				    Eth_Link_ITHandler(KSZ8031_PHY_ADDRESS);
					}
				}

			}
		}

		//double buffer
/*		newline=Line/2;
		if (newline<300) {
			if (vgaline!=newline) {
				vgaline++;
				if (vgaline!=newline) {
					set_event(EVENT_VGA);	//dat sanci ostatnim
					reset_event(EVENT_ETH_POOL);						//nestiham, pryc s ETHernetem
				}
				if (vgaline>=300) {
					vgaline=0;
				}
				//if (vgaline<100)
				pal_buf0_buf1((vgaline+FRAME_BUFFER_SIZE-3)%300);	//TODO smazat podminku
			}
		}*/

}

/*void disk_callback() {	//pred ctenim z disku dorovnat
		int newline=Line/2;
		if (newline<300) {
			while (vgaline!=newline) {
				vgaline++;
				if (vgaline!=newline) {
//					set_event(EVENT_VGA);	//dat sanci ostatnim
//					reset_event(EVENT_ETH_POOL);						//nestiham, pryc s ETHernetem
				}
				if (vgaline>=300) {
					vgaline=0;
				}
				pal_buf0_buf1((vgaline+FRAME_BUFFER_SIZE-3)%300);	//TODO smazat podminku
			}
		}
}*/

void ETH_POOL_event()
{  
		if (PHY_ETH_OK == 1) {
			/* check if any packet received */
			if (ETH_CheckFrameReceived())
			{ 
				/* process received ethernet packet */
				LwIP_Pkt_Handle();
			}
			/* handle periodic timers for LwIP */
			LwIP_Periodic_Handle(LocalTime);
		}
} 

int MK3b;

/**
  * @brief   Main program
  * @param  None
  * @retval None
  */
int main(void)
{
	MK3b=init_periferii();				//inicializace vsech periferii
	SHARP_reset_sync();			//generovani EXRESET Sharpa pro synchronizaci obrazoveho double-bufferu
	interrupt_init(MK3b);				//inicializace preruseni emulatoru
	emu_DAC_open();					//start PSG emulace

  while (1)
  {
		//cekej na event
		volatile t_event event;
		do {event=peek_event();} while (!event);		//cekani na udalost, ktera by se dala obslouzit

		//vyhodnot eventy
		if (event&(1<<EVENT_RAM_READ)) { reset_event(EVENT_RAM_READ); RUN_RAM_read(); }
		if (event&(1<<EVENT_RAM_WRITE)) { reset_event(EVENT_RAM_WRITE); emu_RAMDISC_write(RAM_event_addr,(unsigned int *)&RAM_event_data); EXWAIT_OFF(); }
		if (event&(1<<EVENT_QD_READ)) { reset_event(EVENT_QD_READ); RUN_QD_read(); }
		if (event&(1<<EVENT_QD_WRITE)) { reset_event(EVENT_QD_WRITE); QD_write(QD_event_addr,QD_event_data); EXWAIT_OFF(); }
		if (event&(1<<EVENT_FDD_READ)) { reset_event(EVENT_FDD_READ); RUN_FDD_read(); }
		if (event&(1<<EVENT_FDD_WRITE)) { reset_event(EVENT_FDD_WRITE); emu_FDC_write(FDD_event_addr,(unsigned int *)&FDD_event_data); emu_FDC_mzint();	EXWAIT_OFF(); }
		if (event&(1<<EVENT_SIO_READ)) { reset_event(EVENT_SIO_READ); RUN_SIO_read(); }
		if (event&(1<<EVENT_SIO_WRITE)) { reset_event(EVENT_SIO_WRITE); emu_SIO_write(SIO_event_addr,(unsigned int *)&SIO_event_data); EXWAIT_OFF(); }
		if (event&(1<<EVENT_EMU_READ)) { reset_event(EVENT_EMU_READ); RUN_EMU_read(); }
		if (event&(1<<EVENT_EMU_WRITE)) { reset_event(EVENT_EMU_WRITE); emu_MZFREPO_write(EMU_addr,(unsigned int *)&EMU_data); EXWAIT_OFF(); }
		if (event&(1<<EVENT_USART_SHELL)) {	reset_event(EVENT_USART_SHELL); do_USARTshell(); }
		if (event&(1<<EVENT_DAC1_WAV)) {	reset_event(EVENT_DAC1_WAV); do_DACwav(); }
		if (event&(1<<EVENT_VGA)) {	reset_event(EVENT_VGA); RUN_VGA_event(); }
		if (event&(1<<EVENT_ETH_POOL)) {	reset_event(EVENT_ETH_POOL); ETH_POOL_event(); }
		if (event&(1<<EVENT_RAM_PREREAD)) { RUN_RAM_preread(); reset_event(EVENT_RAM_PREREAD); }
		if (event&(1<<EVENT_REPO_PREREAD)) { RUN_REPO_preread(); reset_event(EVENT_REPO_PREREAD); }

  }	//while (1)
}	//main

/**
  * @brief  Inserts a delay time.
  * @param  nCount: number of 10ms periods to wait for.
  * @retval None
  */
void Delay(uint32_t nCount)
{
  /* Capture the current local time */
  timingdelay = LocalTime + nCount;  

  /* wait until the desired delay finish */
  while(timingdelay > LocalTime)
  {     
  }
}

/**
  * @brief  Updates the system local time
  * @param  None
  * @retval None
  */
void Time_Update(void)
{
  LocalTime += SYSTEMTICK_PERIOD_MS;
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */

/**
  * @}
  */

