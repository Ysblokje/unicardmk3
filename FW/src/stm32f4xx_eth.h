#ifndef __STM32F4xx_ETH_H
#define __STM32F4xx_ETH_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"

extern void ETH_Init(void);

#ifdef __cplusplus
}
#endif

#endif /*__STM32F4xx_DAC_H */

