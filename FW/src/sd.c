#include "fs.h"
#include "ff.h"
#include "string.h"

#define DIR_HEAD_LEN	(17+27)
const char dirhead[DIR_HEAD_LEN]={

	/* HTTP header */
/* "HTTP/1.0 200 OK
" (17 bytes) */
0x48,0x54,0x54,0x50,0x2f,0x31,0x2e,0x30,0x20,0x32,0x30,0x30,0x20,0x4f,0x4b,0x0d,
0x0a,
/* "Content-type: text/html

" (27 bytes) */
0x43,0x6f,0x6e,0x74,0x65,0x6e,0x74,0x2d,0x74,0x79,0x70,0x65,0x3a,0x20,0x74,0x65,
0x78,0x74,0x2f,0x68,0x74,0x6d,0x6c,0x0d,0x0a,0x0d,0x0a,

};

const char dirbody1[]="<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\r\n<html>\r\n <head>\r\n  <title>Index of ";
const char dirbody2[]="</title>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1250\">\r\n<STYLE TYPE=\"text/css\">\r\n @import url(/css/csssd.css);\r\n</STYLE>\r\n </head>\r\n <body>\r\n<TABLE>\r\n<TR height=\"20px\"></TR>\r\n<TR><TD width=\"20px\"></TD><TD>\r\n<h1>Index of ";
//const char dirbody3[]="</h1>\r\n<pre> Name                                      Last modified      Size<hr><A HREF=\"";
//const char dirbody4[]="\">..</A>\r\n";
const char dirbody3[]="</h1>\r\n<pre> Name                               Last modified      Size<hr><A HREF=\"..\">..</A>\r\n";

#define dirbodyitemmax	32
char dirbodyitem[]="<A HREF=\"12345678901234567890123456789012x\">12345678901234567890123456789012x</A>  11-Jan-2001 09:41  1234KB\r\n";

const char dirbodylast[]="<hr></pre>\r\n<address>Unicard HTTP Server Port 80</address>\r\n</TD><TD width=\"20px\"></TD></TR>\r\n<TR height=\"20px\"></TR>\r\n</TABLE>\r\n</body></html>\r\n";

#define FILE_HEAD_LEN	(17+42)
const char filehead[FILE_HEAD_LEN]={

	/* HTTP header */
/* "HTTP/1.0 200 OK
" (17 bytes) */
0x48,0x54,0x54,0x50,0x2f,0x31,0x2e,0x30,0x20,0x32,0x30,0x30,0x20,0x4f,0x4b,0x0d,
0x0a,
/* "Content-type: application/octet-stream

" (42 bytes) */
0x43,0x6f,0x6e,0x74,0x65,0x6e,0x74,0x2d,0x74,0x79,0x70,0x65,0x3a,0x20,0x61,0x70,
0x70,0x6c,0x69,0x63,0x61,0x74,0x69,0x6f,0x6e,0x2f,0x6f,0x63,0x74,0x65,0x74,0x2d,0x73,0x74,0x72,0x65,0x61,0x6d,0x0d,0x0a,0x0d,0x0a,

};

void SD_read_dir(char *buffer, struct fs_file *file, int len) {
	int index=file->index;
	while (len>0) {
		if (index<DIR_HEAD_LEN) {
			*buffer++=dirhead[index];
		} else if (index<DIR_HEAD_LEN+sizeof(dirbody1)-1) {
			*buffer++=dirbody1[index-DIR_HEAD_LEN];
		} else if (index<DIR_HEAD_LEN+sizeof(dirbody1)+strlen(file->name)-1) {
			*buffer++=file->name[index-DIR_HEAD_LEN-sizeof(dirbody1)+1];
		} else if (index<DIR_HEAD_LEN+sizeof(dirbody1)+strlen(file->name)+sizeof(dirbody2)-2) {
			*buffer++=dirbody2[index-DIR_HEAD_LEN-sizeof(dirbody1)-strlen(file->name)+1];
		} else if (index<DIR_HEAD_LEN+sizeof(dirbody1)+strlen(file->name)*2+sizeof(dirbody2)-2) {
			*buffer++=file->name[index-DIR_HEAD_LEN-sizeof(dirbody1)-strlen(file->name)-sizeof(dirbody2)+2];
		} else if (index<DIR_HEAD_LEN+sizeof(dirbody1)+strlen(file->name)*2+sizeof(dirbody2)+sizeof(dirbody3)-3) {
			*buffer++=dirbody3[index-DIR_HEAD_LEN-sizeof(dirbody1)-strlen(file->name)*2-sizeof(dirbody2)+2];
//		} else if (index<DIR_HEAD_LEN+sizeof(dirbody1)+strlen(file->name)*3+sizeof(dirbody2)+sizeof(dirbody3)-3) {
//			*buffer++=file->name[index-DIR_HEAD_LEN-sizeof(dirbody1)-strlen(file->name)*2-sizeof(dirbody2)-sizeof(dirbody3)+3];
//		} else if (index<DIR_HEAD_LEN+sizeof(dirbody1)+strlen(file->name)*3+sizeof(dirbody2)+sizeof(dirbody3)+sizeof(dirbody4)-4) {
//			*buffer++=dirbody4[index-DIR_HEAD_LEN-sizeof(dirbody1)-strlen(file->name)*3-sizeof(dirbody2)-sizeof(dirbody3)+3];
		} else if (index<DIR_HEAD_LEN+sizeof(dirbody1)+strlen(file->name)*2+sizeof(dirbody2)+sizeof(dirbody3)/*+sizeof(dirbody4)*/+(((int)(file->funkce))-100)*(sizeof(dirbodyitem)-1)-3) {
			int offset=index-DIR_HEAD_LEN-sizeof(dirbody1)-strlen(file->name)*2-sizeof(dirbody2)-sizeof(dirbody3)/*-sizeof(dirbody4)*/+3;
//			int i=offset/(sizeof(dirbodyitem)-1);
			offset=offset%(sizeof(dirbodyitem)-1);
			if (offset==0) {
				unsigned int x;
				FRESULT res;
				FILINFO fno;
				char *fn;   /* This function assumes non-Unicode configuration */
				char *item;
#if _USE_LFN
				static char lfn[_MAX_LFN + 1];   /* Buffer to store the LFN */
				fno.lfname = lfn;
				fno.lfsize = sizeof lfn;
#endif
				res = f_readdir(&file->fod.dir, &fno); 
				while ((res == FR_OK) && (fno.fname[0] != 0)) {
					if (fno.fname[0] != '.') break;
					res = f_readdir(&file->fod.dir, &fno); 
				}
#if _USE_LFN
        fn = *fno.lfname ? fno.lfname : fno.fname;
#else
        fn = fno.fname;
#endif
				if (strlen(fn)>dirbodyitemmax) fn[dirbodyitemmax]=0;
				item=dirbodyitem+8;//strchr(dirbodyitem,'"');
				if (item) {
					item++;
					memset(item,' ',dirbodyitemmax+2);
					x=strlen(fn);
					memcpy(item,fn,x);
					{
						int i=x;
						char *s=item;
						while (i>0) {
							if (*s==' ') *s='+';
							s++;i--;
						}
					}
					if (fno.fattrib & AM_DIR) item[x++]='/';
					item[x]='"';

					item+=(dirbodyitemmax+3);
					memset(item,' ',dirbodyitemmax+1);
					x=strlen(fn);
					memcpy(item,fn,strlen(fn));
					if (fno.fattrib & AM_DIR) item[x++]='/';


					item+=(dirbodyitemmax+7);
					x=fno.fdate&0x1F;
					*item++=(x/10)+'0';
					*item++=(x%10)+'0';
					*item++='-';
					switch ((fno.fdate>>5)&0x0F) {
						case 1: memcpy(item,"Jan",3);break;
						case 2: memcpy(item,"Feb",3);break;
						case 3: memcpy(item,"Mar",3);break;
						case 4: memcpy(item,"Apr",3);break;
						case 5: memcpy(item,"May",3);break;
						case 6: memcpy(item,"Jun",3);break;
						case 7: memcpy(item,"Jul",3);break;
						case 8: memcpy(item,"Aug",3);break;
						case 9: memcpy(item,"Sep",3);break;
						case 10: memcpy(item,"Oct",3);break;
						case 11: memcpy(item,"Nov",3);break;
						case 12: memcpy(item,"Dec",3);break;
						default: memcpy(item,"???",3);break;
					}
					item+=3;
					*item++='-';
					x=((fno.fdate>>9)&0x7F)+1980;
					*item++=(x/1000)+'0';
					*item++=((x/100)%10)+'0';
					*item++=((x/10)%10)+'0';
					*item++=(x%10)+'0';
					*item++=' ';
					x=(fno.ftime>>11)&0x1F;
					*item++=(x/10)+'0';
					*item++=(x%10)+'0';
					*item++=':';
					x=(fno.ftime>>5)&0x3F;
					*item++=(x/10)+'0';
					*item++=(x%10)+'0';
					*item++=' ';
					*item++=' ';
					if (fno.fsize<100000) {
						x=fno.fsize;
						if (x>=10000) {
							*item++=(x/10000)+'0';
						} else *item++=' ';
						if (x>=1000) {
							*item++=((x/1000)%10)+'0';
						} else *item++=' ';
						if (x>=100) {
							*item++=((x/100)%10)+'0';
						} else *item++=' ';
						if (x>=10) {
							*item++=((x/10)%10)+'0';
						} else *item++=' ';
						*item++=(x%10)+'0';
						*item++='B';
					} else if (fno.fsize<10000*1024) {
						x=fno.fsize/1024;
						if (x>=1000) {
							*item++=((x/1000)%10)+'0';
						} else *item++=' ';
						if (x>=100) {
							*item++=((x/100)%10)+'0';
						} else *item++=' ';
						if (x>=10) {
							*item++=((x/10)%10)+'0';
						} else *item++=' ';
						*item++=(x%10)+'0';
						*item++='K';
						*item++='B';
					} else {
						if (x>=1000) {
							*item++=((x/1000)%10)+'0';
						} else *item++=' ';
						if (x>=100) {
							*item++=((x/100)%10)+'0';
						} else *item++=' ';
						if (x>=10) {
							*item++=((x/10)%10)+'0';
						} else *item++=' ';
						*item++=(x%10)+'0';
						*item++='M';
						*item++='B';
					}
				}
			}
			*buffer++=dirbodyitem[offset];
		} else if (index<DIR_HEAD_LEN+sizeof(dirbody1)+strlen(file->name)*2+sizeof(dirbody2)+sizeof(dirbody3)/*+sizeof(dirbody4)*/+sizeof(dirbodylast)+(((int)(file->funkce))-100)*(sizeof(dirbodyitem)-1)-4) {
			*buffer++=dirbodylast[index-DIR_HEAD_LEN-sizeof(dirbody1)-strlen(file->name)*2-sizeof(dirbody2)-sizeof(dirbody3)/*-sizeof(dirbody4)*/-(((int)(file->funkce))-100)*(sizeof(dirbodyitem)-1)+3];
		} else {
			*buffer++=0;
		}
		index++;
		len--;
	}
}

void SD_read_file(char *buffer, struct fs_file *file, int len) {
	int index=file->index;
	unsigned int rd;
//	if (index>400*600+BMP_HEAD_LEN) return;
//	if (index+len>400*600+BMP_HEAD_LEN) len=400*600+BMP_HEAD_LEN-index;
	while (len>0) {
		if (index<FILE_HEAD_LEN) {
			*buffer++=filehead[index];
		} else {
				f_read(&file->fod.fil,buffer,len,&rd);
				return;
		}
		index++;
		len--;
	}
}

void SD_close_dir(struct fs_file *file) {
//       f_closedir(&file->fod.dir);
}

void SD_close_file(struct fs_file *file) {
		f_close(&file->fod.fil);
}

int SD_left(char *name, struct fs_file *file) {
	int x;
	char *s=name;
	if (strlen(name)>1) {
		if (name[strlen(name)-1]=='/') name[strlen(name)-1]=0;
	}
	x=strlen(name);
	while (x>0) {
		if (*s=='+') *s=' ';
		s++;x--;
	}
  if ( FR_OK ==  f_opendir (&file->fod.dir, name ) ) {
		FRESULT res;
		FILINFO fno;
#if _USE_LFN
		static char lfn[_MAX_LFN + 1];   /* Buffer to store the LFN */
		fno.lfname = lfn;
		fno.lfsize = sizeof lfn;
#endif
		file->funkce=(void *)100;
		res = f_readdir(&file->fod.dir, &fno); 
		while ((res == FR_OK) && (fno.fname[0] != 0)) {
			if (fno.fname[0] != '.') file->funkce=(void*)(((int)file->funkce)+1);
			res = f_readdir(&file->fod.dir, &fno); 
		}
		f_opendir (&file->fod.dir, name );
		strcpy(&file->name[0],name);
		return DIR_HEAD_LEN+2*strlen(name)+sizeof(dirbody1)+sizeof(dirbody2)+sizeof(dirbody3)/*+sizeof(dirbody4)*/+sizeof(dirbodylast)+(((int)(file->funkce))-100)*(sizeof(dirbodyitem)-1)-5;
	}
	if (FR_OK == f_open(&file->fod.fil,name,FA_READ)) {
		file->funkce=(void *)3;
		return FILE_HEAD_LEN+file->fod.fil.fsize;
	}
	return 0;
}

