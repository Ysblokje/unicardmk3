#ifndef _EMU_SIO
#define _EMU_SIO

#include "stm32f4xx.h"

int emu_SIO_Init ( USART_TypeDef* u );

int emu_SIO_write( int i_addroffset, unsigned int *io_data);

int emu_SIO_read( int i_addroffset, unsigned int *io_data);

void emu_SIO_set_bps(int USART_BaudRate);

//void emu_SIO_doInterrupt ( void );

void emu_SIO_mzint ( void );

#endif
