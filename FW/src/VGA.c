/*
 *    Copyright (c) 2012 by Bohumil Novacek <http://dzi.n.cz/8bit/>
 *
 *    Thies program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    MZ-800 Unicard is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MZ-800 Unicard; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "VGA.h"
#include "emu_DAC.h"
#include "keyboard.h"

static uint32_t InterleaveZeros(uint32_t val)
{
	val=(val|(val<<8))&0x00ff00ff;
	val=(val|(val<<4))&0x0f0f0f0f;
	val=(val|(val<<2))&0x33333333;
	val=(val|(val<<1))&0x55555555;
	return val;
}

static uint32_t SetBits(uint32_t original,uint32_t bits,int state)
{
	if(!state) return original&~bits;
	else return original|bits;
}

static uint32_t SetDoubleBits(uint32_t original,uint16_t bits,int state)
{
	uint32_t mask=InterleaveZeros(bits);
	return (original&~(mask*3))|(mask*state);
}


static void SetGPIOInputMode(GPIO_TypeDef *gpio,uint16_t pins) { gpio->MODER=SetDoubleBits(gpio->MODER,pins,0); }
static void SetGPIOOutputMode(GPIO_TypeDef *gpio,uint16_t pins) {	gpio->MODER=SetDoubleBits(gpio->MODER,pins,1); }

static void SetGPIOPushPullOutput(GPIO_TypeDef *gpio,uint16_t pins) {	gpio->OTYPER=SetBits(gpio->OTYPER,pins,0); }
static void SetGPIOOpenDrainOutput(GPIO_TypeDef *gpio,uint16_t pins) { gpio->OTYPER=SetBits(gpio->OTYPER,pins,1); }

static void SetGPIOSpeed2MHz(GPIO_TypeDef *gpio,uint16_t pins) {	gpio->OSPEEDR=SetDoubleBits(gpio->OSPEEDR,pins,0); }
static void SetGPIOSpeed50MHz(GPIO_TypeDef *gpio,uint16_t pins) {	gpio->OSPEEDR=SetDoubleBits(gpio->OSPEEDR,pins,2); }
static void SetGPIOSpeed100MHz(GPIO_TypeDef *gpio,uint16_t pins) {	gpio->OSPEEDR=SetDoubleBits(gpio->OSPEEDR,pins,3); }

static void SetGPIONoPullResistor(GPIO_TypeDef *gpio,uint16_t pins) {	gpio->PUPDR=SetDoubleBits(gpio->PUPDR,pins,0); }
static void SetGPIOPullUpResistor(GPIO_TypeDef *gpio,uint16_t pins) { gpio->PUPDR=SetDoubleBits(gpio->PUPDR,pins,1); }
//	static void SetGPIOPullDownResistor(GPIO_TypeDef *gpio,uint16_t pins) { gpio->PUPDR=SetDoubleBits(gpio->PUPDR,pins,2); }

static void EnableAHB1PeripheralClock(uint32_t peripherals) { RCC->AHB1ENR|=peripherals; }
static void EnableAPB1PeripheralClock(uint32_t peripherals) { RCC->APB1ENR|=peripherals; }
static void EnableAPB2PeripheralClock(uint32_t peripherals) { RCC->APB2ENR|=peripherals; }

#include "event.h"
int VGA_ready=0;

static void InitializeState(uint8_t *framebuffer,int pixelsperrow);
static void InitializePixelDMA(int pixelclock);

//static void Initialize3546875Hz(void);

static void InitializeVGAScreenMode240(uint8_t *framebuffer,int pixelsperrow,int pixelclock)
{
	InitializeVGAPort();
	InitializeState(framebuffer,pixelsperrow);
	InitializePixelDMA(pixelclock);
	InitializeVGAHorizontalSync();
	Initialize3546875Hz();
}

void IntializeVGAScreenMode400x240(uint8_t *framebuffer)
{
		InitializeVGAScreenMode240(framebuffer,404,(SystemCoreClock/500L)/Pixel_clock);	//pixel clock
}

void Initialize429Ports() {
	//SW1 -> PS/2 DATA
	SetGPIOOutputMode(GPIOC,(1<<13));
	SetGPIOOpenDrainOutput(GPIOC,(1<<13));
	SetGPIOSpeed2MHz(GPIOC,(1<<13));
	SetGPIONoPullResistor(GPIOC,(1<<13));
	GPIOC->BSRRL=(1<<13);	//set bit (log.1)
	//PS/2 CLK
	SetGPIOOutputMode(GPIOD,(1<<0));
	SetGPIOOpenDrainOutput(GPIOD,(1<<0));
	SetGPIOSpeed2MHz(GPIOD,(1<<0));
	SetGPIONoPullResistor(GPIOD,(1<<0));
	GPIOD->BSRRL=(1<<0);	//set bit (log.1)
}

void InitializeALLPorts()
{
	EnableAHB1PeripheralClock(RCC_AHB1ENR_GPIOAEN|RCC_AHB1ENR_GPIOBEN|RCC_AHB1ENR_GPIOCEN|RCC_AHB1ENR_GPIODEN|RCC_AHB1ENR_GPIOEEN|RCC_AHB1ENR_DMA2EN);
	EnableAPB2PeripheralClock(RCC_APB2ENR_SYSCFGEN);

	//DATA 0-7
	SetGPIOInputMode(GPIOB,0x00FF);
	SetGPIOSpeed50MHz(GPIOB,0x00FF);
	SetGPIONoPullResistor(GPIOB,0x00FF);
//	SetGPIOPullDownResistor(GPIOB,0x00FF);

	//EXWAIT,INT,EXRESET
	SetGPIOOutputMode(GPIOC,0x000D);
	SetGPIOOpenDrainOutput(GPIOC,0x000D);
	SetGPIOSpeed2MHz(GPIOC,0x000D);
	SetGPIONoPullResistor(GPIOC,0x000D);
	GPIOC->BSRRL=0x000D;

	//Z80 CONTROL
	SetGPIOInputMode(GPIOD,0xFF00);
	SetGPIOSpeed50MHz(GPIOD,0xFF00);
	SetGPIONoPullResistor(GPIOD,0xFF00);
//	SetGPIOPullDownResistor(GPIOD,0xFF00);

	//ADDRESS 0-15
	SetGPIOInputMode(GPIOE,0xFFFF);
	SetGPIOSpeed50MHz(GPIOE,0xFFFF);
	SetGPIONoPullResistor(GPIOE,0xFFFF);
//	SetGPIOPullDownResistor(GPIOE,0xFFFF);

	//SW1
	SetGPIOInputMode(GPIOC,(1<<13));
	SetGPIOSpeed2MHz(GPIOC,(1<<13));
	SetGPIOPullUpResistor(GPIOC,(1<<13));

	//SW2
	SetGPIOInputMode(GPIOA,(1<<15));
	SetGPIOSpeed2MHz(GPIOA,(1<<15));
	SetGPIOPullUpResistor(GPIOA,(1<<15));

	//RTS
	SetGPIOOutputMode(GPIOA,(1<<8));
	SetGPIOOpenDrainOutput(GPIOA,(1<<8));
	SetGPIOSpeed2MHz(GPIOA,(1<<8));
	SetGPIONoPullResistor(GPIOA,(1<<8));
	GPIOA->BSRRH=(1<<8);

	//CTS
	SetGPIOInputMode(GPIOD,(1<<3));
	SetGPIOSpeed2MHz(GPIOD,(1<<3));
	SetGPIOPullUpResistor(GPIOD,(1<<3));
}

void InitializeVGAPort()
{
  GPIO_InitTypeDef GPIO_InitStructure;	//B.N.

	// Turn on peripherals.
	EnableAHB1PeripheralClock(RCC_AHB1ENR_GPIOBEN|RCC_AHB1ENR_GPIODEN|RCC_AHB1ENR_DMA2EN);
	EnableAPB1PeripheralClock(RCC_APB1ENR_TIM4EN);
	EnableAPB2PeripheralClock(RCC_APB2ENR_TIM8EN|/*RCC_APB2ENR_TIM9EN|*/RCC_APB2ENR_SYSCFGEN);

	// Configure DAC pins, and set to black.
	SetGPIOOutputMode(GPIOD,0x00F3);
	SetGPIOPushPullOutput(GPIOD,0x00F3);
	SetGPIOSpeed100MHz(GPIOD,0x00F3);
	SetGPIOPullUpResistor(GPIOD,0x00F3);
	GPIOD->BSRRH=0x00F3;

	// Configure sync pins and drive them high.
	SetGPIOOutputMode(GPIOB,(1<<9));
	SetGPIOPushPullOutput(GPIOB,(1<<9));
	SetGPIOSpeed50MHz(GPIOB,(1<<9));
	SetGPIOPullUpResistor(GPIOB,(1<<9));
	GPIOB->BSRRL=(1<<9);

	//B.N. test PWM pro generovani HSYNC
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_TIM4);		//TIM4_CH3
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
	VGA_ready=1;
}

void InitializeVGAHorizontalSync()
{
	NVIC_InitTypeDef NVIC_InitStructure; // this is used to configure the NVIC (nested vector interrupt controller)
	unsigned long pc=(SystemCoreClock/500L)/Pixel_clock;
	unsigned long val;

	// Configure timer 9 as the HSYNC timer.
	TIM4->CR1=TIM_CR1_ARPE;
	TIM4->CR2=0x40;		//C1REF=>TRGO
	TIM4->DIER=TIM_DIER_UIE/*|TIM_DIER_CC1IE|TIM_DIER_CC2IE*/; // Enable update, compare 1 and 2 interrupts.
	TIM4->CCMR1=TIM_OCMode_PWM2|TIM_OCFast_Enable;
	TIM4->CCMR2=TIM_OCMode_PWM1;
	if (Hsync_positive) {
		TIM4->CCER=((TIM_OCPolarity_High|TIM_OutputState_Enable)<<TIM_Channel_3)|(TIM_OCPolarity_High|TIM_OutputState_Enable);
	} else {
		TIM4->CCER=((TIM_OCPolarity_Low|TIM_OutputState_Enable)<<TIM_Channel_3)|(TIM_OCPolarity_High|TIM_OutputState_Enable);
	}
	TIM4->PSC=pc-1; // Prescaler = 1
	val=(((SystemCoreClock/2000L)*Whole_line)/Pixel_clock)+pc/2;val/=pc;	//zaokrouhlit na nasobek pixel-clock
	TIM4->ARR=val-1;
	val=(((SystemCoreClock/2000L)*HSync_pulse)/Pixel_clock)+pc/2;val/=pc;
	TIM4->CCR3=val;
	val=(((SystemCoreClock/2000L)*(HSync_pulse+HBack_porch))/Pixel_clock)+pc/2;val/=pc;
	TIM4->CCR1=val;

	// Enable HSYNC timer interrupt and set highest priority.
	NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;		 // we want to configure the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;// this sets the priority group of the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;		 // this sets the subpriority inside the group
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			 // the USART1 interrupts are globally enabled
	NVIC_Init(&NVIC_InitStructure);							 // the properties are passed to the NVIC_Init function which takes care of the low level stuff

	// Enable HSYNC timer.
	TIM4->CR1|=TIM_CR1_CEN;
	TIM4->EGR=1;
}

/*static */void Initialize3546875Hz()	//citac TV synchronizace
{
  GPIO_InitTypeDef GPIO_InitStructure;	//B.N.
	NVIC_InitTypeDef NVIC_InitStructure; // this is used to configure the NVIC (nested vector interrupt controller)
	EnableAHB1PeripheralClock(RCC_AHB1ENR_GPIOAEN);
	EnableAPB1PeripheralClock(RCC_APB1ENR_TIM2EN);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource0, GPIO_AF_TIM2);		//TIM2_ETR
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
	TIM_ETRClockMode2Config(TIM2,TIM_ExtTRGPSC_OFF,TIM_ExtTRGPolarity_NonInverted,0);
	TIM2->DIER=TIM_DIER_UIE; //Enable update
	TIM2->PSC=0; // Prescaler = 1
	TIM2->ARR=354432-1; //5 frames cca 10.00721Hz
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;		 // we want to configure the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;// this sets the priority group of the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;		 // this sets the subpriority inside the group
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			 // the USART1 interrupts are globally enabled
	NVIC_Init(&NVIC_InitStructure);							 // the properties are passed to the NVIC_Init function which takes care of the low level stuff
	TIM2->CR1|=TIM_CR1_CEN;

/*	EnableAPB1PeripheralClock(RCC_APB1ENR_TIM3EN);
	TIM3->CR1=TIM_CR1_ARPE;
	TIM3->CR2=0;
	TIM3->PSC=(SystemCoreClock*2)/62445; //15611.25Hz a pomalejsi
	TIM3->ARR=312-1; //312 radku
	TIM3->CR1|=TIM_CR1_CEN;
	TIM3->EGR=1;*/

	EnableAPB1PeripheralClock(RCC_APB1ENR_TIM5EN);
	TIM5->CR1=TIM_CR1_ARPE;
	TIM5->CR2=0;
	TIM5->DIER=TIM_DIER_UIE; //Enable update
	TIM5->PSC=0;							//1
	TIM5->ARR=(SystemCoreClock*2)/62445; //15611.25Hz a pomalejsi
	NVIC_InitStructure.NVIC_IRQChannel = TIM5_IRQn;		 // we want to configure the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;// this sets the priority group of the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;		 // this sets the subpriority inside the group
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			 // the USART1 interrupts are globally enabled
	NVIC_Init(&NVIC_InitStructure);							 // the properties are passed to the NVIC_Init function which takes care of the low level stuff
	TIM5->CR1|=TIM_CR1_CEN;
	TIM5->EGR=1;
}

volatile signed int Line;
static uint32_t FrameBufferAddress;
static uint32_t CurrentLineAddress;
static uint32_t PixelsPerRow;

static void InitializeState(uint8_t *framebuffer,int pixelsperrow)
{
	// Initialize state.
	Line=0;
	FrameBufferAddress=(uint32_t)framebuffer;
	CurrentLineAddress=FrameBufferAddress;
	PixelsPerRow=pixelsperrow;
}

__forceinline static void StartPixelDMA()
{
	if (!(DMA2_Stream1->CR & DMA_SxCR_EN)) {				/*already running*/
		/* Visible line. Configure and enable pixel DMA. */
		DMA2_Stream1->CR=(7*DMA_SxCR_CHSEL_0)|  /* Channel 7 */
		(3*DMA_SxCR_PL_0)| /* Priority 3 */
		(0*DMA_SxCR_PSIZE_0)| /* PSIZE = 8 bit */
		(1*DMA_SxCR_MSIZE_0)| /* MSIZE = 16 bit */
		DMA_SxCR_MINC| /* Increase memory address */
		(1*DMA_SxCR_DIR_0)| /* Memory to peripheral */
		DMA_SxCR_TCIE|(1<<23); /* Transfer complete interrupt */
		DMA2_Stream1->M0AR=CurrentLineAddress;
		DMA2_Stream1->CR|=DMA_SxCR_EN;
		/* Start pixel clock. */
		TIM8->CR1|=TIM_CR1_CEN;
		TIM8->EGR=1;
	}
}

#define ALL_LINES		600
#define SCAN_LINES	400
#define HALF_BORDER_LINES		((ALL_LINES-SCAN_LINES)/2)
extern void pal_buf0_buf1(int line);

void TIM4_IRQHandler()
{
		TIM4->SR=0;
		if((Line>=HALF_BORDER_LINES)&&(Line<ALL_LINES-HALF_BORDER_LINES))
		{
			StartPixelDMA();
			if (Line==(ALL_LINES-HALF_BORDER_LINES)-1) CurrentLineAddress=FrameBufferAddress+(FRAME_BUFFER_SIZE+(borderbuffer1[(Line+1)/2]&0x0F))*PixelsPerRow;
			else if((Line&1)==1) /*CurrentLineAddress+=PixelsPerRow;*/ CurrentLineAddress=FrameBufferAddress+(((Line+1-HALF_BORDER_LINES)/2)%FRAME_BUFFER_SIZE)*PixelsPerRow;
		}
		else if (Line<ALL_LINES) {
			StartPixelDMA();
			if (Line==HALF_BORDER_LINES-1) CurrentLineAddress=FrameBufferAddress;
			else if((Line&1)==1) CurrentLineAddress=FrameBufferAddress+(FRAME_BUFFER_SIZE+(borderbuffer1[(Line+1)/2]&0x0F))*PixelsPerRow;
		}
		else if(Line==ALL_LINES+Front_porch)
		{
			if (Vsync_positive) {
				RaiseVGAVSYNCLine();
			} else  {
				LowerVGAVSYNCLine();
			}
		}
		else if(Line==ALL_LINES+Front_porch+Sync_pulse)
		{
			if (Vsync_positive) {
				LowerVGAVSYNCLine();
			} else {
				RaiseVGAVSYNCLine();
			}
		}
		else if(Line==Whole_frame-1)
		{
			Line=/*(unsigned int)*/-1;
			CurrentLineAddress=FrameBufferAddress+(FRAME_BUFFER_SIZE+(borderbuffer1[0]&0x0F))*PixelsPerRow;
		}
		Line++;
		//set_event(EVENT_VGA);
		//if (keyb_timeout) { keyb_timeout-=(1000*Whole_line/Pixel_clock);if (keyb_timeout<0) { keyb_timeout=0;put_timeout_vga(); }; }
		if (Line&1) //pal_buf0_buf1((Line/2+FRAME_BUFFER_SIZE-3)%300);
					pal_buf0_buf1((Line+1)/2);
}

static void InitializePixelDMA(int pixelclock)
{
	NVIC_InitTypeDef NVIC_InitStructure; // this is used to configure the NVIC (nested vector interrupt controller)

// Configure timer 8 as the pixel clock.
	TIM8->CR1=TIM_CR1_ARPE;
	TIM8->DIER=TIM_DIER_UDE; // Enable update DMA request.
	TIM8->PSC=0; // Prescaler = 1
	TIM8->ARR=pixelclock-1; // TODO: Should this be -1?
	TIM8->SMCR=0x25; 				 //ITR3 (TIM4) Gated

	// DMA2 stream 1 channel 7 is triggered by timer 8.
	// Stop it and configure interrupts.
	DMA2_Stream1->CR&=~DMA_SxCR_EN;
	NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream1_IRQn;		 // we want to configure the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;// this sets the priority group of the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;		 // this sets the subpriority inside the group
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			 // the USART1 interrupts are globally enabled
	NVIC_Init(&NVIC_InitStructure);							 // the properties are passed to the NVIC_Init function which takes care of the low level stuff

	DMA2_Stream1->NDTR=PixelsPerRow;
	DMA2_Stream1->PAR=((uint32_t)&GPIOD->ODR);

	//pokus
	DMA2_Stream1->FCR=5;
}

void DMA2_Stream1_IRQHandler()
{
	DMA2->LIFCR|=DMA_LIFCR_CTCIF1; // Clear interrupt flag.
	TIM8->CR1&=~TIM_CR1_CEN; // Stop pixel clock.
	DMA2_Stream1->CR&=~DMA_SxCR_EN; // Disable pixel DMA.
}

