/**
  ******************************************************************************
  * @file    httpd_cg_ssi.c
  * @author  MCD Application Team
  * @version V1.1.0
  * @date    31-July-2013
  * @brief   Webserver SSI and CGI handlers
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/


#include "lwip/debug.h"
#include "httpd.h"
#include "lwip/tcp.h"
#include "fs.h"
#include "main.h"
#include "emu_ramdisc.h"
#include "QD.h"
#include "emu_FDC.h"

#include <string.h>
#include <stdlib.h>

tSSIHandler ADC_Page_SSI_Handler;
uint32_t ADC_not_configured=1;

/* we will use character "t" as tag for CGI */
typedef const char * pchar;
#define NUMTAGS	15
pchar TAGCHAR[NUMTAGS]={"mac","ipaddr","MZmode","RAMdisk","ipmsk","gwaddr","QDimage","FDDport","time","date","FD0","FD1","FD2","FD3","PHYid"};
char const** TAGS=(char const**)&TAGCHAR;

/* CGI handler for LED control */ 
const char * TIME_CGI_Handler(int iIndex, int iNumParams, char *pcParam[], char *pcValue[]);

/* Html request for "/leds.cgi" will start LEDS_CGI_Handler */
const tCGI TIME_CGI={"/time.cgi", TIME_CGI_Handler};

/* Cgi call table, only one CGI used */
tCGI CGI_TAB[1];


/**
  * @brief  Configures the ADC.
  * @param  None
  * @retval None
  */
//static void ADC_Configuration(void)
//{
//}

static char* puthex(char *s, unsigned long n, int len) {
	while(len--) {
		unsigned char ch=(n>>(len<<2))&0x0F;
		if (ch>9) *s++=ch-10+'A';
		else *s++=ch+'0';
	}
	return s;
}

static char* putint(char *s, long n) {
	char buf[16];
	int i;
	if (n<0) {
		*s++='-';
		n=-n;
	}
	buf[15]=0;
	for(i=14;(i>=0)&&((n)||(i==14));i--) {
		buf[i]=0x30+(n%10);n/=10;
	}
	memcpy(s, &buf[i+1], 14-i);
	s+=14-i;
	return s;
}

extern volatile int emuQD_status;
extern int IOGetEnabled(unsigned char n);
extern unsigned char MZ_mode;
extern unsigned char MZ_graphic_mode;
#include "pal.h"

/**
  * @brief  ADC_Handler : SSI handler for ADC page 
  */
u16_t SET_Handler(int iIndex, char *pcInsert, int iInsertLen)
{
  /* We have only one SSI handler iIndex = 0 */
	switch (iIndex) {
	case 0:	//mac
		{
			int i,j;
			puthex(pcInsert,MAC_ADDR[0],2);
			for (i=2,j=1;i<17;i+=3) {
				*(pcInsert+i)='-';
				puthex(pcInsert+(i+1),MAC_ADDR[j++],2);
			}
			return 17;
		}
	case 1:	//ip
		{
			int i;
			char *s;
			s=putint(pcInsert,IP_ADDR[0]);
			for (i=1;i<4;i++) {
				*s++='.';
				s=putint(s,IP_ADDR[i]);
			}
			return s-pcInsert;
		}
	case 2:	//MZmode
		if (MZ_mode==MZ_700) {
			strcpy(pcInsert,"MZ700");
			return strlen(pcInsert);
		} else if (MZ_mode==MZ_800) {
			strcpy(pcInsert,"MZ800 mode XX ");
			puthex(pcInsert+11,MZ_graphic_mode,2);
			if ((MZ_graphic_mode&0x0E)==0) {
				strcat(pcInsert,"(320x200x4c)");
			} else if (MZ_graphic_mode==MZ_GRAPH_320x200_16C) {
				strcat(pcInsert,"(320x200x16c)");
			} else if (MZ_graphic_mode&MZ_GRAPH_640x200) {
				strcat(pcInsert,"(640x200x");
				if (MZ_graphic_mode&MZ_GRAPH_320x200_16C) {
					strcat(pcInsert,"4c)");
				} else {
					strcat(pcInsert,"2c)");
				}
			}
			return strlen(pcInsert);
		}
		break;
	case 3:	//RAMdisk
		if (RAMdisc.ready==0) {
			memcpy(pcInsert,"NO",2);
			return 2;
		} else {
			int size=RAMdisc.bank_mask;
			char *s;
			size+=1;
			if (size<16) {
				s=putint(pcInsert,size<<6);
				*s++='K';
			} else {
				s=putint(pcInsert,size>>4);
				*s++='M';
			}
			*s++='B';
			return s-pcInsert;
		}
	case 4:	//ipmsk
		{
			int i;
			char *s;
			s=putint(pcInsert,NETMASK_ADDR[0]);
			for (i=1;i<4;i++) {
				*s++='.';
				s=putint(s,NETMASK_ADDR[i]);
			}
			return s-pcInsert;
		}
	case 5:	//gwaddr
		{
			int i;
			char *s;
			s=putint(pcInsert,GW_ADDR[0]);
			for (i=1;i<4;i++) {
				*s++='.';
				s=putint(s,GW_ADDR[i]);
			}
			return s-pcInsert;
		}
	case 6:	//QDimage
		if (emuQD_status|QD_DISC_READY) {
			if (emuQD_status|QD_FILE2) {
				strcpy(pcInsert,FILEMGR_FILE2);
				return strlen(pcInsert);
			} else {
				strcpy(pcInsert,FILEMGR_FILE);
				return strlen(pcInsert);
			}
		} else {
			memcpy(pcInsert,"NO",2);
			return 2;
		}
	case 7:	//FDDport
		if (IOGetEnabled(0x58)) {
			memcpy(pcInsert,"58h-5Fh",7);
			return 7;
		} else {
			memcpy(pcInsert,"D8h-DFh",7);
			return 7;
		}
	case 8: //time
		{
			RTC_TimeTypeDef rtc;
			char *s=pcInsert;
			RTC_GetTime(RTC_Format_BIN,&rtc);
			*s++=(rtc.RTC_Hours/10)+'0';
			*s++=(rtc.RTC_Hours%10)+'0';
			*s++=':';
			*s++=(rtc.RTC_Minutes/10)+'0';
			*s++=(rtc.RTC_Minutes%10)+'0';
			*s++=':';
			*s++=(rtc.RTC_Seconds/10)+'0';
			*s++=(rtc.RTC_Seconds%10)+'0';
			return s-pcInsert;
		}
	case 9: //date
		{
			RTC_DateTypeDef rtc;
			char *s=pcInsert;
			int m,y;
			RTC_GetDate(RTC_Format_BIN,&rtc);
			*s++=(rtc.RTC_Date/10)+'0';
			*s++=(rtc.RTC_Date%10)+'0';
			*s++='.';
			m=((rtc.RTC_Month & 0x10)?((rtc.RTC_Month&0x0F)+10):(rtc.RTC_Month));
			*s++=(m/10)+'0';
			*s++=(m%10)+'0';
			*s++='.';
			y=2000+rtc.RTC_Year;
			*s++=((y/1000)%10)+'0';
			*s++=((y/100)%10)+'0';
			*s++=((y/10)%10)+'0';
			*s++=(y%10)+'0';
			return s-pcInsert;
		}
	case 10: //FD0
		{
			if (fd0disabled==1) {
				strcpy(pcInsert,"DISABLED by /unicard/fd0disabled.cfg");
				return strlen(pcInsert);
			}
			if (FDC.drive[0].track_offset) {
				strcpy(pcInsert,FDC.drive[0].path);
				return strlen(pcInsert);
			} else {
				memcpy(pcInsert,"NO",2);
				return 2;
			}
		}
	case 11: //FD1
		{
			if (FDC.drive[1].track_offset) {
				strcpy(pcInsert,FDC.drive[1].path);
				return strlen(pcInsert);
			} else {
				memcpy(pcInsert,"NO",2);
				return 2;
			}
		}
	case 12: //FD2
		{
			if (FDC.drive[2].track_offset) {
				strcpy(pcInsert,FDC.drive[2].path);
				return strlen(pcInsert);
			} else {
				memcpy(pcInsert,"NO",2);
				return 2;
			}
		}
	case 13: //FD3
		{
			if (FDC.drive[3].track_offset) {
				strcpy(pcInsert,FDC.drive[3].path);
				return strlen(pcInsert);
			} else {
				memcpy(pcInsert,"NO",2);
				return 2;
			}
		}
	case 14:	//PHYid
		{
			puthex(pcInsert,PHY_ID,8);
			return 8;
		}

	default:
		{
			char *s=putint(pcInsert,iInsertLen);
			return s-pcInsert;
		}
	}
  return 0;
}

char *getint(char *s, int *i) {
	*i=0;
	while (*s==' ') s++;
	if ((*s<'0')||(*s>'9')) return 0;
	while ((*s>='0')&&(*s<='9')) {
		*i=*i*10+(*s)-'0';
		s++;
	}
	while (*s==' ') s++;
	return s;
}

/**
  * @brief  CGI handler for LEDs control 
  */
const char * TIME_CGI_Handler(int iIndex, int iNumParams, char *pcParam[], char *pcValue[])
{
  uint32_t i=0;
  
  /* We have only one SSI handler iIndex = 0 */
  if (iIndex==0)
  {
    /* Check cgi parameter : example GET /leds.cgi?led=2&led=4 */
    for (i=0; i<iNumParams; i++)
    {
      /* check parameter "time" */
      if (strcmp(pcParam[i] , "time")==0)   
      {
				char *s=pcValue[i];
				RTC_TimeTypeDef rtc; //RTC_t rtc;
				int n;
				char *s2;
				while ((s2=strstr(s,"%3A"))!=0) {
					*s2++=':';
					*s2++=' ';
					*s2++=' ';
				}
				while ((s2=strstr(s,"%3a"))!=0) {
					*s2++=':';
					*s2++=' ';
					*s2++=' ';
				}
				rtc.RTC_H12 = RTC_H12_AM;
				s=getint(s,&n);if ((!s)||(n<0)||(n>23)) continue;
        rtc.RTC_Hours = n;
				if (*s++!=':') continue;
				s=getint(s,&n);if ((!s)||(n<0)||(n>59)) continue;
        rtc.RTC_Minutes = n;
				if (*s++!=':') continue;
				s=getint(s,&n);if ((!s)||(n<0)||(n>59)) continue;
        rtc.RTC_Seconds = n;
        RTC_SetTime(RTC_Format_BIN, &rtc);
			}
      if (strcmp(pcParam[i] , "date")==0)   
      {
				char *s=pcValue[i];
				RTC_DateTypeDef rtc; //RTC_t rtc;
				int n;
				s=getint(s,&n);if ((!s)||(n<1)||(n>31)) continue;
        rtc.RTC_Date = n;
				if (*s++!='.') continue;
				s=getint(s,&n);if ((!s)||(n<1)||(n>12)) continue;
        rtc.RTC_Month = n;
				if (*s++!='.') continue;
				s=getint(s,&n);if ((!s)||(n<2000)||(n>2099)) continue;
        rtc.RTC_Year = n-2000;
				rtc.RTC_WeekDay = 0;
				RTC_SetDate(RTC_Format_BIN, &rtc);
			}
    }
  }
  /* uri to send after cgi call*/
  return "/time.shtml";  
}

/**
 * Initialize SSI handlers
 */
void httpd_ssi_init(void)
{  
  /* configure SSI handlers (ADC page SSI) */
  http_set_ssi_handler(SET_Handler, (char const **)TAGS, NUMTAGS);
}

/**
 * Initialize CGI handlers
 */
void httpd_cgi_init(void)
{ 
  /* configure CGI handlers (LEDs control CGI) */
  CGI_TAB[0] = TIME_CGI;
  http_set_cgi_handlers(CGI_TAB, 1);
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
