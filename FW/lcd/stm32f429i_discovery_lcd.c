/**
  ******************************************************************************
  * @file    stm32f429i_discovery_lcd.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    20-September-2013
  * @brief   This file includes the LCD driver for ILI9341 Liquid Crystal 
  *          Display Modules of STM32F429I-DISCO kit (MB1075).
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f429i_discovery_lcd.h"
#include "stm32f4xx_ltdc.h"
#include "../src/stm32f4_discovery.h"
#include "../src/stm32f4xx_gpio.h"
#include "../src/stm32f4xx_rcc.h"
//#include "stm32f4xx_spi.h"
//#include "../Common/fonts.c"

extern   uint32_t SystemCoreClock;

#define	VGA800x600x60Hz_LCD 
//#define	VGA800x600x72Hz_LCD 
//#define	VGA800x600x75Hz_LCD 
//#define	VGA800x600x85Hz_LCD 
//#define	VGA1024x768x60Hz_LCD 
//#define VGA1024x768x85Hz_LCD
//#define VGA1280x1024x60Hz_LCD
//#define VGA1400x1050x60Hz_LCD

#ifdef VGA800x600x60Hz_LCD
#define VGAOUT	VGA800x600x60Hz_LCD
#define Pixel_clock			40000
#define HSync_pulse			128
#define HBack_porch			88
#define Whole_line			1056
#define Hsync_positive	1

//#define Front_porch			1
#define Sync_pulse			4
#define Back_porch		  23
#define Whole_frame			628
#define Vsync_positive	1
#endif

#ifdef VGA800x600x72Hz_LCD
#define VGAOUT	VGA800x600x72Hz_LCD
#define Pixel_clock			50000
#define HSync_pulse			120
#define HBack_porch			64
#define Whole_line			1040
#define Hsync_positive	1

//#define Front_porch			37
#define Sync_pulse			6
#define Back_porch		  23
#define Whole_frame			666
#define Vsync_positive	1
#endif

#ifdef VGA800x600x75Hz_LCD
#define VGAOUT	VGA800x600x75Hz_LCD
#define Pixel_clock			49500
#define HSync_pulse			80
#define HBack_porch			160
#define Whole_line			1056
#define Hsync_positive	1

//#define Front_porch			1
#define Sync_pulse			3
#define Back_porch		  21
#define Whole_frame			625
#define Vsync_positive	1
#endif

#ifdef VGA800x600x85Hz_LCD
#define VGAOUT	VGA800x600x85Hz_LCD
#define Pixel_clock			56250
#define HSync_pulse			64
#define HBack_porch			152
#define Whole_line			1048
#define Hsync_positive	1

//#define Front_porch			1
#define Sync_pulse			3
#define Back_porch		  27
#define Whole_frame			631
#define Vsync_positive	1
#endif

#ifdef VGA1024x768x60Hz_LCD
#define VGAOUT	VGA1024x768x60Hz_LCD
#define Pixel_clock			65000
#define HSync_pulse			136
#define HBack_porch			160
#define Whole_line			1344
#define Hsync_positive	0

//#define Front_porch			3
#define Sync_pulse			6
#define Back_porch		  29
#define Whole_frame			806
#define Vsync_positive	0
#endif

#ifdef VGA1024x768x85Hz_LCD
#define VGAOUT	VGA1024x768x85Hz_LCD
#define Pixel_clock			94500
#define HSync_pulse			96
#define HBack_porch			208
#define Whole_line			1376
#define Hsync_positive	1

//#define Front_porch			1
#define Sync_pulse			3
#define Back_porch		  36
#define Whole_frame			808
#define Vsync_positive	1
#endif

#ifdef VGA1280x1024x60Hz_LCD
#define VGAOUT	VGA1280x1024x60Hz_LCD
#define Pixel_clock			108000
#define HSync_pulse			112
#define HBack_porch			248
#define Whole_line			1688
#define Hsync_positive	1

//#define Front_porch			1
#define Sync_pulse			3
#define Back_porch		  38
#define Whole_frame			1066
#define Vsync_positive	1
#endif

#ifdef VGA1400x1050x60Hz_LCD
#define VGAOUT	VGA1400x1050x60Hz_LCD
#define Pixel_clock			122610
#define HSync_pulse			152
#define HBack_porch			240
#define Whole_line			1880
#define Hsync_positive	0

//#define Front_porch			1
#define Sync_pulse			3
#define Back_porch		  33
#define Whole_frame			1087
#define Vsync_positive	1
#endif

//#ifdef VGA1600x1200x60Hz_LCD
//#define VGA1600x1200x60Hz_LCD
//#define Pixel_clock			162000
//#define HSync_pulse			192
//#define HBack_porch			304
//#define Whole_line			2160
//#define Hsync_positive	1
//
////#define Front_porch			1
////#define Sync_pulse			3
//#define Back_porch		  46
//#define Whole_frame			1250
//#define Vsync_positive	1
//#endif

//unsigned char LCD_FRAME_BUFFER[LCD_PIXEL_WIDTH*LCD_PIXEL_HEIGHT];
unsigned char LCD_FRAME_BUFFER[804];

/** @addtogroup Utilities
  * @{
  */ 

/** @addtogroup STM32F4_DISCOVERY
  * @{
  */ 

/** @addtogroup STM32F429I_DISCOVERY
  * @{
  */
    
/** @defgroup STM32F429I_DISCOVERY_LCD 
  * @brief This file includes the LCD driver for (ILI9341) 
  * @{
  */ 

/** @defgroup STM32F429I_DISCOVERY_LCD_Private_TypesDefinitions
  * @{
  */ 
/**
  * @}
  */ 

/** @defgroup STM32F429I_DISCOVERY_LCD_Private_Defines
  * @{
  */

#define POLY_Y(Z)          ((int32_t)((Points + Z)->X))
#define POLY_X(Z)          ((int32_t)((Points + Z)->Y))   
/**
  * @}
  */ 

/** @defgroup STM32F429I_DISCOVERY_LCD_Private_Macros
  * @{
  */
#define ABS(X)  ((X) > 0 ? (X) : -(X))    
/**
  * @}
  */ 
  
/** @defgroup STM32F429I_DISCOVERY_LCD_Private_Variables
  * @{
  */ 
//static sFONT *LCD_Currentfonts;
/* Global variables to set the written text color */
static uint16_t CurrentTextColor   = 0x0000;
static uint16_t CurrentBackColor   = 0xFFFF;
/* Default LCD configuration with LCD Layer 1 */
static uint32_t CurrentFrameBuffer = (uint32_t)LCD_FRAME_BUFFER;
static uint32_t CurrentLayer = LCD_BACKGROUND_LAYER;
/**
  * @}
  */ 

/** @defgroup STM32F429I_DISCOVERY_LCD_Private_FunctionPrototypes
  * @{
  */ 
//#ifndef USE_Delay
//static void delay(__IO uint32_t nCount);
//#endif /* USE_Delay*/

//static void PutPixel(int16_t x, int16_t y);
//static void LCD_PolyLineRelativeClosed(pPoint Points, uint16_t PointCount, uint16_t Closed);
static void LCD_AF_GPIOConfig(void);

/**
  * @}
  */ 

/** @defgroup STM32F429I_DISCOVERY_LCD_Private_Functions
  * @{
  */ 

/**
  * @brief  DeInitializes the LCD.
  * @param  None
  * @retval None
  */
void LCD_DeInit(void)
{ 
//  GPIO_InitTypeDef GPIO_InitStructure;

  /* LCD Display Off */
//  LCD_DisplayOff();

  /* LCD_SPI disable */
  //SPI_Cmd(LCD_SPI, DISABLE);
  
  /* LCD_SPI DeInit */
  //SPI_I2S_DeInit(LCD_SPI);
   
  /* Disable SPI clock  */
  //RCC_APB2PeriphClockCmd(LCD_SPI_CLK, DISABLE);
    
  /* Configure NCS in Output Push-Pull mode */
  //GPIO_InitStructure.GPIO_Pin = LCD_NCS_PIN;
  //GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  //GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  //GPIO_Init(LCD_NCS_GPIO_PORT, &GPIO_InitStructure);
   
  /* Configure SPI pins: SCK, MISO and MOSI */
  //GPIO_InitStructure.GPIO_Pin = LCD_SPI_SCK_PIN;
  //GPIO_Init(LCD_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

  //GPIO_InitStructure.GPIO_Pin = LCD_SPI_MISO_PIN;
  //GPIO_Init(LCD_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);
  
  //GPIO_InitStructure.GPIO_Pin = LCD_SPI_MOSI_PIN;
  //GPIO_Init(LCD_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

  /* GPIOA configuration */
  //GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_MCO);
  //GPIO_PinAFConfig(GPIOA, GPIO_PinSource4, GPIO_AF_MCO);
  //GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_MCO);
  //GPIO_PinAFConfig(GPIOA, GPIO_PinSource11, GPIO_AF_MCO);
  //GPIO_PinAFConfig(GPIOA, GPIO_PinSource12, GPIO_AF_MCO);
  
  //GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_6 |
  //                              GPIO_Pin_11 | GPIO_Pin_12;
  //GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  //GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  //GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  //GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  //GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* GPIOB configuration */
  //GPIO_PinAFConfig(GPIOB, GPIO_PinSource0, GPIO_AF_MCO);
  //GPIO_PinAFConfig(GPIOB, GPIO_PinSource1, GPIO_AF_MCO);
  //GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_MCO);
  //GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_MCO);
  //GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_MCO);
  //GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_MCO);
  
  //GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0  | GPIO_Pin_1  | GPIO_Pin_8    |
  //                              GPIO_Pin_9  |  GPIO_Pin_10 | GPIO_Pin_11;
  //GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  //GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  //GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  //GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  //GPIO_Init(GPIOB, &GPIO_InitStructure);
  
  /* GPIOC configuration */
  //GPIO_PinAFConfig(GPIOC, GPIO_PinSource0, GPIO_AF_MCO);
  //GPIO_PinAFConfig(GPIOC, GPIO_PinSource1, GPIO_AF_MCO);
  //GPIO_PinAFConfig(GPIOC, GPIO_PinSource8, GPIO_AF_MCO);
  
  //GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6  | GPIO_Pin_7  | GPIO_Pin_12;
  //GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  //GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  //GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  //GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  //GPIO_Init(GPIOC, &GPIO_InitStructure);
  
  /* GPIOD configuration */
/*  GPIO_PinAFConfig(GPIOD, GPIO_PinSource3, GPIO_AF_MCO);
  GPIO_PinAFConfig(GPIOD, GPIO_PinSource6, GPIO_AF_MCO);
  
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3  | GPIO_Pin_6; 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOD, &GPIO_InitStructure);  
*/
  /* GPIOF configuration */
/*  GPIO_PinAFConfig(GPIOF, GPIO_PinSource10, GPIO_AF_MCO);
  
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10; 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOF, &GPIO_InitStructure);  
*/
  /* GPIOG configuration */
/*  GPIO_PinAFConfig(GPIOG, GPIO_PinSource6, GPIO_AF_MCO);
  GPIO_PinAFConfig(GPIOG, GPIO_PinSource7, GPIO_AF_MCO);
  GPIO_PinAFConfig(GPIOG, GPIO_PinSource10, GPIO_AF_MCO);
  GPIO_PinAFConfig(GPIOG, GPIO_PinSource11, GPIO_AF_MCO);
  GPIO_PinAFConfig(GPIOG, GPIO_PinSource12, GPIO_AF_MCO);
  
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6  | GPIO_Pin_7  | GPIO_Pin_10    |
                                GPIO_Pin_11 | GPIO_Pin_12;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOG, &GPIO_InitStructure);*/
}

/**
  * @brief  Initializes the LCD.
  * @param  None
  * @retval None
  */
int  LCD_Init(void)
{ 
  LTDC_InitTypeDef       LTDC_InitStruct;
//  GPIO_InitTypeDef       GPIO_InitStructure;
  
  /* Enable clock for NCS port */
//  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
  
  /* Configure NCS in Output Push-Pull mode */
//  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
//  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
//  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
//  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
//  GPIO_Init(GPIOD, &GPIO_InitStructure);
  
  /* Configure the LCD Control pins ------------------------------------------*/
//  LCD_CtrlLinesConfig();
//  LCD_ChipSelect(DISABLE);
//  LCD_ChipSelect(ENABLE);
  
  /* Configure the LCD_SPI interface -----------------------------------------*/
//  LCD_SPIConfig(); 
  
  /* Power on the LCD --------------------------------------------------------*/
//  LCD_PowerOn();
  
  /* Enable the LTDC Clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_LTDC, ENABLE);
  
  /* Enable the DMA2D Clock */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2D, ENABLE); 
  
  /* Configure the LCD Control pins */
  LCD_AF_GPIOConfig();  
  
  /* Configure the FMC Parallel interface : SDRAM is used as Frame Buffer for LCD */
//  SDRAM_Init();

#ifdef VGAOUT
  /* LTDC Configuration *********************************************************/  
  /* Polarity configuration */
  /* Initialize the horizontal synchronization polarity as active low */
#if Hsync_positive==1
  LTDC_InitStruct.LTDC_HSPolarity = LTDC_HSPolarity_AH;     
#else	
  LTDC_InitStruct.LTDC_HSPolarity = LTDC_HSPolarity_AL;     
#endif
  /* Initialize the vertical synchronization polarity as active low */  
#if Vsync_positive==1
  LTDC_InitStruct.LTDC_VSPolarity = LTDC_VSPolarity_AH;     
#else	
  LTDC_InitStruct.LTDC_VSPolarity = LTDC_VSPolarity_AH;     
#endif
  /* Initialize the data enable polarity as active low */
  LTDC_InitStruct.LTDC_DEPolarity = LTDC_DEPolarity_AL;     
  /* Initialize the pixel clock polarity as input pixel clock */ 
  LTDC_InitStruct.LTDC_PCPolarity = LTDC_PCPolarity_IPC;
  
  /* Configure R,G,B component values for LCD background color */                   
  LTDC_InitStruct.LTDC_BackgroundRedValue = 0;            
  LTDC_InitStruct.LTDC_BackgroundGreenValue = 0;          
  LTDC_InitStruct.LTDC_BackgroundBlueValue = 0;  
  
  /* Configure PLLSAI prescalers for LCD */
  /* Enable Pixel Clock */
  /* PLLSAI_VCO Input = HSE_VALUE/PLL_M = 1 Mhz */
  /* PLLSAI_VCO Output = PLLSAI_VCO Input * PLLSAI_N = 160 Mhz */
  /* PLLLCDCLK = PLLSAI_VCO Output/PLLSAI_R = 160/2 = 80 Mhz */
  /* LTDC clock frequency = PLLLCDCLK / RCC_PLLSAIDivR = 80/2 = 40 Mhz */
  RCC_PLLSAIConfig(Pixel_clock/250, 6, 2);
  RCC_LTDCCLKDivConfig(RCC_PLLSAIDivR_Div2);
  
  /* Enable PLLSAI Clock */
  RCC_PLLSAICmd(ENABLE);
  /* Wait for PLLSAI activation */
  while(RCC_GetFlagStatus(RCC_FLAG_PLLSAIRDY) == RESET)
  {
  }
  
 /* Timing configuration */  
  /* Configure horizontal synchronization width */     
  LTDC_InitStruct.LTDC_HorizontalSync = HSync_pulse - 1;
  /* Configure vertical synchronization height */
  LTDC_InitStruct.LTDC_VerticalSync = Sync_pulse - 1;
  /* Configure accumulated horizontal back porch */
  LTDC_InitStruct.LTDC_AccumulatedHBP = HSync_pulse + HBack_porch - 1; 
  /* Configure accumulated vertical back porch */
  LTDC_InitStruct.LTDC_AccumulatedVBP = Sync_pulse + Back_porch - 1;  
  /* Configure accumulated active width */  
  LTDC_InitStruct.LTDC_AccumulatedActiveW = HSync_pulse + HBack_porch + 800 - 1;
  /* Configure accumulated active height */
  LTDC_InitStruct.LTDC_AccumulatedActiveH = Sync_pulse + Back_porch + 600 - 1;
  /* Configure total width */
  LTDC_InitStruct.LTDC_TotalWidth = Whole_line - 1; 
  /* Configure total height */
  LTDC_InitStruct.LTDC_TotalHeigh = Whole_frame - 1;
#else
  /* LTDC Configuration *********************************************************/  
  /* Polarity configuration */
  /* Initialize the horizontal synchronization polarity as active low */
  LTDC_InitStruct.LTDC_HSPolarity = LTDC_HSPolarity_AL;     
  /* Initialize the vertical synchronization polarity as active low */  
  LTDC_InitStruct.LTDC_VSPolarity = LTDC_VSPolarity_AL;     
  /* Initialize the data enable polarity as active low */
  LTDC_InitStruct.LTDC_DEPolarity = LTDC_DEPolarity_AL;     
  /* Initialize the pixel clock polarity as input pixel clock */ 
  LTDC_InitStruct.LTDC_PCPolarity = LTDC_PCPolarity_IPC;
  
  /* Configure R,G,B component values for LCD background color */                   
  LTDC_InitStruct.LTDC_BackgroundRedValue = 0;            
  LTDC_InitStruct.LTDC_BackgroundGreenValue = 0;          
  LTDC_InitStruct.LTDC_BackgroundBlueValue = 0;  
  
  /* Configure PLLSAI prescalers for LCD */
  /* Enable Pixel Clock */
  /* PLLSAI_VCO Input = HSE_VALUE/PLL_M = 1 Mhz */
  /* PLLSAI_VCO Output = PLLSAI_VCO Input * PLLSAI_N = 192 Mhz */
  /* PLLLCDCLK = PLLSAI_VCO Output/PLLSAI_R = 192/4 = 48 Mhz */
  /* LTDC clock frequency = PLLLCDCLK / RCC_PLLSAIDivR = 48/8 = 6 Mhz */
  RCC_PLLSAIConfig(192, 7, 4);
  RCC_LTDCCLKDivConfig(RCC_PLLSAIDivR_Div8);
  
  /* Enable PLLSAI Clock */
  RCC_PLLSAICmd(ENABLE);
  /* Wait for PLLSAI activation */
  while(RCC_GetFlagStatus(RCC_FLAG_PLLSAIRDY) == RESET)
  {
  }
  
 /* Timing configuration */  
  /* Configure horizontal synchronization width */     
  LTDC_InitStruct.LTDC_HorizontalSync = 9;
  /* Configure vertical synchronization height */
  LTDC_InitStruct.LTDC_VerticalSync = 1;
  /* Configure accumulated horizontal back porch */
  LTDC_InitStruct.LTDC_AccumulatedHBP = 29; 
  /* Configure accumulated vertical back porch */
  LTDC_InitStruct.LTDC_AccumulatedVBP = 3;  
  /* Configure accumulated active width */  
  LTDC_InitStruct.LTDC_AccumulatedActiveW = 269;
  /* Configure accumulated active height */
  LTDC_InitStruct.LTDC_AccumulatedActiveH = 323;
  /* Configure total width */
  LTDC_InitStruct.LTDC_TotalWidth = 279; 
  /* Configure total height */
  LTDC_InitStruct.LTDC_TotalHeigh = 327;
#endif

  LTDC_Init(&LTDC_InitStruct);
	
	return 0;	//init OK
}  

/**
  * @brief  Initializes the LCD Layers.
  * @param  None
  * @retval None
  */
void LCD_LayerInit(void)
{
  LTDC_Layer_InitTypeDef LTDC_Layer_InitStruct; 
  
  /* Windowing configuration */
  /* In this case all the active display area is used to display a picture then :
  Horizontal start = horizontal synchronization + Horizontal back porch = 30 
  Horizontal stop = Horizontal start + window width -1 = 30 + 240 -1
  Vertical start   = vertical synchronization + vertical back porch     = 4
  Vertical stop   = Vertical start + window height -1  = 4 + 320 -1      */      
#ifdef VGAOUT
  LTDC_Layer_InitStruct.LTDC_HorizontalStart = HSync_pulse + HBack_porch;
  LTDC_Layer_InitStruct.LTDC_HorizontalStop = (/*LCD_PIXEL_WIDTH*/800 + HSync_pulse + HBack_porch - 1); 
  LTDC_Layer_InitStruct.LTDC_VerticalStart = Sync_pulse + Back_porch;
  LTDC_Layer_InitStruct.LTDC_VerticalStop = (/*LCD_PIXEL_HEIGHT*/600 + Sync_pulse + Back_porch - 1);
#else
  LTDC_Layer_InitStruct.LTDC_HorizontalStart = 30;
  LTDC_Layer_InitStruct.LTDC_HorizontalStop = (LCD_PIXEL_WIDTH + 30 - 1); 
  LTDC_Layer_InitStruct.LTDC_VerticalStart = 4;
  LTDC_Layer_InitStruct.LTDC_VerticalStop = (LCD_PIXEL_HEIGHT + 4 - 1);
#endif  
	
  /* Pixel Format configuration*/
//  LTDC_Layer_InitStruct.LTDC_PixelFormat = LTDC_Pixelformat_RGB565;
  LTDC_Layer_InitStruct.LTDC_PixelFormat = LTDC_Pixelformat_L8;
  /* Alpha constant (255 totally opaque) */
  LTDC_Layer_InitStruct.LTDC_ConstantAlpha = 255; 
  /* Default Color configuration (configure A,R,G,B component values) */          
  LTDC_Layer_InitStruct.LTDC_DefaultColorBlue = 0;        
  LTDC_Layer_InitStruct.LTDC_DefaultColorGreen = 0;       
  LTDC_Layer_InitStruct.LTDC_DefaultColorRed = 0;         
  LTDC_Layer_InitStruct.LTDC_DefaultColorAlpha = 0;
  /* Configure blending factors */       
  LTDC_Layer_InitStruct.LTDC_BlendingFactor_1 = LTDC_BlendingFactor1_CA;    
  LTDC_Layer_InitStruct.LTDC_BlendingFactor_2 = LTDC_BlendingFactor2_CA;
  
  /* the length of one line of pixels in bytes + 3 then :
  Line Lenth = Active high width x number of bytes per pixel + 3 
  Active high width         = LCD_PIXEL_WIDTH 
  number of bytes per pixel = 2    (pixel_format : RGB565) 
  */
//  LTDC_Layer_InitStruct.LTDC_CFBLineLength = ((LCD_PIXEL_WIDTH * 2) + 3);
#ifdef VGAOUT
  LTDC_Layer_InitStruct.LTDC_CFBLineLength = ((800) + 3);	//delka radky
#else	
  LTDC_Layer_InitStruct.LTDC_CFBLineLength = ((LCD_PIXEL_WIDTH) + 3);
#endif	
  /* the pitch is the increment from the start of one line of pixels to the 
  start of the next line in bytes, then :
  Pitch = Active high width x number of bytes per pixel */ 
//  LTDC_Layer_InitStruct.LTDC_CFBPitch = (LCD_PIXEL_WIDTH * 2);
  LTDC_Layer_InitStruct.LTDC_CFBPitch = 0;//LCD_PIXEL_WIDTH/2;//(LCD_PIXEL_WIDTH); //TODO //TODOMEGATODO 2014 B.N. test posunu 0

	LTDC_LIPConfig(2);

  /* Configure the number of lines */  
#ifdef VGAOUT
  LTDC_Layer_InitStruct.LTDC_CFBLineNumber = 600;
#else
  LTDC_Layer_InitStruct.LTDC_CFBLineNumber = LCD_PIXEL_HEIGHT;
#endif

  /* Start Address configuration : the LCD Frame buffer is defined on SDRAM */    
  LTDC_Layer_InitStruct.LTDC_CFBStartAdress = (int)LCD_FRAME_BUFFER;
  
  /* Initialize LTDC layer 1 */
  LTDC_LayerInit(LTDC_Layer1, &LTDC_Layer_InitStruct);
  
  /* Configure Layer2 */
  /* Start Address configuration : the LCD Frame buffer is defined on SDRAM w/ Offset */     
//  LTDC_Layer_InitStruct.LTDC_CFBStartAdress = LCD_FRAME_BUFFER + BUFFER_OFFSET;
//  LTDC_Layer_InitStruct.LTDC_CFBStartAdress = (int)LCD_FRAME_BUFFER;
  
  /* Configure blending factors */       
//  LTDC_Layer_InitStruct.LTDC_BlendingFactor_1 = LTDC_BlendingFactor1_PAxCA;    
//  LTDC_Layer_InitStruct.LTDC_BlendingFactor_2 = LTDC_BlendingFactor2_PAxCA;
  
  /* Initialize LTDC layer 2 */
//  LTDC_LayerInit(LTDC_Layer2, &LTDC_Layer_InitStruct);
  
  /* LTDC configuration reload */  
  LTDC_ReloadConfig(LTDC_IMReload);
  
  /* Enable foreground & background Layers */
  LTDC_LayerCmd(LTDC_Layer1, ENABLE); 
//  LTDC_LayerCmd(LTDC_Layer2, ENABLE);
  
  /* LTDC configuration reload */  
  LTDC_ReloadConfig(LTDC_IMReload);
  
  /* Set default font */    
//  LCD_SetFont(&LCD_DEFAULT_FONT); 
  
  /* dithering activation */
//  LTDC_DitherCmd(ENABLE);

	RCC->APB1ENR|=RCC_APB1ENR_TIM3EN;
	TIM3->CR1=TIM_CR1_ARPE;
	TIM3->CR2=0;
	TIM3->DIER=TIM_DIER_UIE; //Enable update
	TIM3->PSC=0;
//	TIM3->ARR=960-1;	//TODO !!!!
	TIM3->ARR=1000-1;	//TODO !!!!
//	TIM3->CR1|=TIM_CR1_CEN;
//	TIM3->EGR=1;

	{
		NVIC_InitTypeDef NVIC_InitStructure; // this is used to configure the NVIC (nested vector interrupt controller)
		NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;		 // we want to configure the USART1 interrupts
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;// this sets the priority group of the USART1 interrupts
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;		 // this sets the subpriority inside the group
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			 // the USART1 interrupts are globally enabled
		NVIC_Init(&NVIC_InitStructure);							 // the properties are passed to the NVIC_Init function which takes care of the low level stuff
	}
	
	LTDC_ITConfig(LTDC_IT_LI,ENABLE);	//TODO
	{
		NVIC_InitTypeDef NVIC_InitStructure;
		NVIC_InitStructure.NVIC_IRQChannel = 88;//LTDC_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
	}
}

/**
  * @brief  Sets the LCD Layer.
  * @param  Layerx: specifies the Layer foreground or background.
  * @retval None
  */
void LCD_SetLayer(__IO uint32_t Layerx)
{
  if (Layerx == LCD_BACKGROUND_LAYER)
  {
    CurrentFrameBuffer = (int)LCD_FRAME_BUFFER; 
    CurrentLayer = LCD_BACKGROUND_LAYER;
  }
  else
  {
//    CurrentFrameBuffer = LCD_FRAME_BUFFER + BUFFER_OFFSET;
    CurrentFrameBuffer = (int)LCD_FRAME_BUFFER;
    CurrentLayer = LCD_FOREGROUND_LAYER;
  }
}  

/**
  * @brief  Sets the LCD Text and Background colors.
  * @param  TextColor: specifies the Text Color.
  * @param  BackColor: specifies the Background Color.
  * @retval None
  */
void LCD_SetColors(__IO uint16_t TextColor, __IO uint16_t BackColor)
{
  CurrentTextColor = TextColor; 
  CurrentBackColor = BackColor;
}

/**
  * @brief  Gets the LCD Text and Background colors.
  * @param  TextColor: pointer to the variable that will contain the Text 
            Color.
  * @param  BackColor: pointer to the variable that will contain the Background 
            Color.
  * @retval None
  */
void LCD_GetColors(__IO uint16_t *TextColor, __IO uint16_t *BackColor)
{
  *TextColor = CurrentTextColor;
  *BackColor = CurrentBackColor;
}

/**
  * @brief  Sets the Text color.
  * @param  Color: specifies the Text color code RGB(5-6-5).
  * @retval None
  */
void LCD_SetTextColor(__IO uint16_t Color)
{
  CurrentTextColor = Color;
}

/**
  * @brief  Sets the Background color.
  * @param  Color: specifies the Background color code RGB(5-6-5).
  * @retval None
  */
void LCD_SetBackColor(__IO uint16_t Color)
{
  CurrentBackColor = Color;
}

/**
  * @brief  Sets the Text Font.
  * @param  fonts: specifies the font to be used.
  * @retval None
  */
//void LCD_SetFont(sFONT *fonts)
//{
//  LCD_Currentfonts = fonts;
//}

/**
  * @brief  Configure the transparency.
  * @param  transparency: specifies the transparency, 
  *         This parameter must range from 0x00 to 0xFF.
  * @retval None
  */
void LCD_SetTransparency(uint8_t transparency)
{
  if (CurrentLayer == LCD_BACKGROUND_LAYER)
  {
    LTDC_LayerAlpha(LTDC_Layer1, transparency);
  }
  else
  {     
    LTDC_LayerAlpha(LTDC_Layer2, transparency);
  }
  LTDC_ReloadConfig(LTDC_IMReload);
}

/**
  * @brief  Gets the Text Font.
  * @param  None.
  * @retval the used font.
  */
//sFONT *LCD_GetFont(void)
//{
//  return LCD_Currentfonts;
//}

/**
  * @brief  Clears the selected line.
  * @param  Line: the Line to be cleared.
  *   This parameter can be one of the following values:
  *     @arg LCD_LINE_x: where x can be: 0..13 if LCD_Currentfonts is Font16x24
  *                                      0..26 if LCD_Currentfonts is Font12x12 or Font8x12
  *                                      0..39 if LCD_Currentfonts is Font8x8
  * @retval None
  */
//void LCD_ClearLine(uint16_t Line)
//{
//  uint16_t refcolumn = 0;
//  /* Send the string character by character on lCD */
//  while ((refcolumn < LCD_PIXEL_WIDTH) && (((refcolumn + LCD_Currentfonts->Width)& 0xFFFF) >= LCD_Currentfonts->Width))
//  {
//    /* Display one character on LCD */
//    LCD_DisplayChar(Line, refcolumn, ' ');
//    /* Decrement the column position by 16 */
//    refcolumn += LCD_Currentfonts->Width;
//  }
//}

/**
  * @brief  Clears the hole LCD.
  * @param  Color: the color of the background.
  * @retval None
  */
void LCD_Clear(uint16_t Color)
{
  uint32_t index = 0;
  
  /* erase memory */
  for (index = 0x00; index < BUFFER_OFFSET; index++)
  {
    *(__IO uint16_t*)(CurrentFrameBuffer + (2*index)) = Color;
  } 
}

/**
  * @brief  Sets the cursor position.
  * @param  Xpos: specifies the X position.
  * @param  Ypos: specifies the Y position. 
  * @retval Display Address
  */
uint32_t LCD_SetCursor(uint16_t Xpos, uint16_t Ypos)
{  
  return CurrentFrameBuffer + 2*(Xpos + (LCD_PIXEL_WIDTH*Ypos));
}

/**
  * @brief  Config and Sets the color Keying.
  * @param  RGBValue: Specifies the Color reference. 
  * @retval None
  */
void LCD_SetColorKeying(uint32_t RGBValue)
{  
  LTDC_ColorKeying_InitTypeDef   LTDC_colorkeying_InitStruct;
  
  /* configure the color Keying */
  LTDC_colorkeying_InitStruct.LTDC_ColorKeyBlue = 0x0000FF & RGBValue;
  LTDC_colorkeying_InitStruct.LTDC_ColorKeyGreen = (0x00FF00 & RGBValue) >> 8;
  LTDC_colorkeying_InitStruct.LTDC_ColorKeyRed = (0xFF0000 & RGBValue) >> 16;  

  if (CurrentLayer == LCD_BACKGROUND_LAYER)
  {   
    /* Enable the color Keying for Layer1 */
    LTDC_ColorKeyingConfig(LTDC_Layer1, &LTDC_colorkeying_InitStruct, ENABLE);
    LTDC_ReloadConfig(LTDC_IMReload);
  }
  else
  {
    /* Enable the color Keying for Layer2 */
//    LTDC_ColorKeyingConfig(LTDC_Layer2, &LTDC_colorkeying_InitStruct, ENABLE);
//    LTDC_ReloadConfig(LTDC_IMReload);
  }
}

/**
  * @brief  Disable the color Keying.
  * @param  RGBValue: Specifies the Color reference. 
  * @retval None
  */
void LCD_ReSetColorKeying(void)
{
  LTDC_ColorKeying_InitTypeDef   LTDC_colorkeying_InitStruct;
  
  if (CurrentLayer == LCD_BACKGROUND_LAYER)
  {   
    /* Disable the color Keying for Layer1 */
    LTDC_ColorKeyingConfig(LTDC_Layer1, &LTDC_colorkeying_InitStruct, DISABLE);
    LTDC_ReloadConfig(LTDC_IMReload);
  }
  else
  {
    /* Disable the color Keying for Layer2 */
//    LTDC_ColorKeyingConfig(LTDC_Layer2, &LTDC_colorkeying_InitStruct, DISABLE);
//    LTDC_ReloadConfig(LTDC_IMReload);
  }
} 

/**
  * @brief  Draws a character on LCD.
  * @param  Xpos: the Line where to display the character shape.
  * @param  Ypos: start column address.
  * @param  c: pointer to the character data.
  * @retval None
  */
//
/**
  * @brief  Displays one character (16dots width, 24dots height).
  * @param  Line: the Line where to display the character shape .
  *   This parameter can be one of the following values:
  *     @arg Linex: where x can be 0..29
  * @param  Column: start column address.
  * @param  Ascii: character ascii code, must be between 0x20 and 0x7E.
  * @retval None
  */
//
/**
  * @brief  Displays a maximum of 20 char on the LCD.
  * @param  Line: the Line where to display the character shape .
  *   This parameter can be one of the following values:
  *     @arg Linex: where x can be 0..9
  * @param  *ptr: pointer to string to display on LCD.
  * @retval None
  */
//
/**
  * @brief  Sets a display window
  * @param  Xpos: specifies the X bottom left position from 0 to 240.
  * @param  Ypos: specifies the Y bottom left position from 0 to 320.
  * @param  Height: display window height, can be a value from 0 to 320.
  * @param  Width: display window width, can be a value from 0 to 240.
  * @retval None
  */
void LCD_SetDisplayWindow(uint16_t Xpos, uint16_t Ypos, uint16_t Height, uint16_t Width)
{

  if (CurrentLayer == LCD_BACKGROUND_LAYER)
  { 
    /* reconfigure the layer1 position */
    LTDC_LayerPosition(LTDC_Layer1, Xpos, Ypos);
    LTDC_ReloadConfig(LTDC_IMReload);
    
    /* reconfigure the layer1 size */
    LTDC_LayerSize(LTDC_Layer1, Width, Height);
    LTDC_ReloadConfig(LTDC_IMReload);
 }
 else
 {   
    /* reconfigure the layer2 position */
//    LTDC_LayerPosition(LTDC_Layer2, Xpos, Ypos);
//    LTDC_ReloadConfig(LTDC_IMReload); 
   
   /* reconfigure the layer2 size */
//    LTDC_LayerSize(LTDC_Layer2, Width, Height);
//    LTDC_ReloadConfig(LTDC_IMReload);
  }
}

/**
  * @brief  Disables LCD Window mode.
  * @param  None
  * @retval None
  */
void LCD_WindowModeDisable(void)
{
  LCD_SetDisplayWindow(0, 0, LCD_PIXEL_HEIGHT, LCD_PIXEL_WIDTH); 
}

/**
  * @brief  Displays a line.
  * @param Xpos: specifies the X position, can be a value from 0 to 240.
  * @param Ypos: specifies the Y position, can be a value from 0 to 320.
  * @param Length: line length.
  * @param Direction: line direction.
  *   This parameter can be one of the following values: LCD_DIR_HORIZONTAL or LCD_DIR_VERTICAL.
  * @retval None
  */
//
/**
  * @brief  Displays a rectangle.
  * @param  Xpos: specifies the X position, can be a value from 0 to 240.
  * @param  Ypos: specifies the Y position, can be a value from 0 to 320.
  * @param  Height: display rectangle height, can be a value from 0 to 320.
  * @param  Width: display rectangle width, can be a value from 0 to 240.
  * @retval None
  */
//
/**
  * @brief  Draw a circle.
  * @param  Xpos: specifies the X position, can be a value from 0 to 240.
  * @param  Ypos: specifies the Y position, can be a value from 0 to 320.
  * @param  Radius: radius of the circle.
  * @retval None
  */
void LCD_DrawCircle(uint16_t Xpos, uint16_t Ypos, uint16_t Radius)
{
    int x = -Radius, y = 0, err = 2-2*Radius, e2;
    do {
        *(__IO uint16_t*) (CurrentFrameBuffer + (2*((Xpos-x) + LCD_PIXEL_WIDTH*(Ypos+y)))) = CurrentTextColor; 
        *(__IO uint16_t*) (CurrentFrameBuffer + (2*((Xpos+x) + LCD_PIXEL_WIDTH*(Ypos+y)))) = CurrentTextColor;
        *(__IO uint16_t*) (CurrentFrameBuffer + (2*((Xpos+x) + LCD_PIXEL_WIDTH*(Ypos-y)))) = CurrentTextColor;
        *(__IO uint16_t*) (CurrentFrameBuffer + (2*((Xpos-x) + LCD_PIXEL_WIDTH*(Ypos-y)))) = CurrentTextColor;
      
        e2 = err;
        if (e2 <= y) {
            err += ++y*2+1;
            if (-x == y && e2 <= x) e2 = 0;
        }
        if (e2 > x) err += ++x*2+1;
    }
    while (x <= 0);
}

/**
  * @brief  Draw a full ellipse.
  * @param  Xpos: specifies the X position, can be a value from 0 to 240.
  * @param  Ypos: specifies the Y position, can be a value from 0 to 320.
  * @param  Radius: minor radius of ellipse.
  * @param  Radius2: major radius of ellipse.  
  * @retval None
  */
//
/**
  * @brief  Displays an Ellipse.
  * @param  Xpos: specifies the X position, can be a value from 0 to 240.
  * @param  Ypos: specifies the Y position, can be a value from 0 to 320.
  * @param  Radius: specifies Radius.
  * @param  Radius2: specifies Radius2.
  * @retval None
  */
void LCD_DrawEllipse(int Xpos, int Ypos, int Radius, int Radius2)
{
  int x = -Radius, y = 0, err = 2-2*Radius, e2;
  float K = 0, rad1 = 0, rad2 = 0;
   
  rad1 = Radius;
  rad2 = Radius2;
  
  if (Radius > Radius2)
  { 
    do {
      K = (float)(rad1/rad2);
      *(__IO uint16_t*) (CurrentFrameBuffer + (2*((Xpos-x) + LCD_PIXEL_WIDTH*(Ypos+(uint16_t)(y/K))))) = CurrentTextColor; 
      *(__IO uint16_t*) (CurrentFrameBuffer + (2*((Xpos+x) + LCD_PIXEL_WIDTH*(Ypos+(uint16_t)(y/K))))) = CurrentTextColor;
      *(__IO uint16_t*) (CurrentFrameBuffer + (2*((Xpos+x) + LCD_PIXEL_WIDTH*(Ypos-(uint16_t)(y/K))))) = CurrentTextColor;
      *(__IO uint16_t*) (CurrentFrameBuffer + (2*((Xpos-x) + LCD_PIXEL_WIDTH*(Ypos-(uint16_t)(y/K))))) = CurrentTextColor;
            
      e2 = err;
      if (e2 <= y) {
        err += ++y*2+1;
        if (-x == y && e2 <= x) e2 = 0;
      }
      if (e2 > x) err += ++x*2+1;
    }
    while (x <= 0);
  }
  else
  {
    y = -Radius2; 
    x = 0;
    do { 
      K = (float)(rad2/rad1);
      *(__IO uint16_t*) (CurrentFrameBuffer + (2*((Xpos-(uint16_t)(x/K)) + LCD_PIXEL_WIDTH*(Ypos+y)))) = CurrentTextColor; 
      *(__IO uint16_t*) (CurrentFrameBuffer + (2*((Xpos+(uint16_t)(x/K)) + LCD_PIXEL_WIDTH*(Ypos+y)))) = CurrentTextColor;
      *(__IO uint16_t*) (CurrentFrameBuffer + (2*((Xpos+(uint16_t)(x/K)) + LCD_PIXEL_WIDTH*(Ypos-y)))) = CurrentTextColor;
      *(__IO uint16_t*) (CurrentFrameBuffer + (2*((Xpos-(uint16_t)(x/K)) + LCD_PIXEL_WIDTH*(Ypos-y)))) = CurrentTextColor;
      
      e2 = err;
      if (e2 <= x) {
        err += ++x*2+1;
        if (-y == x && e2 <= y) e2 = 0;
      }
      if (e2 > y) err += ++y*2+1;     
    }
    while (y <= 0);
  }
}

/**
  * @brief GPIO config for LTDC.
  * @retval
  *  None
  */
static void LCD_AF_GPIOConfig(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  
  /* Enable GPIOI, GPIOJ, GPIOG, GPIOF, GPIOH AHB Clocks */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB | \
                         RCC_AHB1Periph_GPIOC, ENABLE);

/* GPIOs Configuration */
/*
 +------------------------+-----------------------+----------------------------+
 +                       LCD pins assignment                                   +
 +------------------------+-----------------------+----------------------------+
 |                        |  LCD_TFT G4 <-> PB.10 |                            |
 |                        |  LCD_TFT G6 <-> PC.07 |  LCD_TFT B6 <-> PB.08      |
 |                        |                       |  LCD_TFT B7 <-> PB.09      |
 -------------------------------------------------------------------------------
          |  LCD_TFT HSYNC <-> PC.06  | LCDTFT VSYNC <->  PA.04 |
           -----------------------------------------------------

*/

 /* GPIOA configuration */
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource4, GPIO_AF_LTDC);

  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_4;
                             
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStruct);
  
 /* GPIOB configuration */  
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_LTDC);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_LTDC);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_LTDC);

  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10;
  
  GPIO_Init(GPIOB, &GPIO_InitStruct);

 /* GPIOC configuration */
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_LTDC);
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_LTDC);
  
  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
                             
  GPIO_Init(GPIOC, &GPIO_InitStruct);
}

/**
  * @brief  Displays a pixel.
  * @param  x: pixel x.
  * @param  y: pixel y.  
  * @retval None
  */
//
//#ifndef USE_Delay
/**
  * @brief  Inserts a delay time.
  * @param  nCount: specifies the delay time length.
  * @retval None
  */
//static void delay(__IO uint32_t nCount)
//{
//  __IO uint32_t index = 0; 
//  for(index = nCount; index != 0; index--)
//  {
//  }
//}
//#endif /* USE_Delay*/
/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
